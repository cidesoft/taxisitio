<?php
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>" />
	<?= Html::csrfMetaTags()?>
	  
	<?php $this->head()?>    
	<title><?php echo $this->title?></title>

<link rel="shortcut icon"
	href="<?=$this->theme->getUrl ( 'img/favicon.ico' )?>" type="image/png">

<link href="<?php echo $this->theme->getUrl('css/slidebars.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('js/switchery/switchery.min.css')?>"
	rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $this->theme->getUrl('css/style.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('css/style-responsive.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('css/layout-theme-two.css')?>"
	rel="stylesheet">
<script type="text/javascript"
	src="<?php echo $this->theme->getUrl('js/custom.js')?>"></script>
</head>
<body class="sticky-header">
	<?php $this->beginBody()?>
	<section>
		<header role="banner" id="top" class="navbar navbar-static-top bs-docs-nav bg-danger light-color ">
			<div class="container">
				<div class="navbar-header">
					<span> 
						<a class="navbar-brand" href="<?php echo Url::home();?>">
							&nbsp; <span class="brand-name"><?= Yii::$app->name?> </span>
						</a>
					</span>
				</div>

				<ul class=" nav navbar-nav navbar-right mega-menu">
					<?php if(\Yii::$app->user->isGuest){?>
						<li><a href="<?php echo Url::to(['user/login']);?>">Login</a></li>
					<?php }	else { ?>
						<li><a href="<?php echo Url::to(['user/dashboard']);?>">Dashboard</a></li>
					<?php }?>
				</ul>
			</div>
		</header>
		
		<div class="well site-index back">
			<div class="main_wrapper">
				<div class="">
                 	<?= $content?>
            	</div>
			</div>
		</div>
		
		<footer>
			<div class="text-center footer-bottom">	<?php echo ' &copy; ' . date('Y').' '. Yii::$app->name .' | All Rights Reserved | Powered by <a href="'.Yii::$app->params['companyUrl'].'">ToXSL TECHNOLOGIES Pvt. Ltd.</a>' ; ?>
				<p class="hosting m-t-10 text-center">
				Hosting Partner <a href="//jiwebhosting.com/" target="_blank">jiWebHosting</a>
				</p>
			</div>
		</footer>
	</section>
	
	<script src="<?= $this->theme->getUrl('js/bootstrap.min.js')?>"></script>
	<script src="<?= $this->theme->getUrl('js/jquery.nicescroll.js')?>"
		type="text/javascript"></script>
	<script src="<?php echo $this->theme->getUrl('js/slidebars.min.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/scripts.js')?>"></script>
	<?php $this->endBody()?>
	
	<script>
		$(document).ready(function() {
			  function setHeight() {
			    windowHeight = $(window).innerHeight()-90;
			    $('.main_wrapper').css('min-height', windowHeight);
			  };
			  setHeight();	  
			  $(window).resize(function() {
			    setHeight();
			  });
		});
	</script>
</body>
<?php $this->endPage()?>
</html>