<?php

namespace app\controllers;

use app\components\TController;
use app\models\Feedback;
use app\models\search\Feedback as FeedbackSearch;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends TController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'ruleConfig' => [ 
								'class' => AccessRule::className () 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'add',
												'view',
												'update',
												'delete',
												'ajax' 
										],
										'allow' => true,
										'matchCallback' => function () {
											return User::isAdmin ();
										} 
								]
						] 
				],
				'verbs' => [ 
						'class' => \yii\filters\VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Feedback models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new FeedbackSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Feedback model.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$model = $this->findModel ( $id );
		if (! ($model->isAllowed ()))
			throw new HttpException ( 403, Yii::t ( 'app', 'You are not allowed to access this page.' ) );
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			$this->updateMenuItems ( $model );
			return $this->render ( 'view', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Creates a new Feedback model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionAdd() {
		$model = new Feedback ();
		if (! ($model->isAllowed ()))
			throw new HttpException ( 403, Yii::t ( 'app', 'You are not allowed to access this page.' ) );
		if (\yii::$app->request->isAjax && $model->load ( \yii::$app->request->post () )) {
			\yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return \yii\widgets\ActiveForm::validate ( $model );
		}
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		}
		return $this->render ( 'add', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Updates an existing Feedback model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		if (! ($model->isAllowed ()))
			throw new HttpException ( 403, Yii::t ( 'app', 'You are not allowed to access this page.' ) );
		$this->updateMenuItems ( $model );
		if (\yii::$app->request->isAjax && $model->load ( \yii::$app->request->post () )) {
			\yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return \yii\widgets\ActiveForm::validate ( $model );
		}
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		}
		return $this->render ( 'update', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Deletes an existing Feedback model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$model = $this->findModel ( $id );
		
		if (! ($model->isAllowed ()))
			throw new HttpException ( 403, Yii::t ( 'app', 'You are not allowed to access this page.' ) );
		$model->delete ();
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Feedback model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id        	
	 * @return Feedback the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Feedback::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	protected function updateMenuItems($model = null) {
		// create static model if model is null
		if ($model == null)
			$model = new Feedback ();
		switch (\Yii::$app->controller->action->id) {
			case 'add' :
				{
					$this->menu [] = array (
							'label' => Yii::t ( 'app', 'Manage' ),
							'url' => array (
									'index' 
							),
							'visible' => User::isAdmin () 
					);
				}
				break;
			default :
			case 'view' :
				{
					$this->menu [] = array (
							'label' => '<span class="glyphicon glyphicon-list"></span>Manage',
							'title' => 'Manage',
							'url' => array (
									'index' 
							),
							'visible' => User::isAdmin () 
					);
					$this->menu [] = array (
							'label' => Yii::t ( 'app', 'Change Password' ),
							'url' => array (
									'change-password',
									'id' => $model->id 
							) 
					);
					$this->menu [] = array (
							'label' => Yii::t ( 'app', 'Update' ),
							'url' => array (
									'update',
									'id' => $model->id 
							),
							'visible' => ! User::isAdmin () 
					);
				}
				break;
		}
		$this->processSEO ( $model );
	}
}
