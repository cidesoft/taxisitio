<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\Ride;
use app\models\search\Ride as RideSearch;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * RideController implements the CRUD actions for Ride model.
 */
class RideController extends TController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'ruleConfig' => [ 
								'class' => AccessRule::className () 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'view',
												'update',
												'delete',
												'ajax',
												'get-location' 
										],
										'allow' => true,
										'matchCallback' => function () {
											return User::isAdmin ();
										} 
								]
						] 
				],
				'verbs' => [ 
						'class' => \yii\filters\VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Ride models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new RideSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		/* $this->updateMenuItems(); */
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Ride model.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$model = $this->findModel ( $id );
		$this->updateMenuItems ( $model );
		return $this->render ( 'view', [ 
				'model' => $model 
		] );
	}
	public function actionGetLocation($id) {
	
		\Yii::$app->response->format = 'json';
		$response ['status'] = 'NOK';
		$rides = Ride::find ()->where ( [ 
				'id' => $id ,
		] )->andWhere([
				'BETWEEN',
				'state_id',
				Ride::STATE_STARTED,
				Ride::STATE_COMPLETED
		])->one ();
		
		if (! empty ( $rides )) {
			$user = User::findOne ( $rides->driver_id );
			
			if (!empty($user)){
				if (! empty ( $user->lat ) && ! empty ( $user->long )) {
					$Info = '<b>Name: </b>' . $user->email . ' </br>';
					$response ['data'] = [
							"lat" => $user->lat,
							"lng" => $user->long,
							"content" => 'Driver'
					];
				}
			}
			
			$response ['status'] = 'OK';
			
		}
		
		return $response;
	}
	
	/**
	 * Creates a new Ride model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionAdd() {
		$model = new Ride ();
		$model->loadDefaultValues ();
		// $model->state_id = Ride::STATE_ACTIVE;
		$post = \yii::$app->request->post ();
		if (\yii::$app->request->isAjax && $model->load ( $post )) {
			\yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return TActiveForm::validate ( $model );
		}
		if ($model->load ( $post ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		}
		$this->updateMenuItems ();
		return $this->render ( 'add', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Updates an existing Ride model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		$post = \yii::$app->request->post ();
		if (\yii::$app->request->isAjax && $model->load ( $post )) {
			\yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return TActiveForm::validate ( $model );
		}
		if ($model->load ( $post ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		}
		$this->updateMenuItems ( $model );
		return $this->render ( 'update', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Deletes an existing Ride model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$model = $this->findModel ( $id );
		
		$model->delete ();
		\Yii::$app->session->setFlash ( 'success', \Yii::t('app', 'Ride Deleted Successfully.') );
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Ride model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id        	
	 * @return Ride the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Ride::findOne ( $id )) !== null) {
			
			if (! ($model->isAllowed ()))
				throw new HttpException ( 403, Yii::t ( 'app', 'You are not allowed to access this page.' ) );
			
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	protected function updateMenuItems($model = null) {
		switch (\Yii::$app->controller->action->id) {
			
			case 'add' :
				{
					$this->menu ['add'] = array (
							'label' => '<span class="glyphicon glyphicon-list"></span>',
							'title' => Yii::t ( 'app', 'Manage' ),
							'url' => [ 
									'index' 
							],
							'visible' => User::isAdmin () 
					);
				}
				break;
			case 'update' :
				{
					$this->menu ['add'] = array (
							'label' => '<span class="glyphicon glyphicon-plus"></span>',
							'title' => Yii::t ( 'app', 'add' ),
							'url' => [ 
									'add' 
							],
							'visible' => User::isAdmin () 
					);
					$this->menu ['manage'] = array (
							'label' => '<span class="glyphicon glyphicon-list"></span>',
							'title' => Yii::t ( 'app', 'Manage' ),
							'url' => [ 
									'index' 
							],
							'visible' => User::isAdmin () 
					);
				}
				break;
			default :
			case 'view' :
				{
					
					$this->menu ['manage'] = array (
							'label' => '<span class="glyphicon glyphicon-list"></span>',
							'title' => Yii::t ( 'app', 'Manage' ),
							'url' => [ 
									'index' 
							],
							'visible' => User::isAdmin () 
					);
					$this->menu ['delete'] = array (
							'label' => '<span class="glyphicon glyphicon-trash"></span>',
							'title' => Yii::t ( 'app', 'Delete' ),
							'url' => [ 
									'delete',
									'id' => $model->id 
							],
							'visible' => User::isAdmin () 
					);
				}
		}
	}
}
