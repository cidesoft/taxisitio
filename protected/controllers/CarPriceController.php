<?php
namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\CarPrice;
use app\models\Country;
use app\models\User;
use app\models\search\CarPrice as CarPriceSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CarPriceController implements the CRUD actions for CarPrice model.
 */
class CarPriceController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    
                    [
                        'actions' => [
                            'index',
                            'add',
                            'view',
                            'update',
                            'delete',
                            'ajax'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all CarPrice models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarPriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single CarPrice model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (! ($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
        
        $this->updateMenuItems($model);
        
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new CarPrice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new CarPrice();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            
            $carExist = CarPrice::find()->where([
                'country_id' => $model->country_id,
                'type_id' => $model->type_id
            ])->count();
            if ($carExist == 0) {
                if (! empty($model->country_id)) {
                    $serviceArea = Country::getCountryName($model->country_id);
                    $model->service_area = $serviceArea[0];
                    if ($model->save()) {
                        return $this->redirect([
                            'view',
                            'id' => $model->id
                        ]);
                    }
                }
            } else {
                if ($carExist > 0) {
                    \Yii::$app->session->setFlash('error', \Yii::t('app', 'This car type is already in this region'));
                }
            }
        }
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing CarPrice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if (! ($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', \Yii::t('app', 'You are not allowed to access this page.')));
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $carCount = CarPrice::find()->where([
                'service_area' => $model->service_area
            ])
                ->andWhere([
                '!=',
                'id',
                $id
            ])
                ->count();
            $carExist = CarPrice::find()->where([
                'service_area' => $model->service_area,
                'type_id' => $model->type_id
            ])
                ->andWhere([
                '!=',
                'id',
                $id
            ])
                ->count();
            if ($carCount < CarPrice::MAX_CAR_COUNT && $carExist == CarPrice::CAR_EXIST) {
                $currency = Country::getCurrency($model->service_area);
                $model->country_id = $model->country_id;
                $model->currency_code = $currency['currencies'][0]['code'];
                $model->currency_symbol = $currency['currencies'][0]['symbol'];
                if ($model->save()) {
                    return $this->redirect([
                        'view',
                        'id' => $model->id
                    ]);
                }
            } else {
                if ($carCount >= CarPrice::MAX_CAR_COUNT) {
                    \Yii::$app->session->setFlash('error', \Yii::t('app', 'Three car types already in this region'));
                }
                if ($carExist > CarPrice::CAR_EXIST) {
                    \Yii::$app->session->setFlash('error', \Yii::t('app', 'This car type is already in this region'));
                }
            }
        }
        $this->updateMenuItems($model);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing CarPrice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (! ($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
        $model->delete();
        \Yii::$app->session->setFlash('success', \Yii::t('app', 'Car Type Deleted Successfully.'));
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the CarPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return CarPrice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarPrice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {
            
            case 'index':
                {
                    
                    $this->menu['add'] = array(
                        'label' => 'Add',
                        'title' => Yii::t('app', 'add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
            case 'view':
                {
                    
                    if ($model != null)
                        $this->menu[] = array(
                            'label' => 'Update',
                            'title' => Yii::t('app', 'Update'),
                            'url' => [
                                'update',
                                'id' => $model->id
                            ],
                            'visible' => User::isAdmin()
                        );
                }
                
                {
                    $this->menu[] = array(
                        'label' => 'Delete',
                        'title' => Yii::t('app', 'delete'),
                        'url' => [
                            'delete',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                {
                    $this->menu[] = array(
                        'label' => 'Manage',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
            
            case 'add':
                {
                    $this->menu[] = array(
                        'label' => 'Manage',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
            case 'update':
                {
                    $this->menu[] = array(
                        'label' => 'Manage',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                {
                    $this->menu[] = array(
                        'label' => 'View',
                        'title' => Yii::t('app', 'view'),
                        'url' => [
                            'view',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                {
                    $this->menu[] = array(
                        'label' => 'Delete',
                        'title' => Yii::t('app', 'delete'),
                        'url' => [
                            'delete',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
        }
        // $this->processSEO ( $model );
    }
}
