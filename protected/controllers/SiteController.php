<?php

namespace app\controllers;

use app\components\TController;
use app\models\ContactForm;
use app\models\EmailQueue;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use app\models\Page;

class SiteController extends TController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'only' => [ 
								'logout' 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'logout',
												'test' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								],
								[ 
										'actions' => [ 
												
												'policy' 
										],
										'allow' => true,
										'roles' => [ 
												'*',
												'?',
												'@' 
										] 
								] 
						] 
				] 
		];
	}
	public function actions() {
		return [ 
				'error' => [ 
						'class' => 'yii\web\ErrorAction' 
				],
				'captcha' => [ 
						'class' => 'yii\captcha\CaptchaAction',
						'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null 
				] 
		];
	}
	public function actionError() {
		$exception = \Yii::$app->errorHandler->exception;
		if ($exception !== null && $exception->status != 404) {
			$que = new EmailQueue ();
			$sub = "There was an Error while visiting this Url: " . \yii::$app->request->url;
			$que->sendNow ( null, $exception->getTraceAsString (), null, $sub );
			\yii::warning ( $exception->getTraceAsString () );
			return $this->render ( 'error', [ 
					'message' => $exception->getTraceAsString (),
					'name' => 'Error' 
			] );
		}
	}
	public function actionIndex() {
		if (! \Yii::$app->user->isGuest) {
			$this->layout = 'main';
			return $this->render ( 'admin-panel' );
		} else {
			$this->layout = 'guest-main';
			return $this->render ( 'index' );
		}
	}
	public function actionContact() {
		$this->layout = 'guest-main';
		$model = new ContactForm ();
		if ($model->load ( Yii::$app->request->post () ) && $model->contact ( Yii::$app->params ['adminEmail'] )) {
			Yii::$app->session->setFlash ( 'contactFormSubmitted' );
			
			return $this->refresh ();
		}
		return $this->render ( 'contact', [ 
				'model' => $model 
		] );
	}
	public function actionAbout() {
		$this->layout = 'guest-main';
		return $this->render ( 'about' );
	}
	public function actionPolicy() {
		$policy = Page::find ()->where ( [ 
				'type_id' => Page::PRIVACY_POLICY 
		] )->one ();
		return $this->render ( 'policy', [ 
				'policy' => $policy 
		] );
	}
	protected function updateMenuItems($model = null) {
		switch (\Yii::$app->controller->action->id) {
			
			default :
			case 'view' :
				{
					
					if ($model != null)
						$this->menu [] = array (
								'label' => '<span class="glyphicon glyphicon-pencil"></span>',
								'title' => Yii::t ( 'app', 'Update' ),
								'url' => [ 
										'update',
										'id' => $model->id 
								],
								'visible' => User::isAdmin () 
						);
				}
			
			case 'add' :
				{
					$this->menu [] = array (
							'label' => '<span class="glyphicon glyphicon-list"></span>',
							'title' => Yii::t ( 'app', 'Manage' ),
							'url' => [ 
									'index' 
							],
							'visible' => User::isAdmin () 
					);
				}
				break;
		}
	}
}
