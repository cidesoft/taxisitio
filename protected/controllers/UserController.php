<?php

namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\Driver;
use app\models\EmailQueue;
use app\models\LoginForm;
use app\models\User;
use app\models\UserAddress;
use app\models\search\UserSearch;
use Yii;
use pheme\grid\actions\ToggleAction;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\httpclient\Exception;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use app\models\Company;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends TController
{

    public $enableCsrfValidation = FALSE;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'editabledemo',
                            'update-admin',
                            'update-customer',
                            'update-driver',
                            'delete',
                            'delete-driver',
                            'reset',
                            'login',
                            'logout',
                            'changepassword',
                            'resetpassword',
                            'profilepic',
                            'toggle',
                            'download',
                            'clear',
                            'update-admin',
                            'dashboard',
                            'recover',
                            'add-admin',
                            'image-manager',
                            'image-upload',
                            'profile',
                            'profile-update',
                            'driver',
                            'ajax'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],
                    [
                        'actions' => [
                            'login',
                            'logout',
                            'signup',
                            'reset',
                            'recover',
                            'changepassword',
                            'resetpassword',
                            'profilepic',
                            'download',
                            'clear-data',
                            'test-email',
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    public function actions()
    {
        return [
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => 'app\models\search\UserSearch',
                'setFlash' => true
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [
                    $this,
                    'onAuthSuccess'
                ]
            ]
        ];
    }

    public function actionDownload($image_file)
    {
        $file = UPLOAD_PATH . $image_file;

        if (file_exists($file)) {

            Yii::$app->response->sendFile($file);
        }
        return false;
    }

    public function actionClear()
    {
        $runtime = Yii::getAlias('@runtime');
        $this->cleanRuntimeDir($runtime);

        $this->cleanAssetsDir();
        return $this->goBack();
    }

    public function actionIndex()
    {
        $this->layout = 'main';
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionDriver()
    {
        $this->layout = 'main';
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchDriver(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;
        return $this->render('driver_grid', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionAddAdmin()
    {
        $this->layout = 'registration';
        $count = User::find()->count();
        if ($count == 0) {
            $model = new User();
            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                $model->scenario = 'signup';
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->load(Yii::$app->request->post())) {
                $model->scenario = 'signup';
                $model->role_id = User::ROLE_ADMIN;
                $model->state_id = User::STATE_ACTIVE;

                if ($model->save()) {
                    $model->setPassword($model->password);
                    return $this->redirect([
                                'login'
                    ]);
                }
            } else {
                return $this->render('signup', [
                            'model' => $model
                ]);
            }
        } else {
            return $this->redirect();
        }
    }

    /*
     * public function actionGetName(){
     *
     * }
     */

    /**
     * Displays a single User model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
        $user_address = UserAddress::find()->where([
                    'create_user_id' => $id
                ])->all();
        $address_array = [];
        if ($user_address) {
            foreach ($user_address as $address) {
                $address_array[] = [
                    'attribute' => $address->getTypeOptions($address->type_id) . ' Address',
                    'value' => $address->title
                ];
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                        'view',
                        'id' => $model->id
            ]);
        }
        $this->updateMenuItems($model);
        return $this->render('view', [
                    'model' => $model,
                    'user_address' => $address_array
        ]);
    }

    public function actionProfile($id)
    {
        $model = $this->findModel($id);

        return $this->render('profile', [
                    'model' => $model
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $this->layout = 'main';
        $model = new User();
        $model->role_id = User::ROLE_ADMIN;
        $model->state_id = User::STATE_ACTIVE;
        if ($model->load(Yii::$app->request->post())) {
            $model->saveUploadedFile($model, 'image_file');
            if ($model->save()) {
                $model->generatePasswordResetToken();
                $email = $model->email;

                $view = 'sendPassword';
                $sub = "Your Account is created  at: " . \Yii::$app->params['company'];

                $sendMessage = \Yii::$app->controller->renderPartial('/../mail/sendPassword', [
                    'user' => $email
                ]);
                $emailQueue = new EmailQueue();
                $emailQueue->sendNow($email, $sendMessage, \Yii::$app->params['adminEmail'], $sub);

                Yii::$app->getSession()->setFlash('success', ' User Added Successfully.');
                return $this->redirect([
                            'view',
                            'id' => $model->id
                ]);
            }
        }
        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionRecover()
    {
        $this->layout = 'guest-main';
        $model = new User([
            'scenario' => 'token_request'
        ]);
        $emailQueue = new EmailQueue();
        if (isset($_POST['User'])) {
            $email = trim($_POST['User']['email']);
            $user = User::findOne([
                        'email' => $email
            ]);

            if ($user) {
                $user->generatePasswordResetToken();
                if (!$user->save()) {
                    throw new Exception("Cant Generate Authentication Key");
                }

                $email = $user->email;
                $view = "passwordResetToken";
                $sub = "Recover Your Account at: " . \Yii::$app->params['company'];

                $sendMessage = \Yii::$app->controller->renderPartial('/../mail/passwordResetToken', [
                    'user' => $user
                ]);
                $send = $emailQueue->sendNow($email, $sendMessage, \Yii::$app->params['adminEmail'], $sub);

                \Yii::$app->session->setFlash('success', 'Please check your email to reset your password.');
            } else {
                \Yii::$app->session->setFlash('error', 'Email is not registered.');
            }
        }
        return $this->render('requestPasswordResetToken', [
                    'model' => $model
        ]);
    }

    public function actionResetpassword($token)
    {
        $this->layout = 'guest-main';
        $model = User::findByPasswordResetToken($token);

        if (!($model)) {
            throw new BadRequestHttpException('Wrong password reset token.');
        }
        $newModel = new User([
            'scenario' => 'resetpassword'
        ]);
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate()) {
            $model->setPassword($newModel->password);
            $model->removePasswordResetToken();
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'New password is saved successfully.');
                return $this->redirect([
                            'login'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', 'Error while saving new password.');
            }
        }
        return $this->render('resetpassword', [
                    'model' => $newModel
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateAdmin($id)
    {
        $this->layout = 'main';

        $model = $this->findModel($id);

        $model->scenario = 'update-admin';
        $old_image = $model->image_file;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->image_file = $old_image;
            $model->saveUploadedFile($model, 'image_file');
            if ($model->save()) {
                return $this->redirect([
                            'profile',
                            'id' => $model->id
                ]);
            }
        }
        $this->updateMenuItems($model);
        return $this->render('update', [
                    'model' => $model
        ]);
    }

    public function actionUpdateCustomer($id)
    {
        $this->layout = 'main';
        $model = $this->findModel($id);
        $model->scenario = "customer-update-admin";
        $old_image = $model->image_file;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->scenario = 'signup';
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->saveUploadedFile($model, 'image_file');
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Customer Updated Successfully.');
                return $this->redirect([
                            'view',
                            'id' => $model->id
                ]);
            }
        }
        $this->updateMenuItems($model);
        return $this->render('update-customer', [
                    'model' => $model
        ]);
    }

    public function actionUpdateDriver($id)
    {
        $this->layout = 'main';

        $model = $this->findModel($id);
        $model->scenario = "customer-update-admin";

        $driver = Driver::find()->where([
                    'create_user_id' => $id
                ])->one();

        if (!empty($driver)) {
            $driver->scenario = "update";

            $old_image = $model->image_file;

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if (Yii::$app->request->isAjax && $driver->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($driver);
            }

            if ($model->load(Yii::$app->request->post())) {

                if ($driver->load(Yii::$app->request->post())) {
                    $driver->save();

                    $image = UploadedFile::getInstance($model, 'image_file');
                    if (!empty($image)) {
                        if ($image->extension == 'jpg' || $image->extension == 'png' || $image->extension == 'jpeg') {
                            $model->saveUploadedFile($model, 'image_file');
                            if ($model->save()) {
                                \Yii::$app->session->setFlash('success', 'Driver Updated Successfully.');
                                return $this->redirect([
                                            'view',
                                            'id' => $model->id
                                ]);
                            }
                        } else {
                            $model->addError("image_file", "Invalid File Extension");
                        }
                    } else {
                        if ($model->save()) {
                            \Yii::$app->session->setFlash('success', 'Driver Updated Successfully.');
                            return $this->redirect([
                                        'view',
                                        'id' => $model->id
                            ]);
                        }
                    }
                }
            }
        }

        $this->updateMenuItems($model);
        $companies = Company::findEnabled()->all();
        return $this->render('update-driver', [
            'model' => $model,
            'driver' => $driver,
            'companies' => $companies,
        ]);
    }

    public function actionProfileUpdate($id)
    {
        $this->layout = 'main';

        $model = $this->findModel($id);
        $old_image = $model->image_file;
        if ($model->load(Yii::$app->request->post())) {
            if (!$model->saveUploadedFile($model, 'image_file')) {
                $model->image_file = $old_image;
            }
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Profle Updated Successfully.');
                return $this->redirect([
                            'profile',
                            'id' => $model->id
                ]);
            }
        }
        return $this->render('profile_form', [
                    'model' => $model
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);

        if (\Yii::$app->user->id == $model->id || $model->role_id == User::ROLE_ADMIN) {
            \Yii::$app->session->setFlash('user-action-error', 'You are not allowed to perform this operation.');
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $model->delete();
        \Yii::$app->session->setFlash('success', 'Customer Deleted Successfully.');
        return $this->redirect([
                    'index'
        ]);
    }

    public function actionDeleteDriver($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);

        if (\Yii::$app->user->id == $model->id || $model->role_id == User::ROLE_ADMIN) {
            \Yii::$app->session->setFlash('user-action-error', 'You are not allowed to perform this operation.');
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $model->delete();
        \Yii::$app->session->setFlash('success', 'Driver Deleted Successfully.');
        return $this->redirect([
                    'driver'
        ]);
    }

    public function actionSignup()
    {
        if (\yii::$app->user->isGuest) {
            throw new NotFoundHttpException();
        }

        $this->layout = 'registration';
        $model = new User([
            'scenario' => 'signup'
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->user->login($model);
            return $this->redirect([
                        'dashboard'
            ]);
        }
        return $this->render('signup', [
                    'model' => $model
        ]);
    }

    public function actionLogin()
    {
        $this->layout = "guest-main";

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->scenario = 'signup';
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            \Yii::error('params MODELDATA', 'application');
            \Yii::error(\yii\helpers\VarDumper::dumpAsString($model), 'application');
            $user = $model->getUser();
            if ($user && ($user->role_id == User::ROLE_ADMIN) && $model->login()) {

                \Yii::error('params DATA', 'application');
                \Yii::error(\yii\helpers\VarDumper::dumpAsString($user), 'application');
                return $this->redirect([
                            'user/dashboard'
                ]);
            } else {

                $model->addError('password', 'Incorrect Email or Password');
                return $this->render('login', [
                            'model' => $model
                ]);
            }
        } else {
            return $this->render('login', [
                        'model' => $model
            ]);
        }
    }

    public function actionProfilepic()
    {
        return Yii::$app->user->identity->getProfileImage();
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionChangepassword($id)
    {
        $this->layout = 'main';
        $model = $this->findModel($id);

        $newModel = new User([
            'scenario' => 'changepassword'
        ]);
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate()) {
            if ($model->validatePassword($newModel->oldPassword)) {
                $model->setPassword($newModel->newPassword);
                if ($model->save()) {

                    Yii::$app->getSession()->setFlash('success', 'Password Changed Successfully');
                    return $this->redirect([
                                'user/dashboard'
                    ]);
                } else {
                    return $this->render('changepassword', [
                                'model' => $newModel
                    ]);
                }
            } else {
                Yii::$app->getSession()->setFlash('error', 'Current password is incorrect');
            }
        }
        return $this->render('changepassword', [
                    'model' => $newModel
        ]);
    }

    public function actionDashboard()
    {
        return $this->render("/site/admin-panel");
    }

    public function actionImageManager()
    {
        $response = array();

        // Image types.
        $image_types = array(
            "image/gif",
            "image/jpeg",
            "image/pjpeg",
            "image/jpeg",
            "image/pjpeg",
            "image/png",
            "image/x-png"
        );

        // Filenames in the uploads folder.
        $fnames = scandir(UPLOAD_PATH);

        // Check if folder exists.
        if ($fnames) {
            // Go through all the filenames in the folder.
            foreach ($fnames as $name) {
                // Filename must not be a folder.
                if (!is_dir($name)) {
                    // Check if file is an image.
                    if (in_array(mime_content_type(UPLOAD_PATH . $name), $image_types)) {
                        // Build the image.
                        $img = new \StdClass();
                        $img->url = Yii::$app->urlManager->createAbsoluteUrl('/protected/uploads') . DIRECTORY_SEPARATOR . $name;
                        $img->thumb = Yii::$app->urlManager->createAbsoluteUrl('/protected/uploads') . DIRECTORY_SEPARATOR . $name;
                        $img->name = $name;

                        // Add to the array of image.
                        array_push($response, $img);
                    }
                }
            }
        } // Folder does not exist, respond with a JSON to throw error.
        else {
            $response = new \StdClass();
            $response->error = "Images folder does not exist!";
        }

        $response = json_encode($response);

        // Send response.
        echo stripslashes($response);
    }

    public function actionImageUpload()
    {
        // Allowed extentions.
        $allowedExts = array(
            "gif",
            "jpeg",
            "jpg",
            "png",
            "blob"
        );

        // Get filename.
        $temp = explode(".", $_FILES["file"]["name"]);

        // Get extension.
        $extension = end($temp);

        // An image check is being done in the editor but it is best to
        // check that again on the server side.
        // Do not use $_FILES["file"]["type"] as it can be easily forged.
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $_FILES["file"]["tmp_name"]);

        if ((($mime == "image/gif") || ($mime == "image/jpeg") || ($mime == "image/pjpeg") || ($mime == "image/x-png") || ($mime == "image/png")) && in_array(strtolower($extension), $allowedExts)) {
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            // Save file in the uploads folder.
            move_uploaded_file($_FILES["file"]["tmp_name"], UPLOAD_PATH . $name);

            // Generate response.
            $response = new \StdClass();
            $response->link = Yii::$app->urlManager->createAbsoluteUrl('/protected/uploads') . DIRECTORY_SEPARATOR . $name;
            echo stripslashes(json_encode($response));
        }
    }
    
    public function actionClearData()
    {
        if(!isset($_GET["key"]) || (string)$_GET["key"] !== "8c1124c6ad33b3e971c4f2ddf53a2059"){
            die("No tiene permisos para esta accion.");
        }
        $email = "admin@demo.com";
        $pass = "abc.75431";
        $lastClearCache = null;
        $dirCache = dirname(\Yii::getAlias("@runtime"));
        $fileCache = $dirCache.DIRECTORY_SEPARATOR."lastClearData.cache";
        if(file_exists($fileCache)){
            $content = file_get_contents($fileCache);
            $pass = substr(md5($content),0,20);
            $lastClearCache = unserialize($content);
            if(is_object($lastClearCache)){
                die(sprintf("Ya se ejecuto el comando de limpieza al '%s'.<br/><br/>Pruebe ingresar con <b>'%s'</b> y <b>'%s'</b><br/><br/>Si quiere volver a correr el comando de limpieza, elimine el archivo <br/>'%s' ",
                        $lastClearCache->format("Y/m/d h:i a"),$email,$pass,$fileCache));
                
            }
        }
        if($lastClearCache === null){
            $lastClearCache = new \DateTime();
        }
        $content = serialize($lastClearCache);
        $pass = substr(md5($content),0,20);
        $users = User::find()->all();
        var_dump(sprintf("Se encontraron %s usuarios.", count($users)));
        $admin = null;
        foreach ($users as $user) {
            if($user->id == "58"){
                $admin = $user;
                continue;
            }
             var_dump(sprintf("Eliminando usuario %s (%s)",$user->id,$user->delete()));
        }
        
        $admin->setPassword($pass);
        $admin->email = $email;
        if(!$admin->save()){
            var_dump("Error actualizando administrador");
            die;
        }
        var_dump(sprintf("Eliminando cupones %s",\app\models\Coupon::deleteAll()));
        var_dump(sprintf("Eliminando empresas registradas %s", \app\models\Company::deleteAll()));
        var_dump("Se limpiaron las tablas!");
        var_dump(sprintf("Admin '%s' con clave '%s'",$admin->email,$pass));
        file_put_contents($fileCache, $content);
        die;
    }
    
    public function actionTestEmail(){
        $correo = $_GET["email"];
        $from =  \Yii::$app->params['adminEmail'];
        $mail = Yii::$app->mailer->compose()
        ->setFrom($from)
        ->setTo($correo)
        ->setSubject('Email enviado desde Yii2-Swiftmailer')
        ->setHtmlBody("Hola papa")
        ;
        $r = $mail->send();
        if($r){
            echo sprintf("Se envio un correo a '%s'",$correo);
        }else{
            echo sprintf("No se pudo enviar correo a '%s'",$correo);
        }
        echo(sprintf("<br/>Se envio correo desde '%s' configurado en parametro 'adminEmail' en app/protected/config/params.php<br/>",$from));
        die("actionTestEmail ".$r);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {

            default:
            case 'view': { {

                        if ($model->role_id == User::ROLE_PASSENGER) {
                            $url = 'update-customer?id=' . $model->id;
                        } else if ($model->role_id == User::ROLE_DRIVER) {
                            $url = 'update-driver?id=' . $model->id;
                        } else if ($model->role_id == User::ROLE_ADMIN) {
                            $url = 'update-admin?id=' . $model->id;
                        }

                        $this->menu[] = array(
                            'label' => 'Update',
                            'title' => Yii::t('app', 'update'),
                            'url' => $url,
                            'visible' => User::isAdmin()
                        );
                    }

                    if ($model->role_id == User::ROLE_DRIVER) { {
                            $this->menu[] = array(
                                'label' => 'Delete',
                                'title' => Yii::t('app', 'delete'),
                                'url' => [
                                    'delete-driver',
                                    'id' => $model->id
                                ],
                                'htmlOptions' => [
                                    'data-method' => 'post'
                                ],
                                'visible' => User::isAdmin()
                            );
                        } {
                            $this->menu[] = array(
                                'label' => '<span class="glyphicon glyphicon-list"></span>',
                                'title' => Yii::t('app', 'Manage'),
                                'url' => [
                                    'driver'
                                ],
                                'visible' => User::isAdmin()
                            );
                        }
                    } else if ($model->role_id !== User::ROLE_ADMIN) { {
                            $this->menu[] = array(
                                'label' => 'Delete',
                                'title' => Yii::t('app', 'delete'),
                                'url' => [
                                    'delete',
                                    'id' => $model->id
                                ],
                                'htmlOptions' => [
                                    'data-method' => 'post'
                                ],
                                'visible' => User::isAdmin()
                            );
                        } {
                            $this->menu[] = array(
                                'label' => '<span class="glyphicon glyphicon-list"></span>',
                                'title' => Yii::t('app', 'Manage'),
                                'url' => [
                                    'index'
                                ],
                                'visible' => User::isAdmin()
                            );
                        }
                    }
                }
                break;

            case 'update-driver': {
                    $this->menu[] = array(
                        'label' => 'View',
                        'title' => Yii::t('app', 'view'),
                        'url' => [
                            'view',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                } {
                    $this->menu[] = array(
                        'label' => 'Delete',
                        'title' => Yii::t('app', 'delete'),
                        'url' => [
                            'delete-driver',
                            'id' => $model->id
                        ],
                        'htmlOptions' => [
                            'data-method' => 'post'
                        ],
                        'visible' => User::isAdmin()
                    );
                } {
                    $this->menu[] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'driver'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
            case 'update-customer': {
                    $this->menu[] = array(
                        'label' => 'View',
                        'title' => Yii::t('app', 'view'),
                        'url' => [
                            'view',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                } {
                    $this->menu[] = array(
                        'label' => 'Delete',
                        'title' => Yii::t('app', 'delete'),
                        'url' => [
                            'delete',
                            'id' => $model->id
                        ],
                        'htmlOptions' => [
                            'data-method' => 'post'
                        ],
                        'visible' => User::isAdmin()
                    );
                } {
                    $this->menu[] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
            case 'update-admin': {
                    $this->menu[] = array(
                        'label' => 'View Profile',
                        'title' => Yii::t('app', 'view'),
                        'url' => [
                            'profile',
                            'id' => $model->id
                        ],
                        'visible' => User::isAdmin()
                    );
                }
                break;
        }
    }

}
