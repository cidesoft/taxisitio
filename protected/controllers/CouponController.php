<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\Coupon;
use app\models\Sepomex;
use app\models\User;
use app\models\search\Coupon as CouponSearch;
use app\models\search\Sepomex as SepomexSearch;
use app\models\search\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * CouponController implements the CRUD actions for Coupon model.
 */
class CouponController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'add',
                            'view',
                            // 'update',
                            'delete',
                            'ajax',
                            'search-cologen',
                            'search-customer'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return (User::isAdmin());
                        }
                    ]
                ]
            
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Coupon models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CouponSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionSearchCologen($settlement = null, $municipality = null, $page = null)
    {
        $data = [];
        \Yii::$app->response->format = 'json';
        $response = [];
        $list = [];
        if ($page != null)
            $page = $page - 1;
        $searchModel = new SepomexSearch();
        $dataProvider = $searchModel->searchSepomex(Yii::$app->request->queryParams, $settlement, $municipality, $page);
        if (count($dataProvider->models) > 0) {
            $response['total_count'] = Sepomex::find()->count(); // $dataProvider->getCount();
            foreach ($dataProvider->models as $mod) {
                if (! empty($settlement)) {
                    if (in_array($mod->settlement, $list)) {
                        continue;
                    } else {
                        $list[] = $mod->settlement;
                        $data[] = [
                            'id' => $mod->id,
                            'text' => $mod->settlement
                        ];
                    }
                } else {
                    if (in_array($mod->municipality, $list)) {
                        continue;
                    } else {
                        $list[] = $mod->municipality;
                        $data[] = [
                            'id' => $mod->id,
                            'text' => $mod->municipality
                        ];
                    }
                }
            }
            
            $response['items'] = $data;
            
            // $response ['page'] = 1;
        } else {
            // $response ['error'] = \yii::t ( 'app', 'No Postal Code Found' );
        }
        return $response;
    }

    public function actionSearchCustomer($user_id, $page = null)
    {
        $data = [];
        \Yii::$app->response->format = 'json';
        $response = [];
        $list = [];
        if ($page != null)
            $page = $page - 1;
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchCustomer(\Yii::$app->request->queryParams, $user_id, $page);
        if (count($dataProvider->models) > 0) {
            $response['total_count'] = User::find()->where([
                'role_id' => User::ROLE_PASSENGER
            ])->count();
            foreach ($dataProvider->models as $mod) {
                if (! empty($user_id)) {
                    if (in_array($mod->full_name, $list)) {
                        continue;
                    } else {
                        $list[] = $mod->full_name;
                        $data[] = [
                            'id' => $mod->id,
                            'text' => $mod->full_name
                        ];
                    }
                }
            }
            $response['items'] = $data;
        }
        return $response;
    }

    /**
     * Displays a single Coupon model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Coupon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new Coupon();
        $model->loadDefaultValues();
        $model->state_id = Coupon::STATE_ACTIVE;
        $post = \yii::$app->request->post();
        
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {
            $model->max_amount = $post['Coupon']['max_amount'];
            if ($model->save()) {
                
                $model->sendCoupon($model);
                
                return $this->redirect([
                    'view',
                    'id' => $model->id
                ]);
            } /*
               * else {
               * print_r ( $model->getErrors () );
               * exit ();
               * }
               */
        }
        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Coupon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }
        $this->updateMenuItems($model);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Coupon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        $model->delete();
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the Coupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Coupon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = Coupon::findOne($id)) !== null) {
            
            if ($accessCheck && ! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
            
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {
            
            case 'add':
                {
                    $this->menu['manage'] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                    );
                    // 'visible' => User::isAdmin ()
                }
                break;
            case 'index':
                {
                    $this->menu['add'] = array(
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add'
                        ]
                    );
                    // 'visible' => User::isAdmin ()
                }
                break;
            case 'update':
                {
                    $this->menu['add'] = array(
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'add'),
                        'url' => [
                            'add'
                        ]
                    );
                    // 'visible' => User::isAdmin ()
                    
                    $this->menu['manage'] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                    );
                    // 'visible' => User::isAdmin ()
                }
                break;
            default:
            case 'view':
                {
                    $this->menu['manage'] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                    );
                    // 'visible' => User::isAdmin ()
                    
                    if ($model != null) {
                        
                        $this->menu['delete'] = array(
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'title' => Yii::t('app', 'Delete'),
                            'url' => [
                                'delete',
                                'id' => $model->id
                            ]
                        );
                        // 'visible' => User::isAdmin ()
                    }
                }
        }
    }
}
