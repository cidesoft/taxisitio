<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
$params = require (__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Taxi-sitio',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'site/index',
    'bootstrap' => [
        'log',
        'languagepicker'
    ],
    'vendorPath' => VENDOR_PATH,
    'timeZone' => date_default_timezone_get(),
    'components' => [
        
        'request' => [
            'enableCsrfValidation' => YII_ENV == 'dev' ? false : true,
            'cookieValidationKey' => md5('Taxi-sitio'),
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
        
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
        
        'user' => [
            'class' => 'app\components\WebUser'
        ],
        
        'mailer' => require (MAILER_CONFIG_FILE_PATH),
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => [
                        'error',
                        'warning'
                    ]
                ]
            ]
        ],
        
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => ',',
            'decimalSeparator' => '.',
            'defaultTimeZone' => date_default_timezone_get(),
            'datetimeFormat' => 'php:Y-m-d h:i:s A',
            'dateFormat' => 'php:Y-m-d'
        ],
        
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',
            'languages' => [
                'es-ES' => 'es',
                'en' => 'en'
            ]
        ],
        
        'urlManager' => [
            'class' => 'app\components\TUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                
                [
                    'pattern' => 'aboutus',
                    'route' => 'site/about'
                ],
                [
                    'pattern' => 'contactus',
                    'route' => 'site/contact'
                ],
                
                '<controller:[A-Za-z-]+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:post>/<id:\d+>/<title>' => 'blog/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<title>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[A-Za-z-]+>/<action:[A-Za-z-]+>/<id:\d+>' => '<controller>/<action>',
                '<action:about|careers|privacy|settings|guidelines|copyright|contact|notice|faq|terms|login>' => 'site/<action>'
            ]
        ],
        'response' => [
            'formatters' => [
                'html' => [
                    'class' => 'yii\web\HtmlResponseFormatter'
                ],
                'pdf' => [
                    'class' => 'robregonm\pdf\PdfResponseFormatter'
                ]
            ]
        ],
        
        'apnsGcm' => [
            'class' => 'bryglen\apnsgcm\ApnsGcm'
        ],
        'apns' => [
            'class' => 'bryglen\apnsgcm\Apns',
            'pemFile' => PEM_FILE_PATH_DRIVER,
            'environment' => "sandbox"
        ],
        'apns1' => [
            'class' => 'bryglen\apnsgcm\Apns',
            'pemFile' => PEM_FILE_PATH_PASSENGER,
            'environment' => "sandbox"
        ],
        
        'view' => [
            'theme' => [
                'class' => 'app\components\AppTheme',
                'name' => 'green'
            ]
        ]
    ],
    'params' => $params,
    'modules' => [
        'utility' => [
            'class' => 'c006\utility\migration\Module'
        ],
        'api' => [
            'class' => 'app\modules\api\Api'
        ]
    ]
];

if (file_exists(DB_CONFIG_FILE_PATH)) {
    $config['components']['db'] = require (DB_CONFIG_FILE_PATH);
} else {
    $config['modules']['install'] = [
        'class' => 'app\modules\install\Install',
        'sqlfile' => __DIR__ . '/../backup/db_backup.sql'
    ];
    define('MODE_INSTALL', true);
}
if (YII_ENV == 'dev') {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    
    $config['modules']['tugii'] = [
        'class' => 'app\modules\tugii\Module'
    ];
}

$config['components']['errorHandler'] = [
    'errorAction' => 'site/error'
];

$config['modules']['backup'] = [
    'class' => 'app\modules\backup\Module'
];

if (defined('MAINTANANCE')) {
    $config['catchAll'] = [
        'site/notice'
    ];
}
return $config;
