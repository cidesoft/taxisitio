<?php

/**
 * @copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * @author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
return [
    'class' => 'yii\swiftmailer\Mailer',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'mail.mailnotificaction.com.mx',
        'username' => 'taxisitioapp@mailnotificaction.com.mx',
        'password' => 'fAV&+Z=#p%ag',
        'port' => '587',
//        'encryption' => 'tls',//Da un error con el servidor configurado
        'streamOptions' => [
            'ssl' => [
                'allow_self_signed' => true,
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ],
    ],
    //'useFileTransport' => YII_ENV == 'dev' ? true : false
    'useFileTransport' => false,
];
