<?php

$params = require (__DIR__ . '/params.php');
$db = require (__DIR__ . '/db-dev.php');
require (__DIR__ . '/../../common.php');

return [
		'id' => 'taxisitio',
		'basePath' => dirname ( __DIR__ ),
		'bootstrap' => [
				'log',
				'gii'
		],
		'controllerNamespace' => 'app\commands',
		'modules' => [
				'gii' => 'yii\gii\Module'
		],
		'timeZone' => date_default_timezone_get (),
		'components' => [
				'cache' => [
						'class' => 'yii\caching\FileCache'
				],
				'log' => [
						'targets' => [
								[
										'class' => 'yii\log\FileTarget',
										'levels' => [
												'error',
												'warning'
										]
								]
						]
				],
				'urlManager' => [
						'class' => 'yii\web\UrlManager',
						'scriptUrl' => 'http://path/to'
				],
				
				'db' => $db ,
				'apnsGcm' => [
						'class' => 'bryglen\apnsgcm\ApnsGcm'
				],
				'apns' => [
						'class' => 'bryglen\apnsgcm\Apns',
						'pemFile' => PEM_FILE_PATH_DRIVER,
						'environment' => "sandbox"
				],
				
		],
		'params' => $params
];
