<?php

return [
    'adminEmail' => 'taxisitioapp@mailnotificaction.com.mx',
    'logEmail' => 'taxisitioapp@mailnotificaction.com.mx',
    'company' => 'Taxi Sitio',
    'title' => 'Taxi Sitio',
    'companyUrl' => 'http://192.223.28.190/',
    'user.passwordResetTokenExpire' => 3600,
    'radius' => 30,
    'driver_limit' => 10
];
