<?php

namespace app\services\util;

use DateTime;

/**
 * Description of MyDateTime
 *
 * @author Carlos Mendoza <inhack20@gmail.com>
 */
class MyDateTime extends DateTime
{
    const DB_DATE_FORMAT = "Y-m-d H:i:s";
    
    public function __construct(string $time = "now", DateTimeZone $timezone = NULL)
    {
        parent::__construct($time, new \DateTimeZone("UTC"));
    }
    
    public function __toString()
    {
        return $this->format(self::DB_DATE_FORMAT);
    }
}
