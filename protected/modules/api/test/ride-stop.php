<?php
return [ 
		"ridestop" => [ 
				"add" => [ 
						"RideStop[start_time]" => "2017-02-17 10:29:43",
						"RideStop[stop_time]" => "2017-02-17 10:29:43",
						"RideStop[start_lat]" => "Test string",
						"RideStop[start_long]" => "Test string",
						"RideStop[end_lat]" => "Test string",
						"RideStop[end_long]" => "Test string",
						"RideStop[state_id]" => "0",
						"RideStop[type_id]" => "0",
						"RideStop[create_time]" => "2017-02-17 10:29:43",
						"RideStop[update_time]" => "2017-02-17 10:29:43",
						"RideStop[ride_id]" => "1" 
				],
				"update?id={id}" => [ 
						"RideStop[start_time]" => "2017-02-17 10:29:43",
						"RideStop[stop_time]" => "2017-02-17 10:29:43",
						"RideStop[start_lat]" => "Test string",
						"RideStop[start_long]" => "Test string",
						"RideStop[end_lat]" => "Test string",
						"RideStop[end_long]" => "Test string",
						"RideStop[state_id]" => "0",
						"RideStop[type_id]" => "0",
						"RideStop[create_time]" => "2017-02-17 10:29:43",
						"RideStop[update_time]" => "2017-02-17 10:29:43",
						"RideStop[ride_id]" => "1" 
				],
				"get?id={}" => [ ],
				"delete?id={}" => [ ], 
				"stop?id={ride-id}"=>[
						"RideStop[ride_id]" => "1",
						"RideStop[state_id]" =>"1",
						"RideStop[lat]" =>"",
						"RideStop[long]" =>"",
				]
		] 
]
;
?>
