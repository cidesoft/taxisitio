<?php
return [ 
		"driver" => [ 
				"add" => [ 
						"Driver[vehicle]" => "Test string",
						"Driver[model]" => "Test string",
						"Driver[license_no]" => "Test string",
						"Driver[registration_no]" => "Test string",
						"Driver[state_id]" => "0",
						"Driver[type_id]" => "0",
						"Driver[create_time]" => "2017-01-25 11:52:42",
						"Driver[update_time]" => "2017-01-25 11:52:42",
						"Driver[create_user_id]" => "Test string" 
				],
				"update?id={id}" => [ 
						"Driver[vehicle]" => "Test string",
						"Driver[car_make]" => "Test string",
						"Driver[model]" => "Test string",
						"Driver[license_no]" => "Test string",
						"Driver[registration_no]" => "Test string",
						"Driver[state_id]" => "0",
						"Driver[type_id]" => "0",
						'ImageProof[id_proof_file]' => '',
						'ImageProof[license_file]' => '',
						'ImageProof[document_file]' => '',
				],
				
				
				"vehicle-update" => [
						"Driver[vehicle]" => "Test string",
						"Driver[car_make]" => "Test string",
						"Driver[model]" => "Test string",
						"Driver[license_no]" => "Test string",
						"Driver[registration_no]" => "Test string",
						'ImageProof[id_proof_file]' => '',
						'ImageProof[license_file]' => '',
						'ImageProof[document_file]' => '',
				],
				
				"get?id={}" => [ ],
				"delete?id={}" => [ ] 
		] 
];
?>
