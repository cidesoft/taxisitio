<?php
return [ 
		"notification" => [ 
				"add" => [ 
						"Notification[title]" => "Test string",
						"Notification[description]" => "This is autogenerated test description.

					This is autogenerated test description.

					This is autogenerated test description.
",
						"Notification[model_type]" => "Test string",
						"Notification[model_id]" => "1",
						"Notification[state_id]" => "0",
						"Notification[type_id]" => "0",
						"Notification[is_read]" => "Test string",
						"Notification[create_time]" => "2017-02-13 14:28:25",
						"Notification[update_time]" => "2017-02-13 14:28:25",
						"Notification[user_id]" => "1",
						"Notification[create_user_id]" => "1" 
				],
				"update?id={id}" => [ 
						"Notification[title]" => "Test string",
						"Notification[description]" => "This is autogenerated test description.

					This is autogenerated test description.

					This is autogenerated test description.
",
						"Notification[model_type]" => "Test string",
						"Notification[model_id]" => "1",
						"Notification[state_id]" => "0",
						"Notification[type_id]" => "0",
						"Notification[is_read]" => "Test string",
						"Notification[create_time]" => "2017-02-13 14:28:25",
						"Notification[update_time]" => "2017-02-13 14:28:25",
						"Notification[user_id]" => "1",
						"Notification[create_user_id]" => "1" 
				],
				"get?id={}" => [ ],
				"delete?id={}" => [ ],
				"ride-update?id={}" => [ 
						"Notification[state_id]" => "1" 
				] 
		] 
];
?>
