<?php
return [ 
		"ride" => [ 
				
				"wallet-pay?id=" => [ 
						"Ride[amount]" => "10" 
				],
				"paypal-pay?id=" => [ 
						"transaction[transaction_id]" => "78FGGK",
						"transaction[amount]" => "10" 
				],
				
				"request" => [ 
						"Ride[amount]"=>"50",
						"Ride[location]" => "Mohali",
						"Ride[location_lat]" => "30.7333",
						"Ride[location_long]" => "76.7794",
						"Ride[destination]" => "Chandigarh",
						"Ride[destination_lat]" => "30.7046",
						"Ride[destination_long]" => "76.7179",
						"Ride[number_of_passengers]" => "2",
						"Ride[number_of_bags]" => "1",
						"Ride[is_pet]" => "1",
						"Ride[number_of_hours]" => "1",
						"Ride[car_price_id]" => "",
						"Ride[is_hourly]" => "0",
						"Ride[journey_type]" => "0",
						"Ride[journey_time]" => "0" 
				],
				"car-type" => [ 
						"lat" => "",
						"long" => "" 
				],
				"feedback" => [ 
						"Ride[rate_driver]" => "",
						"Ride[review]" => "" 
				],
				
				"update?id=" => [ 
						'Ride[updated_lat]' => '30.7333',
						'Ride[updated_long]' => '76.7794',
						'Ride[destination]' => 'Chandigarh',
						'Ride[destination_lat]' => '30.7333',
						'Ride[destination_long]' => '76.7794' 
				],
				
				"get?id={}" => [ ],
				"service-area?" => [ ],
				"delete?id={}" => [ ],
				"get-drivers?type_id=" => [ 
						"lat" => "Test string",
						"long" => "Test string" 
				],
				
				"reject-ride?id={}" => [ ], // id Ride ID
				"ride-detail?id={}" => [ ], // id Ride ID
				"start?id={}" => [ ], // id Ride ID
				"driver-arrival?id={}" => [ ], // id Ride ID
				"journey-start?id={}" => [ ] 
		] 
];
?>
