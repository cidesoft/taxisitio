<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
return [ 
		"user-wallet" => [ 
				"add" => [ 
						"user-wallet[transaction-amount]" => "10",
						"user-wallet[transaction-id]" => "1" 
				],
				"get?id=" => [],
				
		] 
];
?>
