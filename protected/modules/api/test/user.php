     <?php
    return [
        'user' => [
            
            'login' => [
                'LoginForm[username]' => '',
                'LoginForm[password]' => '',
                'LoginForm[device_token]' => '',
                'LoginForm[device_type]' => '',
                'User[role_id]' => ''
            ],
            'facebook-login' => [
                'User[email]' => 'vaibhav.jain@toxsl.in',
                'User[userId]' => '123456789',
                'User[provider]' => 'Facebook',
                'User[first_name]' => 'Vaibhav',
                'User[last_name]' => 'Jain',
                'User[contact_no]' => '78788788788',
                'User[country_id]' => '2',
                'User[device_token]' => '12345',
                'User[device_type]' => '1',
                'User[country_id]' => 'Jain',
                'img_url' => 'https://www.planwallpaper.com/static/images/i-should-buy-a-boat.jpg'
            ],
            
            'passenger-signup' => [
                'User[first_name]' => 'Test String',
                'User[last_name]' => 'Test String',
                'User[email]' => 'Test String',
                'User[image_file]' => 'Test String',
                'User[sepomex_id]' => '452',
                'User[password]' => 'Test String',
                'User[contact_no]' => 'Test String',
                'User[country_id]' => 'Test String',
                'User[device_token]' => '',
                'User[device_type]' => ''
            ],
            
            'set-location' => [
                'User[lat]' => 'Test String',
                'User[long]' => 'Test String',
                'User[direction]' => 'Test String'
            
            ],
            
            'status' => [
                'User[is_online]' => '0'
            
            ],
            
            'passenger-update' => [
                'User[full_name]' => 'Test String',
                'User[email]' => 'Test String',
                'User[password]' => 'Test String',
                'User[contact_no]' => 'Test String',
                'User[sepomex_id]' => '',
                'User[image_file]' => '',
                'User[address]' => '[{"type_id":"0","title":"home2 address","lat":"10","long":"20"},{"type_id":"1","title":"w address","lat":"11","long":"22"}]'
            ],
            'get-driver?type=' => [],
            
            'driver-status' => [],
            
            'driver-step1' => [
                'User[first_name]' => 'Test String',
                'User[last_name]' => 'Test String',
                'User[email]' => 'Test String',
                'User[password]' => 'Test String',
                'User[contact_no]' => 'Test String',
                'User[country_id]' => 'Test String',
                'Driver[vehicle]' => 'Test String',
                'Driver[car_make]' => 'Test String',
                'Driver[model]' => 'Test String'
            
            ],
            
            'driver-step2' => [
                'Driver[license_no]' => 'Test String',
                'Driver[registration_no]' => '',
                'ImageProof[id_proof_file]' => '',
                'ImageProof[vehicle_image]' => '',
                'ImageProof[license_file]' => '',
                'ImageProof[document_file]' => '',
                'LoginForm[device_token]' => '',
                'LoginForm[device_type]' => ''
            
            ],
            
            'driver-update' => [
                'User[first_name]' => 'Test String',
                'User[last_name]' => 'Test String',
                'User[email]' => 'Test String',
                'User[contact_no]' => 'Test String',
                'User[country_id]' => 'Test String'
            
            ],
            'recover' => [
                'User[email]' => 'vaibhav.jain@toxsltech.com'
            ],
            
            'change-password' => [
                'User[oldPassword]' => 'Test String',
                'User[newPassword]' => 'Test String',
                'User[confirm_password]' => 'Test String'
            ],
            'address-search' => [
                'Sepomex[zipcode]' => '100'
            ]
        ]
    ];
    ?>
