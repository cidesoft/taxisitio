<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api\controllers;

use app\models\Complain;
use app\models\Notification;
use app\models\Ride;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * ComplainController implements the API actions for Complain model.
 */
class ComplainController extends ApiTxController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            // 'index',
                            'add'
                            // 'get',
                            // 'update',
                            // 'delete'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [ // 'index',
                                           // 'get',
                                           // 'update'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Complain models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->txindex("app\models\Complain");
    }

    /**
     * Displays a single app\models\Complain model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        return $this->txget($id, "app\models\Complain");
    }

    /**
     * Creates a new Complain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd($ride_id)
    {
        $response = $this->getResponse();
        $params = Yii::$app->request->bodyParams;
        
        $ride = Ride::findOne($ride_id);
        
        if (! empty($ride)) {
            $user = \Yii::$app->user->id;
            $complainExists = Complain::findOne([['ride_id'=>$ride_id, 'created_by_id' => $user]]);
            if (! empty($complainExists)){
                $response['error'] = Yii::t('app', 'You have Already Submitted A complain');
                return $response;
            }
            $model = new Complain();
            if ($model->load($params)) {
                
                $admin = User::find()->where([
                    'role_id' => User::ROLE_ADMIN
                ])->one();
                
                if (isset($ride->driver_id)) {
                    $model->driver_id = $ride->driver_id;
                } else {
                    $model->driver_id = $admin->id;
                }
                
                $model->ride_id = $ride_id;
                if ($model->save()) {
                    $notification = new Notification();
                    $notification->user_id = $model->driver_id;
                    $notification->message = Yii::t('app','Warning');
                    $notification->model_id = $model->id;
                    $notification->model_type = Complain::className();
                    $notification->type_id = $ride->journey_type;
                    $notification->created_by_id = $user;
                    if ($notification->save()) {
                        $model->requestNotification($model->driver_id, $notification->message);
                    }
                    $response['status'] = 'OK';
                    $response['detail'] = $model->asJson();
                } else {
                    $response['error'] = Yii::t('app', 'Unable To Save Data');
                }
            } else {
                $response = $model->getErrorsString();
            }
        } else {
            $response['error'] = Yii::t('app', 'No Ride Found');
        }
        return $response;
    }

    /**
     * Updates an existing Complain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $data = [];
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->save()) {
                
                $data['status'] = self::API_OK;
                
                $data['detail'] = $model;
            } else {
                $data['error'] = $model->flattenErrors;
            }
        } else {
            $data['error_post'] = \Yii::t('app', 'No Data Posted');
        }
        
        return $this->sendResponse($data);
    }

    /**
     * Deletes an existing Complain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->txDelete($id, "app\models\Complain");
    }
}
