<?php

namespace app\modules\api\controllers;

use app\models\AuthSession;
use app\models\Country;
use app\models\Driver;
use app\models\EmailQueue;
use app\models\HaLogin;
use app\models\ImageProof;
use app\models\Log;
use app\models\LoginForm;
use app\models\Ride;
use app\models\Sepomex;
use app\models\User;
use app\models\UserAddress;
use app\models\UserWallet;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\UploadedFile;
use app\models\MyResponse;

/**
 * UserController implements the API actions for User model.
 */
class UserController extends ApiTxController
{

    private $onlineusercount = null;

    public function beforeAction($action)
    {
        AuthSession::authenticateSession();
        $model = new User();
        // $model->setUserOnline ();
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'check',
                            'get',
                            'update',
                            'delete',
                            'country',
                            'view',
                            'add',
                            'logout',
                            'change-password',
                            'logout',
                            'add-log',
                            'passenger-update',
                            'passenger-address',
                            'driver-update',
                            'profile',
                            'driver-vehicle-update',
                            'driver-step2',
                            'set-location',
                            'current-location',
                            'status',
                            'driver-step2',
                            'direction',
                            'check-notification',
                            'address-search'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [
                            'login',
                            'signup',
                            'recover',
                            'check',
                            'mode',
                            'beat',
                            'add-log',
                            'passenger-signup',
                            'facebook-login',
                            'country',
                            'driver-step1',
                            'driver-step2',
                            'address-search'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->txIndex("User");
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        return $this->txget($id, "User");
    }

    public function actionAddressSearch($page = null)
    {
        $list = [];
        $response = $this->getResponse();
        $params = Yii::$app->request->bodyParams;

        $model = new \app\models\search\Sepomex();
        $dataProvider = $model->searchFilter(\Yii::$app->request->post(), $page);

        if (count($dataProvider->models) > 0) {

            foreach ($dataProvider->models as $mod) {
                $list[] = $mod->asJson();
            }

            $response['status'] = "OK";
            $response['details'] = $list;
            $response['pageCount'] = isset($page) ? $page : '0';
            $response['pageSize'] = $dataProvider->pagination->pageSize;
            $response['totalPage'] = isset($dataProvider->pagination->pageCount) ? $dataProvider->pagination->pageCount : '0';
        } else {
            $response['error'] = \yii::t('app', 'No Postal Code Found');
        }

        return $response;
    }

    public function actionSetLocation()
    {
        $response = $this->getResponse();
        $model = \Yii::$app->user->identity;
        if (isset($_POST['User'])) {

            $latitude = $_POST['User']['lat'];
            $longtitude = $_POST['User']['long'];
            $direction = $_POST['User']['direction'];
            if (!empty($latitude) && (!empty($longtitude))) {
                $model->lat = $latitude;
                $model->long = $longtitude;
                $model->direction = $direction;
                $model->last_location_time = new \app\services\util\MyDateTime();
                if ($model->save(false)) {
                    $response['status'] = "OK";
                    $response['detail'] = \Yii::t('app', "Lat long saved successfully");
                }
            } else {
                $response['error'] = \Yii::t('app', 'latitude and longitude cannot be blank');
            }
        } else {
            $response['error'] = \Yii::t('app', 'no data posted');
        }
        return $response;
    }

    public function actionCheckNotification($rideId, $stateId)
    {
        $response = $this->sendResponse();
        $model = Ride::findOne($rideId)->where([
            'state_id' => $stateId
        ]);
        if (!empty($model)) {
            $response['status'] = 'OK';
            $response['detail'] = $model->asJson(false, false, true, false);
        } else {
            $response['status'] = 'NOK';
        }
        return $response;
    }

    public function actionCurrentLocation()
    {
        $response = $this->getResponse();
        $model = \Yii::$app->user->identity;
        if ($model) {

            $response['status'] = 'OK';
            $response['latitude'] = Yii::$app->user->identity->lat;
            $response['longitude'] = Yii::$app->user->identity->long;
        } else {
            $response['status'] = 'NOK';
            $response['error'] = \Yii::t('app', 'Login To Continue');
        }
        return $response;
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        return $this->txSave("User");
    }

    public function actionProfile()
    {
        $response = $this->getResponse();
        $model = User::findOne(\yii::$app->user->id);
        if (!empty($model)) {
            $response['status'] = "OK";
            $response['detail'] = $model->asJson();
        } else {
            $response['error'] = \Yii::t('app', 'Not able to find your profile, please contact the administrator');
        }
        return $response;
    }

    public function actionStatus()
    {
        $response = $this->getResponse();
        $model = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save(false)) {
                $response['status'] = "OK";
                if ($model->is_online == User::IS_ONLINE) {
                    $response['state'] = $model->is_online;
                    $response['detail'] = \Yii::t('app', 'You are now online');
                } else {
                    $response['state'] = $model->is_online;
                    $response['detail'] = \Yii::t('app', 'You are now offline');
                }
            } else {
                $response = '';
                foreach ($model->getErrors() as $error)
                    $err .= implode(".", $error);
                $response['error'] = $err;
            }
        } else {
            $response['error'] = \Yii::t('app', 'No data posted');
        }

        return $response;
    }

    public function actionDirection($id)
    {
        $response = $this->getResponse();
        $model = User::find()->where([
                    'id' => $id
                ])->one();
        if (empty($model)) {
            $reponse['error'] = \Yii::t('app', 'No user found');
            return $response;
        }
        $response['detail'] = $model->asJson();
        $response['status'] = 'OK';
        return $response;
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $response = $this->getResponse();
        $model = User::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['User']['sepomex_id'])) {
                $sepomexModel = Sepomex::find()->where([
                            'id' => $_POST['User']['sepomex_id']
                        ])->one();

                if (!empty($sepomexModel)) {
                    $model->settlement = $sepomexModel->settlement;
                    $model->municipality = $sepomexModel->municipality;
                    $model->zipcode = $sepomexModel->zipcode;
                }
            }

            if ($model->save()) {
                $response['status'] = 'OK';
                $response['detail'] = $model;
            } else {
                $response['error'] = $model->getErrors();
            }
        } else {
            $response['error_post'] = \Yii::t('app', 'No Data Posted');
        }

        return $response;
    }

    public function actionCheck()
    {
        $response = $this->getResponse();
        if (!\Yii::$app->user->isGuest) {
            $user = \Yii::$app->user->identity;
            $user = User::findOne(\yii::$app->user->id);
            $user->last_visit_time = date("Y-m-d H:i:s");
            $user->save();
            $response['status'] = 'OK';
            $response['detail'] = $user->asJson(true);
        } else {
            $headers = getallheaders();
            $auth_code = isset($headers['auth_code']) ? $headers['auth_code'] : null;
            if ($auth_code == null)
                $auth_code = \Yii::$app->request->getQueryString('auth_code');
            if ($auth_code) {
                $auth_session = AuthSession::find()->where([
                            'auth_code' => $auth_code
                        ])->one();
                if ($auth_session) {
                    $response['status'] = 'OK';
                    if (isset($_POST['AuthSession'])) {
                        $auth_session->device_token = $_POST['AuthSession']['device_token'];
                        if ($auth_session->save()) {
                            $user = User::findOne($auth_session->create_user_id);
                            $user->last_visit_time = date("Y-m-d H:i:s");
                            $user->save();
                            $response['detail'] = $user->asJson(true);
                            $response['status'] = 'OK';
                            $response['auth_session'] = \Yii::t('app', 'Auth Session updated');
                        } else {
                            $response = '';
                            foreach ($model->getErrors() as $error)
                                $err .= implode(".", $error);
                            $response['error'] = $err;
                        }
                    }
                } else
                    $response['error'] = \Yii::t('app', 'Session not found');
            } else {
                $response['error'] = \Yii::t('app', 'Auth code not found');
                $response['auth'] = isset($auth_code) ? $auth_code : '';
            }
        }
        return $response;
    }

    public function actionDriverStep1()
    {
        $response = $this->getResponse();
        $valid = false;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = new User();
            $model->scenario = 'driver-signup';
            if ($model->load(Yii::$app->request->post())) {
                $model->role_id = User::ROLE_DRIVER;
                $model->state_id = User::STATE_INACTIVE;
                $model->is_available = User::IS_AVAILABLE;
                $model->is_online = User::IS_ONLINE;
                $model->type_id = User::TYPE_STEP_1;
                $model->full_name = $model->first_name . ' ' . $model->last_name;
                $email_identify = User::findByUsername($model->email);
                if (empty($email_identify)) {
                    if ($model->save()) {

                        \yii::warning("user_saved");
                        $driver = new Driver();
                        $driver->scenario = "step1";

                        if ($driver->load(Yii::$app->request->post())) {

                            $driver->create_user_id = $model->id;
                            if ($driver->save()) {
                                \yii::warning("driver_saved");
                                $valid = true;
                            } else {
                                $err = '';
                                foreach ($driver->getErrors() as $error) {
                                    $err .= implode(',', $error);
                                }
                                $response['error'] = $err;
                            }
                        }
                    } else {
                        $err = '';
                        foreach ($model->getErrors() as $error) {
                            $err .= implode(',', $error);
                        }
                        $response['error'] = $err;
                    }
                } else {
                    $response['error'] = \Yii::t('app', 'Email already exists');
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['error'] = $e;
            \yii::warning($e);
        }
        if ($valid == true) {
            $transaction->commit();
            $response['status'] = 'OK';
            $response['detail'] = $model->asJson(true, true);
        } else {
            $response['status'] = 'NOK';
        }
        return $response;
    }

    public function actionDriverStep2($id)
    {
        $response = $this->getResponse();
        $valid = false;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = Driver::find()->where([
                        'create_user_id' => $id
                    ])->one();

            $model->scenario = "step2";

            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {
                    $user = User::find()->where([
                                'id' => $id
                            ])->one();

                    $user->type_id = User::TYPE_STEP_2;
                    $user->avg_rating = "0.0";

                    if (!$user->save()) {
                        $response['error'] = \Yii::t('app', 'User data not saved');
                        return $response;
                    }

                    if (isset($_FILES['ImageProof'])) {

                        $imageProof = new ImageProof();

                        if (!$imageProof->saveUploadedFile($imageProof, 'id_proof_file')) {
                            $response['error'] = \Yii::t('app', 'Id proof file not uploaded successfully.');
                            return $response;
                        }

                        if (!$imageProof->saveUploadedFile($imageProof, 'license_file')) {
                            $response['error'] = \Yii::t('app', 'License file not uploaded successfully.');
                            return $response;
                        }
                        if (!$imageProof->saveUploadedFile($imageProof, 'document_file')) {
                            $response['error'] = \Yii::t('app', 'Document file not uploaded successfully.');
                            return $response;
                        }

                        if (!$imageProof->saveUploadedFile($imageProof, 'vehicle_image')) {
                            $response['error'] = \Yii::t('app', 'Id proof file not uploaded successfully.');
                            return $response;
                        }
                        $imageProof->create_user_id = $user->id;

                        if ($imageProof->save()) {
                            \yii::warning($imageProof);
                            $valid = true;
                        } else {
                            $err = '';
                            foreach ($imageProof->getErrors() as $error) {
                                $err .= implode(',', $error);
                            }
                            $response['error'] = $err;
                        }
                    }
                } else {

                    $err = '';
                    foreach ($model->getErrors() as $error) {
                        $err .= implode(',', $error);
                    }
                    $response['error'] = $err;
                }
            } else {

                $response['error'] = \Yii::t('app', 'Data Not Posted');
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['error'] = $e;
            \yii::warning($e);
        }
        if ($valid == true) {
            $transaction->commit();
            $login = new LoginForm();
            \Yii::$app->user->login($user);
            $loginForm = new LoginForm();
            $loginForm->username = $user->email;
            $loginForm->password = $user->password;
            $loginForm->device_token = $_POST['LoginForm']['device_token'];
            $loginForm->device_type = $_POST['LoginForm']['device_type'];
            $response['status'] = 'OK';
            $response['auth_code'] = AuthSession::newSession($loginForm)->auth_code;
            $response['detail'] = \Yii::$app->user->identity->asJson(true, true);
            $response['success'] = \Yii::t('app', 'Login Successfully');
        } else {
            $response['status'] = 'NOK';
        }
        return $response;
    }

    public function actionCountry()
    {
        $response = $this->getResponse();
        $list = [];
        $countries = Country::find()->orderBy([
                    'id' => SORT_ASC
                ])->all();
        if (!empty($countries)) {
            foreach ($countries as $country) {
                $list[] = $country->asJson();
            }
            $response['status'] = 'OK';
            $response['country'] = $list;
        }
        return $response;
    }

    public function actionPassengerSignup()
    {
        $response = $this->getResponse();
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();
        $valid = false;
        $model = new User();
        $userWallet = new UserWallet();
        $model->scenario = 'passenger-signup';
        if ($model->load(Yii::$app->request->post())) {
            $model->role_id = User::ROLE_PASSENGER;
            $model->country_id = $_POST['User']['country_id'];
            $model->full_name = $model->first_name . ' ' . $model->last_name;
            if (isset($_POST['User']['sepomex_id'])) {
                $sepomexModel = Sepomex::find()->where([
                            'id' => $_POST['User']['sepomex_id']
                        ])->one();

                if (!empty($sepomexModel)) {
                    $model->settlement = $sepomexModel->settlement;
                    $model->municipality = $sepomexModel->municipality;
                    $model->zipcode = $sepomexModel->zipcode;
                }
            }

            $email_identify = User::findByUsername($model->email);
            if (empty($email_identify)) {

                try {
                    if ($model->save()) {

                        $userWallet->amount = 0;
                        $country = Country::getCountryName($model->country_id);
                        $userWallet->currency_symbol = $country[1];
                        $userWallet->currency_code = $country[2];
                        $userWallet->created_by_id = $model->id;
                        if ($userWallet->save()) {

                            $valid = true;
                            \Yii::$app->user->login($model);
                            $loginForm = new LoginForm();
                            $loginForm->username = $model->email;
                            $loginForm->password = $model->password;
                            $loginForm->device_token = $model->device_token;
                            $loginForm->device_type = $model->device_type;
                            $response['auth_code'] = AuthSession::newSession($loginForm)->auth_code;
                            if ($valid == true) {
                                $transaction->commit();
                                $response['status'] = 'OK';
                                $response['detail'] = $model->asJson(false, false);
                            }
                        } else {
                            $response['error'] = $userWallet->getErrorsString();
                        }
                    } else {
                        $response['error'] = $model->getErrorsString();
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                $response['error'] = \Yii::t('app', 'Email already exists');
            }
        }
        return $response;
    }

    public function actionPassengerUpdate()
    {
        $response = $this->getResponse();
        $model = User::findOne(\yii::$app->user->id);
        if (!empty($model)) {
            if ($model->load(Yii::$app->request->post())) {

                if (isset($_POST['User']['sepomex_id'])) {
                    $sepomexModel = Sepomex::find()->where([
                                'id' => $_POST['User']['sepomex_id']
                            ])->one();
                    // print_r($sepomexModel);exit;
                    if (!empty($sepomexModel)) {
                        $model->sepomex_id = $sepomexModel->id;
                        $model->settlement = $sepomexModel->settlement;
                        $model->municipality = $sepomexModel->municipality;
                        $model->zipcode = $sepomexModel->zipcode;
                    }
                }

                $model->saveUploadedFile($model, 'image_file');
                $model->role_id = User::ROLE_PASSENGER;
                if (isset($_POST ['User'] ['country_id'])) {
                    $model->country_id = $_POST ['User']['country_id'];
                }
                if ($model->save()) {

                    $response['status'] = 'OK';
                    $response['detail'] = $model->asJson(false, false);
                } else {
                    $err = '';
                    foreach ($model->getErrors() as $error) {
                        $err .= implode(',', $error);
                    }
                    $response['error'] = $err;
                }
            }
        } else {
            $response['error'] = \Yii::t('app', 'Passenger not found');
        }
        return $response;
    }

    /**
     *
     * @return Passenger Address Update
     */
    public function actionPassengerAddress()
    {
        $response = $this->getResponse();
        $model = User::findOne(\yii::$app->user->id);
        $userId = \yii::$app->user->id;
        if (!empty($userId)) {
            if (isset($_POST['User']['address'])) {

                $addresses = json_decode($_POST['User']['address'], true);

                foreach ($addresses as $address) {

                    if (!isset($address['type_id']))
                        continue;

                    $user_address = UserAddress::findOne([
                                'create_user_id' => $userId,
                                'type_id' => $address['type_id']
                    ]);

                    if (!$user_address) {
                        $user_address = new UserAddress();
                    }
                    $user_address->create_user_id = $userId;
                    $user_address->type_id = $address['type_id'];
                    $user_address->title = $address['title'];
                    $user_address->lat = $address['lat'];
                    $user_address->long = $address['long'];
                    if ($user_address->save()) {
                        $response['status'] = 'OK';
                        $user = User::findOne($userId);
                        $response['detail'] = $user->asJson(false, false);
                    } else {
                        $err = '';
                        foreach ($user_address->getErrors() as $error) {
                            $err .= implode(',', $error);
                        }
                        $response['error'] = $err;
                    }
                }
            }
        } else {
            $response['error'] = \Yii::t('app', 'Passenger not found');
        }
        return $response;
    }

    /**
     *
     * @return string|string[]|NULL[]
     */
    public function actionLogin()
    {
        $response = $this->getResponse();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $user = User::findByUsername($model->username);
            $id = $_POST['User']['role_id'];
            if ($user && $user->role_id == $id) {
                if ($user->state_id == User::STATE_ACTIVE) {
                    if ($model->login()) {
                        $response['status'] = 'OK';
                        $response['auth_code'] = AuthSession::newSession($model)->auth_code;
                        $response['user_detail'] = $user->asJson(true);
                    } else {
                        $response['error'] = \Yii::t('app', 'Incorrect Password');
                    }
                } else {
                    $response['error'] = \Yii::t('app', 'Admin Has Not Verified Your Account Yet');
                    $response['driver_profile_step'] = $user->type_id;
                    $response['id'] = $user->id;
                }
            } else {
                $response['error'] = \Yii::t('app', 'Incorrect Email');
            }
        } else {
            $response['error'] = \Yii::t('app', 'Unable To Load Data');
        }
        return $response;
    }

    public function actionRecover($role_id)
    {
        $response = $this->getResponse();
        $model = new User();
        $emailQueue = new EmailQueue();
        if (isset($_POST['User']['email'])) {
            $email = trim($_POST['User']['email']);
            $user = User::findOne([
                        'email' => $email,
                        'role_id' => $role_id
            ]);
            if ($user) {
                $user->generatePasswordResetToken();
                $user->save();
                $email = $user->email;
                $view = "passwordResetToken";
                $sub = "Recover Your Account at: " . \Yii::$app->params['company'];

                $sendMessage = \Yii::$app->controller->renderPartial('/../../../mail/passwordResetToken', [
                    'user' => $user
                ]);

                $emailQueue->sendNow($email, $sendMessage, \Yii::$app->params['adminEmail'], $sub);

                $response['success'] = Yii::t('app', 'Please check your email to reset your password');
                $response['status'] = 'OK';
                $response['recover-email'] = $user->email;
            } else {
                $response['error'] = Yii::t('app', 'Email is not registered');
            }
        } else {
            $response['error'] = Yii::t('app', 'Please enter Email Address');
        }
        return $response;
    }

    public function actionLogout()
    {
        $response = $this->getResponse();
        $model = \yii::$app->user->Identity;
        if ($model->role_id == User::ROLE_DRIVER) {
            $model->is_online = User::IS_OFFLINE;
            $model->save();
        }
        $auth_code = isset($headers['auth_code']) ? $headers['auth_code'] : null;
        if ($auth_code == null)
            $auth_code = \Yii::$app->request->get('auth_code');
        if (!empty($auth_code)) {
            $auth_session = AuthSession::find()->where([
                        'auth_code' => $auth_code
                    ])->one();
            if ($auth_session) {
                $auth_session->delete();
            }
        }
        if (Yii::$app->user->logout())
            $response['status'] = 'OK';

        return $response;
    }

    public function actionChangePassword()
    {
        $response = $this->getResponse();
        $response['post'] = $_POST;
        $model = User::findOne([
                    'id' => \Yii::$app->user->identity->id
        ]);
        $newModel = new User([
            'scenario' => 'changepassword'
        ]);
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate()) {
            if ($model->validatePassword($newModel->oldPassword)) {
                $model->setPassword($newModel->newPassword);
                if ($model->save()) {

                    $response['status'] = 'OK';
                } else {
                    $response['error'] = \Yii::t('app', 'Incorrect Password');
                }
            } else {
                $response['error'] = \Yii::t('app', 'Current password is incorrect');
            }
        }
        return $response;
    }

    public function actionAddLog()
    {
        $response = $this->getResponse();
        $model = new Log();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $email = $model->email;
                $view = 'errorlog';
                $sub = "An Error/Crash was reported : " . \Yii::$app->params['company'];
                Yii::$app->mailer->compose([
                            'html' => 'errorlog'
                                ], [
                            'user' => $model
                        ])
                        ->setTo(\Yii::$app->params['adminEmail'])
                        ->setFrom(\Yii::$app->params['logEmail'])
                        ->setSubject($sub)
                        ->send();
            }
        }
        $response['status'] = 'OK';
        return $response;
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->txDelete($id, "User");
    }

    public function actionDriverUpdate()
    {
        $response = $this->getResponse();
        $model = User::findOne(\yii::$app->user->id);

        $old_image = $model->image_file;
        if (!empty($model)) {

            $model->scenario = 'driver-update';

            if ($model->load(Yii::$app->request->post())) {

                $image = UploadedFile::getInstance($model, 'image_file');
                if (!empty($image)) {
                    $file = UPLOAD_PATH . $image->baseName . '.' . $image->extension;
                    $image->saveAs($file);
                    $model->image_file = $image->baseName . '.' . $image->extension;
                } else {
                    $model->image_file = $old_image;
                }

                if ($model->save()) {
                    $response['status'] = 'OK';
                    $response['detail'] = $model->asJson(true, true);
                } else {
                    $err = '';
                    foreach ($model->getErrors() as $error) {
                        $err .= implode(',', $error);
                    }
                    $response['error'] = $err;
                }
            } else {
                $response['error'] = \Yii::t('app', 'Driver not found');
            }
        }

        return $response;
    }

    public function actionFacebookLogin()
    {
        $response = $this->getResponse();
        if (isset($_POST['User'])) {
            $email = isset($_POST['User']['email']) ? $_POST['User']['email'] : '';
            $id = isset($_POST['User']['userId']) ? $_POST['User']['userId'] : '';
            $provider = isset($_POST['User']['provider']) ? $_POST['User']['provider'] : '';
            $first_name = isset($_POST['User']['first_name']) ? $_POST['User']['first_name'] : '';
            $countryID = isset($_POST['User']['country_id']) ? $_POST['User']['country_id'] : '';
            $contactNo = isset($_POST['User']['contact_no']) ? $_POST['User']['contact_no'] : '';
            $last_name = isset($_POST['User']['last_name']) ? $_POST['User']['last_name'] : '';
            $token = isset($_POST['User']['device_token']) ? $_POST['User']['device_token'] : '';
            $type = isset($_POST['User']['device_type']) ? $_POST['User']['device_type'] : '';
            $auth = HaLogin::find()->where([
                        'loginProvider' => $provider,
                        'userId' => $id
                    ])->one();
            if (\Yii::$app->user->isGuest) {
                if ($auth) {
                    $user = $auth->user;
                    if ($user->state_id == User::STATE_ACTIVE) {
                        \Yii::$app->user->login($user, 3600 * 24 * 30);
                        $response['status'] = 'OK';
                        $login = new LoginForm();
                        $login->username = $user->email;
                        $login->password = $id;
                        $login->device_token = $token;
                        $login->device_type = $type;
                        $response['auth_code'] = AuthSession::newSession($login)->auth_code;
                        $response['detail'] = \Yii::$app->user->identity->asJson();
                        $response['success'] = yii::t('app', 'Login Successfully');
                    } else {
                        $response['error'] = yii::t('app', 'You are not authorized to login');
                        $response['status'] = 'NOK';
                    }
                } else {
                    // signup
                    if ($email !== null && User::find()->where([
                                'email' => $email
                            ])->exists()) {
                        $user = User::find()->where([
                                    'email' => $email
                                ])->one();
                        $user->password = md5($id);
                        $user->saveUploadedFile($user, 'image_file');

                        if (!$user->save()) {
                            $response['error'] = yii::t('app', 'Unable to save data');
                        }
                    } else {
                        $user = new User([
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'email' => $email,
                            'password' => $id
                        ]);
                        if (isset($_POST['img_url'])) {
                            $random = rand(0, 999) . 'user.jpg';
                            $user->image_file = $random;
                            copy($_POST['img_url'], UPLOAD_PATH . $random);
                        }

                        $user->full_name = $first_name . " " . $last_name;
                        $user->is_fb = User::FB_SIGNUP;
                        $user->role_id = User::ROLE_PASSENGER;
                        $user->state_id = User::STATE_ACTIVE;
                        $user->create_time = (string)new \app\services\util\MyDateTime();
                    }
                    $transaction = \Yii::$app->db->beginTransaction();

                    if ($user->save(false)) {

                        $auth = new HaLogin([
                            'user_id' => $user->id,
                            'loginProvider' => $provider,
                            'userId' => (string) $id,
                            'loginProviderIdentifier' => md5($id)
                        ]);

                        $hasWallet = UserWallet::find()->where([
                                    'created_by_id' => $user->id
                                ])->one();
                        if (!empty($hasWallet)) {
                            $response['error'] = Yii::t('app', 'Wallet Already Exists', [
                                        "email" => $email,
                            ]);
                            $response[MyResponse::RESPONSE_ERROR_NAME] = MyResponse::ERROR_EMAIL_ALREADY_REGISTERED;
                        } else if (!empty($contactNo) && !empty($countryID)) {

                            $userWallet = new UserWallet();

                            $userWallet->amount = 0;
                            $country = Country::getCountryName($countryID);
                            $userWallet->currency_symbol = $country[1];
                            $userWallet->currency_code = $country[2];
                            $userWallet->created_by_id = $user->id;
                            if ($userWallet->save()) {

                                $user->country_id = $countryID;
                                $user->contact_no = (int) $contactNo;
                                if ($user->save()) {
                                    if ($auth->save(false)) {
                                        $transaction->commit();
                                        \Yii::$app->user->login($user, 3600 * 24 * 30);
                                        $response['status'] = 'OK';
                                        $login = new LoginForm();
                                        $login->username = $user->email;
                                        $login->password = $id;
                                        $login->device_token = $token;
                                        $login->device_type = $type;
                                        $login->login();
                                        $response['status'] = 'OK';
                                        $response['auth_code'] = AuthSession::newSession($login)->auth_code;
                                        $response['detail'] = \Yii::$app->user->identity->asJson();
                                        $response['success'] = yii::t('app', 'Login Successfully');
                                    } else {
                                        $err = '';
                                        if ($auth->hasErrors()) {
                                            foreach ($auth->getErrors() as $error)
                                                $err .= implode(".", $error);
                                        }
                                        $response['error'] = $err;
                                    }
                                } else {
                                    $response['error'] = $user->getErrors();
                                }
                            }
                        } else {
                            $response['status'] = Yii::t('app', 'NOK');
                            $response['error'] = Yii::t('app', 'Contact No or Country is Empty');
                            $response['country'] = false;
                        }
                    } else {
                        $err = '';
                        if ($user->hasErrors()) {
                            foreach ($user->getErrors() as $error)
                                $err .= implode(".", $error);
                        }
                        $response['error'] = $err;
                    }
                }
            } else { // user already logged in
                if (!$auth) { // add auth provider
                    $auth = new HaLogin([
                        'user_id' => \Yii::$app->user->identity->id,
                        'loginProvider' => $provider,
                        'userId' => $id
                    ]);
                    if ($auth->save()) {
                        /** @var User $user */
                        $user = $auth->user;
                        $response['status'] = 'OK';
                        $login = new LoginForm();
                        $login->username = $user->email;
                        $login->password = $id;
                        $login->device_token = $token;
                        $login->device_type = $type;
                        $response['auth_code'] = AuthSession::newSession($login)->auth_code;
                        $response['detail'] = \Yii::$app->user->identity->asJson();
                        $response['success'] = yii::t('app', 'Login Successfully');
                    } else {
                        $err = '';
                        if ($auth->hasErrors()) {
                            foreach ($auth->getErrors() as $error)
                                $err .= implode(".", $error);
                        }
                        $response['error'] = $err;
                    }
                } else { // there's existing auth
                    $response['error'] = yii::t('app', 'There is another user using it');
                }
                $response['error'] = yii::t('app', 'You are already logged in with different Account');
            }
        }
        $response['posts'] = $_POST;
        $response['files'] = $_FILES;
        return $response;
    }

}
