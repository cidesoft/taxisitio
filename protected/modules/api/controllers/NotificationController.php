<?php
namespace app\modules\api\controllers;

use app\models\Notification;
use app\models\Ride;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * NotificationController implements the API actions for Notification model.
 */
class NotificationController extends ApiTxController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'add',
                            'get',
                            'update',
                            'delete',
                            'ride-update'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [
                            'index',
                            'get',
                            'update'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionRideUpdate($id)
    {
        $response = $this->getResponse();
        $notification = Notification::findOne($id);
        if (! empty($notification)) {
            if (isset($_POST['Notification']['state_id'])) {
                $notification->state_id = $_POST['Notification']['state_id'];
                if ($notification->save()) {
                    
                    if ($_POST['Notification']['state_id'] == Notification::STATE_ACCEPT) {
                        
                        $user = User::findOne(\Yii::$app->user->id);
                        
                        if ($user->role_id != User::ROLE_DRIVER) {
                            $response['error'] = \Yii::t('app', 'Only Allowed to Drivers.');
                            return $response;
                        }
                        
                        if ($user->is_available != User::IS_AVAILABLE) {
                            $response['error'] = \Yii::t('app', 'You cannot accept this ride.');
                            return $response;
                        }
                        
                        $ride = Ride::findOne($notification->model_id);
                        
                        if (! empty($ride)) {
                            if ($ride->state_id == Ride::STATE_NEW && empty($ride->driver_id)) {
                                $ride->state_id = Ride::STATE_ACCEPTED;
                                $ride->driver_id = $userId;
                                
                                if ($ride->save()) {
                                    $ride = Ride::findOne($notification->model_id);
                                    if ($ride->driver_id == $userId) {
                                        $user->is_available = User::NOT_AVAILABLE;
                                        $user->save();
                                        $firstContent = \Yii::t('app', 'Ride from :');
                                        $secondContent = \Yii::t('app', ' accepted by ');
                                        $content = $firstContent . $ride->location . $secondContent . \Yii::$app->user->identity->full_name;
                                        $notification = new Notification();
                                        $notification->saveNotification($ride->create_user_id, $ride, $content, Notification::TYPE_RIDE_ACCEPTED, \Yii::$app->user->id);
                                        Notification::clearDuplicateNotification($id, \Yii::$app->user->id, Notification::TYPE_CAR_REQUEST, $ride->label(), Notification::STATE_NO_RESPONSE);
                                        $response['status'] = 'OK';
                                        $response['message'] = \Yii::t('app', 'Request Accepted Successfully');
                                    } else {
                                        $response['error'] = \Yii::t('app', 'Ride already accepted.');
                                    }
                                } else {
                                    $err = '';
                                    foreach ($ride->getErrors() as $error) {
                                        $err .= implode(',', $error);
                                    }
                                    $response['error'] = $err;
                                }
                            } else {
                                $response['error'] = \Yii::t('app', 'The Ride State is Different.');
                            }
                        } else {
                            $response['error'] = \Yii::t('app', 'No ride Found.');
                        }
                    }
                }
            } else {
                $response['error'] = \Yii::t('app', 'Request Id not posted');
            }
        } else {
            $response['error'] = \Yii::t('app', 'Request does not exists');
        }
        return $response;
    }

    /**
     * Lists all Notification models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->txIndex("app\models\search\NotificationSearch");
    }

    /**
     * Displays a single app\models\Notification model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        return $this->txget($id, "app\models\Notification");
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        return $this->txSave("app\models\Notification");
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $response = $this->getResponse();
        $model = Notification::findOne($id);
        if ($model->load(\Yii::$app->request->post())) {
            
            if ($model->save()) {
                $response['status'] = 'OK';
                $response['detail'] = $model;
            } else {
                $response['error'] = $model->getErrors();
            }
        } else {
            $response['error_post'] = \Yii::t('app', 'No Data Posted');
        }
        
        return $response;
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->txDelete($id, "app\models\Notification");
    }
}
