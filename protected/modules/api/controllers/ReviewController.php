<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api\controllers;

use app\models\Review;
use app\models\Ride;
use app\models\User;
use app\modules\api\controllers\ApiTxController;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * ReviewController implements the API actions for Review model.
 */
class ReviewController extends ApiTxController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'ruleConfig' => [ 
								'class' => AccessRule::className () 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'add',
												'get',
												'update',
												'delete' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								],
								[ 
										'actions' => [ 
												'index',
												'get',
												'update' 
										],
										'allow' => true,
										'roles' => [ 
												'?',
												'*' 
										] 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Review models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->txindex ( "app\models\Review" );
	}
	
	/**
	 * Displays a single app\models\Review model.
	 *
	 * @return mixed
	 */
	public function actionAdd($id) {
		$response = $this->getResponse ();
		$param = \yii::$app->request->bodyParams;
		$valid = false;
		$db = \Yii::$app->db;
		
		$review = Review::find ()->where ( [ 
				'ride_id' => $id 
		] )->one ();
		if (! empty ( $review )) {
		    $response ['error'] = \Yii::t('app','You have already reviewed for this ride' );
		} else {
			
			// transaction begin
			$transaction = $db->beginTransaction ();
			try {
				$model = Ride::findOne ( $id );
				if (! empty ( $model )) {
					$review = new Review ();
					
					if ($review->load ( $param )) {
						$review->comment = $param ['Review'] ['comment'];
						$review->rate = $param ['Review'] ['rate'];
						$review->driver_id = $model->driver_id;
						$review->ride_id = $id;
						
						if ($review->save ()) {
							$avgRating = Review::find ()->where ( [ 
									'driver_id' => $model->driver_id 
							] )->average ( 'rate' );
							$formatedRating = sprintf ( '%.1f', $avgRating );
							$user = User::find ()->where ( [ 
									'id' => $model->driver_id 
							] )->one ();
							
							if (! empty ( $user )) {
								$user->avg_rating = $formatedRating;
								
								if ($user->save ()) {
									$valid = true;
								} else {
									throw new \Exception ( $user->getErrorsString () );
								}
							} else {
							    $response ['error'] = \Yii::t('app','Unable to find driver' );
							}
						} else {
							$response ['error'] = \Yii::t ( 'app', 'Unable To Save Review ' );
						}
					} else {
						throw new \Exception ( $review->getErrorsString () );
					}
				} else {
					$response ['error'] = \Yii::t ( 'app', 'Ride Not Found' );
				}
				if ($valid == true) {
					$transaction->commit ();
					$response ['status'] = 'OK';
					$response ['list'] = $review->asJson ();
				}
			} catch ( \Exception $e ) {
				$transaction->rollBack ();
				$response ['error'] = $e->getMessage ();
			}
		}
		return $response;
	}
	public function actionGet($driver_id, $page = 0) {
		$response = $this->getResponse ();
		
		$model = Review::find ()->where ( [ 
				'driver_id' => $driver_id 
		] );
		$user = User::findOne ( $driver_id );
		$avgRating = $user->avg_rating;
		
		$dataProvider = new \yii\data\ActiveDataProvider ( [ 
				'query' => $model,
				'pagination' => [ 
						'pageSize' => '20',
						'page' => $page 
				],
				
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		if (count ( $dataProvider->models ) > 0) {
			foreach ( $dataProvider->models as $model ) {
				
				$response ['list'] [] = $model->asJson ( true );
			}
			$response ['status'] = 'OK';
			$response ['avg_rating'] = $avgRating;
			$response ['pageCount'] = isset ( $page ) ? $page : '0';
			$response ['pageSize'] = $dataProvider->pagination->pageSize;
			$response ['totalPage'] = isset ( $dataProvider->pagination->pageCount ) ? $dataProvider->pagination->pageCount : '0';
		} else {
			$response ['error'] = \yii::t ( 'app', 'No Reviews Found' );
		}
		
		return $response;
	}
	
	/**
	 * Creates a new Review model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	
	/**
	 * Updates an existing Review model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$data = [ ];
		$model = $this->findModel ( $id );
		if ($model->load ( Yii::$app->request->post () )) {
			
			if ($model->save ()) {
				
				$data ['status'] = self::API_OK;
				
				$data ['detail'] = $model;
			} else {
				$data ['error'] = $model->flattenErrors;
			}
		} else {
		    $data ['error_post'] = \Yii::t('app','No Data Posted' );
		}
		
		return $this->sendResponse ( $data );
	}
	
	/**
	 * Deletes an existing Review model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		return $this->txDelete ( $id, "app\models\Review" );
	}
}
