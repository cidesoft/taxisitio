<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api\controllers;

use app\models\Transaction;
use app\models\UserWallet;
use app\models\WalletHistory;
use app\modules\api\controllers\ApiTxController;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * UserWalletController implements the API actions for UserWallet model.
 */
class UserWalletController extends ApiTxController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'ruleConfig' => [ 
								'class' => AccessRule::className () 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'add',
												'get',
												'update',
												'delete' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								],
								[ 
										'actions' => [ 
												'index',
												'get',
												'update' 
										],
										'allow' => true,
										'roles' => [ 
												'?',
												'*' 
										] 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all UserWallet models.
	 *
	 * @return mixed
	 */
	/*
	 * public function actionIndex()
	 * {
	 *
	 *
	 * }
	 */
	
	/**
	 * Displays a single app\models\UserWallet model.
	 *
	 * @return mixed
	 */
	public function actionGet($id) {
		$response = $this->getResponse ();
		$model = UserWallet::find ()->where ( [ 
				'created_by_id' => $id 
		] )->one ();
		if (! empty ( $model )) {
			
			$response ['status'] = 'OK';
			$response ['list'] = $model->asJson ();
		} else {
			$response ['error'] = \yii::t ( 'app', 'Wallet not found' );
		}
		return $response;
	}
	
	/**
	 * Creates a new UserWallet model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	
	/**
	 * Updates an existing UserWallet model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionAdd() {
		$response = $this->getResponse ();
		$userId = \yii::$app->user->id;
		$param = \yii::$app->request->bodyParams;
		$valid = false;
		$db = \Yii::$app->db;
		$model = UserWallet::find ()->where ( [ 
				'created_by_id' => $userId 
		] )->one ();
		if (empty ( $model ) && empty ( $param )) {
			return $response;
		}
		
		// Transaction begin
		$transaction = $db->beginTransaction ();
		try {
			if ($model->load ( Yii::$app->request->post () )) {
				$wallet_history = new WalletHistory ();
				$amount = $model->amount + $param ['UserWallet'] ['transaction-amount'];
				
				$wallet_history->wallet_id = $model->id;
				$wallet_history->initial_amount = $model->amount;
				$wallet_history->transaction_amount = $param ['UserWallet'] ['transaction-amount'];
				$wallet_history->total_balance = $amount;
				$wallet_history->transaction_id = $param ['UserWallet'] ['transaction-id'];
				$wallet_history->type_id = WalletHistory::TYPE_ADD;
				if (! $wallet_history->save ()) {
					throw new \Exception ( $wallet_history->getErrorsString () );
				}
				
				$transactionAdd = new Transaction ();
				$transactionAdd->wallet_history_id = $wallet_history->id;
				$transactionAdd->amount = $param ['UserWallet'] ['transaction-amount'];
				$transactionAdd->transaction_id = $param ['UserWallet'] ['transaction-id'];
				$transactionAdd->type_id = Transaction::TYPE_WALLET;
				if (! $transactionAdd->save ()) {
					throw new \Exception ( $transactionAdd->getErrorsString () );
				}
				
				$model->amount = $amount;
				
				if ($model->save ()) {
					$flag = true;
				} else {
					$response ['status'] = 'NOK';
					$response ['error'] = \yii::t ( 'app', 'No Data Posted ' );
				}
				
				if ($flag == true) {
					$transaction->commit ();
					$response ['status'] = 'OK';
					$response ['detail'] = $model->asJson ();
				}
			}
		} catch ( \Exception $e ) {
			$transaction->rollBack ();
			$response ['error'] = $e->getMessage ();
		}
		
		return $response;
	}
	
	/**
	 * Deletes an existing UserWallet model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		return $this->txDelete ( $id, "app\models\UserWallet" );
	}
}
