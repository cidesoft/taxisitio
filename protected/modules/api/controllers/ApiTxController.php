<?php

namespace app\modules\api\controllers;

use app\components\TController;
use Yii;
use yii\web\HttpException;

abstract class ApiTxController extends TController
{

    public $enableCsrfValidation = false;

    public function getResponse()
    {
        $response = array(
            'controller' => $this->id,
            'action' => $this->action->id,
            'status' => 'NOK',
            'datecheck' => defined('DATECHECK') ? DATECHECK : null,
            'maintainence' => defined('MAINTANANCE') ? "The project is being upgraded now, please wait" : null,
        );
        return $response;
    }

    public function txDelete($id, $modelClass)
    {
        $response = $this->getResponse();
        $model = $modelClass::findOne($id);

        if (!($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

        if ($model != null) {
            if ($model->delete()) {
                $response ['status'] = 'OK';
                $response ['msg'] = $modelClass . ' is deleted Successfully.';
            }
        }
        return $response;
    }

    public function txSave($modelClass)
    {
        $response = $this->getResponse();
        $model = new $modelClass ();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                $response ['status'] = 'OK';
                $response ['detail'] = $model;
            } else {
                $err = '';
                foreach ($model->getErrors() as $error) {
                    $err .= implode(',', $error);
                }
                $response ['error'] = $err;
            }
        }
        return $response;
    }

    public function sendResponse($data = null)
    {
        if ($data != null)
            $this->setResponse($data);

        return $this->response;
    }

    public function txGet($id, $modelClass)
    {
        $response = $this->getResponse();
        $model = $modelClass::findOne($id);

        if (!($model->isAllowed()))
            throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

        if ($model != null) {
            $response ['detail'] = $model;

            $response ['status'] = 'OK';
        }
        return $response;
    }

    public function txIndex($model_name)
    {
        $response = $this->getResponse();
        $model = new $model_name ();
        $dataProvider = $model->search(\Yii::$app->request->queryParams);
        $response ['list'] = $dataProvider->getModels();

        $response ['status'] = 'OK';

        return $response;
    }

}
