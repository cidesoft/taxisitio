<?php

namespace app\modules\api\controllers;

use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\search\Company;

/**
 * CompanyController implements the API actions for Company model.
 *
 * @author Carlos Mendoza <inhack20@gmail.com>
 */
class CompanyController extends ApiTxController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }
    
    /**
     * Lists all Company models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->txIndex(Company::class);
    }
}
