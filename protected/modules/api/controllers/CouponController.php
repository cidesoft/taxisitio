<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api\controllers;

use app\models\Coupon;
use app\models\CouponApplied;
use app\models\CouponSent;
use app\modules\api\controllers\ApiTxController;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * CouponController implements the API actions for Coupon model.
 */
class CouponController extends ApiTxController {
	public function behaviors() {
		return [ 
				'access' => [ 
						'class' => AccessControl::className (),
						'ruleConfig' => [ 
								'class' => AccessRule::className () 
						],
						'rules' => [ 
								[ 
										'actions' => [ 
												'index',
												'apply' 
										],
										'allow' => true,
										'roles' => [ 
												'@' 
										] 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Coupon models.
	 *
	 * @return mixed
	 */
	public function actionIndex($page = null) {
		$response = $this->getResponse ();
		$user = \Yii::$app->user->id;
		
		$applied = CouponApplied::find ()->where ( [ 
				'created_by_id' => \Yii::$app->user->id 
		] )->one ();
		
		if (empty ( $applied )) {
			$model = CouponSent::find ()->where ( [ 
					'user_id' => $user,
					'state_id' => CouponSent::COUPON_NOT_APPLIED 
			] )->andWhere ( [ 
					'>',
					'end_date',
					date ( "Y-m-d H:i:s" ) 
			] );
			
			$dataProvider = new \yii\data\ActiveDataProvider ( [ 
					'query' => $model,
					'pagination' => [ 
							'pageSize' => '20',
							'page' => $page 
					],
					
					'sort' => [ 
							'defaultOrder' => [ 
									'id' => SORT_DESC 
							] 
					] 
			] );
			if (count ( $dataProvider->models ) > 0) {
				foreach ( $dataProvider->models as $model ) {
					
					$response ['list'] [] = $model->asJson ();
				}
				$response ['status'] = 'OK';
				$response ['pageCount'] = isset ( $page ) ? $page : '0';
				$response ['pageSize'] = $dataProvider->pagination->pageSize;
				$response ['totalPage'] = isset ( $dataProvider->pagination->pageCount ) ? $dataProvider->pagination->pageCount : '0';
			} else {
				$response ['error'] = \yii::t ( 'app', 'No Coupons Found' );
			}
		} else {
			$response ['error'] = \Yii::t ( 'app', 'Coupon Already Applied' );
			$response ['list'] [] = $applied->asJson ();
		}
		
		return $response;
	}
	public function actionApply($id) {
		$response = $this->getResponse ();
		
		$couponSent = CouponSent::find ()->where ( [ 
				'id' => $id 
		] )->one ();
		if (! empty ( $couponSent )) {
		
			if ($couponSent->start_date < (string)new \app\services\util\MyDateTime()){
				$coupon = Coupon::find ()->where ( [
						'id' => $couponSent->coupon_id
				] )->one ();
				$couponApply = new CouponApplied ();
				$couponApply->coupon_id = $couponSent->coupon_id;
				$couponApply->coupon_sent_id = $couponSent->id;
				$couponApply->coupon_code = $couponSent->coupon_code;
				$couponApply->discount = $couponSent->discount;
				if (! empty ( $coupon ))
					$couponApply->max_amount = $coupon->max_amount;
					$couponApply->start_date = $couponSent->start_date;
					$couponApply->end_date = $couponSent->end_date;
					$couponApply->ride_count = $couponSent->ride_count;
					$couponApply->created_by_id = \Yii::$app->user->id;
					if ($couponApply->save ()) {
						$couponSent->state_id = CouponSent::COUPON_APPLIED;
						if ($couponSent->save ()) {
							$response ['status'] = 'OK';
							$response ['detail'] = $couponApply->asJson ();
						} else {
							$response ['error'] = \Yii::t ( 'app', 'Coupon Cannot Be Applied' );
						}
					} else {
						$response ['error'] = \Yii::t ( 'app', 'Unable To Apply Coupon' )/* $couponApply->getErrors() */;
					}
			}else {
				$response['error'] = \Yii::t('app', 'Cannot Apply Coupon Before Timeline');
			}
			
		} else {
			$response ['error'] = \Yii::t ( 'app', 'Coupon Code Not Found' );
		}
		
		return $response;
	}
}
