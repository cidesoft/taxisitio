<?php
namespace app\modules\api\controllers;

use app\models\Driver;
use app\models\ImageProof;
use app\models\Review;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * DriverController implements the API actions for Driver model.
 */
class DriverController extends ApiTxController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            
                            'profile',
                            'vehicle-update',
                            'review'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionProfile()
    {
        $response = $this->getResponse();
        $model = \yii::$app->user->Identity;
        if (! empty($model)) {
            $response['status'] = "OK";
            $response['detail'] = $model->asJson();
        } else {
            $response['error'] = \Yii::t('app', 'Not able to find your profile, please contact the administrator');
        }
        return $response;
    }

    public function actionVehicleUpdate()
    {
        $response = $this->getResponse();
        $valid = false;
        $imageProof = ImageProof::find()->where([
            'create_user_id' => \Yii::$app->user->id
        ])->one();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = Driver::find()->where([
                'create_user_id' => \Yii::$app->user->id
            ])->one();
            
            $model->scenario = "update";
            
            if ($model->load(Yii::$app->request->post())) {
                
                if ($model->save()) {
                    
                    if ($imageProof) {
                        
                        if (isset($_FILES['ImageProof'])) {
                            
                            $image = $imageProof->saveUploadedFile($imageProof, 'id_proof_file');
                            $imageProof->saveUploadedFile($imageProof, 'license_file');
                            $imageProof->saveUploadedFile($imageProof, 'document_file');
                            $imageProof->saveUploadedFile($imageProof, 'vehicle_image');
                            
                            if ($imageProof->save()) {
                                $transaction->commit();
                                $response['status'] = 'OK';
                                $response['detail'] = \Yii::$app->user->identity->asJson(true, true);
                            } else {
                                $err = '';
                                foreach ($imageProof->getErrors() as $error) {
                                    $err .= implode(',', $error);
                                }
                                $response['error'] = $err;
                            }
                        } else {
                            $transaction->commit();
                            $response['status'] = 'OK';
                            $response['detail'] = \Yii::$app->user->identity->asJson(true, true);
                        }
                    }
                } else {
                    $err = '';
                    foreach ($model->getErrors() as $error) {
                        $err .= implode(',', $error);
                    }
                    $response['error'] = $err;
                }
            } else {
                
                $response['error'] = \Yii::t('app', 'No Data Posted');
            }
        } catch (\Exception $e) {
            // $transaction->rollBack ();
            $response['error'] = $e;
            \yii::warning($e);
        }
        
        return $response;
    }

    public function actionReview()
    {
        $model = new Review();
        if ($model->load(\yii::$app->request->post())) {
            if ($model->save()) {
                $response['status'] = 'OK';
            } else {
                $response['error'] = $model->getErrorsString();
            }
        }
        return $response;
    }
}
