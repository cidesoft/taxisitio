<?php
namespace app\modules\api\controllers;

use app\components\TController;
use Yii\web\Response;
use yii\filters\AccessControl;

/**
 * Default controller for the `Api` module
 */
class DefaultController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index'
                        ],
                        'allow' => true,
                         //'ips' => ['192.168.2.*', '127.0.0.1'],
                    ]
                ]
            
            ]
        ];
    }

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
        \Yii::$app->response->format = Response::FORMAT_HTML;
        
        $filelist = $this->getTestFiles();
        $test_file_array = array();
        foreach ($filelist as $file) {
            $test_file_array[] = require ($file);
        }
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $test_file_array
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function getTestFiles()
    {
        $this->layout = '/../layouts/guest-main';
        $path = __DIR__ . '/../test/*';
        $filelist = glob($path . "*");
        return $filelist;
    }
}
