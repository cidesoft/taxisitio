<?php
namespace app\modules\api\controllers;

use app\models\CarPrice;
use app\models\CouponApplied;
use app\models\Notification;
use app\models\Ride;
use app\models\Transaction;
use app\models\User;
use app\models\UserWallet;
use app\models\WalletHistory;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\helpers\VarDumper;

/**
 * RideController implements the API actions for Ride model.
 */
class RideController extends ApiTxController
{
    //Llave temporal de ludwinds
    const GOOGLE_KEY = "AIzaSyB5rjUG2LjS61zbe7e5FBmyGCi_cwgnCLM";
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'get-estimate',
                            'get-drivers',
                            'started',
                            'canceled',
                            'rejected',
                            'accept',
                            'car-type',
                            'request',
                            'arrived',
                            'completed',
                            'detail',
                            'paid',
                            'wallet-pay',
                            'user-ride-history',
                            'driver-ride-history',
                            'cash-pay',
                            'paypal-pay',
                            'update'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [
                            
                            'car-type'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionCarType()
    {
        $response = $this->getResponse();
        $param = \yii::$app->request->bodyParams;
        $lat = $long = 0.0;
        if (! empty($param['lat']) && ! empty($param['long'])) {
            $lat = $param['lat'];
            $long = $param['long'];
        }
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($long) . '&sensor=false&key='.self::GOOGLE_KEY;
        $json = @file_get_contents($url);
        $data = json_decode($json);
        if (count($data->results) > 0) {
            if ($data->results[0]->address_components) {
                for ($j = 0; $j < count($data->results[0]->address_components); $j ++) {
                    $cn = array(
                        $data->results[0]->address_components[$j]->types[0]
                    );
                    if (in_array("country", $cn)) {
                        $short_name = $data->results[0]->address_components[$j]->short_name;
                    }
                }
            }
            if (! empty($short_name)) {
                $models = CarPrice::find()->where([
                    'like',
                    'service_area',
                    $short_name
                ])->all();
            }
            if ($models) {
                $coupon = CouponApplied::find()->where([
                    'created_by_id' => \Yii::$app->user->id
                ])->one();
                $list = [];
                foreach ($models as $model) {
                    $list[] = $model->asjson();
                }
                if (! empty($coupon)) {
                    $response['discount'] = $coupon->discount;
                    $response['max_amount'] = $coupon->max_amount;
                }
                $response['coupon'] = $coupon ? true : false;
                $response['status'] = 'OK';
                $response['region'] = $short_name;
                $response['list'] = $list;
            }
        }
        return $response;
    }

    public function actionGetDrivers($type_id = null)
    {
        $response = $this->getResponse();
        if (! empty($_POST['lat']) && ! empty($_POST['long'])) {
            $errorMessage = "";
            if(isset($_POST["show_all_message"])){
                $errorMessage = "No drivers found near your location";
            }
            $latitude = $_POST['lat'];
            $longtitude = $_POST['long'];
            $now = new \app\services\util\MyDateTime();
            $now->modify('-1 mins');
            $users = User::find()->select("u.*,	( 6371 * acos( cos( radians({$latitude}) ) * cos( radians( `lat` ) ) * cos( radians( `long` ) - radians({$longtitude}) ) + sin( radians({$latitude}) ) * sin( radians( `lat` ) ) ) ) AS distance")
                ->having("distance <:distance")
                ->addParams([
                ':distance' => Ride::RANGE_DISTANCE
            ])
                ->alias('u')
                ->joinWith('driver as d')
                ->andWhere([
                'u.role_id' => User::ROLE_DRIVER,
                'u.is_online' => User::IS_ONLINE,
                'u.is_available' => User::IS_AVAILABLE,
                'u.state_id' => User::STATE_ACTIVE
            ])
            ->andWhere("u.last_location_time >= '".$now."'")
                    ;
            if ($type_id !== null) {
                $users = $users->andWhere([
                    'd.vehicle' => $type_id
                ]);
            }
            $companies = [];
            if(isset($_POST["companies"]) 
                    && is_array($_POST["companies"])){
                $users->andWhere([
                    "d.company_id" => $_POST["companies"]
                ]);
                $errorMessage = "no_units_found_selected_companies";
            }
            
            $closestDriver = $users->orderBy('distance ASC')->one();
            $users = $users->orderBy('distance ASC');
            $users = $users->all();
            if (! empty($users) && ! empty($closestDriver)) {
                $response['closestdriver'] = $closestDriver->distance($closestDriver->lat, $closestDriver->long, $latitude, $longtitude);
                $list = [];
                foreach ($users as $user) {
                    $list[] = $user->asJson();
                }
                $response['status'] = 'OK';
                $response['list'] = $list;
            } else {
                $response['error'] = \Yii::t('app', $errorMessage);
            }
        } else {
            $response['error'] = \Yii::t('app', 'Unable to fetch location');
        }
        return $response;
    }
    /**
     * Solicita un servicio de taxi
     * @return type
     */
    public function actionRequest()
    {
        $response = $this->getResponse();
        $param = \yii::$app->request->bodyParams;
        $rideData = Yii::$app->request->post("Ride");
        $companies = [];
        if(isset($rideData["companies"]) 
                && is_array($rideData["companies"])){
            $companies = \app\models\Company::find()->andWhere([
                "id" => $rideData["companies"],
            ])->all();
        }
       
        
        $model = new Ride();
        if ($model->load(Yii::$app->request->post())) {
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $model->location_lat . ',' . $model->location_long . '&sensor=false&key='.self::GOOGLE_KEY;
            $json = @file_get_contents($url);
            $data = json_decode($json);
            if (count($data->results) > 0) {
                if ($data->results[0]->address_components) {
                    for ($j = 0; $j < count($data->results[0]->address_components); $j ++) {
                        $cn = array(
                            $data->results[0]->address_components[$j]->types[0]
                        );
                        if (in_array("country", $cn)) {
                            $short_name = $data->results[0]->address_components[$j]->short_name;
                        }
                    }
                }
            }
            if (! empty($short_name)) {
                $model->country_code = $short_name;
                $model->state_id = Ride::STATE_NEW;
                
                $appliedCoupon = CouponApplied::find()->where([
                    'created_by_id' => \Yii::$app->user->id
                ])->one();
                
                if ($model->save()) {
                    foreach ($companies as $company) {
                        $model->link("companies", $company);
                    }
                    if ($model->journey_type == Ride::RIDE_NOW) {
                        $user_id = \Yii::$app->user->id;
                        $model->saveNotification($user_id);
                        if (! empty($appliedCoupon)) {
                            
                            $ride = $appliedCoupon->ride_taken + 1;
                            $appliedCoupon->ride_taken = $ride;
                            if (! $appliedCoupon->save()) {
                                $response['error'] = $appliedCoupon->getErrors();
                                return $response;
                            }else {
                                $model->coupon_id = $appliedCoupon->coupon_id;
                                $model->save(false);
                            }
                        }
                        
                        $response['status'] = 'OK';
                        $response['detail'] = $model->asJson(false, false, $passenger_detail = true);
                    } else if ($model->journey_type == Ride::RIDE_LATER) {
                        $response['status'] = 'OK';
                        $response['detail'] = \Yii::t('app', 'Your Ride Has Been Scheduled As Per Your Request');
                    } else {
                        $response['error'] = \Yii::t('app', 'Please Select Correct Journey Type');
                    }
                } else {
                    $response['error'] = $model->getErrorsString();
                }
                
                
                
                
            }
        }
        return $response;
    }

    public function actionUpdate($id)
    {
        $response = $this->getResponse();
        $params = Yii::$app->request->bodyParams;
        $currentLat = $params['Ride']['updated_lat'];
        $currentLong = $params['Ride']['updated_long'];
        $destinationLat = $params['Ride']['destination_lat'];
        $destinationLong = $params['Ride']['destination_long'];
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            
            if ($model->state_id < Ride::STATE_COMPLETED) {
                if ($model->load($params)) {
                    $model->is_updated = Ride::RIDE_UPDATED;
                    $amount = $model->getUpdatedAmount($currentLat, $currentLong, $destinationLat, $destinationLong);
                    if (isset($amount)) {
                        $model->amount = $amount;
                        
                        \Yii::error(VarDumper::dumpAsString($model->amount. "MODEL_FINAL_AMOUNT"));
                    }
                    
                    if ($model->save()) {
                        $user_id = \Yii::$app->user->id;
                        if ($model->driver_id) {
                            $model->saveUpdateNotification($user_id, $model->driver_id);
                        } else {
                            $model->saveNotification($user_id);
                        }
                        $response['status'] = 'OK';
                        $response['detail'] = $model->asJson(false, true, $passenger_detail = false);
                    } else {
                        $response['error'] = $model->getErrorsString();
                    }
                    /*
                     * }else {
                     * $response ['error'] = Yii::t ( 'app', 'Short Code' );
                     * }
                     */
                } else {
                    $response['error'] = $model->getErrors();
                }
            } else {
                $response['error'] = Yii::t('app', 'Ride Cannot Be Updated Now');
            }
        } else {
            $response['error'] = Yii::t('app', 'No Ride Found');
        }
        return $response;
    }

    public function actionDetail($id, $flag = false)
    {
        \Yii::error(VarDumper::dumpAsString('RIDE_DETAIL_DUMP'));
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            // $model->getEstimatedAmount ();
            
            if ($flag == true) {
                if ($user->role_id == User::ROLE_DRIVER) {
                    $response['detail'] = $this->asJson(false, false, true, false);
                }
                if ($user->role_id == User::ROLE_PASSENGER) {
                    $response['detail'] = $this->asJson(false, true, false);
                }
            } else {
                $response['detail'] = $model->asJson(true, true, true);
                $response['status'] = 'OK';
            }
        } else {
            $rsponse['error'] = \Yii::t('app', 'No record found');
        }
        
        return $response;
    }

    public function actionUserRideHistory($page = 0)
    {
        $response = $this->getResponse();
        $user_id = \Yii::$app->user->id;
        $model = Ride::find()->where([
            'create_user_id' => $user_id
        ]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => '20',
                'page' => $page
            ],
            
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if (count($dataProvider->models) > 0) {
            foreach ($dataProvider->models as $model) {
                
                $response['list'][] = $model->asJson(false, true, false);
            }
            $response['status'] = 'OK';
            $response['pageCount'] = isset($page) ? $page : '0';
            $response['pageSize'] = $dataProvider->pagination->pageSize;
            $response['totalPage'] = isset($dataProvider->pagination->pageCount) ? $dataProvider->pagination->pageCount : '0';
        } else {
            $response['error'] = \Yii::t('app', 'No Rides found');
        }
        return $response;
    }

    public function actionDriverRideHistory($page = 0)
    {
        $response = $this->getResponse();
        $user_id = \Yii::$app->user->id;
        $model = Ride::find()->where([
            'driver_id' => $user_id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => '20',
                'page' => $page
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if (count($dataProvider->models) > 0) {
            foreach ($dataProvider->models as $model) {
                
                $response['list'][] = $model->asJson(false, false, true);
            }
            $response['status'] = 'OK';
            $response['pageCount'] = isset($page) ? $page : '0';
            $response['pageSize'] = $dataProvider->pagination->pageSize;
            $response['totalPage'] = isset($dataProvider->pagination->pageCount) ? $dataProvider->pagination->pageCount : '0';
        } else {
            $response['error'] = \yii::t('app', 'No Rides found');
        }
        return $response;
    }

    public function actionRejected($id)
    {
        $response = $this->getResponse();
        $model = Ride::findOne($id);
        if (! empty($model)) {
            if ($model->state_id == Ride::STATE_NEW) {
                $response['status'] = 'OK';
                $response['success'] = \Yii::t('app', 'Ride rejected successfully.');
            } else {
                $response['error'] = \Yii::t('app', 'Ride state is changed');
            }
        } else {
            $response['error'] = \Yii::t('app', 'No record found');
        }
        return $response;
    }

    public function actionCanceled($id)
    {
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])
            ->andWhere([
            'IN',
            'state_id',
            [
                RIDE::STATE_ACCEPTED,
                RIDE::STATE_DRIVER_ARRIVED,
                Ride::STATE_NEW,
                Ride::STATE_CANCELLED
            ]
        ])
            ->one();
        if (empty($model)) {
            $response['error'] = \Yii::t('app', 'You cannot cancel this ride now.');
            return $response;
        }
        
        $model->state_id = Ride::STATE_CANCELLED;
        if ($model->save()) {
            if ($model->driver_id != null) {
                $user = User::findOne($model->driver_id);
                if (! empty($user)) {
                    $user->is_available = User::IS_AVAILABLE;
                    if ($user->save()) {
                        $notification = new Notification();
                        $notification->user_id = $model->driver_id;
                        $notification->message = Yii::t('app','Ride Cancelled');
                        $notification->model_id = $model->id;
                        $notification->model_type = Ride::className();
                        $notification->type_id = $model->journey_type;
                        $notification->created_by_id = \Yii::$app->user->id;
                        if ($notification->save()) {
                            $model->requestNotification($model->driver_id, $notification->message);
                        }
                    } else {
                        $response['error'] = \Yii::t('app', 'You cannot cancel this ride now.');
                    }
                }
            }
            $response['status'] = 'OK';
            $response['message'] = \Yii::t('app', 'Ride is successfully canceled');
            $response['detail'] = $model->asJson(false, true, true);
        }
        return $response;
    }

    public function actionCompleted($id)
    {
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            $model->state_id = Ride::STATE_COMPLETED;
            $model->end_time = date('Y-m-d H:i');
            $user = \yii::$app->user->Identity;
            
            if ($model->country_code !== $user->country->country_code) {
                $transaction_amount = $model->amount;
                
                $from_currency = $model->getCurrency($user->country->country_code);
                $to_currency = $model->getCurrency($model->country_code);
                if ($to_currency != 'USD') {
                    $amount = Ride::currencyConverter($from_currency, 'USD', $transaction_amount);
                } else {
                    $amount = $model->amount;
                }
            } else {
                $amount = $model->amount;
            }
            $model->amount = $amount;
            if ($model->save()) {
                $user->is_available = User::IS_AVAILABLE;
                if ($user->save()) {
                    $customer = User::find()->where([
                        'id' => $model->create_user_id
                    ])->one();
                    if (! empty($customer)) {
                        $customer->ride_count = $customer->ride_count + 1;
                        $customer->save();
                    }
                    $notification = new Notification();
                    $notification->user_id = $model->create_user_id;
                    $notification->message = Yii::t('app','Ride Completed');
                    $notification->model_id = $model->id;
                    $notification->model_type = Ride::className();
                    $notification->type_id = $model->journey_type;
                    $notification->created_by_id = \Yii::$app->user->id;
                    if ($notification->save()) {
                        $model->requestNotification($model->create_user_id, $notification->message);
                    }
                    $response['status'] = 'OK';
                    $response['detail'] = $model->asJson(false, true, true);
                }
            }
        } else {
            $response['error'] = \Yii::t('app', 'No Record found');
        }
        return $response;
    }

    public function actionArrived($id)
    {
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            if ($model->driver_id == \Yii::$app->user->id) {
                if ($model->state_id != Ride::STATE_CANCELLED) {
                    $model->state_id = Ride::STATE_DRIVER_ARRIVED;
                    $model->arrived_time = date('Y-m-d H:i');
                    if ($model->save()) {
                        $notification = new Notification();
                        $notification->user_id = $model->create_user_id;
                        $notification->message = Yii::t('app','Driver Arrived');
                        $notification->model_id = $model->id;
                        $notification->model_type = Ride::className();
                        $notification->type_id = $model->journey_type;
                        $notification->created_by_id = \Yii::$app->user->id;
                        try {
                            $model->requestNotification($model->create_user_id, $notification->message);
                        } catch (\Exception $e) {
                            \Yii::error('DRIVER ARRIVE ERROR NOTIFICATION');
                            \Yii::error(\yii\helpers\VarDumper::dumpAsString($e->getMessage()));
                        }
                        $notification->save();
                        $response['status'] = 'OK';
                        $response['detail'] = $model->asJson(false, true, true);
                    }
                } else {
                    $response['error'] = \Yii::t('app', 'Ride is cancelled now');
                }
            } else {
                $response['error'] = \Yii::t('app', 'Sorry!! You Cannot Change Continue With This Ride.');
            }
        } else {
            $response['error'] = \Yii::t('app', 'No Record found');
        }
        return $response;
    }

    public function actionStarted($id)
    {
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            if ($model->driver_id == \Yii::$app->user->id) {
                $model->state_id = Ride::STATE_STARTED;
                $model->start_time = date('Y-m-d H:i');
                if ($model->save()) {
                    $notification = new Notification();
                    $notification->user_id = $model->create_user_id;
                    $notification->message = Yii::t("app",'Ride Started');
                    $notification->model_id = $model->id;
                    $notification->model_type = Ride::className();
                    $notification->type_id = $model->journey_type;
                    $notification->created_by_id = \Yii::$app->user->id;
                    if ($notification->save()) {
                        $model->requestNotification($model->create_user_id, $notification->message);
                    }
                    $response['status'] = 'OK';
                    $response['detail'] = $model->asJson(false, true, true);
                }
            } else {
                $response['error'] = \Yii::t('app', 'You are not allowed to perform this action');
            }
        } else {
            
            $response['error'] = \Yii::t('app', 'No Record found');
        }
        return $response;
    }

    public function actionPaid($id)
    {
        $response = $this->getResponse();
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            $model->state_id = Ride::STATE_PAID;
            if ($model->save()) {
                $response['status'] = 'OK';
                $response['detail'] = $model->asJson(true, true, true);
            }
        } else {
            $response['error'] = \Yii::t('app', 'No Record found');
        }
        return $response;
    }

    public function actionCashPay($id)
    {
        $response = $this->getResponse();
        $transaction = new Transaction();
        $param = \yii::$app->request->bodyParams;
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if ($param['amount'] != '') {
            if (! empty($model)) {
                $transaction->amount = $param['amount'];
                $transaction->ride_id = $model->id;
                $transaction->type_id = Transaction::TYPE_CASH;
                if ($transaction->save()) {
                    $model->state_id = Ride::STATE_PAID;
                    if ($model->save()) {
                        $user = User::findOne($model->driver_id);
                        if (! empty($user)) {
                            $user->is_available = User::IS_AVAILABLE;
                            if (! $user->save()) {
                                $response['error'] = \Yii::t('app', 'Unable to save User Data');
                            }
                            $notification = new Notification();
                            $notification->user_id = $model->driver_id;
                            $notification->message = \Yii::t("app",'Ride paid');
                            $notification->model_id = $model->id;
                            $notification->model_type = Ride::className();
                            $notification->type_id = $model->journey_type;
                            $notification->created_by_id = \yii::$app->user->id;
                            if ($notification->save()) {
                                $model->requestNotification($model->driver_id, $notification->message);
                                $response['status'] = 'OK';
                                $response['detail'] = $model->asJson(true, true, true);
                            } else {
                                $response['error'] = $notification->getErrorsString();
                            }
                        }
                    } else {
                        $response['error'] = $model->getErrorsString();
                    }
                } else {
                    $response['error'] = $transaction->getErrorsString();
                }
            } else {
                $response['error'] = \Yii::t('app', 'No Record found');
            }
        } else {
            $response['error'] = \Yii::t('app', 'Invalid Amount');
        }
        
        return $response;
    }

    public function actionWalletPay($id)
    {
        $response = $this->getResponse();
        $valid = false;
        $db = \Yii::$app->db;
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        
        $transactionAdd = new Transaction();
        $param = \yii::$app->request->bodyParams;
        
        // Transaction begin
        $transaction = $db->beginTransaction();
        try {
            if (! empty($model)) {
                $driver = User::findOne($model->driver_id);
                $user = User::findOne($model->create_user_id);
                
                if (! empty($driver) && ! empty($user)) {
                    
                    /*
                     * if ($model->country_code !== $user->country->country_code) {
                     *
                     * $transaction_amount = $param['Ride']['amount'];
                     *
                     * $from_currency = $model->getCurrency($user->country->country_code);
                     *
                     * $to_currency = $model->getCurrency($model->country_code);
                     * $amount = Ride::currencyConverter($from_currency, $to_currency, $transaction_amount);
                     * } else {
                     */
                    $amount = $param['Ride']['amount'];
                    /* } */
                    
                    $userWallet = UserWallet::find()->where([
                        'created_by_id' => $user->id
                    ])->one();
                    
                    if (! empty($userWallet)) {
                        
                        $walletHistory = new WalletHistory();
                        $walletHistory->wallet_id = $userWallet->id;
                        $walletHistory->type_id = WalletHistory::TYPE_DEBIT;
                        $walletHistory->initial_amount = $userWallet->amount;
                        $walletHistory->transaction_amount = $amount;
                        $walletHistory->ride_id = $model->id;
                        $response['pending'] = false;
                        if ($userWallet->amount >= $amount) {
                            
                            $walletHistory->total_balance = $userWallet->amount - $amount;
                            $model->state_id = Ride::STATE_PAID;
                        } else {
                            
                            $walletHistory->total_balance = $userWallet->amount - $amount;
                            $leftAmount = $amount - $userWallet->amount;
                            $response['left_amount'] = $leftAmount;
                            $response['pending'] = true;
                        }
                        if ($walletHistory->save()) {
                            
                            if ($response['pending'] == true) {
                                
                                $userWallet->amount = $walletHistory->total_balance + $leftAmount;
                            } else {
                                
                                $userWallet->amount = $walletHistory->total_balance;
                            }
                            
                            if ($userWallet->save()) {
                                
                                $transactionAdd->amount = "$amount";
                                $transactionAdd->ride_id = $model->id;
                                $transactionAdd->wallet_history_id = $walletHistory->id;
                                $transactionAdd->type_id = Transaction::TYPE_WALLET;
                                if ($transactionAdd->save()) {
                                    
                                    if ($model->save()) {
                                        $valid = true;
                                        if ($response['pending'] == true) {
                                            
                                            $notification = new Notification();
                                            $notification->user_id = $model->driver_id;
                                            $notification->message = Yii::t("app","Pending amount").' ' . $leftAmount;
                                            $notification->model_id = $model->id;
                                            $notification->model_type = Ride::className();
                                            $notification->type_id = $model->journey_type;
                                            $notification->created_by_id = \Yii::$app->user->id;
                                            if ($notification->save()) {
                                                $model->requestNotification($model->driver_id, $notification->message);
                                            } else {
                                                $valid = false;
                                            }
                                        }
                                        $transaction->commit();
                                        $response['status'] = 'OK';
                                    } else {
                                        throw new \Exception($model->getErrorsString());
                                    }
                                } else {
                                    throw new \Exception($transactionAdd->getErrorsString());
                                }
                            } else {
                                throw new \Exception($userWallet->getErrorsString());
                            }
                        } else {
                            throw new \Exception($walletHistory->getErrorsString());
                        }
                    } else {
                        $response['error'] = \Yii::t('app', 'No wallet found');
                    }
                } else {
                    $response['error'] = \Yii::t('app', 'No Record found');
                }
            } else {
                throw new \Exception($model->getErrorsString());
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['error'] = $e->getMessage();
        }
        
        return $response;
    }

    public function actionPaypalPay($id)
    {
        $response = $this->getResponse();
        $transaction = new Transaction();
        $param = \yii::$app->request->bodyParams;
        $model = Ride::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model) && ! empty($param)) {
            $transaction->amount = $param['transaction']['amount'];
            $transaction->transaction_id = $param['transaction']['transaction_id'];
            $transaction->ride_id = $model->id;
            $transaction->type_id = Transaction::TYPE_PAYPAL;
            if ($transaction->save()) {
                $model->state_id = Ride::STATE_PAID;
                if ($model->save()) {
                    $user = User::findOne($model->driver_id);
                    if (! empty($user)) {
                        $user->is_available = User::IS_AVAILABLE;
                        if (! $user->save()) {
                            $response['error'] = \Yii::t('app', 'Unable to save User Data');
                        }
                        $notification = new Notification();
                        $notification->user_id = $model->driver_id;
                        $notification->message = Yii::t('app','Ride paid');
                        $notification->model_id = $model->id;
                        $notification->model_type = Ride::className();
                        $notification->type_id = $model->journey_type;
                        $notification->created_by_id = \yii::$app->user->id;
                        if ($notification->save()) {
                            $model->requestNotification($model->driver_id, $notification->message);
                            $response['status'] = 'OK';
                            $response['detail'] = $model->asJson(true, true, true);
                        } else {
                            $response['error'] = $notification->getErrorsString();
                        }
                    }
                } else {
                    $response['error'] = $model->getErrorsString();
                }
            } else {
                $response['error'] = $transaction->getErrorsString();
            }
        } else {
            $response['error'] = \Yii::t('app', 'No Record found');
        }
        return $response;
    }

    public function actionAccept($id)
    {
        $response = $this->getResponse();
        $userId = \yii::$app->user->id;
        $ride = Ride::findOne($id);
        $user = User::findOne($userId);
        if (! empty($ride)) {
            if ($ride->state_id > Ride::STATE_NEW) {
                $response['error'] = \Yii::t('app', 'Either the Ride Has Been Canceled or Accepted By Another Driver');
                return $response;
            }
            $ride->driver_id = $user->id;
            $ride->state_id = Ride::STATE_ACCEPTED;
            if ($ride->save()) {
                $user->is_available = User::NOT_AVAILABLE;
                if (! $user->save()) {
                    $response['error'] = \Yii::t('app', 'Unable to save User Data');
                }
                $notification = new Notification();
                $notification->user_id = $ride->create_user_id;
                $notification->message = Yii::t('app','Ride Accepted');
                $notification->model_id = $ride->id;
                $notification->model_type = Ride::className();
                $notification->type_id = $ride->journey_type;
                $notification->created_by_id = \Yii::$app->user->id;
                if ($ride->requestNotification($ride->create_user_id, $notification->message)) {
                    $notification->save();
                    $response['status'] = 'OK';
                    $response['message'] = \Yii::t('app', 'Ride accepted successfully.');
                    $response['detail'] = $ride->asJson(false, true, true);
                } else {
                    $response['error'] = $model->getErrorsString();
                }
            } else {
                $response['error'] = $model->getErrorsString();
            }
        } else {
            $response['error'] = \Yii::t('app', 'No ride Found');
        }
        return $response;
    }
}