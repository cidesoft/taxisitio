<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    ' Review Deleted Successfully.' => 'Calificación eliminada con éxito',
    ' accepted by ' => 'aceptado por',
    'About Me' => 'Acerca de mi',
    'Accepted' => 'Aceptado',
    'Activation Key' => 'Clave de activación',
    'Add' => 'Añadir',
    'Add ' => 'Añadir ',
    'Address' => 'Dirección',
    'Address Colonia' => 'Dirección Colonia',
    'Address Municipality' => 'Dirección Municipio',
    'Address Number' => 'Dirección Número',
    'Address Status' => 'Dirección Estado',
    'Admin Has Not Verified Your Account Yet' => 'El administrador aún no ha verificado su cuenta',
    'Age' => 'Edad',
    'Amount' => 'Cantidad',
    'Api' => 'Api',
    'Are you sure you want to delete this ?' => 'Estás seguro que quieres eliminar esto?',
    'Assets cleaned' => 'Activos limpiados',
    'Assigned Base' => 'Base asignada',
    'Assigned Driver' => 'Conductor asignado',
    'Attempts' => 'Intentos',
    'Auth Code' => 'Código de autenticación',
    'Auth Session updated' => 'Sesión de autenticación actualizada',
    'Auth code not found' => 'Código de autenticación no encontrado',
    'Average Rating' => 'Puntuación media',
    'Avg Rating' => 'Valoración media',
    'Backup' => 'Respaldo',
    'Backup File|Backup Files' => 'Archivo de respaldo | Archivos de respaldo',
    'Base Price' => 'Precio base',
    'Brand' => 'Marca',
    'Cancelled' => 'Cancelado',
    'Cannot Apply Coupon Before Timeline' => 'No se puede aplicar el cupón antes de la línea de tiempo',
    'Car Price' => 'Precio del coche',
    'Car Prices' => 'Precios de autos',
    'Car Requests' => 'Solicitudes de autos',
    'Car Type Deleted Successfully.' => 'Tipo de automóvil eliminado con éxito.',
    'Catalogue' => 'Catalogar',
    'Change Password' => 'Cambia la contraseña',
    'City' => 'Ciudad',
    'Clean Database' => 'Limpiar base de datos',
    'Comment' => 'Comentario',
    'Complains' => 'Quejas',
    'Completed' => 'Completado',
    'Confirm Password' => 'Confirmar contraseña',
    'Contact No' => 'Número de contacto',
    'Contact No or Country is Empty' => 'Número de contacto o el país está vacío',
    'Contact No.' => 'Número de contacto.',
    'Countries' => 'Países',
    'Country' => 'País',
    'Country Code' => 'Código de país',
    'Country Deleted Successfully.' => 'País eliminado exitosamente',
    'Country already added' => 'País ya agregado',
    'Coupon' => 'Cupón',
    'Coupon Already Applied' => 'Cupón ya aplicado',
    'Coupon Applieds' => 'Cupones Aplicados',
    'Coupon Cannot Be Applied' => 'El cupón no se puede aplicar',
    'Coupon Code' => 'Código promocional',
    'Coupon Code Not Found' => 'Código de cupón no encontrado',
    'Coupon Sent' => 'Cupón enviado',
    'Coupon Sents' => 'Cupones enviados',
    'Coupon Type' => 'Tipo de cupón',
    'Coupons' => 'Cupones',
    'Cp Address' => 'Dirección CP',
    'Create Backup' => 'Crear copia de seguridad',
    'Create Time' => 'Hora de creación',
    'Create Time (in UTC)' => 'Hora de creación (UTC)',
    'Create User' => 'Crear usuario',
    'Create User ID' => 'Crear usuario ID',
    'Created By' => 'Creado por',
    'Created By (Passenger)' => 'Creado por (pasajero)',
    'Created By ID' => 'Creado por ID',
    'Created For' => 'Creado para',
    'Created For (Driver)' => 'Creado para (Conductor)',
    'Created On' => 'Creado en',
    'Curp' => '',
    'Currency Code' => 'Código de moneda',
    'Currency Symbol' => 'Símbolo de moneda',
    'Current Password' => 'Contraseña actual',
    'Current password is incorrect' => 'La contraseña actual es incorrecta',
    'Customer' => 'Cliente',
    'Customers' => 'Clientes',
    'Dashboard' => 'Tablero',
    'Dashboard|Taxi-sitio' => 'Tablero|Taxi-sitio',
    'Data Not Posted' => 'Datos no publicados',
    'Date Added' => 'Fecha Agregada',
    'Date Of Birth' => 'Fecha de nacimiento',
    'Date Published' => 'Fecha de publicación',
    'Date Sent' => 'Fecha de envío',
    'Delegation / municipality' => 'Delegación / municipio',
    'Delete' => 'Borrar',
    'Delete this backup' => 'Eliminar esta copia de seguridad',
    'Description' => 'Descripción',
    'Destination' => 'Destino',
    'Destination Latitude' => 'Latitud de destino',
    'Destination Longitude' => 'Longitud de destino',
    'Device Token' => 'Token de dispositivo',
    'Discount' => 'Descuento',
    'Discount (in %)' => 'Descuento (en %)',
    'Document file not uploaded successfully.' => 'El archivo del documento no se cargó correctamente.',
    'Drival Arrival' => 'Llegada del conductor',
    'Driver' => 'Conductor',
    'Driver Arrived' => 'El conductor ha llegado',
    'Driver Catelogs' => 'Catálogos de conductores',
    'Driver not found' => 'Conductor no encontrado',
    'Drivers' => 'Conductores',
    'Economic No' => 'No económico',
    'Either the Ride Has Been Canceled or Accepted By Another Driver' => 'O el viaje ha sido cancelado o aceptado por otro conductor',
    'Email' => 'Email',
    'Email already exists' => 'El email ya existe',
    'Email is not registered' => 'El correo electrónico no está registrado',
    'End Date' => 'Fecha final',
    'End Time (in UTC)' => 'Hora de finalización (en UTC)',
    'Error' => 'Error',
    'Expiration Date' => 'Fecha de caducidad',
    'Feedback' => 'Realimentación',
    'Feedbacks' => 'Comentarios',
    'Fields with' => 'Campos con',
    'File not found' => 'Archivo no encontrado',
    'File|Files' => 'Archivo | Archivos',
    'First Name' => 'Primer nombre',
    'From Email' => 'Desde el e-mail',
    'Front Photo' => 'Foto frontal',
    'Full Name' => 'Nombre completo',
    'Gender' => 'Género',
    'Hard Working Time' => 'Tiempo de trabajo duro',
    'ID' => 'IDs',
    'Id Proof' => 'Prueba de Identificación',
    'Id proof file not uploaded successfully.' => 'El archivo de prueba de identificación no se cargó correctamente.',
    'Image' => 'Imagen',
    'Image File' => 'Archivo de imagen',
    'Incorrect Email' => 'Email incorrecto',
    'Incorrect Password' => 'Contraseña incorrecta',
    'Index' => 'Índice',
    'Ine' => 'Ine',
    'Initial Amount' => 'Cantidad inicial',
    'Insurance Carrier' => 'Compañía de seguros',
    'Invalid Amount' => 'Monto invalido',
    'Is Hourly' => 'Es cada hora',
    'Is Read' => 'Leído',
    'Is Updated' => 'Se actualiza',
    'Journey Time' => 'Tiempo de viaje',
    'Journey Type' => 'Tipo de viaje',
    'Last Action Time' => 'Último tiempo de acción',
    'Last Attempt' => 'Último intento',
    'Last Name' => 'Apellido',
    'Last Visit Time' => 'Hora de la última visita',
    'Lat' => 'Lat',
    'Lat long saved successfully' => 'Latitud guardada correctamente',
    'Latitude' => 'Latitud',
    'Left Photo' => 'Foto izquierda',
    'License File' => 'Archivo de licencia',
    'License No' => 'Número de licencia',
    'License file not uploaded successfully.' => 'El archivo de licencia no se cargó correctamente.',
    'List Backup' => 'Lista de copia de seguridad',
    'Location Latitude' => 'Ubicación Latitud',
    'Location Longitude' => 'Longitud de ubicación',
    'Login Provider' => 'Proveedor de inicio de sesión',
    'Login Provider Identifier' => 'Identificador del proveedor de inicio de sesión',
    'Login Successfully' => 'Inicie sesión correctamente',
    'Login To Continue' => 'Inicie sesión para continuar',
    'Long' => 'Long',
    'Longitude' => 'Longitud',
    'Low Date' => 'Baja fecha',
    'Manage' => 'Gestionar',
    'Marital Status' => 'Estado civil',
    'Maturity Concession' => 'Concesión de vencimientos',
    'Message' => 'Mensaje',
    'Model' => 'Modelo',
    'Model ID' => 'ID del modelo',
    'Model Type' => 'Tipo de modelo',
    'Mother Last Name' => 'Apellido de la madre',
    'Municipality Code' => 'Municipality Code',
    'NOK' => 'NOK',
    'New' => 'Nueva',
    'No Data Posted' => 'No hay datos publicados',
    'No Record found' => 'Ningún record fue encontrado',
    'No Ride Found' => 'No se encontró viaje',
    'No Rides found' => 'No se encontraron viajes',
    'No data posted' => 'No hay datos publicados.',
    'No drivers found near your location' => 'No se encontraron conductores cerca',
    'No record found' => 'Ningún registro fue encontrado',
    'No ride Found' => 'Ningún viaje encontrado',
    'No ride Found.' => 'Ningún viaje encontrado.',
    'No user found' => 'Usuario no encontrado',
    'No wallet found' => 'No se encontró billetera',
    'Not Defined' => 'No definida',
    'Not able to find your profile, please contact the administrator' => 'No puede encontrar su perfil, póngase en contacto con el administrador',
    'Notification Deleted Successfully.' => 'Notificación eliminada exitosamente.',
    'Notifications' => 'Notificaciones',
    'Number Concession' => 'Concesión de números',
    'Number Of Bags' => 'Número de bolsas',
    'Number Of Hours' => 'Número de horas',
    'Number Of Passengers' => 'Número de pasajeros',
    'Number Series' => 'Series de números',
    'Off' => 'Apagado',
    'Office Code' => 'Código de oficina',
    'On' => 'Encendido',
    'Only Allowed to Drivers.' => 'Solo permitido a los conductores.',
    'Open Ride Request' => 'Solicitud de viaje abierto',
    'Owner Address' => 'Dirección del propietario',
    'Owner Name' => 'Nombre del dueño',
    'Page Type already added.' => 'Tipo de página ya agregado',
    'Pages' => 'Páginas',
    'Paid' => 'Pagado',
    'Panic Request' => 'Solicitud de pánico',
    'Passenger' => 'Pasajero',
    'Passenger Arrival' => 'Llegada de pasajeros',
    'Passenger not found' => 'Pasajero no encontrado',
    'Password' => 'Contraseña',
    'Placas' => 'Placas',
    'Please Select Correct Journey Type' => 'Seleccione el tipo de viaje correcto',
    'Please check your email to reset your password' => 'Por favor revise su correo electrónico para restablecer su contraseña',
    'Please enter Email Address' => 'Por favor, introduzca la dirección de correo electrónico',
    'Price (Per Km)' => 'Precio (por km)',
    'Price (Per Min)' => 'Precio (por min)',
    'Rate' => 'Tarifa',
    'Rate Driver' => 'Tarifa del conductor',
    'Rear Photo' => 'Foto trasera',
    'Registration No' => 'Número de registro',
    'Rejected' => 'Rechazado',
    'Request Accepted Successfully' => 'Solicitud aceptada con éxito',
    'Request Id not posted' => 'ID de solicitud no publicado',
    'Request does not exists' => 'La solicitud no existe.',
    'Restore this backup' => 'Restaurar esta copia de seguridad',
    'Review' => 'Revisión',
    'Reviews' => 'Comentarios',
    'Rfc' => '',
    'Ride' => 'Viaje',
    'Ride Accepted' => 'Viaje aceptado',
    'Ride Cancelled' => 'Viaje cancelado',
    'Ride Cannot Be Updated Now' => 'El viaje no se puede actualizar ahora',
    'Ride Completed' => 'Viaje completado',
    'Ride Deleted Successfully.' => 'Viaje eliminado con éxito',
    'Ride Id' => 'ID de paseo',
    'Ride Later' => 'Paseo más tarde',
    'Ride Not Found' => 'Viaje no encontrado',
    'Ride Now' => 'Montar ahora',
    'Ride Rejected' => 'El viaje fue rechazado',
    'Ride Started' => 'El viaje comenzó',
    'Ride Type' => 'Tipo de paseo',
    'Ride Updated' => 'Viaje actualizado',
    'Ride With Pet' => 'Paseo con mascota',
    'Ride accepted successfully.' => 'Paseo aceptado con éxito.',
    'Ride already accepted.' => 'Paseo ya aceptado',
    'Ride from :' => 'Paseo desde',
    'Ride is cancelled now' => 'El viaje se cancela ahora',
    'Ride is successfully canceled' => 'El viaje se canceló con éxito',
    'Ride rejected successfully.' => 'Paseo rechazado con éxito',
    'Ride state is changed' => 'El estado de viaje ha cambiado',
    'Rides' => 'Viajes',
    'Right Photo' => 'Foto derecha',
    'Role ' => 'Papel',
    'Runtime cleaned' => 'Tiempo de ejecución limpiado',
    'Safety Request' => 'Solicitud de seguridad',
    'Save' => 'Guardar',
    'Secure Policy' => 'Política de seguridad',
    'Sepomexes' => '',
    'Session not found' => 'Sesión no encontrada',
    'Settlement' => 'Asentamiento',
    'Settlement Post' => 'Puesto de liquidación',
    'Share Request' => 'Compartir solicitud',
    'Signup Type' => 'Tipo de registro',
    'Sorry!! You Cannot Change Continue With This Ride.' => '¡¡Lo siento!! No puede cambiar este viaje.',
    'Source' => 'Fuente',
    'Start Date' => 'Fecha de inicio',
    'Start Time (in UTC)' => 'Hora de inicio (en UTC)',
    'Started' => 'Empezado',
    'State' => 'Estado',
    'State Code' => 'Código del estado',
    'State ID' => 'ID del estado',
    'Status' => 'Estado',
    'Street Address' => 'Dirección',
    'Subject' => 'Tema',
    'Submarca' => '',
    'Telephone Code' => 'Código de teléfono',
    'The Ride State is Different.' => 'El estado de conducción es diferente',
    'This car type is already in this region' => 'Este tipo de auto ya está en esta región',
    'Three car types already in this region' => 'Tres tipos de autos ya en esta región',
    'Timezone' => 'Zona horaria',
    'Title' => 'Título',
    'To Email' => 'Al correo electrónico',
    'Tos' => '',
    'Total Balance' => 'Balance total',
    'Total Rides' => 'Viajes totales',
    'Transaction' => 'Transacción',
    'Transaction Amount' => 'Cantidad de transacción',
    'Transaction Deleted Successfully.' => 'Transacción eliminada exitosamente',
    'Transaction Id' => 'ID de transacción',
    'Transactions' => 'Transacciones',
    'Type' => 'Tipo',
    'Type ID' => 'ID de tipo',
    'Type Of Settlement Code' => 'Tipo de código de liquidación',
    'Type Of Settlemet' => 'Tipo de liquidación',
    'Unable To Apply Coupon' => 'No se puede aplicar el cupón',
    'Unable To Load Data' => 'Incapaz de cargar datos',
    'Unable To Save Data' => 'Incapaz de guardar datos',
    'Unable To Save Review ' => 'No se puede guardar la reseña',
    'Unable to fetch location' => 'No se puede recuperar la ubicación',
    'Unable to find driver' => 'No se puede encontrar el conductor',
    'Unable to save User Data' => 'No se pueden guardar los datos del usuario',
    'Unique Identifier Settling' => 'Establecimiento de identificador único',
    'Update' => 'Actualizar',
    'Update Time' => 'Tiempo de actualizacion',
    'Update Time (in UTC)' => 'Hora de actualización (en UTC)',
    'Update {modelClass}: ' => 'Actualización {modelClass}: ',
    'Updated Latitude' => 'Latitud actualizada',
    'Updated Longitude' => 'Longitud actualizada',
    'Updated On' => 'Actualizado en',
    'Updated On (in UTC)' => 'Actualizado en (en UTC)',
    'Upload Backup' => 'Subir copia de seguridad',
    'User' => 'Usuario',
    'User ID' => 'ID de usuario',
    'User Wallets' => 'Carteras de usuario',
    'User data not saved' => 'Datos de usuario no guardados',
    'Users' => 'Usuarios',
    'Valid authcode required' => 'Se requiere código de autenticación válido',
    'Vehicle' => 'Vehículo',
    'Vehicle Catelogs' => 'Catálogos de vehículos',
    'Vehicle Document File' => 'Archivo de documento del vehículo',
    'View Site' => 'Ver sitio',
    'Wallet' => 'Billetera',
    'Wallet Already Exists' => 'La cuenta con {email} ya existe',
    'Wallet Histories' => 'Historias de billetera',
    'Wallet History' => 'Historial de billetera',
    'You are not allowed to access this page.' => 'No tienes permiso para acceder a esta página.',
    'You are not allowed to perform this action' => 'No tienes permiso para realizar esta acción',
    'You are now offline' => 'Ahora estás fuera de línea',
    'You are now online' => 'Ahora estás en línea',
    'You cannot accept this ride.' => 'No puedes aceptar este paseo.',
    'You cannot cancel this ride now.' => 'No puedes cancelar este viaje ahora.',
    'You have already reviewed for this ride' => 'Ya has revisado este viaje',
    'Your Ride Has Been Scheduled As Per Your Request' => 'Su viaje ha sido programado según su solicitud',
    'Zipcode' => 'Código postal',
    'add' => 'añadir',
    'are required' => 'son requeridos',
    'auth token not generated' => 'token de autenticación no generado',
    'delete' => 'borrar',
    'latitude and longitude cannot be blank' => 'latitud y longitud no pueden estar en blanco',
    'no data posted' => 'sin datos publicados',
    'update' => 'actualizar',
    'view' => 'ver',
    '{modelClass} Module' => '{modelClass} Módulo',
    'Country ID' => 'País',
    'no_units_found_selected_companies' => 'No se encontraron unidades con las empresas seleccionadas',
    'No Postal Code Found' => 'No se encontró el código postal',
    'Ride paid' => 'Viaje pagado',
    'you_have_offer' => 'Viaje pagado',
    'Warning' => 'Advertencia',
    'Pending amount' => 'Monto pendiente',
    'Ride Request' => 'Solicitud de viaje',
];
