<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_transaction".
 *
 * @property integer $id
 * @property integer $wallet_history_id
 * @property string $amount
 * @property string $transaction_id
 * @property integer $ride_id
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id
 *
 */
namespace app\models;

use Yii;

class Transaction extends \app\components\TActiveRecord
{

    const TYPE_CASH = 1;

    const TYPE_WALLET = 2;

    const TYPE_PAYPAL = 3;

    public function __toString()
    {
        return (string) $this->wallet_history_id;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            self::TYPE_CASH => "Cash",
            self::TYPE_PAYPAL => "Paypal",
            self::TYPE_WALLET => "Wallet"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'wallet_history_id',
                    'ride_id',
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'amount',
                    'created_on',
                    'updated_on',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'amount',
                    'transaction_id'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'ride_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Ride::className(),
                'targetAttribute' => [
                    'ride_id' => 'id'
                ]
            ],
            [
                [
                    'wallet_history_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => WalletHistory::className(),
                'targetAttribute' => [
                    'wallet_history_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wallet_history_id' => Yii::t('app', 'Wallet History'),
            'amount' => Yii::t('app', 'Amount'),
            'transaction_id' => Yii::t('app', 'Transaction Id'),
            'ride_id' => Yii::t('app', 'Ride Id'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Create Time (in UTC)'),
            'updated_on' => Yii::t('app', 'Updated On (in UTC)'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        return $relations;
    }

    public function getWalletHistory()
    {
        return $this->hasMany(WalletHistory::className(), [
            'id' => 'wallet_history_id'
        ]);
    }

    public function beforeDelete()
    {
        WalletHistory::deleteRelatedAll([
            'id' => 'wallet_history_id'
        ]);
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['wallet_history_id'] = $this->wallet_history_id;
        $json['amount'] = $this->amount;
        $json['transaction_id'] = $this->transaction_id;
        $json['ride_id'] = $this->ride_id;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function getRides()
    {
        return $this->hasOne(Ride::className(), [
            'id' => 'ride_id'
        ]);
    }
}