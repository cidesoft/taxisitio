<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_feedback".
 *
 * @property integer $id
 * @property string $rate_driver
 * @property string $review
 * @property integer $model_id
 * @property string $model_type
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $created_by_id ===Relative data ===
 *          
 * @property User $createdBy
 */
namespace app\models;

class Feedback extends \app\models\base\FeedbackBase {
	public function asJson($with_relations = false) {
		$json = [ ];
		if (isset ( $this->id ))
			$json ['id'] = $this->id;
		
		if (isset ( $this->rate_driver ))
			$json ['rate_driver'] = $this->rate_driver;
		
		if (isset ( $this->review ))
			$json ['review'] = $this->review;
		
		if (isset ( $this->model_id ))
			$json ['model_id'] = $this->model_id;
		
		if (isset ( $this->model_type ))
			$json ['model_type'] = $this->model_type;
		
		if (isset ( $this->state_id ))
			$json ['state_id'] = $this->state_id;
		
		if (isset ( $this->type_id ))
			$json ['type_id'] = $this->type_id;
		
		if (isset ( $this->created_on ))
			$json ['created_on'] = $this->created_on;
		
		if (isset ( $this->created_by_id ))
			$json ['created_by_id'] = $this->created_by_id;
		
		if ($with_relations) {
			
			// CreatedBy
			$list = $this->getCreatedBy ()->all ();
			
			if (is_array ( $list )) {
				$relationData = [ ];
				foreach ( $list as $item ) {
					$relationData [] = $item->asJson ();
				}
				$json ['CreatedBy'] = $relationData;
			} else {
				$json ['CreatedBy'] = $list;
			}
		}
		return $json;
	}
	public function beforeSave($insert) {
		return parent::beforeSave ( $insert );
	}
	public function beforeDelete() {
		return parent::beforeDelete ();
	}
}