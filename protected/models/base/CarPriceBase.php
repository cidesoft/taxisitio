<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\Country;
use app\models\User;
use Yii;

/**
 * This is the model class for table "tbl_car_price".
 *
 * @property integer $id
 * @property integer $base_price_mile
 * @property integer $base_price_hour
 * @property string $price_mile
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property User $createUser
 */
class CarPriceBase extends \app\components\TActiveRecord
{

    const MAX_CAR_COUNT = 3;

    const CAR_EXIST = 0;

    const TYPE_CAR = 0;

    const TYPE_SUV = 1;

    const TYPE_MINIVAN = 2;

    const STATUS_DRAFT = 0;

    const STATUS_PUBLISHED = 1;

    const STATUS_ARCHIEVED = 2;

    public function __toString()
    {
        return (string) $this->price_mile;
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            self::TYPE_CAR => "CAR",
            self::TYPE_SUV => "SUV",
            self::TYPE_MINIVAN => "MINIVAN"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
        } else {}
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'base_price',
                    'price_min',
                    'price_mile',
                    'type_id',
                    'description',
                    'seat',
                    'country_id'
                ],
                'required'
            ],
            [
                [
                    'base_price',
                    'price_min',
                    'price_mile'
                ],
                'number'
            ],
            [
                [
                    'state_id',
                    'type_id',
                    'create_user_id',
                    'country_id'
                ],
                'integer'
            ],
            [
                [
                    'type_id',
                    'create_time',
                    'service_area',
                    'currency_code',
                    'currency_symbol'
                ],
                'safe'
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'base_price' => Yii::t('app', 'Base Price'),
            'price_min' => Yii::t('app', 'Price (Per Min)'),
            'price_mile' => Yii::t('app', 'Price (Per Km)'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'create_time' => Yii::t('app', 'Create Time (in UTC)'),
            'image_file' => Yii::t('app', 'Image'),
            'country_id' => Yii::t('app', 'Country'),
            'create_user_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), [
            'id' => 'country_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'CreateUser',
            'User',
            'id'
        ];
        return $relations;
    }
}