<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\AuthSession;
use app\models\Complain;
use app\models\Country;
use app\models\CouponSent;
use app\models\Driver;
use app\models\ImageProof;
use app\models\Ride;
use app\models\UserWallet;
use Yii;
use yii\widgets\Menu;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property string $contact_no
 * @property string $address
 * @property string $city
 * @property string $country
 * @property integer $zipcode
 * @property integer $gender
 * @property integer $currency_type
 * @property string $timezone
 * @property string $date_of_birth
 * @property string $about_me
 * @property string $image_file
 * @property string $lat
 * @property string $long
 * @property integer $tos
 * @property integer $role_id
 * @property integer $state_id
 * @property string $last_visit_time
 * @property string $last_action_time
 * @property string $last_location_time
 * @property string $last_password_change
 * @property string $activation_key
 * @property integer $login_error_count
 * @property integer $create_user_id
 * @property string $create_time
 *
 *
 * @property AuthSession[] $authSessions
 * @property Blog[] $blogs
 * @property Comment[] $comments
 * @property Menu[] $menus
 * @property Page[] $pages
 * @property Post[] $posts
 */
class UserBase extends \app\components\TActiveRecord
{

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_BANNED = 2;

    const MALE = 0;

    const FEMALE = 1;

    const ROLE_ADMIN = 0;

    const ROLE_PASSENGER = 1;

    const ROLE_DRIVER = 2;

    const TYPE_ON = 0;

    const TYPE_OFF = 1;

    const NOT_AVAILABLE = 0;

    const IS_AVAILABLE = 1;

    const IS_ONLINE = 1;

    const IS_OFFLINE = 0;

    const TYPE_STEP_1 = 1;

    const TYPE_STEP_2 = 2;

    const NORMAL_SIGNUP = 0;

    const FB_SIGNUP = 1;

    public $uid;

    public $valid = true;

    public $confirm_password;

    public $newPassword;

    public $oldPassword;

    public $device_type;

    public $device_token;

    public static function getAvailabilityOptions($id = null)
    {
        $list = array(
            self::NOT_AVAILABLE => "Not Available",
            self::IS_AVAILABLE => "Available"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public static function getOnlineOptions($id = null)
    {
        $list = array(
            self::IS_OFFLINE => "Offline",
            self::IS_ONLINE => "Online"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public function getOnlineBadge()
    {
        $list = array(
            self::IS_OFFLINE => "danger",
            self::IS_ONLINE => "primary"
        
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getAvailabilityOptions($this->is_online), [
            'class' => 'label label-' . $list[$this->is_online]
        ]);
    }

    public static function getSignupOption($id = null)
    {
        $list = array(
            self::NORMAL_SIGNUP => "Normal Signup",
            self::FB_SIGNUP => "Facebook Signup"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public function getSignupBadge()
    {
        $list = array(
            self::NORMAL_SIGNUP => "danger",
            self::FB_SIGNUP => "primary"
        
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getSignupOption($this->is_fb), [
            'class' => 'label label-' . $list[$this->is_fb]
        ]);
    }

    public function getAvailabilityBadge()
    {
        $list = array(
            self::NOT_AVAILABLE => "danger",
            self::IS_AVAILABLE => "success"
        
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getAvailabilityOptions($this->is_available), [
            'class' => 'label label-' . $list[$this->is_available]
        ]);
    }

    public function getStateBadge()
    {
        $list = array(
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_BANNED => 'danger'
        
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getStateOptions($this->state_id), [
            'class' => 'label label-' . $list[$this->state_id]
        ]);
    }

    public static function getGenderOptions($id = null)
    {
        $list = array(
            self::MALE => "Male",
            self::FEMALE => "Female"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public static function getRoleOptions($id = null)
    {
        $list = array(
            self::ROLE_ADMIN => "Admin",
            self::ROLE_MANAGER => "Manager"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATE_INACTIVE => "In-active",
            self::STATE_ACTIVE => "Active",
            self::STATE_BANNED => 'Banned'
        
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            self::TYPE_ON => "ON",
            self::TYPE_OFF => "OFF"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id];
        return $id;
    }

    public function beforeValidate()
    {
        if ($this->valid == false)
            return true;
        if ($this->isNewRecord) {
            if (! isset($this->state_id))
                $this->state_id = self::STATE_ACTIVE;
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->gender))
                $this->gender = self::MALE;
        } else {}
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        
        $scenarios['register'] = [
            'full_name',
            'email',
            'confirm_password',
            'password'
        ];
        
        $scenarios['signup'] = [
            'full_name',
            'email',
            'city',
            // 'country',
            'password'
        ];
        $scenarios['passenger-signup'] = [
            'first_name',
            'last_name',
            'email',
            'contact_no',
            'country_id',
            'sepomex_id',
            'password',
            'device_type',
            'device_token',
            'full_name'
        ];
        
        $scenarios['customer-update-admin'] = [
            'first_name',
            'last_name',
            'email',
            'contact_no'
            // 'country_id',
        
        ];
        $scenarios['driver-signup'] = [
            'first_name',
            'last_name',
            'email',
            'country_id',
            'password',
            'contact_no',
            'full_name'
        
        ];
        
        $scenarios['driver-update'] = [
            'first_name',
            'last_name',
            'image_file',
            'country_id',
            'contact_no'
        
        ];
        
        $scenarios['update-admin'] = [
            'first_name',
            'last_name',
            'email',
            'contact_no',
            'image_file'
        
        ];
        
        $scenarios['changepassword'] = [
            'newPassword',
            'oldPassword',
            'confirm_password'
        ];
        $scenarios['resetpassword'] = [
            'password'
        ];
        
        $scenarios['recover'] = [
            'email'
        ];
        $scenarios['token_request'] = [
            'email'
        ];
        
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'contact_no',
                    'password',
                    'country_id',
                    'device_type',
                    'device_token',
                    'sepomex_id'
                ],
                'required',
                'on' => [
                    
                    'passenger-signup'
                ]
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'contact_no',
                    'password',
                    'country_id'
                
                ],
                'required',
                'on' => [
                    'driver-signup'
                
                ]
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'contact_no'
                    // 'country_id',
                
                ],
                'required',
                'on' => [
                    'customer-update-admin'
                
                ]
            ],
            
            [
                [
                    
                    'email'
                
                ],
                'required',
                'on' => [
                    'recover'
                
                ]
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'country_id',
                    'contact_no'
                ],
                'required',
                'on' => [
                    'driver-update'
                
                ]
            ],
            
            [
                [
                    'first_name',
                    'last_name',
                    'email',
                    'contact_no'
                ],
                'required',
                'on' => [
                    'update-admin'
                
                ]
            ],
            
            [
                [
                    'image_file'
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'png, jpg,jpeg'
            ],
            
            [
                [
                    // 'full_name',
                    'first_name',
                    // 'last_name',
                    'city'
                    // 'country'
                ],
                'match',
                'pattern' => '/^[a-z ]*$/i'
            ],
            [
                'email',
                'email'
            ],
            [
                'email',
                'unique'
            ],
            
            [
                [
                    'is_verified',
                    'ride_count',
                    'is_online',
                    'is_available',
                    'zipcode',
                    'gender',
                    'tos',
                    'role_id',
                    'state_id',
                    'create_user_id',
                    'contact_no',
                    'is_fb'
                ],
                'integer'
            ],
            [
                [
                    'image_file'
                ],
                'checkImage'
            ],
            [
                [
                    'image_file'
                ],
                'image',
                'extensions' => 'png, jpg, jpeg'
            ],
            [
                [
                    'date_of_birth',
                    'create_time',
                    'direction',
                    'avg_rating'
                
                ],
                'safe'
            ],
            [
                'password',
                'required',
                'on' => 'resetpassword'
            ],
            [
                
                [
                    'email',
                    'password',
                    'confirm_password'
                ],
                'required',
                'on' => [
                    'register'
                ]
            ],
            [
                [
                    'newPassword',
                    'oldPassword',
                    'confirm_password'
                ],
                'required',
                'on' => [
                    'changepassword'
                ]
            ],
            [
                'confirm_password',
                'compare',
                'compareAttribute' => 'newPassword',
                'message' => "Passwords don't match",
                'on' => [
                    'changepassword'
                ]
            ],
            
            [
                [
                    'full_name',
                    'email',
                    'city',
                    // 'country',
                    'password'
                ],
                'required',
                'on' => [
                    'signup'
                ]
            ],
            
            [
                [
                    'about_me'
                ],
                'string'
            ],
            [
                [
                    'full_name',
                    'first_name',
                    'last_name'
                ],
                'string',
                'max' => 55
            ],
            [
                [
                    'email'
                ],
                'string',
                'max' => 128
            ],
            [
                [
                    //
                    // 'timezone',
                    'image_file',
                    'activation_key'
                ],
                'string',
                'max' => 512
            ],
            [
                'password',
                'string'
            ],
            [
                [
                    'address',
                    'city'
                    // 'country'
                ],
                'string',
                'max' => 256
            ]
        ];
    }

    public function checkImage($attribute, $param)
    {
        $allowedExtension = [
            'png',
            'jpg',
            'jpeg'
        ];
        if (isset($_FILES['User']['name'][$attribute])) {
            $image = \yii\web\UploadedFile::getInstance($this, $attribute);
            if ($image && ! in_array($image->extension, $allowedExtension)) {
                $this->addError($attribute, 'Only Files with extension png, jpg, jpeg are allowed');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
            'oldPassword' => Yii::t('app', 'Current Password'),
            'contact_no' => Yii::t('app', 'Contact No.'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            // 'country' => Yii::t ( 'app', 'Country' ),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'gender' => Yii::t('app', 'Gender'),
            'timezone' => Yii::t('app', 'Timezone'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'ride_count' => \Yii::t('app', 'Total Rides'),
            'about_me' => Yii::t('app', 'About Me'),
            'image_file' => Yii::t('app', 'Image'),
            'lat' => Yii::t('app', 'Latitude'),
            'long' => Yii::t('app', 'Longitude'),
            'tos' => Yii::t('app', 'Tos'),
            'role_id' => Yii::t('app', 'Role '),
            'is_fb' => Yii::t('app', 'Signup Type'),
            // 'state_id' => Yii::t ( 'app', 'State ' ),
            'last_visit_time' => Yii::t('app', 'Last Visit Time'),
            'last_action_time' => Yii::t('app', 'Last Action Time'),
            'last_location_time' => Yii::t('app', 'Last Location Time'),
            'activation_key' => Yii::t('app', 'Activation Key'),
            'create_user_id' => Yii::t('app', 'Create User ID'),
            'create_time' => Yii::t('app', 'Create Time (in UTC)'),
            'avg_rating' => Yii::t('app', 'Average Rating')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthSessions()
    {
        return $this->hasMany(AuthSession::className(), [
            'create_user_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), [
            'create_user_id' => 'id'
        ]);
    }

    public function getImageProof()
    {
        return $this->hasOne(ImageProof::className(), [
            'create_user_id' => 'id'
        ]);
    }

    public function getDriverRides()
    {
        return $this->hasMany(Ride::className(), [
            'driver_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getCustomerRides()
    {
        return $this->hasMany(Ride::className(), [
            'create_user_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getUserWallet()
    {
        return $this->hasOne(UserWallet::className(), [
            'created_by_id' => 'id'
        ]);
    }

    public function getDriverComplains()
    {
        return $this->hasMany(Complain::className(), [
            'driver_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getCustomerComplains()
    {
        return $this->hasMany(Complain::className(), [
            'created_by_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getCouponList()
    {
        return $this->hasMany(CouponSent::className(), [
            'user_id' => 'id'
        ])->orderBy('id desc');
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), [
            'create_user_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), [
            'id' => 'country_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    /*
     * public function getPosts()
     * {
     * return $this->hasMany(post::className(), [
     * 'create_user_id' => 'id'
     * ]);
     * }
     */
    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'Posts',
            'Post',
            'id'
        ];
        return $relations;
    }
}