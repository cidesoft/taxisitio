<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

namespace app\models\base;

use app\models\User;
use Yii;
use app\models\Company;

/**
 * This is the model class for table "tbl_driver".
 *
 * @property integer $id
 * @property string $vehicle
 * @property string $model
 * @property string $license_no
 * @property string $registration_no
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id
 * @property integer $company_id
 *
 * @property Company $company
 * @property User $createUser
 */
class DriverBase extends \app\components\TActiveRecord
{

    const TYPE_CAR = 0;
    const TYPE_SUV = 1;
    const TYPE_MINIVAN = 2;

    public function __toString()
    {
        return (string) $this->vehicle;
    }

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_ARCHIEVED = 2;

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATUS_DRAFT => "Draft",
            self::STATUS_PUBLISHED => "Published",
            self::STATUS_ARCHIEVED => "Archieved"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function getStateBadge()
    {
        $list = array(
            self::STATUS_DRAFT => "default",
            self::STATUS_PUBLISHED => "success",
            self::STATUS_ARCHIEVED => "danger"
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getStateOptions($this->state_id), [
                    'class' => 'label label-' . $list[$this->state_id]
        ]);
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            self::TYPE_CAR => "CAR",
            self::TYPE_SUV => "SUV",
            self::TYPE_MINIVAN => "MINIVAN"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (!isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (!isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
        } else {
            
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver}}';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['step1'] = [
            'vehicle',
            'model',
            'car_make',
            'company_id',
        ];

        $scenarios['step2'] = [
            'license_no',
            'registration_no',
            'vehicle_image'
        ];

        $scenarios['update'] = [
            'license_no',
            'registration_no',
            'vehicle',
            'model',
            'car_make',
            'company_id',
        ];

        return $scenarios;
    }

    public function rules()
    {
        return [
            [
                [
                    'state_id',
                    'type_id',
                    'create_user_id',
                    'company_id',
                ],
                'integer'
            ],
            [
                [
                    'license_no',
                    'registration_no',
                    'vehicle',
                    'model',
                    'car_make',
                    'company_id',
                ],
                'required',
                'on' => [
                    'step1'
                ]
            ],
            [
                [
                    'vehicle',
                    'model',
                    'car_make',
                    'company_id',
                ],
                'required',
                'on' => [
                    'step1'
                ]
            ],
            [
                [
                    'license_no',
                    'registration_no',
                    'vehicle',
                    'model',
                    'car_make',
                ],
                'required',
                'on' => [
                    'update'
                ]
            ],
            [
                [
                    'license_no',
                    'registration_no'
                ],
                'required',
                'on' => [
                    'step2'
                ]
            ],
            [
                [
                    'create_time'
                ],
                'safe'
            ],
            [
                [
                    'vehicle',
                    'model',
                    'license_no',
                    'registration_no',
                    'car_make'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vehicle' => Yii::t('app', 'Vehicle'),
            'model' => Yii::t('app', 'Model'),
            'license_no' => Yii::t('app', 'License No'),
            'registration_no' => Yii::t('app', 'Registration No'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'create_time' => Yii::t('app', 'Create Time'),
            'create_user_id' => Yii::t('app', 'Created By'),
            'company_id' => Yii::t('app', 'Company'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
                    'id' => 'create_user_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'CreateUser',
            'User',
            'id'
        ];
        return $relations;
    }

}
