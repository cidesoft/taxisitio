<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\User;
use Yii;

/**
 * This is the model class for table "tbl_auth_session".
 *
 * @property integer $id
 * @property string $auth_code
 * @property string $device_token
 * @property integer $type_id
 * @property integer $create_user_id
 * @property string $create_time
 * @property string $update_time
 *
 * @property User $createUser
 */
class AuthSessionBase extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->auth_code;
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            "TYPE1",
            "TYPE2",
            "TYPE3"
        );
        if ($id == null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
        } else {
            if (! isset($this->update_time))
                $this->update_time = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_session}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'auth_code',
                    'device_token',
                    'create_user_id'
                ],
                'required'
            ],
            [
                [
                    'type_id',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'create_time',
                    'update_time'
                ],
                'safe'
            ],
            [
                [
                    'auth_code',
                    'device_token'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auth_code' => Yii::t('app', 'Auth Code'),
            'device_token' => Yii::t('app', 'Device Token'),
            'type_id' => Yii::t('app', 'Type'),
            'create_user_id' => Yii::t('app', 'Created By'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'CreateUser',
            'User',
            'id'
        ];
        return $relations;
    }
}