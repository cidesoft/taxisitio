<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\User;
use Yii;

/**
 * This is the model class for table "tbl_log".
 *
 * @property integer $id
 * @property string $error
 * @property string $api
 * @property string $description
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id
 *
 * @property User $createUser
 */
class LogBase extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->error;
    }

    const STATUS_DRAFT = 0;

    const STATUS_PUBLISHED = 1;

    const STATUS_ARCHIEVED = 2;

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATUS_DRAFT => "Draft",
            self::STATUS_PUBLISHED => "Published",
            self::STATUS_ARCHIEVED => "Archieved"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function getStateBadge()
    {
        $list = array(
            self::STATUS_DRAFT => "default",
            self::STATUS_PUBLISHED => "success",
            self::STATUS_ARCHIEVED => "danger"
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getStateOptions($this->state_id), [
            'class' => 'label label-' . $list[$this->state_id]
        ]);
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            "TYPE1",
            "TYPE2",
            "TYPE3"
        );
        if ($id === null)
            return $list;
        if (is_numeric($id))
            return $list[$id % count($list)];
        return $id;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! \Yii::$app->user->isGuest) {
                if (! isset($this->create_user_id))
                    $this->create_user_id = Yii::$app->user->id;
            }
        } else {}
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'error'
                ],
                'required'
            ],
            [
                [
                    'api'
                ],
                'string'
            ],
            [
                [
                    'state_id',
                    'type_id',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'create_time',
                    'description',
                    'api',
                    'error'
                ],
                'safe'
            ],
            [
                [
                    'error'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'error' => Yii::t('app', 'Error'),
            'api' => Yii::t('app', 'Api'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'create_time' => Yii::t('app', 'Create Time'),
            'create_user_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'CreateUser',
            'User',
            'id'
        ];
        return $relations;
    }
}