<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\User;
use Yii;

/**
 * This is the model class for table "tbl_feedback".
 *
 * @property integer $id
 * @property string $rate_driver
 * @property string $review
 * @property integer $model_id
 * @property string $model_type
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $created_by_id
 *
 * @property User $createdBy
 */
class FeedbackBase extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->rate_driver;
    }

    const STATUS_DRAFT = 0;

    const STATUS_PUBLISHED = 1;

    const STATUS_ARCHIEVED = 2;

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATUS_DRAFT => "Draft",
            self::STATUS_PUBLISHED => "Published",
            self::STATUS_ARCHIEVED => "Archieved"
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = array(
            self::STATUS_DRAFT => "default",
            self::STATUS_PUBLISHED => "success",
            self::STATUS_ARCHIEVED => "danger"
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getStateOptions($this->state_id), [
            'class' => 'label label-' . $list[$this->state_id]
        ]);
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            "TYPE1",
            "TYPE2",
            "TYPE3"
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'rate_driver',
                    'created_on',
                    'review'
                ],
                'required'
            ],
            [
                [
                    'model_id',
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on'
                ],
                'safe'
            ],
            [
                [
                    'rate_driver',
                    'review'
                ],
                'string',
                'max' => 100
            ],
            [
                [
                    'model_type'
                ],
                'string',
                'max' => 1255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rate_driver' => Yii::t('app', 'Rate Driver'),
            'review' => Yii::t('app', 'Review'),
            'model_id' => Yii::t('app', 'Model ID'),
            'model_type' => Yii::t('app', 'Model Type'),
            'state_id' => Yii::t('app', 'State ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By ID')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'CreatedBy',
            'User',
            'id'
        ];
        return $relations;
    }
}