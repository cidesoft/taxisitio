<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use Yii;

/**
 * This is the model class for table "email_queue".
 *
 * @property integer $id
 * @property string $from_name
 * @property string $from_email
 * @property string $to_email
 * @property string $subject
 * @property string $message
 * @property integer $max_attempts
 * @property integer $attempts
 * @property integer $success
 * @property string $date_published
 * @property string $last_attempt
 * @property string $date_sent
 *
 */
class EmailQueueBase extends \app\components\TActiveRecord
{

    public $mail_sent = 0;

    public function __toString()
    {
        return (string) $this->from_email;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%email_queue}}';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->state_id = self::STATUS_PENDING;
            $this->date_published = (string)new \app\services\util\MyDateTime();
            $this->attempts = 1;
        } else {
            $this->attempts += 1;
        }
        $this->last_attempt = (string)new \app\services\util\MyDateTime();
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'from_email',
                    'to_email',
                    
                    'message'
                ],
                'required'
            ],
            [
                [
                    'message'
                ],
                'string'
            ],
            [
                [
                    'attempts',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'date_published',
                    'last_attempt',
                    'date_sent'
                ],
                'safe'
            ],
				/* [ 
						[ 
								'from_name' 
						],
						'string',
						'max' => 64 
				], */
				[
                [
                    'from_email',
                    'to_email'
                ],
                'string',
                'max' => 128
            ],
            [
                [
                    'subject'
                ],
                'string',
                'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            // 'from_name' => Yii::t ( 'app', 'From Name' ),
            'from_email' => Yii::t('app', 'From Email'),
            'to_email' => Yii::t('app', 'To Email'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Message'),
            'attempts' => Yii::t('app', 'Attempts'),
            'state_id' => Yii::t('app', 'State'),
            'date_published' => Yii::t('app', 'Date Published'),
            'last_attempt' => Yii::t('app', 'Last Attempt'),
            'date_sent' => Yii::t('app', 'Date Sent')
        ];
    }

    public static function getRelations()
    {
        $relations = [];
        return $relations;
    }

    const STATUS_PENDING = 0;

    const STATUS_SUCCESS = 1;

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATUS_PENDING => "Pending",
            self::STATUS_SUCCESS => "Success"
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }
}
