<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\base;

use app\models\User;
use Yii;

/**
 * This is the model class for table "tbl_notification".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $model_type
 * @property integer $model_id
 * @property integer $state_id
 * @property integer $type_id
 * @property integer $is_read
 * @property string $create_time
 * @property string $update_time
 * @property integer $user_id
 * @property integer $create_user_id
 *
 * @property User $createUser
 * @property User $user
 */
class NotificationBase extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    const STATE_NO_RESPONSE = 0;

    const STATE_ACCEPT = 1;

    const STATE_REJECT = 2;

    const UNREAD = 0;

    const READ = 1;

    const TYPE_CAR_REQUEST = 0;

    const TYPE_RIDE_ACCEPTED = 1;

    const TYPE_RIDE_REJECTED = 2;

    const TYPE_RIDE_CANCELLED = 3;

    const TYPE_DRIVER_ARRIVAL = 4;

    const TYPE_PASSENGER_ARRIVAL = 5;

    const TYPE_RIDE_STARTED = 6;

    const TYPE_RIDE_STOPPED = 7;

    const TYPE_RIDE_COMPLETED = 8;

    const TYPE_CHATTING = 9;

    const TYPE_RIDE_UPDATED = 10;

    const TYPE_FEEDBACK = 11;

    const TYPE_SAFETY = 12;

    const TYPE_SHARE = 13;

    const TYPE_OPEN_RIDE = 14;

    const TYPE_PANIC = 15;

    public $to_user;

    public $create_user;

    public static function getStateOptions($id = null)
    {
        $list = array(
            self::STATE_ACCEPT => "Accept",
            self::STATE_REJECT => "Reject",
            self::STATE_NO_RESPONSE => 'No Response'
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = array(
            self::STATUS_DRAFT => "default",
            self::STATUS_PUBLISHED => "success",
            self::STATUS_ARCHIEVED => "danger"
        );
        // return \yii\helpers\Html::tag('span', self::getStateOptions( $this->state_id), ['class' => 'badge vd_bg-' . $list[$this->state_id]]);
        return \yii\helpers\Html::tag('span', self::getStateOptions($this->state_id), [
            'class' => 'label label-' . $list[$this->state_id]
        ]);
    }

    public static function getTypeOptions($id = null)
    {
        $list = array(
            self::TYPE_CAR_REQUEST => \Yii::t('app', 'Car Requests'),
            self::TYPE_RIDE_CANCELLED => \Yii::t('app', 'Ride Cancelled'),
            self::TYPE_RIDE_ACCEPTED => \Yii::t('app', 'Ride Accepted'),
            self::TYPE_RIDE_REJECTED => \Yii::t('app', 'Ride Rejected'),
            self::TYPE_RIDE_STARTED => \Yii::t('app', 'Ride Started'),
            self::TYPE_DRIVER_ARRIVAL => \Yii::t('app', 'Drival Arrival'),
            self::TYPE_PASSENGER_ARRIVAL => \Yii::t('app', 'Passenger Arrival'),
            self::TYPE_RIDE_COMPLETED => \Yii::t('app', 'Ride Completed'),
            self::TYPE_CHATTING => \Yii::t('app', 'Message'),
            self::TYPE_RIDE_UPDATED => \Yii::t('app', 'Ride Updated'),
            self::TYPE_FEEDBACK => \Yii::t('app', 'Feedback'),
            self::TYPE_SAFETY => \Yii::t('app', 'Safety Request'),
            self::TYPE_SHARE => \Yii::t('app', 'Share Request'),
            self::TYPE_OPEN_RIDE => \Yii::t('app', 'Open Ride Request'),
            self::TYPE_PANIC => \Yii::t('app', 'Panic Request')
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->user_id))
                $this->user_id = Yii::$app->user->id;
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
        }
        if (! isset($this->update_time))
            $this->update_time = (string)new \app\services\util\MyDateTime();
        
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'create_time',
                    'update_time',
                    'user_id',
                    'create_user_id'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'model_id',
                    'state_id',
                    'type_id',
                    'is_read',
                    'user_id',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'create_time',
                    'update_time'
                ],
                'safe'
            ],
            [
                [
                    'title',
                    'model_type'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'title' => \Yii::t('app', 'Title'),
            'description' => \Yii::t('app', 'Description'),
            'model_type' => \Yii::t('app', 'Model Type'),
            'model_id' => \Yii::t('app', 'Model ID'),
            'state_id' => \Yii::t('app', 'State ID'),
            'type_id' => \Yii::t('app', 'Type ID'),
            'is_read' => \Yii::t('app', 'Is Read'),
            'create_time' => \Yii::t('app', 'Create Time'),
            'update_time' => \Yii::t('app', 'Update Time'),
            'user_id' => \Yii::t('app', 'User ID'),
            'create_user_id' => \Yii::t('app', 'Create User ID')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public static function getRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'CreateUser',
            'User',
            'id'
        ];
        $relations['user_id'] = [
            'User',
            'User',
            'id'
        ];
        return $relations;
    }
}