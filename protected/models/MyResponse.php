<?php

namespace app\models;

/**
 * Codigos de respuesta
 *
 * @author Carlos Mendoza <inhack20@gmail.com>
 */
class MyResponse
{
    const RESPONSE_ERROR_NAME = "error_code";
    const ERROR_EMAIL_ALREADY_REGISTERED = "ERROR_EMAIL_ALREADY_REGISTERED";
}
