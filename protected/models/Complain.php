<?php
/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_complain".
 *
 * @property integer $id
 * @property string $comment
 * @property integer $driver_id
 * @property integer $ride_id
 * @property integer $state_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property User $driver
 * @property Ride $ride
 */
namespace app\models;

use app\components\FirebaseNotifications;
use Yii;

class Complain extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->comment;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%complain}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'comment',
                    'ride_id'
                ],
                'required'
            ],
            [
                [
                    'comment'
                ],
                'string'
            ],
            [
                [
                    'driver_id',
                    'ride_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'driver_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'driver_id' => 'id'
                ]
            ],
            [
                [
                    'ride_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Ride::className(),
                'targetAttribute' => [
                    'ride_id' => 'id'
                ]
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'comment' => Yii::t('app', 'Comment'),
            'driver_id' => Yii::t('app', 'Created For (Driver)'),
            'ride_id' => Yii::t('app', 'Ride'),
            'state_id' => Yii::t('app', 'State'),
            'created_on' => Yii::t('app', 'Create Time (in UTC)'),
            'updated_on' => Yii::t('app', 'Updated On (in UTC)'),
            'created_by_id' => Yii::t('app', 'Created By (Passenger)')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), [
            'id' => 'driver_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRide()
    {
        return $this->hasOne(Ride::className(), [
            'id' => 'ride_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['driver_id'] = [
            'driver',
            'User',
            'id'
        ];
        $relations['ride_id'] = [
            'ride',
            'Ride',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['comment'] = $this->comment;
        $json['driver_id'] = $this->driver_id;
        $json['ride_id'] = $this->ride_id;
        $json['state_id'] = $this->state_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {
            // createdBy
            $list = $this->createdBy;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['CreatedBy'] = $list;
            }
            // driver
            $list = $this->driver;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['driver'] = $relationData;
            } else {
                $json['Driver'] = $list;
            }
            // ride
            $list = $this->ride;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['ride'] = $relationData;
            } else {
                $json['Ride'] = $list;
            }
        }
        return $json;
    }

    public function requestNotification($id, $message)
    {
        $notification = new FirebaseNotifications();
        $androidtoken = [];
        $iostoken = [];
        $tokens = "";
        $data = [];
        $data['controller'] = \yii::$app->controller->id;
        $data['action'] = \yii::$app->controller->action->id;
        $data['message'] = $message;
        
        $user = User::find()->where([
            'id' => $id
        ])->one();
        if (! empty($user)) {
            $tokens = $user->authSessions;
            if (count($tokens) > 0) {
                foreach ($tokens as $token) {
                    if ($token->type_id == 1) {
                        $androidtoken[] = $token->device_token;
                    }
                    if ($token->type_id == 2)
                        $iostoken[] = $token->device_token;
                }
                if (! empty($androidtoken)) {
                    return $notification->sendDataMessage($androidtoken, $data) ? true : false;
                }
                if (! empty($iostoken)) {
                    $apns = \Yii::$app->apns;
                    $out = '';
                    if ($user->role_id == User::ROLE_DRIVER) {
                        foreach ($iostoken as $tokn) {
                            $out = $apns->send($tokn, $data['message'], $data, [
                                'sound' => 'default',
                                'badge' => 1
                            ]);
                            \Yii::error('DRIVER_NOTIFY');
                            \Yii::error(\yii\helpers\VarDumper::dumpAsString($out));
                            return $out ? true : false;
                        }
                    }
                    if ($user->role_id == User::ROLE_PASSENGER) {
                        $apns = \Yii::$app->apns1;
                        foreach ($iostoken as $tokn) {
                            $out = $apns->send($tokn, $data['message'], $data, [
                                'sound' => 'default',
                                'badge' => 1
                            ]);
                            \Yii::error('PASSENGER_NOTIFY');
                            \Yii::error(\yii\helpers\VarDumper::dumpAsString($out));
                            return $out ? true : false;
                        }
                    }
                }
            }
        }
        return false;
    }
}
