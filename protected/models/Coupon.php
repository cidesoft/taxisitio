<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_coupon".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property integer $discount
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 */
namespace app\models;

use Yii;

class Coupon extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const TYPE_SPECIFIC_USER = 0;

    const TYPE_HIGHER_TRIPS = 1;

    const TYPE_TOP_RATED = 2;

    const TYPE_COLOGNE = 3;

    const TYPE_MUNICIPALITY = 4;

    const TYPE_STATE = 5;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getCouponTypeOptions()
    {
        return [
            self::TYPE_SPECIFIC_USER => "Specific User",
            self::TYPE_HIGHER_TRIPS => "Higher Trip",
            self::TYPE_COLOGNE => "Cologne",
            self::TYPE_MUNICIPALITY => "Municipality"
            
            /*
         * self::TYPE_TOP_RATED => "Top Rated"
         *
         * self::TYPE_COLOGNE => "Cologne",
         * self::TYPE_MUNICIPALITY => "Municipality",
         * self::TYPE_STATE => "State"
         */
        
        ];
    }

    public function getCouponType()
    {
        $list = self::getCouponTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function getCouponTypeBadge()
    {
        $list = [
            self::TYPE_SPECIFIC_USER => "danger",
            self::TYPE_HIGHER_TRIPS => "primary",
            self::TYPE_TOP_RATED => "primary",
            self::TYPE_COLOGNE => "primary",
            self::TYPE_MUNICIPALITY => "success",
            self::TYPE_STATE => "dander"
        ];
        return isset($list[$this->type_id]) ? \yii\helpers\Html::tag('span', $this->getCouponType(), [
            'class' => 'label label-' . $list[$this->type_id]
        ]) : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->start_date))
                $this->start_date = date('Y-m-d');
            if (! isset($this->end_date))
                $this->end_date = date('Y-m-d');
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } 
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%coupon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'description',
                    'start_date',
                    'end_date',
                    'discount',
                    'type_id',
                    'created_on',
                    'ride_count',
                    'max_amount'
                ],
                'required'
            ],
            [
                'end_date',
                'compare',
                'compareAttribute' => 'start_date',
                'operator' => '>',
                
                'message' => '{attribute} must be greater than "{compareValue}".'
            ],
            array(
                'start_date',
                'compare',
                'compareValue' => date("Y-m-d"),
                'operator' => '>='
            ),
            
            [
                [
                    'start_date',
                    'end_date',
                    'created_on'
                ],
                'safe'
            ],
            [
                'user_id',
                'required',
                'when' => function ($model) {
                return $model->type_id == self::TYPE_SPECIFIC_USER;
                }
                ],
            [
                'cologne',
                'required',
                'when' => function ($model) {
                    return $model->type_id == self::TYPE_COLOGNE;
                }
            ],
            [
                'municipality',
                'required',
                'when' => function ($model) {
                return $model->type_id == self::TYPE_MUNICIPALITY;
                }
                ],
            
            [
                [
                    'discount',
                    'state_id',
                    'type_id',
                    'created_by_id',
                    'ride_count'
                ],
                'integer'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'description'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'title',
                    'description'
                ],
                'trim'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
            /*
         * [
         * [
         * 'type_id'
         * ],
         * 'in',
         * 'range' => array_keys ( self::getTypeOptions () )
         * ]
         */
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'Customer'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'ride_count' => Yii::t('app', 'Rides'),
            'discount' => Yii::t('app', 'Discount (in %)'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Coupon Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public function sendCoupon($model)
    {
        $counts = '';
        if ($model->type_id == Coupon::TYPE_SPECIFIC_USER) {
            $this->couponNotification($model, $model->user_id);
        }
        if ($model->type_id == Coupon::TYPE_HIGHER_TRIPS) {
            $counts = User::find()->select('id')
                ->where([
                'role_id' => User::ROLE_PASSENGER,
                'ride_count' => User::find()->max('ride_count')
            ])
                ->all();
        }
        
        if ($model->type_id == Coupon::TYPE_COLOGNE) {
            $cologne_name = Sepomex::find()->select('settlement')->where([
                'id' => $model->cologne
            ]);
            $counts = User::find()->select('id')
                ->where([
                'role_id' => User::ROLE_PASSENGER,
                'settlement' => $cologne_name
            ])
                ->all();
            // print_r($counts);exit;
        }
        
        if ($model->type_id == Coupon::TYPE_MUNICIPALITY) {
            $municipality = Sepomex::find()->select('municipality')->where([
                'id' => $model->municipality
            ]);
            $counts = User::find()->select('id')
                ->where([
                'role_id' => User::ROLE_PASSENGER,
                'municipality' => $municipality
            ])
                ->all();
            // print_r($counts);exit;
        }
        if (! empty($counts)) {
            foreach ($counts as $count) {
                $this->couponNotification($model, $count->id);
            }
        }
    }

    public function couponNotification($model, $user)
    {
        $couponSent = new CouponSent();
        $couponSent->coupon_id = $model->id;
        $couponSent->user_id = $user;
        $couponSent->coupon_code = $model->title;
        $couponSent->discount = $model->discount;
        $couponSent->start_date = $model->start_date;
        $couponSent->end_date = $model->end_date;
        $couponSent->ride_count = $model->ride_count;
        $couponSent->type_id = $model->type_id;
        
        if ($couponSent->save()) {
            
            $ride = new Ride();
            $notification = new Notification();
            $notification->user_id = $user;
            $notification->message = \Yii::t('app','you_have_offer');
            $notification->model_id = $couponSent->id;
            $notification->type_id = $couponSent->type_id;
            $notification->model_type = CouponSent::className();
            $notification->created_by_id = \Yii::$app->user->id;
            
            if ($ride->requestNotification($user, $notification->message)) {
                $notification->save();
            }
        }
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function getSentCoupons()
    {
        return $this->hasMany(CouponSent::className(), [
            'coupon_id' => 'id'
        ])->orderBy('id desc');
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function getUser()
    {
        $user = User::find()->where([
            'id' => $this->user_id
        ])->one();
        return $user;
    }

    public function beforeDelete()
    {
        CouponSent::deleteRelatedAll([
            'coupon_id' => $this->id
        ]);
        
        CouponApplied::deleteRelatedAll([
            'coupon_id' => $this->id
        ]);
        
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['description'] = $this->description;
        $json['start_date'] = date('Y-m-d', strtotime($this->start_date));
        $json['end_date'] = date('Y-m-d', strtotime($this->end_date));
        $json['ride_count'] = $this->ride_count;
        $json['discount'] = $this->discount;
        $json['max_amount'] = round($this->max_amount, 2);
        // $json ['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        // $json ['created_on'] = $this->created_on;
        // $json ['created_by_id'] = $this->created_by_id;
        if ($with_relations) {
            // createdBy
            $list = $this->createdBy;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['CreatedBy'] = $list;
            }
        }
        return $json;
    }
}
