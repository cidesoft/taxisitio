<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_country".
 *
 * @property integer $id
 * @property string $title
 * @property string $country_code
 * @property string $telephone_code
 * @property string $currency_code
 * @property string $currency_symbol
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id
 *
 */
namespace app\models;

use Yii;

class Country extends \app\components\TActiveRecord
{

    public $country;

    public function __toString()
    {
        return (string) $this->title;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        
        $scenarios['update'] = [
            'telephone_code'
        ];
        
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'country',
                    'title',
                    'telephone_code',
                    'currency_code',
                    'currency_symbol',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'title',
                    'country',
                    'country_code',
                    'telephone_code',
                    'currency_code',
                    'currency_symbol'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'country_code' => Yii::t('app', 'Country Code'),
            'telephone_code' => Yii::t('app', 'Telephone Code'),
            'currency_code' => Yii::t('app', 'Currency Code'),
            'currency_symbol' => Yii::t('app', 'Currency Symbol'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Create Time (in UTC)'),
            'updated_on' => Yii::t('app', 'Updated On (in UTC)'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public function getCountryCarPrices()
    {
        return $this->hasMany(CarPrice::className(), [
            'country_id' => 'id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $list = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['country_code'] = $this->country_code;
        $json['telephone_code'] = '+' . $this->telephone_code;
        $json['currency_code'] = $this->currency_code;
        $json['currency_symbol'] = $this->currency_symbol;
        if (! empty($this->countryCarPrices)) {
            foreach ($this->countryCarPrices as $carPrice) {
                $list[] = $carPrice->type_id;
            }
        }
        $json['car_type'] = $list;
        $json['flag'] = \yii::$app->urlManager->createAbsoluteUrl('protected/themes/green/png250px/' . strtolower($this->country_code) . ".png");
        if ($with_relations) {}
        return $json;
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function getCountry()
    {
        $country = [];
        $json = file_get_contents('http://country.io/names.json');
        if (! empty($json)) {
            $country = json_decode($json, true);
        }
        return $country;
    }

    public static function getCurrency($code)
    {
        $json = file_get_contents('https://restcountries.eu/rest/v2/alpha/' . $code);
        $currency = json_decode($json, true);
        return $currency;
    }

    public static function getCountryName($id)
    {
        $model = Country::findOne($id);
        return $list[] = [
            $model->country_code,
            $model->currency_symbol,
            $model->currency_code
        ];
    }
}