<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_car_price".
 *
 * @property integer $id
 * @property string $base_price_mile
 * @property string $base_price_hour
 * @property string $price_mile
 * @property integer $car_type
 * @property integer $max_person
 * @property integer $max_bag
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id ===Relative data ===
 *          
 * @property User $createUser
 */
namespace app\models;

class CarPrice extends \app\models\base\CarPriceBase {
	public function asJson() {
		$Json = [ ];
		$Json ['id'] = $this->id;
		$Json ['country'] = $this->country_id;
		$Json ['currency_code'] = $this->currency_code;
		$Json ['currency_symbol'] = $this->currency_symbol;
		$Json ['seat'] = $this->seat;
		$Json ['description'] = $this->description;
		$Json ['base_price'] = $this->base_price;
		$Json ['price_mile'] = $this->price_mile;
		$Json ['price_min'] = $this->price_min;
		$Json ['type_id'] = $this->type_id;
		return $Json;
	}
	public function beforeSave($insert) {
		return parent::beforeSave ( $insert );
	}
	public function beforeDelete() {
		Ride::deleteRelatedAll ( [ 
				'car_price_id' => $this->id 
		] );
		return parent::beforeDelete ();
	}
	
}