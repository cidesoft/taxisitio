<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_vehicle_catelog".
 *
 * @property integer $id
 * @property string $economic_no
 * @property string $brand
 * @property string $submarca
 * @property string $model
 * @property string $placas
 * @property string $number_series
 * @property string $date_added
 * @property string $secure_policy
 * @property string $insurance_carrier
 * @property string $number_concession
 * @property string $maturity_concession
 * @property string $owner_name
 * @property string $owner_address
 * @property string $assigned_driver
 * @property integer $assigned_base
 * @property string $front_photo
 * @property string $rear_photo
 * @property string $right_photo
 * @property string $left_photo
 * @property integer $status
 * @property string $low_date
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id === Related data ===
 * @property User $createUser
 */
namespace app\models;

use Yii;

class VehicleCatelog extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->economic_no;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->low_date))
                $this->low_date = date('Y-m-d');
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->update_time))
                $this->update_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
        } else {
            $this->update_time = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vehicle_catelog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'economic_no',
                    'brand',
                    'date_added',
                    'status',
                    'low_date',
                    'state_id',
                    'create_time',
                    'update_time',
                    'driver_id'
                ],
                'required'
            ],
            [
                [
                    'date_added',
                    'maturity_concession',
                    'low_date',
                    'create_time',
                    'update_time'
                ],
                'safe'
            ],
            [
                [
                    'assigned_base',
                    'status',
                    'state_id',
                    'type_id',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'economic_no'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'brand',
                    'submarca',
                    'model',
                    'placas',
                    'number_series',
                    'secure_policy',
                    'insurance_carrier',
                    'number_concession',
                    'owner_name',
                    'owner_address',
                    'assigned_driver',
                    'front_photo',
                    'rear_photo',
                    'right_photo',
                    'left_photo'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ],
            [
                [
                    'economic_no',
                    'brand',
                    'submarca',
                    'model',
                    'placas',
                    'number_series',
                    'secure_policy',
                    'insurance_carrier',
                    'number_concession',
                    'owner_name',
                    'owner_address',
                    'assigned_driver',
                    'front_photo',
                    'rear_photo',
                    'right_photo',
                    'left_photo'
                ],
                'trim'
            ],
            [
                [
                    'owner_name'
                ],
                'app\components\TNameValidator'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'economic_no' => Yii::t('app', 'Economic No'),
            'brand' => Yii::t('app', 'Brand'),
            'submarca' => Yii::t('app', 'Submarca'),
            'model' => Yii::t('app', 'Model'),
            'placas' => Yii::t('app', 'Placas'),
            'number_series' => Yii::t('app', 'Number Series'),
            'date_added' => Yii::t('app', 'Date Added'),
            'secure_policy' => Yii::t('app', 'Secure Policy'),
            'insurance_carrier' => Yii::t('app', 'Insurance Carrier'),
            'number_concession' => Yii::t('app', 'Number Concession'),
            'maturity_concession' => Yii::t('app', 'Maturity Concession'),
            'owner_name' => Yii::t('app', 'Owner Name'),
            'owner_address' => Yii::t('app', 'Owner Address'),
            'assigned_driver' => Yii::t('app', 'Assigned Driver'),
            'assigned_base' => Yii::t('app', 'Assigned Base'),
            'front_photo' => Yii::t('app', 'Front Photo'),
            'rear_photo' => Yii::t('app', 'Rear Photo'),
            'right_photo' => Yii::t('app', 'Right Photo'),
            'left_photo' => Yii::t('app', 'Left Photo'),
            'status' => Yii::t('app', 'Status'),
            'low_date' => Yii::t('app', 'Low Date'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'create_user_id' => Yii::t('app', 'Create User')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public function getDriverCatelog()
    {
        return $this->hasOne(DriverCatelog::className(), [
            'id' => 'driver_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'createUser',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['economic_no'] = $this->economic_no;
        $json['brand'] = $this->brand;
        $json['submarca'] = $this->submarca;
        $json['model'] = $this->model;
        $json['placas'] = $this->placas;
        $json['number_series'] = $this->number_series;
        $json['date_added'] = $this->date_added;
        $json['secure_policy'] = $this->secure_policy;
        $json['insurance_carrier'] = $this->insurance_carrier;
        $json['number_concession'] = $this->number_concession;
        $json['maturity_concession'] = $this->maturity_concession;
        $json['owner_name'] = $this->owner_name;
        $json['owner_address'] = $this->owner_address;
        $json['assigned_driver'] = $this->assigned_driver;
        $json['assigned_base'] = $this->assigned_base;
        $json['front_photo'] = $this->front_photo;
        $json['rear_photo'] = $this->rear_photo;
        $json['right_photo'] = $this->right_photo;
        $json['left_photo'] = $this->left_photo;
        $json['status'] = $this->status;
        $json['low_date'] = $this->low_date;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['create_time'] = $this->create_time;
        $json['create_user_id'] = $this->create_user_id;
        if ($with_relations) {
            // createUser
            $list = $this->createUser;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createUser'] = $relationData;
            } else {
                $json['CreateUser'] = $list;
            }
        }
        return $json;
    }
}
