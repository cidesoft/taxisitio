<?php

/**
 * @copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * @author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
/**
 * This is the model class for table "email_queue".
 *
 * @property integer $id
 * @property string $from_name
 * @property string $from_email
 * @property string $to_email
 * @property string $subject
 * @property string $message
 * @property integer $max_attempts
 * @property integer $attempts
 * @property integer $success
 * @property string $date_published
 * @property string $last_attempt
 * @property string $date_sent
 *
 */

namespace app\models;

use yii\helpers\Html;

class EmailQueue extends \app\models\base\EmailQueueBase
{

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function afterSave($insert = true, $changedAttributes = NULL)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    public function sendNow($to = null, $message, $from = null, $sub = null, $attachmentsPath = [])
    {
        $to = (isset($to)) ? $to : \Yii::$app->params ['adminEmail'];
        $from = (isset($from)) ? $from : \Yii::$app->params ['adminEmail'];
        $sub = (isset($sub)) ? $sub : null;
        $mail = \Yii::$app->mailer->compose()->setHtmlBody($message)
                ->setTo($to)
                ->setFrom($from)
                ->setSubject($sub)
        ;
        if ($attachmentsPath) {
            if (is_array($attachmentsPath)) {
                foreach ($attachmentsPath as $file) {
                    if (file_exists($file) && !is_dir($file))
                        $mail->attach($fileName);
                }
            } else {
                if (file_exists($attachmentsPath) && !is_dir($attachmentsPath))
                    $mail->attach($attachmentsPath);
            }
        }
        $mail_sent = 0;
        try {
            $mail_sent = $mail->send();
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), 'EmailQueue-ERROR');
            \Yii::error($e->getTraceAsString(), 'EmailQueue-ERROR-TRACE');
        }
        if (!$mail_sent) {
            return $this->saveEmail($to, $message, $from, $sub);
        } else {
            if (!$this->isNewRecord) {
                $this->delete();
            }
            return true;
        }
    }

    public function saveEmail($to, $message, $from, $sub)
    {
        $this->to_email = $to;
        $this->message = $message;
        $this->from_email = $from;
        $this->subject = $sub;
        return $this->save();
    }

    public function add($args = [])
    {
        /*
         * arguments :
         *
         * to (required, mail send to)
         * view or message (required only one of the two,view is the mail html file in mail folder)
         * viewArgs ( it should be an associative array with arguments you want to pass in the mail html file )
         * from (sender's email, by default it is \Yii::$app->params ['adminEmail'])
         * subject (optional)
         */
        if (!$args)
            return false;

        if (isset($args ['view'])) {
            $this->message = (isset($args ['viewArgs'])) ? Html::encode(\Yii::$app->mailer->render('@app/mail/' . $args ['view'], $args ['viewArgs'])) : Html::encode(\Yii::$app->mailer->render('@app/mail/' . $args ['view']));
        } else {
            $this->message = $args ['message'];
        }
        $this->from_email = (isset($args ['from'])) ? $args ['from'] : \Yii::$app->params ['adminEmail'];
        $this->to_email = $args ['to'];
        $this->subject = (isset($args ['subject'])) ? $args ['subject'] : null;
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

}
