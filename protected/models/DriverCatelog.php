<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_driver_catelog".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mother_last_name
 * @property string $contact_no
 * @property string $street_address
 * @property string $address_number
 * @property integer $cp_address_id
 * @property string $cp_address
 * @property integer $address_colonia_id
 * @property string $address_colonia
 * @property integer $address_municipality_id
 * @property string $address_municipality
 * @property string $address_status
 * @property string $rfc
 * @property string $date_of_birth
 * @property integer $age
 * @property string $ine
 * @property string $curp
 * @property integer $marital_status
 * @property string $date_added
 * @property string $license_no
 * @property string $expiration_date
 * @property string $hard_working_time
 * @property string $comment
 * @property string $avg_rating
 * @property string $image_file
 * @property integer $status
 * @property string $low_date
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id === Related data ===
 * @property User $createUser
 */
namespace app\models;

use Yii;

class DriverCatelog extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->first_name;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->expiration_date))
                $this->expiration_date = date('Y-m-d');
            if (! isset($this->low_date))
                $this->low_date = date('Y-m-d');
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->update_time))
                $this->update_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->create_user_id))
                $this->create_user_id = Yii::$app->user->id;
        } else {
            $this->update_time = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%driver_catelog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'first_name',
                    'last_name',
                    'date_added',
                    'license_no',
                    'comment',
                    'status',
                    'low_date',
                    'state_id',
                    'create_time',
                    'update_time'
                ],
                'required'
            ],
            [
                [
                    'cp_address_id',
                    'address_colonia_id',
                    'address_municipality_id',
                    'age',
                    'marital_status',
                    'status',
                    'state_id',
                    'type_id',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'date_of_birth',
                    'date_added',
                    'expiration_date',
                    'hard_working_time',
                    'low_date',
                    'create_time',
                    'update_time'
                ],
                'safe'
            ],
            [
                [
                    'comment'
                ],
                'string'
            ],
            [
                [
                    'first_name',
                    'rfc',
                    'ine',
                    'curp',
                    'license_no'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'last_name',
                    'mother_last_name',
                    'contact_no',
                    'avg_rating',
                    'image_file'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'street_address',
                    'address_number',
                    'cp_address',
                    'address_colonia',
                    'address_municipality',
                    'address_status'
                ],
                'string',
                'max' => 512
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ],
            [
                [
                    'first_name',
                    'rfc',
                    'ine',
                    'curp',
                    'license_no',
                    'last_name',
                    'mother_last_name',
                    'contact_no',
                    'avg_rating',
                    'image_file',
                    'street_address',
                    'address_number',
                    'cp_address',
                    'address_colonia',
                    'address_municipality',
                    'address_status'
                ],
                'trim'
            ],
            [
                [
                    'first_name'
                ],
                'app\components\TNameValidator'
            ],
            [
                [
                    'last_name'
                ],
                'app\components\TNameValidator'
            ],
            [
                [
                    'mother_last_name'
                ],
                'app\components\TNameValidator'
            ],
            [
                [
                    'image_file'
                ],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'png, jpg,jpeg'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'mother_last_name' => Yii::t('app', 'Mother Last Name'),
            'contact_no' => Yii::t('app', 'Contact No'),
            'street_address' => Yii::t('app', 'Street Address'),
            'address_number' => Yii::t('app', 'Address Number'),
            'cp_address_id' => Yii::t('app', 'Cp Address'),
            'cp_address' => Yii::t('app', 'Cp Address'),
            'address_colonia_id' => Yii::t('app', 'Address Colonia'),
            'address_colonia' => Yii::t('app', 'Address Colonia'),
            'address_municipality_id' => Yii::t('app', 'Address Municipality'),
            'address_municipality' => Yii::t('app', 'Address Municipality'),
            'address_status' => Yii::t('app', 'Address Status'),
            'rfc' => Yii::t('app', 'Rfc'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'age' => Yii::t('app', 'Age'),
            'ine' => Yii::t('app', 'Ine'),
            'curp' => Yii::t('app', 'Curp'),
            'marital_status' => Yii::t('app', 'Marital Status'),
            'date_added' => Yii::t('app', 'Date Added'),
            'license_no' => Yii::t('app', 'License No'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
            'hard_working_time' => Yii::t('app', 'Hard Working Time'),
            'comment' => Yii::t('app', 'Comment'),
            'avg_rating' => Yii::t('app', 'Avg Rating'),
            'image_file' => Yii::t('app', 'Image File'),
            'status' => Yii::t('app', 'Status'),
            'low_date' => Yii::t('app', 'Low Date'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'create_user_id' => Yii::t('app', 'Create User')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public function getVehicleCatelog()
    {
        return $this->hasOne(VehicleCatelog::className(), [
            'driver_catelog_id' => 'id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['create_user_id'] = [
            'createUser',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['first_name'] = $this->first_name;
        $json['last_name'] = $this->last_name;
        $json['mother_last_name'] = $this->mother_last_name;
        $json['contact_no'] = $this->contact_no;
        $json['street_address'] = $this->street_address;
        $json['address_number'] = $this->address_number;
        $json['cp_address_id'] = $this->cp_address_id;
        $json['cp_address'] = $this->cp_address;
        $json['address_colonia_id'] = $this->address_colonia_id;
        $json['address_colonia'] = $this->address_colonia;
        $json['address_municipality_id'] = $this->address_municipality_id;
        $json['address_municipality'] = $this->address_municipality;
        $json['address_status'] = $this->address_status;
        $json['rfc'] = $this->rfc;
        $json['date_of_birth'] = $this->date_of_birth;
        $json['age'] = $this->age;
        $json['ine'] = $this->ine;
        $json['curp'] = $this->curp;
        $json['marital_status'] = $this->marital_status;
        $json['date_added'] = $this->date_added;
        $json['license_no'] = $this->license_no;
        $json['expiration_date'] = $this->expiration_date;
        $json['hard_working_time'] = $this->hard_working_time;
        $json['comment'] = $this->comment;
        $json['avg_rating'] = $this->avg_rating;
        if (isset($this->image_file))
            $json['image_file'] = \Yii::$app->createAbsoluteUrl('drivercatelog/download', array(
                'file' => $this->image_file
            ));
        $json['status'] = $this->status;
        $json['low_date'] = $this->low_date;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['create_time'] = $this->create_time;
        $json['create_user_id'] = $this->create_user_id;
        if ($with_relations) {
            // createUser
            $list = $this->createUser;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createUser'] = $relationData;
            } else {
                $json['CreateUser'] = $list;
            }
        }
        return $json;
    }
}
