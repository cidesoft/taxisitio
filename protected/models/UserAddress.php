<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
 
/**
* This is the model class for table "tbl_user_address".
*
    * @property integer $id
    * @property string $title
    * @property string $lat
    * @property string $long
    * @property integer $state_id
    * @property integer $type_id
    * @property string $create_time
    * @property string $update_time
    * @property integer $create_user_id
    
    * ===Relative data ===
    
        	* @property User $createUser
    */

namespace app\models;

class UserAddress extends \app\models\base\UserAddressBase
{
    public function beforeSave($insert) {
		return parent::beforeSave ($insert);
	}
	public function beforeDelete() {
		return parent::beforeDelete ();
	}
	
	public function asJson(){
		$Json = [ ];
		$Json ['title'] = $this->title;
		$Json ['type_id'] = $this->type_id;
		$Json ['lat'] = $this->lat;
		$Json ['long'] = $this->long;
		return $Json;
	}
	
}