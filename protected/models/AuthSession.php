<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_auth_session".
 *
 * @property integer $id
 * @property string $auth_code
 * @property string $device_token
 * @property integer $type_id
 * @property integer $create_user_id
 * @property string $create_time
 * @property string $update_time ===Relative data ===
 *          
 * @property User $createUser
 */
namespace app\models;

use app\models\User;
use yii;
use yii\web\HttpException;
use yii\helpers\VarDumper;
use yii\log\Logger;

class AuthSession extends \app\models\base\AuthSessionBase {
	private static $session_expiration_days = 0;
	public function beforeSave($insert) {
		return parent::beforeSave ( $insert );
	}
	public function beforeDelete() {
		return parent::beforeDelete ();
	}
	public static function randomCode($count) {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array (); // remember to declare $pass as an array
		$alphaLength = strlen ( $alphabet ) - 1; // put the length -1 in cache
		for($i = 0; $i < $count; $i ++) {
			$n = rand ( 0, $alphaLength );
			$pass [] = $alphabet [$n];
		}
		$p = implode ( $pass );
		return implode ( $pass );
	}
	public static function newSession($model) {
		self::deleteOldSession ( \Yii::$app->user->identity->id );
		$auth_session = new AuthSession ();
		$auth_session->create_user_id = Yii::$app->user->identity->id;
		$auth_session->auth_code = self::randomCode ( $count = 32 );
		$auth_session->device_token = $model->device_token;
		$auth_session->type_id = $model->device_type;
		if ($auth_session->save ()) {
			return $auth_session;
		}
		throw new HttpException ( 500, Yii::t ( 'app', 'auth token not generated' ) );
	}
	public static function deleteOldSession($id) {
		$old = AuthSession::findAll ( [ 
				'create_user_id' => $id 
		] );
		if (! empty ( $old )) {
			foreach ( $old as $session ) {
				$session->delete ();
			}
		}
		return true;
	}
	public static function getHead() {
		if (! function_exists ( 'getallheaders' )) {
			function getallheaders() {
				$headers = '';
				foreach ( $_SERVER as $name => $value ) {
					if (substr ( $name, 0, 5 ) == 'HTTP_') {
						$headers [str_replace ( ' ', '-', ucwords ( strtolower ( str_replace ( '_', ' ', substr ( $name, 5 ) ) ) ) )] = $value;
					}
				}
				return $headers;
			}
		}
		return getallheaders ();
	}
	public static function authenticateSession($auth_code = null) {
		if ($auth_code == null) {
			if ($auth_code == null) {
			    
			    
			    $auth_code = isset ( \Yii::$app->request->headers ['auth_code'] ) ? \Yii::$app->request->headers ['auth_code'] : Yii::$app->request->getQueryParam ( 'auth_code' );
			     $language = isset ( \Yii::$app->request->headers ['User-Agent'] ) ? \Yii::$app->request->headers ['User-Agent'] : "es-Es";
			     $langarray = explode ( 'language/', $language );
			     \Yii::warning ( VarDumper::dumpAsString ( 'array' . $langarray [0] ), Logger::LEVEL_WARNING, 'array' );
			     
			     if (isset ( $langarray ['1'] )) {
			     
			     $langarr = explode ( "/", $langarray ['1'], 2 );
			     $language_new = $langarr [0];
			     
			     if ($language_new == 'en') {
                                $language_final = 'en';
			     } else {
                                $language_final = 'es-Es';
			     }
			     //\Yii::$app->language = $language_final;
                             \Yii::$app->language = "es-Es";
			     \Yii::warning ( VarDumper::dumpAsString ( 'final  ' . \Yii::$app->language ), Logger::LEVEL_WARNING, 'user' );
			     } 
			    
				//$auth_code = isset ( \Yii::$app->request->headers ['auth_code'] ) ? \Yii::$app->request->headers ['auth_code'] : Yii::$app->request->getQueryParam ( 'auth_code' );
			}
			if ($auth_code == null)
				return false;
		}
		
		$auth_session = AuthSession::findOne ( array (
				'auth_code' => $auth_code 
		) );
		
		if ($auth_session != null) {
			if ($auth_session->createUser != null && $auth_session->createUser->state_id == User::STATE_ACTIVE) {
				Yii::$app->user->login ( $auth_session->createUser );
				$auth_session->save ();
				return true;
			}
			$auth_session->delete ();
		}
		throw new HttpException ( 403, Yii::t ( 'app', 'Valid authcode required' ) );
	}
}

