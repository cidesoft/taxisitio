<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

namespace app\models;

/**
 * This is the model class for table "tbl_driver".
 *
 * @property integer $id
 * @property string $vehicle
 * @property string $model
 * @property string $license_no
 * @property string $registration_no
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id ===Relative data ===
 * @property integer $company_id
 *          
 * @property Company $company
 * @property User $createUser
 */
class Driver extends \app\models\base\DriverBase
{

    public function asJson($user = true)
    {
        $Json = [];
        $Json ['vehicle'] = $this->vehicle;
        $Json ['model'] = $this->model;
        $Json ['car_make'] = $this->car_make;
        $Json ['license_no'] = $this->license_no;
        $Json ['registration_no'] = $this->registration_no;
        $Json ['type_id'] = $this->type_id;
        if($this->company_id){
            $Json ['company'] = $this->company->asJson();
        }

        if ($user == false) {
            $Json ['user'] = $this->createUser->asJson();
        }
        return $Json;
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

}
