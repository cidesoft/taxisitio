<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_sepomex".
 *
 * @property integer $id
 * @property integer $settlement_post
 * @property string $settlement
 * @property string $type_of_settlemet
 * @property string $municipality
 * @property string $state
 * @property string $city
 * @property integer $zipcode
 * @property integer $state_code
 * @property integer $office_code
 * @property integer $type_of_settlement_code
 * @property integer $municipality_code
 * @property integer $unique_identifier_settling
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 */
namespace app\models;

use Yii;

class Sepomex extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->settlement_post;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sepomex}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'settlement_post',
                    'settlement',
                    'type_of_settlemet',
                    'municipality',
                    'state',
                    'zipcode',
                    'state_code',
                    'office_code',
                    'type_of_settlement_code',
                    'municipality_code',
                    'unique_identifier_settling',
                    'created_on',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'settlement_post',
                    'zipcode',
                    'state_code',
                    'office_code',
                    'type_of_settlement_code',
                    'municipality_code',
                    'unique_identifier_settling',
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'settlement'
                ],
                'string',
                'max' => 512
            ],
            [
                [
                    'type_of_settlemet',
                    'municipality',
                    'state',
                    'city'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'settlement',
                    'type_of_settlemet',
                    'municipality',
                    'state',
                    'city'
                ],
                'trim'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
            /*
         * [
         * [
         * 'type_id'
         * ],
         * 'in',
         * 'range' => array_keys ( self::getTypeOptions () )
         * ]
         */
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'settlement_post' => Yii::t('app', 'Settlement Post'),
            'settlement' => Yii::t('app', 'Settlement'),
            'type_of_settlemet' => Yii::t('app', 'Type Of Settlemet'),
            'municipality' => Yii::t('app', 'Delegation / municipality'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'state_code' => Yii::t('app', 'State Code'),
            'office_code' => Yii::t('app', 'Office Code'),
            'type_of_settlement_code' => Yii::t('app', 'Type Of Settlement Code'),
            'municipality_code' => Yii::t('app', 'Municipality Code'),
            'unique_identifier_settling' => Yii::t('app', 'Unique Identifier Settling'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        // $json ['settlement_post'] = $this->settlement_post;
        $json['settlement'] = $this->settlement;
        // $json ['type_of_settlemet'] = $this->type_of_settlemet;
        $json['municipality'] = $this->municipality;
        $json['state'] = $this->state;
        // $json ['city'] = $this->city;
        $json['zipcode'] = $this->zipcode;
        // $json ['state_code'] = $this->state_code;
        // $json ['office_code'] = $this->office_code;
        // $json ['type_of_settlement_code'] = $this->type_of_settlement_code;
        // $json ['municipality_code'] = $this->municipality_code;
        // $json ['unique_identifier_settling'] = $this->unique_identifier_settling;
        // $json ['state_id'] = $this->state_id;
        // $json ['type_id'] = $this->type_id;
        // $json ['created_on'] = $this->created_on;
        // $json ['created_by_id'] = $this->created_by_id;
        if ($with_relations) {
            // createdBy
            $list = $this->createdBy;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['CreatedBy'] = $list;
            }
        }
        return $json;
    }
}
