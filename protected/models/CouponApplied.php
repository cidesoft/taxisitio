<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_coupon_applied".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $coupon_sent_id
 * @property string $coupon_code
 * @property integer $discount
 * @property string $start_date
 * @property string $end_date
 * @property string $created_on
 * @property string $updated_on
 * @property integer $type_id
 * @property integer $state_id
 * @property integer $created_by_id === Related data ===
 * @property Coupon $coupon
 * @property CouponSent $couponSent
 * @property User $createdBy
 */
namespace app\models;

use Yii;

class CouponApplied extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->coupon_id;
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->start_date))
                $this->start_date = date('Y-m-d');
            if (! isset($this->end_date))
                $this->end_date = date('Y-m-d');
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%coupon_applied}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'coupon_id',
                    'coupon_sent_id',
                    'coupon_code',
                    'discount',
                    'start_date',
                    'end_date',
                    'created_on',
                    'ride_count'
                ],
                'required'
            ],
            [
                [
                    'coupon_id',
                    'coupon_sent_id',
                    'discount',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'start_date',
                    'end_date',
                    'created_on',
                    'updated_on',
                    'ride_taken',
                    'ride_count',
                    'max_amount'
                ],
                'safe'
            ],
            [
                [
                    'coupon_code'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'coupon_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Coupon::className(),
                'targetAttribute' => [
                    'coupon_id' => 'id'
                ]
            ],
            [
                [
                    'coupon_sent_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => CouponSent::className(),
                'targetAttribute' => [
                    'coupon_sent_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'coupon_code'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'coupon_id' => Yii::t('app', 'Coupon'),
            'coupon_sent_id' => Yii::t('app', 'Coupon Sent'),
            'coupon_code' => Yii::t('app', 'Coupon Code'),
            'discount' => Yii::t('app', 'Discount'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'State'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCoupon()
    {
        return $this->hasOne(Coupon::className(), [
            'id' => 'coupon_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCouponSent()
    {
        return $this->hasOne(CouponSent::className(), [
            'id' => 'coupon_sent_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['coupon_id'] = [
            'coupon',
            'Coupon',
            'id'
        ];
        $relations['coupon_sent_id'] = [
            'couponSent',
            'CouponSent',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['coupon_code'] = $this->coupon_code;
        $json['description'] = isset($this->coupon) ? $this->coupon->description : '';
        $json['discount'] = $this->discount;
        $json['max_amount'] = $this->coupon ? round($this->coupon->max_amount, 2) : '';
        $json['start_date'] = date('Y-m-d', strtotime($this->start_date));
        $json['end_date'] = date('Y-m-d', strtotime($this->end_date));
        $json['ride_count'] = $this->ride_count;
        $json['ride_taken'] = $this->ride_taken;
        if ($with_relations) {
            // coupon
            $list = $this->coupon;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['coupon'] = $relationData;
            } else {
                $json['Coupon'] = $list;
            }
            // couponSent
            $list = $this->couponSent;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['couponSent'] = $relationData;
            } else {
                $json['CouponSent'] = $list;
            }
            // createdBy
            $list = $this->createdBy;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['CreatedBy'] = $list;
            }
        }
        return $json;
    }
}
