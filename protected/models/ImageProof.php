<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_image_proof".
 *
 * @property integer $id
 * @property string $front_file
 * @property string $back_file
 * @property string $left_file
 * @property string $right_file
 * @property string $insurance_file
 * @property string $driving_file
 * @property string $police_file
 * @property string $inspection_file
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property integer $create_user_id ===Relative data ===
 *          
 * @property User $createUser
 */
namespace app\models;

class ImageProof extends \app\models\base\ImageProofBase {
	public function asJson() {
		$Json = [ ];
		
		$Json ['id_proof_file'] = \yii::$app->urlManager->createAbsoluteUrl ( [ 
				'user/download',
				'image_file' => $this->id_proof_file 
		] );
		$Json ['license_file'] = \yii::$app->urlManager->createAbsoluteUrl ( [ 
				'user/download',
				'image_file' => $this->license_file 
		] );
		$Json ['document_file'] = \yii::$app->urlManager->createAbsoluteUrl ( [ 
				'user/download',
				'image_file' => $this->document_file 
		] );
		$Json ['vehicle_image'] = \yii::$app->urlManager->createAbsoluteUrl ( [ 
				'user/download',
				'image_file' => $this->vehicle_image 
		] );
		
		return $Json;
	}
	public function beforeSave($insert) {
		return parent::beforeSave ( $insert );
	}
	public function beforeDelete() {
		return parent::beforeDelete ();
	}
}