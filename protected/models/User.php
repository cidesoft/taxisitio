<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property string $date_of_birth
 * @property string $gender
 * @property string $about_me
 * @property string $contact_no
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $zipcode
 * @property string $image_file
 * @property integer $role_ide
 * @property integer $state_id
 * @property integer $type_id
 * @property string $last_visit_time
 * @property string $last_action_time
 * @property string $last_location_time
 * @property string $last_password_change
 * @property integer $login_error_count
 * @property string $activation_key
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id ===Relative data ===
 *          
 * @property Comment[] $comments
 * @property Faq[] $faqs
 * @property JoinRequest[] $joinRequests
 * @property Maid[] $mas
 * @property MaidEmployment[] $maidEmployments
 * @property MaidImage[] $maidImages
 * @property News[] $news
 * @property Profile[] $profiles
 */
namespace app\models;

use Exception;
use Yii;

class User extends \app\models\base\UserBase implements \yii\web\IdentityInterface
{

    public function asJson($last_ride = false)
    {
        $Json = [];
        $Json['id'] = $this->id;
        $Json['first_name'] = $this->first_name;
        $Json['sepomex'] = $this->getSepomex() ? $this->getSepomex() : '';
        $Json['last_name'] = $this->last_name;
        $Json['email'] = $this->email;
        $Json['contact_no'] = $this->contact_no;
        $Json['is_fb'] = $this->is_fb;
        $Json['state_id'] = $this->getStateOptions($this->state_id);
        
        $wallet = $this->getWallet();
        if (! empty($wallet)) {
            $Json['wallet'] = $wallet->asJson();
        }
        
        if ($last_ride == true) {
            $ride = $this->getLastRide();
            if (! empty($ride)) {
                if ($this->role_id == User::ROLE_DRIVER) {
                    $Json['last_ride'] = $ride->asJson(false, false, true);
                }
                if ($this->role_id == User::ROLE_PASSENGER) {
                    $Json['last_ride'] = $ride->asJson(false, true, false);
                }
            }
        }
        if ($this->role_id == User::ROLE_PASSENGER) {
            $userAddresses = UserAddress::find()->where([
                'create_user_id' => \yii::$app->user->id
            ])->all();
            if (! empty($userAddresses)) {
                foreach ($userAddresses as $userAddress) {
                    $Json['addresses'][$userAddress->getTypeOptions($userAddress->type_id)] = $userAddress->asJson();
                }
            }
        }
        if ($this->role_id == User::ROLE_DRIVER) {
            
            $Json['driver_rating'] = $this->avg_rating;
            $Json['driver_profile_step'] = $this->type_id;
            $Json['is_available'] = $this->is_available;
            $Json['is_online'] = (int) $this->is_online;
            $Json['lat'] = $this->lat;
            $Json['long'] = $this->long;
            $Json['direction'] = $this->direction ? (string) $this->direction : '';
            if ($this->driver) {
                $Json['driver'] = $this->driver ? $this->driver->asJson() : '';
            }
            if ($this->imageProof) {
                $Json['imageProof'] = $this->imageProof ? $this->imageProof->asJson() : '';
            }
        }
        if (! empty($this->country)) {
            $Json['country'] = $this->country ? $this->country->asJson() : "";
        }
        if (isset($this->image_file) && $this->image_file != '') {
            $Json['image_file'] = \yii::$app->urlManager->createAbsoluteUrl([
                '/user/download',
                'image_file' => $this->image_file
            ]);
        } else {
            $Json['image_file'] = '';
        }
        return $Json;
    }

    public function asPassengerJson()
    {
        $Json = [];
        $Json['id'] = $this->id;
        $Json['first_name'] = $this->first_name;
        $Json['last_name'] = $this->last_name;
        $Json['email'] = $this->email;
        $Json['contact_no'] = $this->contact_no;
        $Json['is_fb'] = $this->is_fb;
        $Json['state_id'] = $this->getStateOptions($this->state_id);
        if (! empty($this->country)) {
            $Json['country'] = $this->country ? $this->country->asJson() : "";
        }
        
        if (isset($this->image_file) && $this->image_file != '') {
            $Json['image_file'] = \yii::$app->urlManager->createAbsoluteUrl([
                '/user/download',
                'image_file' => $this->image_file
            ]);
        } else {
            $Json['image_file'] = '';
        }
        return $Json;
    }

    public function getLastRide()
    {
        $ride = [];
        if ($this->role_id == User::ROLE_DRIVER) {
            $ride = Ride::find()->where([
                'driver_id' => $this->id
            ])
                ->orderBy([
                'id' => SORT_DESC
            ])
                ->one();
        }
        if ($this->role_id == User::ROLE_PASSENGER) {
            $ride = Ride::find()->where([
                'create_user_id' => $this->id
            ])
                ->orderBy([
                'id' => SORT_DESC
            ])
                ->one();
        }
        return $ride;
    }

    public function getSepomex($data = false)
    {
        $sepomex = Sepomex::findOne($this->sepomex_id);
        if (! empty($sepomex)) {
            if ($data == true)
                return $sepomex;
            return $sepomex->asJson();
        }
    }

    public function getWallet()
    {
        $wallet = [];
        $wallet = UserWallet::find()->where([
            'created_by_id' => \Yii::$app->user->id
        ])->one();
        return $wallet;
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'state_id' => [
                self::STATE_ACTIVE
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne([
            'activation_key' => $token
        ]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'email' => $username
        ]);
    }

    public function getLoginUrl()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            'user/login'
        ]);
    }

    public static function randomPassword($count = 8)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = [];
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $count; $i ++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token
     *            password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (! static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'activation_key' => $token,
            'state_id' => self::STATE_ACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token
     *            password reset token
     * @return boolean
     */
    public function getResetUrl()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            'user/resetpassword',
            'token' => $this->activation_key
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->activation_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password
     *            password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * convert normal password to hash password before saving it to database
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->setPassword($this->password);
        $this->setGender($this->gender);
        $this->setAddress($this->address);
        $this->setCity($this->city);
        $date_of_birth = $this->date_of_birth;
        $this->generateAuthKey();
        $this->generatePasswordResetToken();
        return true;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->activation_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->activation_key = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->activation_key = null;
    }

    public static function isDriver()
    {
        $user = Yii::$app->user->identity;
        if ($user == null)
            return false;
        
        return ($user->role_id == User::ROLE_DRIVER);
    }

    public static function isAdmin()
    {
        $user = Yii::$app->user->identity;
        if ($user == null)
            return false;
        
        return ($user->role_id == User::ROLE_ADMIN || self::isAdmin());
    }

    public static function isPassenger()
    {
        $user = \Yii::$app->user->identity;
        if ($user == null)
            return false;
        return ($user->role_id == User::ROLE_PASSENGER);
    }

    public function getProfileImage()
    {
        $user = Yii::$app->user->identity;
        $image_path = UPLOAD_PATH . $user->image_file;
        
        if (! file_exists($image_path))
            throw new Exception(Yii::t('app', "File not found"), 404);
        
        return \yii::$app->response->sendFile($image_path, "test.jpg");
    }

    public function beforeDelete()
    {
        Driver::deleteRelatedAll([
            'create_user_id' => $this->id
        ]);
        ImageProof::deleteRelatedAll([
            'create_user_id' => $this->id
        ]);
        HaLogin::deleteRelatedAll([
            'user_id' => $this->id
        ]);
        AuthSession::deleteRelatedAll([
            'create_user_id' => $this->id
        ]);
        Notification::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        Notification::deleteRelatedAll([
            'user_id' => $this->id
        ]);
        Transaction::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        
        Ride::deleteRelatedAll([
            'create_user_id' => $this->id
        ]);
        Ride::deleteRelatedAll([
            'driver_id' => $this->id
        ]);
        CouponSent::deleteRelatedAll([
            'user_id' => $this->id
        ]);
        UserAddress::deleteRelatedAll([
            'create_user_id' => $this->id
        ]);
        UserWallet::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        WalletHistory::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        Complain::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        Sepomex::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        Review::deleteRelatedAll([
            'created_by_id' => $this->id
        ]);
        
        return parent::beforeDelete();
    }

    public static function MonthlySignups()
    {
        $date = new \app\services\util\MyDateTime();
        $date->modify('-12 months');
        $last = $date;
        $dates = array();
        $count = array();
        for ($i = 1; $i <= 12; $i ++) {
            $date->modify('+1 months');
            $month = $date->format('Y-m');
            
            $count[$month] = (int) User::find()->where([
                'like',
                'create_time',
                $month
            ])->count();
        }
        return $count;
    }

    public static function UserGraph($role_id)
    {
        $arr = [];
        $jan_users = User::monthlyUsers(date('Y-01-01'), date('Y-01-31'), $role_id);
        $arr['jan'] = $jan_users;
        
        $feb_users = User::monthlyUsers(date('Y-02-01'), date('Y-02-31'), $role_id);
        $arr['feb'] = $feb_users;
        
        $march_users = User::monthlyUsers(date('Y-03-01'), date('Y-03-31'), $role_id);
        $arr['march'] = $march_users;
        
        $april_users = User::monthlyUsers(date('Y-04-01'), date('Y-04-31'), $role_id);
        $arr['april'] = $april_users;
        
        $may_users = User::monthlyUsers(date('Y-05-01'), date('Y-05-31'), $role_id);
        $arr['may'] = $may_users;
        
        $june_users = User::monthlyUsers(date('Y-06-01'), date('Y-06-31'), $role_id);
        $arr['june'] = $june_users;
        
        $july_users = User::monthlyUsers(date('Y-07-01'), date('Y-07-31'), $role_id);
        $arr['july'] = $july_users;
        
        $august_users = User::monthlyUsers(date('Y-08-01'), date('Y-08-31'), $role_id);
        $arr['august'] = $august_users;
        
        $sept_users = User::monthlyUsers(date('Y-09-01'), date('Y-09-31'), $role_id);
        $arr['sept'] = $sept_users;
        
        $oct_users = User::monthlyUsers(date('Y-10-01'), date('Y-10-31'), $role_id);
        $arr['oct'] = $oct_users;
        
        $nov_users = User::monthlyUsers(date('Y-11-01'), date('Y-11-31'), $role_id);
        $arr['nov'] = $nov_users;
        
        $dec_users = User::monthlyUsers(date('Y-12-01'), date('Y-12-31'), $role_id);
        $arr['dec'] = $dec_users;
        
        return $arr;
    }

    public static function monthlyUsers($start_date, $end_date, $role_id)
    {
        $user_count = User::find()->where([
            'between',
            'date(create_time)',
            $start_date,
            $end_date
        ])
            ->andWhere([
            'role_id' => $role_id
        ])
            ->count();
        
        return $user_count;
    }

    public static function RideGraph()
    {
        $arr = [];
        $jan_rides = User::monthlyRides(date('Y-01-01'), date('Y-01-31'));
        $arr['jan'] = $jan_rides;
        
        $feb_rides = User::monthlyRides(date('Y-02-01'), date('Y-02-31'));
        $arr['feb'] = $feb_rides;
        
        $march_rides = User::monthlyRides(date('Y-03-01'), date('Y-03-31'));
        $arr['march'] = $march_rides;
        
        $april_rides = User::monthlyRides(date('Y-04-01'), date('Y-04-31'));
        $arr['april'] = $april_rides;
        
        $may_rides = User::monthlyRides(date('Y-05-01'), date('Y-05-31'));
        $arr['may'] = $march_rides;
        
        $june_rides = User::monthlyRides(date('Y-06-01'), date('Y-06-31'));
        $arr['june'] = $june_rides;
        
        $july_rides = User::monthlyRides(date('Y-07-01'), date('Y-07-31'));
        $arr['july'] = $july_rides;
        
        $august_rides = User::monthlyRides(date('Y-08-01'), date('Y-08-31'));
        $arr['august'] = $august_rides;
        
        $sept_rides = User::monthlyRides(date('Y-09-01'), date('Y-09-31'));
        $arr['sept'] = $sept_rides;
        
        $oct_rides = User::monthlyRides(date('Y-10-01'), date('Y-10-31'));
        $arr['oct'] = $oct_rides;
        
        $nov_rides = User::monthlyRides(date('Y-11-01'), date('Y-11-31'));
        $arr['nov'] = $nov_rides;
        
        $dec_rides = User::monthlyRides(date('Y-12-01'), date('Y-12-31'));
        $arr['dec'] = $dec_rides;
        
        return $arr;
    }

    public static function monthlyRides($start_date, $end_date)
    {
        $ride_count = Ride::find()->where([
            'between',
            'date(create_time)',
            $start_date,
            $end_date
        ])->count();
        
        return $ride_count;
    }

    public function distance($lat1, $long1, $lat2, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=en&key=AIzaSyDgEavzqVAP_Fi92X6lZJI9uc_ikmbFFwA";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        
        if (! empty($response_a['rows'][0]['elements'][0]['distance'])) {
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        } else {
            $dist = "Too far";
        }
        
        if (! empty($response_a['rows'][0]['elements'][0]['duration'])) {
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        } else {
            $time = "Time Exeeded";
        }
        
        return array(
            'distance' => $dist,
            'time' => $time
        );
    }

    /*
     * public function getPeakPercent()
     * {
     * $date = date('Y-m-d');
     * $time = date('H:i:s');
     * $model = PeakPrice::find()->where([
     * '<=',
     * 'start_date',
     * $date
     * ])
     * ->andWhere([
     * '>=',
     * 'end_date',
     * $date
     * ])
     * ->andWhere([
     * '<=',
     * 'start_time',
     * $time
     * ])
     * ->andWhere([
     * '>=',
     * 'end_time',
     * $time
     * ])
     * ->one();
     * if ($model) {
     * return $model->percent;
     * }
     * return '';
     * }
     */
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }
}