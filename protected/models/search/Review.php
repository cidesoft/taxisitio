<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Review as ReviewModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Review represents the model behind the search form about `app\models\Review`.
 */
class Review extends ReviewModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'ride_id',
								'state_id',
								 
						],
						'integer' 
				],
				[ 
						[ 
								'comment',
								'rate',
								'created_on',
								'created_by_id',
								'driver_id',
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = ReviewModel::find ()->alias('r')->joinWith('createdBy as c')->joinWith('driver as d');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'r.id' => $this->id,
				'r.ride_id' => $this->ride_id,
				'r.state_id' => $this->state_id,
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'comment',
				$this->comment 
		] )->andFilterWhere ( [
				'like',
				'c.full_name',
				$this->created_by_id
		] )->andFilterWhere ( [
				'like',
				'd.full_name',
				$this->driver_id
		] )->andFilterWhere ( [
				'like',
				'r.created_on',
				$this->created_on
		] )->andFilterWhere ( [ 
				'like',
				'rate',
				$this->rate 
		] );
		
		return $dataProvider;
	}
}
