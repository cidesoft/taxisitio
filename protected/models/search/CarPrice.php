<?php

namespace app\models\search;

use app\models\CarPrice as CarPriceModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarPrice represents the model behind the search form about `app\models\CarPrice`.
 */
class CarPrice extends CarPriceModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'type_id' 
						],
						'integer' 
				],
				[ 
						[ 
								'base_price',
								'price_min',
								'price_mile',
								'create_time',
								'country_id' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = CarPriceModel::find ();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		$dataProvider->sort->attributes ['price_mile'] = [ 
				'asc' => [ 
						'CAST(price_mile as UNSIGNED)' => SORT_ASC 
				],
				'desc' => [ 
						'CAST(price_mile as UNSIGNED)' => SORT_DESC 
				] 
		];
		$dataProvider->sort->attributes ['base_price'] = [
				'asc' => [
						'CAST(base_price as UNSIGNED)' => SORT_ASC
				],
				'desc' => [
						'CAST(base_price as UNSIGNED)' => SORT_DESC
				]
		];
		$dataProvider->sort->attributes ['price_min'] = [
				'asc' => [
						'CAST(price_min as UNSIGNED)' => SORT_ASC
				],
				'desc' => [
						'CAST(price_min as UNSIGNED)' => SORT_DESC
				]
		];
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'id' => $this->id,
				'type_id' => $this->type_id 
		] );
		
		if ($this->country_id) {
			$query->joinWith ( [ 
					'country as c' 
			] );
		}
		
		$query->andFilterWhere ( [ 
				'like',
				'price_mile',
				$this->price_mile 
		] )->andFilterWhere ( [ 
				'like',
				'base_price',
				$this->base_price 
		] )->andFilterWhere ( [ 
				'like',
				'price_min',
				$this->price_min 
		] )->andFilterWhere ( [ 
				'like',
				'c.title',
				$this->country_id 
		] );
		
		return $dataProvider;
	}
}
