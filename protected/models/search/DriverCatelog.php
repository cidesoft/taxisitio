<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DriverCatelog as DriverCatelogModel;

/**
 * DriverCatelog represents the model behind the search form about `app\models\DriverCatelog`.
 */
class DriverCatelog extends DriverCatelogModel {
	public $economic_no;
	public $number_series;
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'cp_address_id',
								'address_colonia_id',
								'address_municipality_id',
								'age',
								'marital_status',
								'status',
								'state_id',
								'type_id',
								'create_user_id' 
						],
						'integer' 
				],
				[ 
						[ 
								'first_name',
								'last_name',
								'mother_last_name',
								'contact_no',
								'street_address',
								'address_number',
								'cp_address',
								'address_colonia',
								'address_municipality',
								'address_status',
								'rfc',
								'date_of_birth',
								'ine',
								'curp',
								'date_added',
								'license_no',
								'expiration_date',
								'hard_working_time',
								'comment',
								'avg_rating',
								'image_file',
								'low_date',
								'create_time',
								'update_time',
								'economic_no',
								'number_series' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = DriverCatelogModel::find ()->alias('dc')->joinWith('vehicleCatelog as vc');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'dc.id' => $this->id,
				'dc.cp_address_id' => $this->cp_address_id,
				'dc.address_colonia_id' => $this->address_colonia_id,
				'dc.address_municipality_id' => $this->address_municipality_id,
				'dc.date_of_birth' => $this->date_of_birth,
				'dc.age' => $this->age,
				'dc.marital_status' => $this->marital_status,
				'dc.date_added' => $this->date_added,
				'dc.expiration_date' => $this->expiration_date,
				'dc.hard_working_time' => $this->hard_working_time,
				'dc.status' => $this->status,
				'dc.low_date' => $this->low_date,
				'dc.state_id' => $this->state_id,
				'dc.type_id' => $this->type_id,
				'dc.create_time' => $this->create_time,
				'dc.update_time' => $this->update_time,
				'dc.create_user_id' => $this->create_user_id 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'dc.first_name',
				$this->first_name 
		] )->andFilterWhere ( [ 
				'like',
				'dc.last_name',
				$this->last_name 
		] )->andFilterWhere ( [ 
				'like',
				'dc.mother_last_name',
				$this->mother_last_name 
		] )->andFilterWhere ( [ 
				'like',
				'dc.contact_no',
				$this->contact_no 
		] )->andFilterWhere ( [ 
				'like',
				'dc.street_address',
				$this->street_address 
		] )->andFilterWhere ( [ 
				'like',
				'dc.address_number',
				$this->address_number 
		] )->andFilterWhere ( [ 
				'like',
				'dc.cp_address',
				$this->cp_address 
		] )->andFilterWhere ( [ 
				'like',
				'dc.address_colonia',
				$this->address_colonia 
		] )->andFilterWhere ( [ 
				'like',
				'dc.address_municipality',
				$this->address_municipality 
		] )->andFilterWhere ( [ 
				'like',
				'dc.address_status',
				$this->address_status 
		] )->andFilterWhere ( [ 
				'like',
				'dc.rfc',
				$this->rfc 
		] )->andFilterWhere ( [ 
				'like',
				'dc.ine',
				$this->ine 
		] )->andFilterWhere ( [ 
				'like',
				'dc.curp',
				$this->curp 
		] )->andFilterWhere ( [ 
				'like',
				'dc.license_no',
				$this->license_no 
		] )->andFilterWhere ( [ 
				'like',
				'dc.comment',
				$this->comment 
		] )->andFilterWhere ( [ 
				'like',
				'dc.avg_rating',
				$this->avg_rating 
		] )->andFilterWhere ( [ 
				'like',
				'vc.economic_no',
				$this->economic_no 
		] )->andFilterWhere ( [ 
				'like',
				'vc.number_series',
				$this->number_series 
		] );
		
		return $dataProvider;
	}
}
