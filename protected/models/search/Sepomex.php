<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Sepomex as SepomexModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Sepomex represents the model behind the search form about `app\models\Sepomex`.
 */
class Sepomex extends SepomexModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'settlement_post',
								'state_code',
								'office_code',
								'type_of_settlement_code',
								'municipality_code',
								'unique_identifier_settling',
								'state_id',
								'type_id',
								'created_by_id' 
						],
						'integer' 
				],
				[ 
						[ 
								'settlement',
								'type_of_settlemet',
								'municipality',
								'state',
								'city',
								'created_on',
								'updated_on',
								'zipcode' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = SepomexModel::find ();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'id' => $this->id,
				'settlement_post' => $this->settlement_post,
				'state_code' => $this->state_code,
				'office_code' => $this->office_code,
				'type_of_settlement_code' => $this->type_of_settlement_code,
				'municipality_code' => $this->municipality_code,
				'unique_identifier_settling' => $this->unique_identifier_settling,
				'state_id' => $this->state_id,
				'type_id' => $this->type_id,
				'created_on' => $this->created_on,
				'updated_on' => $this->updated_on,
				'created_by_id' => $this->created_by_id 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'settlement',
				$this->settlement 
		] )->andFilterWhere ( [ 
				'like',
				'type_of_settlemet',
				$this->type_of_settlemet 
		] )->andFilterWhere ( [ 
				'like',
				'zipcode',
				$this->zipcode 
		] )->andFilterWhere ( [ 
				'like',
				'municipality',
				$this->municipality 
		] )->andFilterWhere ( [ 
				'like',
				'state',
				$this->state 
		] )->andFilterWhere ( [ 
				'like',
				'city',
				$this->city 
		] );
		
		return $dataProvider;
	}
	public function searchFilter($params, $page) {
		$query = SepomexModel::find ();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				],
				'pagination' => [ 
						'pageSize' => '20',
						'page' => $page 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'like',
				'zipcode',
				$this->zipcode . '%',
				false 
		] );
		
		return $dataProvider;
	}
	public function searchSepomex($params, $settlement = null, $municipality = null, $page = null) {
		$query = SepomexModel::find ();
		
		if (! empty ( $settlement )) {
			$query->andFilterWhere ( [ 
					'like',
					'settlement',
					$settlement/*  .'%' */
					 
			] );
		} else {
			
			$query->andFilterWhere ( [ 
					'like',
					'municipality',
					$municipality /* .'%' */
					
			] );
		}
	/* 	print_r ( $query->createCommand ()->rawSql );
		exit (); */
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				],
				'pagination' => [ 
						'pageSize' => '10',
						'page' => $page 
				] 
		] );
		
		return $dataProvider;
	}
}
