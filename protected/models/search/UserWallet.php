<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\UserWallet as UserWalletModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserWallet represents the model behind the search form about `app\models\UserWallet`.
 */
class UserWallet extends UserWalletModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'state_id',
								'type_id' 
						
						],
						'integer' 
				],
				[ 
						[ 
								'amount',
								'currency_code',
								'currency_symbol',
								'created_on',
								'updated_on',
								'created_by_id' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = UserWalletModel::find ()->alias('u')->joinWith ( 'createUser as c' );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		$dataProvider->sort->attributes ['amount'] = [ 
				'asc' => [ 
						'CAST(amount as UNSIGNED)' => SORT_ASC 
				],
				'desc' => [ 
						'CAST(amount as UNSIGNED)' => SORT_DESC 
				] 
		];
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'u.state_id' => $this->state_id,
				'u.type_id' => $this->type_id,
				'u.updated_on' => $this->updated_on 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'amount',
				$this->amount 
		] )->andFilterWhere ( [
				'like',
				'u.id',
				$this->id
		] )->andFilterWhere ( [ 
				'like',
				'currency_code',
				$this->currency_code 
		] )->andFilterWhere ( [ 
				'like',
				'c.first_name',
				$this->created_by_id 
		] )->andFilterWhere ( [ 
				'like',
				'created_on',
				$this->created_on 
		] )->andFilterWhere ( [ 
				'like',
				'currency_symbol',
				$this->currency_symbol 
		] );
		
		return $dataProvider;
	}
}
