<?php
namespace app\models\search;

use app\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'zipcode',
                    'gender',
                    
                    'tos',
                    'role_id',
                    'state_id',
                    // 'login_error_count',
                    'create_user_id'
                ],
                'integer'
            ],
            [
                [
                    'first_name',
                    'last_name',
                    'full_name',
                    'email',
                    'password',
                    'contact_no',
                    'address',
                    'city',
                    'is_verified',
                    'country',
                    'date_of_birth',
                    'about_me',
                    'image_file',
                    'activation_key',
                    'create_time',
                    'avg_rating'
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->where([
            'role_id' => User::ROLE_PASSENGER
        ]);
        
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        
        $this->load($params);
        $this->valid = false;
        if (! $this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'like',
            'first_name',
            $this->first_name
        ])
            ->andFilterWhere([
            'like',
            'id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'last_name',
            $this->last_name
        ])
            ->andFilterWhere([
            'like',
            'email',
            $this->email
        ])
            ->andFilterWhere([
            'like',
            'contact_no',
            $this->contact_no
        ]);
        
        return $dataProvider;
    }

    public function searchdriver($params)
    {
        $query = User::find()->where([
            'role_id' => User::ROLE_DRIVER
        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        
        $this->load($params);
        $this->valid = false;
        if (! $this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'state_id' => $this->state_id,
            'is_verified' => $this->is_verified
        ]);
        
        $query->andFilterWhere([
            'like',
            'first_name',
            $this->first_name
        ])
            ->andFilterWhere([
            'like',
            'id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'last_name',
            $this->last_name
        ])
            ->andFilterWhere([
            'like',
            'email',
            $this->email
        ])
            ->andFilterWhere([
            'like',
            'contact_no',
            $this->contact_no
        ]);
        
        return $dataProvider;
    }

    public function searchCustomer($params, $user_id, $page = null)
    {
        $query = User::find()->filterWhere([
            'like',
            'full_name',
            $user_id /* .'%' */
            
        ])->andWhere(['role_id' => User::ROLE_PASSENGER]);
        
       
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        
        return $dataProvider;
    }
}
