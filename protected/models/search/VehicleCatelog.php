<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VehicleCatelog as VehicleCatelogModel;

/**
 * VehicleCatelog represents the model behind the search form about `app\models\VehicleCatelog`.
 */
class VehicleCatelog extends VehicleCatelogModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'assigned_base', 'status', 'state_id', 'type_id', 'create_user_id'], 'integer'],
            [['economic_no', 'brand', 'submarca', 'model', 'placas', 'number_series', 'date_added', 'secure_policy', 'insurance_carrier', 'number_concession', 'maturity_concession', 'owner_name', 'owner_address', 'assigned_driver', 'front_photo', 'rear_photo', 'right_photo', 'left_photo', 'low_date', 'create_time', 'update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function beforeValidate(){
            return true;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VehicleCatelogModel::find();

		        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
						'defaultOrder' => [
								'id' => SORT_DESC
						]
				]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_added' => $this->date_added,
            'maturity_concession' => $this->maturity_concession,
            'assigned_base' => $this->assigned_base,
            'status' => $this->status,
            'low_date' => $this->low_date,
            'state_id' => $this->state_id,
            'type_id' => $this->type_id,
            'create_time' => $this->create_time,
            'update_time' => $this->update_time,
            'create_user_id' => $this->create_user_id,
        ]);

        $query->andFilterWhere(['like', 'economic_no', $this->economic_no])
            ->andFilterWhere(['like', 'brand', $this->brand])
            ->andFilterWhere(['like', 'submarca', $this->submarca])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'placas', $this->placas])
            ->andFilterWhere(['like', 'number_series', $this->number_series])
            ->andFilterWhere(['like', 'secure_policy', $this->secure_policy])
            ->andFilterWhere(['like', 'insurance_carrier', $this->insurance_carrier])
            ->andFilterWhere(['like', 'number_concession', $this->number_concession])
            ->andFilterWhere(['like', 'owner_name', $this->owner_name])
            ->andFilterWhere(['like', 'owner_address', $this->owner_address])
            ->andFilterWhere(['like', 'assigned_driver', $this->assigned_driver])
            ->andFilterWhere(['like', 'front_photo', $this->front_photo])
            ->andFilterWhere(['like', 'rear_photo', $this->rear_photo])
            ->andFilterWhere(['like', 'right_photo', $this->right_photo])
            ->andFilterWhere(['like', 'left_photo', $this->left_photo]);

        return $dataProvider;
    }
}
