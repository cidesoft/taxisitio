<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Notification as NotificationModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Notification represents the model behind the search form about `app\models\Notification`.
 */
class Notification extends NotificationModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'model_id',
								'state_id',
								'type_id',
						],
						'integer' 
				],
				[ 
						[ 
								'message',
								'user_id',
								'model_type',
								'create_time',
								'update_time',
								'created_by_id' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = NotificationModel::find ()->alias('n')->joinWith('createdBy as c')->joinWith('driver as d');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'n.id' => $this->id,
				'n.model_id' => $this->model_id,
				'n.state_id' => $this->state_id,
				'n.type_id' => $this->type_id,
				'n.update_time' => $this->update_time,
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'n.message',
				$this->message 
		] )->andFilterWhere ( [ 
				'like',
				'n.model_type',
				$this->model_type 
		] )->andFilterWhere ( [
				'like',
				'd.first_name',
				$this->user_id
		] )->andFilterWhere ( [
				'like',
				'n.create_time',
				$this->create_time
		] )->andFilterWhere ( [
				'like',
				'c.first_name',
				$this->created_by_id
		] );
		
		return $dataProvider;
	}
}
