<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Transaction as TransactionModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Transaction represents the model behind the search form about `app\models\Transaction`.
 */
class Transaction extends TransactionModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'wallet_history_id',
								'ride_id',
								'state_id',
								'type_id',
								
						],
						'integer' 
				],
				[ 
						[ 
								'amount',
								'transaction_id',
								'created_on',
								'updated_on',
								'created_by_id' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = TransactionModel::find ()->alias('t')->joinWith('createUser as c');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				't.id' => $this->id,
				't.wallet_history_id' => $this->wallet_history_id,
				't.ride_id' => $this->ride_id,
				't.state_id' => $this->state_id,
				't.type_id' => $this->type_id,
				't.updated_on' => $this->updated_on,
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'amount',
				$this->amount 
		] )->andFilterWhere ( [
				'like',
				'c.full_name',
				$this->created_by_id
		] )->andFilterWhere ( [ 
				'like',
				'transaction_id',
				$this->transaction_id 
		] )->andFilterWhere ([
				'like',
				'created_on',
				$this->created_on
		]);
		
		return $dataProvider;
	}
}
