<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Complain as ComplainModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Complain represents the model behind the search form about `app\models\Complain`.
 */
class Complain extends ComplainModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'ride_id',
								'state_id' 
						],
						'integer' 
				],
				[ 
						[ 
								'comment',
								'driver_id',
								'created_on',
								'updated_on',
								'created_by_id' 
						
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = ComplainModel::find ()->alias ( 'c' )->joinWith ( 'createdBy as cb' )->joinWith ( 'driver as d' );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'c.state_id' => $this->state_id,
				'c.updated_on' => $this->updated_on 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'comment',
				$this->comment 
		] )->andFilterWhere ( [
				'like',
				'ride_id',
				$this->ride_id
		] )->andFilterWhere ( [
				'like',
				'c.id',
				$this->id
		] )->andFilterWhere ( [ 
				'like',
				'cb.first_name',
				$this->created_by_id 
		] )->andFilterWhere ( [
				'like',
				'd.first_name',
				$this->driver_id
		] )->andFilterWhere ( [ 
				'like',
				'created_on',
				$this->created_on 
		] );
		
		return $dataProvider;
	}
}
