<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\WalletHistory as WalletHistoryModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * WalletHistory represents the model behind the search form about `app\models\WalletHistory`.
 */
class WalletHistory extends WalletHistoryModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'wallet_id',
								'ride_id',
								'state_id',
								'type_id',
								'created_by_id' 
						],
						'integer' 
				],
				[ 
						[ 
								'initial_amount',
								'transaction_amount',
								'total_balance',
								'transaction_id',
								'comment',
								'created_on',
								'updated_on' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = WalletHistoryModel::find ();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'id' => $this->id,
				'wallet_id' => $this->wallet_id,
				'ride_id' => $this->ride_id,
				'state_id' => $this->state_id,
				'type_id' => $this->type_id,
				'created_on' => $this->created_on,
				'updated_on' => $this->updated_on,
				'created_by_id' => $this->created_by_id 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'initial_amount',
				$this->initial_amount 
		] )->andFilterWhere ( [ 
				'like',
				'transaction_amount',
				$this->transaction_amount 
		] )->andFilterWhere ( [ 
				'like',
				'total_balance',
				$this->total_balance 
		] )->andFilterWhere ( [ 
				'like',
				'transaction_id',
				$this->transaction_id 
		] )->andFilterWhere ( [ 
				'like',
				'comment',
				$this->comment 
		] );
		
		return $dataProvider;
	}
}
