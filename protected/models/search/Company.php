<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company as CompanyModel;

/**
 * Buscador de empresas
 *
 * @author Carlos Mendoza <inhack20@gmail.com>
 */
class Company extends CompanyModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'created_by_id',
                    'country_id',
                ],
                'integer'
            ],
            [
                [
                    'enabled',
                ],
                'boolean'
            ],
            [
                [
                    'name',
                    'created_on',
                ],
                'safe'
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params        	
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere([
            'like',
            'name',
            $this->name
        ])->andFilterWhere([
            'like',
            'enabled',
            $this->enabled
        ])->andFilterWhere([
            'like',
            'created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
