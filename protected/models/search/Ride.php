<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\Ride as RideModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Ride represents the model behind the search form about `app\models\Ride`.
 */
class Ride extends RideModel {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'number_of_passengers',
								'number_of_bags',
								'is_hourly',
								'number_of_hours',
								'journey_type',
								'car_price_id',
								'state_id',
								'type_id',
								
						],
						'integer' 
				],
				[ 
						[ 
								'amount',
								'location',
								'location_lat',
								'location_long',
								'destination',
								'destination_lat',
								'destination_long',
								'journey_time',
								'start_time',
								'end_time',
								'create_time',
								'update_time' ,
								'create_user_id',
								'driver_id',
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function beforeValidate() {
		return true;
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = RideModel::find ()->alias('r')->joinWith('createUser as c')->joinWith('driver as d');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'sort' => [ 
						'defaultOrder' => [ 
								'id' => SORT_DESC 
						] 
				] 
		] );
		
		if (! ($this->load ( $params ) && $this->validate ())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere ( [ 
				'r.id' => $this->id,
				'r.number_of_passengers' => $this->number_of_passengers,
				'r.number_of_bags' => $this->number_of_bags,
				'r.is_hourly' => $this->is_hourly,
				'r.number_of_hours' => $this->number_of_hours,
				'r.journey_time' => $this->journey_time,
				'r.journey_type' => $this->journey_type,
				'r.car_price_id' => $this->car_price_id,
				'r.start_time' => $this->start_time,
				'r.end_time' => $this->end_time,
				'r.state_id' => $this->state_id,
				'r.type_id' => $this->type_id,
				'r.create_time' => $this->create_time,
				'r.update_time' => $this->update_time,
				
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'amount',
				$this->amount 
		] )->andFilterWhere ( [ 
				'like',
				'location',
				$this->location 
		] )->andFilterWhere ( [ 
				'like',
				'location_lat',
				$this->location_lat 
		] )->andFilterWhere ( [ 
				'like',
				'location_long',
				$this->location_long 
		] )->andFilterWhere ( [
				'like',
				'd.first_name',
				$this->driver_id
		] )->andFilterWhere ( [
				'like',
				'c.first_name',
				$this->create_user_id
		] )->andFilterWhere ( [ 
				'like',
				'destination',
				$this->destination 
		] )->andFilterWhere ( [ 
				'like',
				'destination_lat',
				$this->destination_lat 
		] )->andFilterWhere ( [ 
				'like',
				'destination_long',
				$this->destination_long 
		] );
		
		return $dataProvider;
	}
}
