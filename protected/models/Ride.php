<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_ride".
 *
 * @property integer $id
 * @property string $amount
 * @property string $location
 * @property string $location_lat
 * @property string $location_long
 * @property string $destination
 * @property string $destination_lat
 * @property string $destination_long
 * @property integer $number_of_passengers
 * @property integer $number_of_bags
 * @property integer $is_hourly
 * @property integer $number_of_hours
 * @property string $journey_time
 * @property integer $journey_type
 * @property integer $car_price_id
 * @property string $start_time
 * @property string $end_time
 * @property integer $state_id
 * @property integer $type_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $driver_id
 * @property integer $create_user_id
 *
 */
namespace app\models;

use app\components\FirebaseNotifications;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use app\models\Company;

class Ride extends \app\components\TActiveRecord
{
    /**
     * Rango de distancia
     */
    const RANGE_DISTANCE = 5;
    
    const RIDE_LATER = 2;

    const RIDE_NOW = 1;

    const RIDE_UPDATED = 1;

    const IS_HOURLY = 1;

    const NOT_HOURLY = 0;

    const IS_PET = 1;

    const NO_PET = 0;

    const STATE_NEW = 1;

    const STATE_ACCEPTED = 2;

    const STATE_REJECTED = 3;

    // rejected by Driver
    const STATE_CANCELLED = 4;

    // Cancelled by User
    const STATE_DRIVER_ARRIVED = 5;

    const STATE_STARTED = 6;

    const STATE_COMPLETED = 7;

    const STATE_PAID = 8;

    public function __toString()
    {
        return (string) $this->amount;
    }

    public static function getStateOptions()
    {
        return [
            self::STATE_NEW => \Yii::t('app', 'New'),
            self::STATE_ACCEPTED => \Yii::t('app', 'Accepted'),
            self::STATE_REJECTED => \Yii::t('app', 'Rejected'),
            self::STATE_CANCELLED => \Yii::t('app', 'Cancelled'),
            self::STATE_DRIVER_ARRIVED => \Yii::t('app', 'Driver Arrived'),
            self::STATE_STARTED => \Yii::t('app', 'Started'),
            self::STATE_COMPLETED => \Yii::t('app', 'Completed'),
            self::STATE_PAID => \Yii::t('app', 'Paid')
        ];
    }

    public static function getJourneyOptions($id = null)
    {
        $list = [
            self::RIDE_LATER => \Yii::t('app', 'Ride Later'),
            self::RIDE_NOW => \Yii::t('app', 'Ride Now')
        ];
        if ($id == null) {
            return $list;
        }
        return isset($list[$id]) ? $list[$id] : \Yii::t('app', 'Not Defined');
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : \Yii::t('app', 'Not Defined');
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_NEW => "primary",
            self::STATE_ACCEPTED => "primary",
            self::STATE_REJECTED => "danger",
            self::STATE_CANCELLED => "danger",
            self::STATE_DRIVER_ARRIVED => "primary",
            self::STATE_STARTED => "primary",
            self::STATE_COMPLETED => "success",
            self::STATE_PAID => "success"
        ];
        return isset($list[$this->state_id]) ? Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : \Yii::t('app', 'Not Defined');
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->create_time))
                $this->create_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->update_time))
                $this->update_time = (string)new \app\services\util\MyDateTime();
            if (! isset($this->create_user_id))
                if (is_a(\Yii::$app, 'yii\web\Application'))
                    $this->create_user_id = \Yii::$app->user->id;
                else
                    $this->create_user_id = self::findAdmin();
        } else {
            $this->update_time = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ride}}';
    }

    public function findAdmin()
    {
        $model = User::find()->where([
            'role_id' => User::ROLE_ADMIN
        ])->one();
        if (! empty($model))
            return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'location',
                    'location_lat',
                    'location_long',
                    'destination',
                    'destination_lat',
                    'destination_long',
                    'number_of_passengers',
                    'number_of_bags',
                    'is_hourly',
                    'car_price_id',
                    'state_id',
                    'country_code'
                ],
                'required'
            ],
            [
                [
                    
                    'is_hourly',
                    'number_of_hours',
                    'journey_type',
                    'car_price_id',
                    'state_id',
                    'type_id',
                    'driver_id',
                    'create_user_id',
                    'is_pet',
                    'coupon_id',
                    'is_updated'
                ],
                'integer'
            ],
            [
                [
                    'journey_time',
                    'start_time',
                    'end_time',
                    'create_time',
                    'update_time',
                    'number_of_passengers',
                    'number_of_bags',
                    'amount',
                    'country_code',
                    'updated_lat',
                    'updated_long'
                ],
                'safe'
            ],
            [
                [
                    
                    'location',
                    'location_lat',
                    'location_long',
                    'destination',
                    'destination_lat',
                    'destination_long'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'car_price_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarPrice::className(),
                'targetAttribute' => [
                    'car_price_id' => 'id'
                ]
            ],
            [
                [
                    'create_user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'create_user_id' => 'id'
                ]
            ],
            [
                [
                    'driver_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'driver_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'amount' => \Yii::t('app', 'Amount'),
            'location' => \Yii::t('app', 'Source'),
            'location_lat' => \Yii::t('app', 'Location Latitude'),
            'location_long' => \Yii::t('app', 'Location Longitude'),
            'destination' => \Yii::t('app', 'Destination'),
            'destination_lat' => \Yii::t('app', 'Destination Latitude'),
            'destination_long' => \Yii::t('app', 'Destination Longitude'),
            'updated_lat' => \Yii::t('app', 'Updated Latitude'),
            'updated_long' => \Yii::t('app', 'Updated Longitude'),
            'number_of_passengers' => \Yii::t('app', 'Number Of Passengers'),
            'is_pet' => \Yii::t('app', 'Ride With Pet'),
            'number_of_bags' => \Yii::t('app', 'Number Of Bags'),
            'is_hourly' => \Yii::t('app', 'Is Hourly'),
            'number_of_hours' => \Yii::t('app', 'Number Of Hours'),
            'journey_time' => \Yii::t('app', 'Journey Time'),
            'journey_type' => \Yii::t('app', 'Journey Type'),
            'car_price_id' => \Yii::t('app', 'Car Price'),
            'start_time' => \Yii::t('app', 'Start Time (in UTC)'),
            'is_updated' => \Yii::t('app', 'Is Updated'),
            'end_time' => \Yii::t('app', 'End Time (in UTC)'),
            'state_id' => \Yii::t('app', 'State'),
            'type_id' => \Yii::t('app', 'Type'),
            'create_time' => \Yii::t('app', 'Create Time (in UTC)'),
            'update_time' => \Yii::t('app', 'Update Time (in UTC)'),
            'driver_id' => \Yii::t('app', 'Driver'),
            'country_code' => \Yii::t('app', 'Country Code'),
            'create_user_id' => \Yii::t('app', 'Passenger')
        ];
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'create_user_id'
        ]);
    }

    public function getDriver()
    {
        return $this->hasOne(User::className(), [
            'id' => 'driver_id'
        ]);
    }

    public function getCarPrice()
    {
        return $this->hasOne(CarPrice::className(), [
            'id' => 'car_price_id'
        ]);
    }

    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), [
            'ride_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getReview()
    {
        return $this->hasOne(Review::className(), [
            'ride_id' => 'id'
        ]);
    }

    public function getNotification()
    {
        return $this->hasMany(Notification::className(), [
            'model_id' => 'id'
        ])->orderBy('id desc');
    }

    public function getCoupon()
    {
        return $this->hasOne(Coupon::className(), [
            'id' => 'coupon_id'
        ]);
    }
    
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
          ->viaTable('tbl_ride_company', ['ride_id' => 'id']);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        return $relations;
    }

    public function beforeDelete()
    {
        WalletHistory::deleteRelatedAll([
            'ride_id' => $this->id
        ]);
        Review::deleteRelatedAll([
            'ride_id' => $this->id
        ]);
        Transaction::deleteRelatedAll([
            'ride_id' => $this->id
        ]);
        Complain::deleteRelatedAll([
            'ride_id' => $this->id
        ]);
        foreach ($this->companies as $company) {
            $this->unlink("companies", $company,true);
        }
        
        return parent::beforeDelete();
    }

    public function getRoleids($id)
    {
        $user = User::find()->where([
            'id' => $id
        ])->one();
        if (! empty($user)) {
            return $user->role_id;
        }
    }

    public function getComplain()
    {
        return $this->hasMany(Complain::className(), [
            'ride_id' => 'id'
        ]);
    }

    public function asJson($car_price = false, $driver_detail = false, $passenger_detail = false, $notification = true)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['amount'] = (string) $this->amount;
        
        // if ($notification == true) {
        // if ($this->is_updated !== RIDE::RIDE_UPDATED) {
        // if ($this->state_id <= Ride::STATE_NEW && \yii::$app->user->identity->role_id == User::ROLE_PASSENGER)
        // $json['amount'] = (string) $this->getEstimatedAmount();
        // else {
        // if ($this->state_id <= Ride::STATE_NEW && \yii::$app->user->identity->role_id == User::ROLE_DRIVER)
        // $json['amount'] = (string) $this->getEstamount();
        // else
        // $json['amount'] = $this->amount;
        // }
        // } else {
        // $json['amount'] = $this->amount;
        // }
        // }
        
        // if ($this->journey_type == Ride::RIDE_LATER && $this->state_id == Ride::STATE_NEW) {
        // $later = $this->getEstimatedAmount();
        // } else {
        // $later = '';
        // }
        // $json['later_amount'] = $later;
        
        // if ($this->is_updated !== RIDE::RIDE_UPDATED) {
        // if ($this->state_id <= Ride::STATE_NEW && \yii::$app->user->identity->role_id == User::ROLE_PASSENGER)
        // $json['customer_amount'] = $this->getEstimatedAmount();
        // }
        
        $json['amt_ios_notification'] = $this->amount ? $this->amount : '';
        $json['remaining_balance'] = $this->balance ? $this->balance : '';
        $json['location'] = $this->location;
        $json['location_lat'] = $this->location_lat;
        $json['location_long'] = $this->location_long;
        $json['destination'] = $this->destination;
        $json['destination_lat'] = $this->destination_lat;
        $json['destination_long'] = $this->destination_long;
        $json['updated_lat'] = $this->updated_lat;
        $json['updated_long'] = $this->updated_long;
        $json['number_of_passengers'] = (string) $this->number_of_passengers;
        $json['number_of_bags'] = (string) $this->number_of_bags;
        $json['is_pet'] = ($this->is_pet == RIDE::IS_PET) ? true : false;
        $json['is_updated'] = ($this->is_updated == RIDE::RIDE_UPDATED) ? true : false;
        $json['is_hourly'] = ($this->is_hourly == RIDE::IS_HOURLY) ? true : false;
        $json['coupon_applied'] = (null !== $this->checkCoupon()) ? $this->checkCoupon() : false;
        if ($this->is_hourly == RIDE::IS_HOURLY) {
            $json['number_of_hours'] = (string) $this->number_of_hours;
        }
        
        $json['journey_type'] = (int) $this->journey_type;
        
        if ($this->journey_type == Ride::RIDE_LATER) {
            $json['journey_time'] = $this->journey_time;
        }
        
        if ($car_price == true) {
            $json['car_price_id'] = $this->carPrice ? $this->carPrice->asJson() : '';
        }
        
        $json['start_time'] = ! empty($this->start_time) ? $this->start_time : '';
        $json['end_time'] = ! empty($this->end_time) ? $this->end_time : '';
        $json['state_id'] = (int) $this->state_id;
        $json['create_time'] = $this->create_time;
        
        if ($driver_detail == true) {
            $json['driver_detail'] = ! empty($this->driver) ? $this->driver->asJson() : '';
        }
        
        if ($passenger_detail == true) {
            $json['passenger_detail'] = ! empty($this->createUser) ? $this->createUser->asPassengerJson() : '';
        }
        return $json;
    }

    public function checkCoupon()
    {
        $applied = CouponApplied::findOne([
            'created_by_id' => $this->create_user_id
        ]);
        if (! empty($applied))
            return true;
        return false;
    }

    public function saveUpdateNotification($id, $toId)
    {
        $model = new Notification();
        $model->user_id = $toId;
        $model->message = \Yii::t("app",'Ride Updated');
        $model->model_id = $this->id;
        $model->type_id = $this->journey_type;
        $model->model_type = User::className();
        $model->created_by_id = $id;
        if ($this->requestNotification($toId, $model->message)) {
            $model->save();
        }
    }

    public function saveNotification($id)
    {
        $notification = Notification::find()->select([
            'user_id'
        ])
            ->where([
            'type_id' => Ride::STATE_NEW
        ])
            ->andWhere([
            '>=',
            'create_time',
            date("Y-m-d H:i:s", strtotime("-1 minutes"))
        ]);
        $companies = $this->companies;
        $qb = User::find()->select("u.*,	( 6371 * acos( cos( radians({$this->location_lat}) ) * cos( radians( `lat` ) ) * cos( radians( `long` ) - radians({$this->location_long}) ) + sin( radians({$this->location_lat}) ) * sin( radians( `lat` ) ) ) ) AS distance")
            ->having("distance <:distance")
            ->addParams([
            ':distance' => self::RANGE_DISTANCE,
        ])
            ->alias('u')
            ->andWhere([
            'u.role_id' => User::ROLE_DRIVER,
            'u.is_online' => User::IS_ONLINE,
            'u.is_available' => User::IS_AVAILABLE,
            'u.state_id' => User::STATE_ACTIVE
        ])
            ->joinWith([
            'driver as d'
        ])
            ->andWhere([
            'd.vehicle' => $this->carPrice->type_id
        ])
            ->andWhere([
            'not in',
            'u.id',
            $notification
        ]);
        if(count($companies) > 0){
            $cIds = [];
            foreach ($companies as $company) {
                $cIds[] = $company->id;
            }
            $qb->andWhere([
                'd.company_id' => $cIds,
            ]);
        }
        $users = $qb->all();
        if (! empty($users)) {
            foreach ($users as $user) {
                $model = new Notification();
                $model->user_id = $user->id;
                $model->message = \Yii::t("app",'Ride Request');
                $model->model_id = $this->id;
                $model->type_id = $this->journey_type;
                $model->model_type = User::className();
                $model->created_by_id = $id;
                if ($this->requestNotification($user->id, $model->message)) {
                    $model->save();
                }
            }
        }
    }

    public function requestNotification($id, $message)
    {
        $notification = new FirebaseNotifications();
        $androidtoken = [];
        $iostoken = [];
        $tokens = "";
        $data = [];
        $data['controller'] = \yii::$app->controller->id;
        $data['action'] = \yii::$app->controller->action->id;
        $data['message'] = $message;
        
        $user = User::find()->where([
            'id' => $id
        ])->one();
        if (! empty($user)) {
            $data['ride_id'] = $this->id;
            $tokens = $user->authSessions;
            if (count($tokens) > 0) {
                foreach ($tokens as $token) {
                    if ($token->type_id == 1) {
                        $androidtoken[] = $token->device_token;
                    }
                    if ($token->type_id == 2)
                        $iostoken[] = $token->device_token;
                }
                if (! empty($androidtoken)) {
                    return $notification->sendDataMessage($androidtoken, $data) ? true : false;
                }
                if (! empty($iostoken)) {
                    $apns = \Yii::$app->apns;
                    $out = '';
                    if ($user->role_id == User::ROLE_DRIVER) {
                        foreach ($iostoken as $tokn) {
                            $out = $apns->send($tokn, $data['message'], $data, [
                                'sound' => 'default',
                                'badge' => 1
                            ]);
                            \Yii::error('DRIVER_NOTIFY');
                            \Yii::error(\yii\helpers\VarDumper::dumpAsString($out));
                            return $out ? true : false;
                        }
                    }
                    if ($user->role_id == User::ROLE_PASSENGER) {
                        $apns = \Yii::$app->apns1;
                        foreach ($iostoken as $tokn) {
                            $out = $apns->send($tokn, $data['message'], $data, [
                                'sound' => 'default',
                                'badge' => 1
                            ]);
                            \Yii::error('PASSENGER_NOTIFY');
                            \Yii::error(\yii\helpers\VarDumper::dumpAsString($out));
                            return $out ? true : false;
                        }
                    }
                }
            }
        }
        return false;
    }

    public function getBalance()
    {
        $remainig = WalletHistory::find()->where([
            'ride_id' => $this->id
        ])
            ->orderBy([
            'id' => SORT_DESC
        ])
            ->one();
        if (! empty($remainig)) {
            if ($remainig->total_balance < 0) {
                return $remainig->total_balance;
            }
        }
    }

    public function getEstamount()
    {
        $distance = $this->distance($this->location_lat, $this->location_long, $this->destination_lat, $this->destination_long);
        $amount = 0;
        if ($this->carPrice) {
            $carPrice = $this->carPrice;
            $amount = $carPrice->base_price + ($distance['time'] * $carPrice->price_min) + ($distance['distance'] * $carPrice->price_mile);
        }
        $model = Ride::findOne($this->id);
        if (! empty($model)) {
            if ($model->journey_type == Ride::RIDE_NOW) {
                $coupon = CouponApplied::find()->where([
                    'created_by_id' => \Yii::$app->user->id
                ])->one();
                if (! empty($coupon)) {
                    $model->coupon_id = $coupon->coupon_id;
                    $percent = (int) $coupon->discount;
                    $amt = ($percent / 100) * $amount;
                    $amt = round($amt, 2);
                    if ($coupon->max_amount < $amt)
                        $amount = $amount - $coupon->max_amount;
                    else
                        $amount = $amount - $amt;
                }
            }
            if ($amount <= 0) {
                $amount = 0.00;
            }
            $amount = round($amount, 2);
        }
        return $amount;
    }

    public function getEstimatedAmount()
    {
        $distance = $this->distance($this->location_lat, $this->location_long, $this->destination_lat, $this->destination_long);
        $amount = 0;
        if ($this->carPrice) {
            $carPrice = $this->carPrice;
            $amount = $carPrice->base_price + ($distance['time'] * $carPrice->price_min) + ($distance['distance'] * $carPrice->price_mile);
        }
        $model = Ride::findOne($this->id);
        if (! empty($model)) {
            if ($model->journey_type == Ride::RIDE_NOW) {
                $coupon = CouponApplied::find()->where([
                    'created_by_id' => \Yii::$app->user->id
                ])->one();
                
                if (! empty($coupon)) {
                    $model->coupon_id = $coupon->coupon_id;
                    $percent = (int) $coupon->discount;
                    $amt = ($percent / 100) * $amount;
                    $amt = round($amt, 2);
                    if ($coupon->max_amount < $amt)
                        $amount = $amount - $coupon->max_amount;
                    else
                        $amount = $amount - $amt;
                }
            }
            // elseif (($model->journey_type == Ride::RIDE_LATER) && $model->state_id == Ride::STATE_NEW) {
            // $model->amount = $amount;
            // }
            if ($amount <= 0) {
                $amount = 0.00;
            }
            $model->amount = round($amount, 2);
            $model->save();
        }
        return $amount;
    }

    public function getUpdatedAmount($currentLat, $currentLong, $destinationLat, $destinationLong)
    {
        $distance = $this->distance($this->location_lat, $this->location_long, $currentLat, $currentLong);
        // \Yii::error(VarDumper::dumpAsString( " || Start_Location-" . $this->location_lat . " , " . $this->location_long));
        // \Yii::error(VarDumper::dumpAsString( " || Current_Location-" . $currentLat . " , " . $currentLong));
        $amount = 0;
        if ($this->carPrice) {
            $carPrice = $this->carPrice;
            $amount = $carPrice->base_price + ($distance['time'] * $carPrice->price_min) + ($distance['distance'] * $carPrice->price_mile);
            // \Yii::error(VarDumper::dumpAsString($amount . "START_CURRENT_AMOUNT"));
        }
        $newdistance = $this->distance($currentLat, $currentLong, $destinationLat, $destinationLong);
        // \Yii::error(VarDumper::dumpAsString( " || NEW_Start_Location-" . $currentLat . " , " . $currentLong));
        // \Yii::error(VarDumper::dumpAsString( " || UPDATED_DESTINATION_Location-" . $destinationLat . " , " . $destinationLong));
        $newAmount = 0;
        if ($this->carPrice) {
            $carPrice = $this->carPrice;
            $newAmount = $carPrice->base_price + ($newdistance['time'] * $carPrice->price_min) + ($newdistance['distance'] * $carPrice->price_mile);
            // \Yii::error(VarDumper::dumpAsString($newAmount . "NEW_AMOUNT"));
        }
        $finalAmount = $amount + $newAmount;
        // \Yii::error(VarDumper::dumpAsString($finalAmount . "FINAL_AMOUNT"));
        return $finalAmount;
    }

    public static function currencyConverter($fromCurrency, $toCurrency, $amount)
    {
        $amount = urlencode($amount);
        $fromCurrency = urlencode($fromCurrency);
        $toCurrency = urlencode($toCurrency);
        //TODO falta implementar llave con permisos para calcular el costo
        $rawdata = file_get_contents("http://data.fixer.io/api/latest?access_key=927805356b7078eae5c530d40ff9f6b0&format=0&base=$fromCurrency");
        $to = json_decode($rawdata);
        $amount = isset($to->rates->$toCurrency) ? round(($to->rates->$toCurrency * $amount), 2) : 0;
        return $amount;
        /*
         * $amount = urlencode($amount);
         * $fromCurrency = urlencode($fromCurrency);
         * $toCurrency = urlencode($toCurrency);
         * $rawdata = file_get_contents("http://www.google.com/finance/converter?a=$amount&from=$fromCurrency&to=$toCurrency");
         * $data = explode('bld>', $rawdata);
         * $data = explode($toCurrency, $data[1]);
         * return round($data[0], 2);
         */
    }

    public function getCurrency($countryCode)
    {
        $data = Country::find()->where([
            'country_code' => $countryCode
        ])->one();
        if (! empty($data)) {
            return $data->currency_code;
        }
    }

    public function distance($lat1, $long1, $lat2, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=en&key=AIzaSyDgEavzqVAP_Fi92X6lZJI9uc_ikmbFFwA";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        if (! empty($response_a['rows'][0]['elements'][0]['distance'])) {
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            
            $distance = explode(' ', $dist);
            // $dis = trim($distance[0]);
            $dis = trim($distance[0] / 1000);
        } else {
            $dis = 0;
        }
        
        if (! empty($response_a['rows'][0]['elements'][0]['duration'])) {
            $time = $response_a['rows'][0]['elements'][0]['duration']['value']; // time in seconds
            $est_time = explode(' ', $time);
            $total_time = trim($est_time[0] / 60);
        } else {
            $total_time = 0;
        }
        /*
         * print_r($dis);
         * echo "<br/>";
         * print_r($total_time);exit;
         */
        \Yii::error(VarDumper::dumpAsString($dis . "TOTAL_DISTANCE"));
        \Yii::error(VarDumper::dumpAsString($total_time . "TOTAL_TIME"));
        return array(
            'distance' => $dis,
            'time' => $total_time
        );
    }
}