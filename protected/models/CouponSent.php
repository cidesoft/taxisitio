<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_coupon_sent".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $user_id
 * @property string $coupon_code
 * @property integer $discount
 * @property string $start_date
 * @property string $end_date
 * @property string $created_on
 * @property string $updated_on
 * @property integer $type_id
 * @property integer $state_id
 * @property integer $created_by_id === Related data ===
 * @property Coupon $coupon
 * @property User $createdBy
 * @property User $user
 */
namespace app\models;

use Yii;

class CouponSent extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->coupon_id;
    }

    public static function getTypeOptions()
    {
        return [
            self::TYPE_SPECIFIC_USER => "Specific User",
            self::TYPE_HIGHER_TRIPS => "Higher Trip",
            self::TYPE_COLOGNE => "Cologne",
            self::TYPE_MUNICIPALITY => "Municipality",
            self::TYPE_STATE => "State"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const COUPON_NOT_APPLIED = 0;

    const COUPON_APPLIED = 1;

    const TYPE_SPECIFIC_USER = 0;

    const TYPE_HIGHER_TRIPS = 1;

    const TYPE_TOP_RATED = 2;

    const TYPE_COLOGNE = 3;

    const TYPE_MUNICIPALITY = 4;

    const TYPE_STATE = 5;

    public static function getStateOptions()
    {
        return [
            self::COUPON_NOT_APPLIED => "Not Applied",
            self::COUPON_APPLIED => "Applied"
        
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::COUPON_NOT_APPLIED => "primary",
            self::COUPON_APPLIED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->user_id))
                $this->user_id = Yii::$app->user->id;
            if (! isset($this->start_date))
                $this->start_date = date('Y-m-d');
            if (! isset($this->end_date))
                $this->end_date = date('Y-m-d');
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%coupon_sent}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'coupon_id',
                    'user_id',
                    'coupon_code',
                    'discount',
                    'start_date',
                    'end_date',
                    'created_on',
                    'ride_count'
                ],
                'required'
            ],
            [
                [
                    'coupon_id',
                    'user_id',
                    'discount',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'start_date',
                    'end_date',
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'coupon_code'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'coupon_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Coupon::className(),
                'targetAttribute' => [
                    'coupon_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ],
            [
                [
                    'coupon_code'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'coupon_id' => Yii::t('app', 'Coupon'),
            'user_id' => Yii::t('app', 'User'),
            'coupon_code' => Yii::t('app', 'Coupon Code'),
            'discount' => Yii::t('app', 'Discount (in %)'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'State'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCoupon()
    {
        return $this->hasOne(Coupon::className(), [
            'id' => 'coupon_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['coupon_id'] = [
            'coupon',
            'Coupon',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['user_id'] = [
            'user',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        CouponApplied::deleteRelatedAll([
            'coupon_sent_id' => $this->id
        ]);
        
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
         if ($this->coupon) {
            $json['description'] = $this->coupon->description;
        }
        $json['coupon_code'] = $this->coupon_code;
        $json['discount'] = $this->discount;
        $json['start_date'] = date('Y-m-d', strtotime($this->start_date));
        $json['end_date'] = date('Y-m-d', strtotime($this->end_date));
        $json['ride_count'] = $this->ride_count;
        if ($with_relations) {
            // coupon
            $list = $this->coupon;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['coupon'] = $relationData;
            } else {
                $json['Coupon'] = $list;
            }
            // createdBy
            $list = $this->createdBy;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['CreatedBy'] = $list;
            }
            // user
            $list = $this->user;
            
            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['user'] = $relationData;
            } else {
                $json['User'] = $list;
            }
        }
        return $json;
    }
}
