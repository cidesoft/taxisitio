<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
 
/**
* This is the model class for table "tbl_log".
*
    * @property integer $id
    * @property string $error
    * @property string $api
    * @property string $description
    * @property integer $state_id
    * @property integer $type_id
    * @property string $create_time
    * @property integer $create_user_id
    
    * ===Relative data ===
    
        	* @property User $createUser
    */

namespace app\models;

class Log extends \app\models\base\LogBase
{
    public function beforeSave($insert) {
		return parent::beforeSave ($insert);
	}
	public function beforeDelete() {
		return parent::beforeDelete ();
	}
}