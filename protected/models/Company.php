<?php

namespace app\models;

use Yii;
use app\components\TActiveRecord;

/**
 * This is the model class for table "tbl_company".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enabled
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id
 * @property integer $country_id
 *
 * @property Country $country
 * @property User $createdBy
 * @property Driver[] $drivers
 */
class Company extends TActiveRecord
{

    const STATE_ENABLED = true;
    const STATE_DISABLED = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'enabled', 'created_on', 'updated_on', 'created_by_id','country_id'], 'required'],
            [['enabled', 'created_by_id', 'country_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 120],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'created_by_id' => 'Created By ID',
            'country_id' => 'Country ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by_id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['company_id' => 'id']);
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (!isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (!isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (!isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    public static function getStatusOptions()
    {
        return [
            self::STATE_ENABLED => "Enabled",
            self::STATE_DISABLED => "Disabled",
        ];
    }

    /**
     * Busca las empresas habilitadas
     * @param type $onlyEnabled
     * @return \yii\db\ActiveQuery
     */
    public static function findEnabled($onlyEnabled = true)
    {
        $ar = self::find();
        if($onlyEnabled === true){
            $ar->andWhere([
                "enabled" => self::STATE_ENABLED,
            ]);
        }
        return $ar;
    }
    
    public function asJson()
    {
        $json = [];
        $json ['id'] = $this->id;
        $json ['name'] = $this->name;
        return $json;
    }
    
    public function __toString()
    {
        return $this->name?:"-";
    }
}
