<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/**
 * This is the model class for table "tbl_wallet_history".
 *
 * @property integer $id
 * @property integer $wallet_id
 * @property string $initial_amount
 * @property string $transaction_amount
 * @property string $total_balance
 * @property string $transaction_id
 * @property integer $ride_id
 * @property string $comment
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id
 *
 */
namespace app\models;

use Yii;

class WalletHistory extends \app\components\TActiveRecord
{

    const TYPE_ADD = 1;

    const TYPE_DEBIT = 2;

    public function __toString()
    {
        return (string) $this->wallet_id;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Archieved"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getTypeOptions()
    {
        return [
            self::TYPE_ADD => "Added to wallet",
            self::TYPE_DEBIT => "Debt"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->updated_on))
                $this->updated_on = (string)new \app\services\util\MyDateTime();
            if (! isset($this->created_by_id))
                $this->created_by_id = Yii::$app->user->id;
        } else {
            $this->updated_on = (string)new \app\services\util\MyDateTime();
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wallet_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'wallet_id',
                    'initial_amount',
                    'transaction_amount',
                    'total_balance',
                    'created_on',
                    'updated_on',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'wallet_id',
                    'ride_id',
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on',
                    'initial_amount',
                    'transaction_amount',
                    'total_balance'
                ],
                'safe'
            ],
            [
                [
                    
                    'transaction_id',
                    'comment'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'ride_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Ride::className(),
                'targetAttribute' => [
                    'ride_id' => 'id'
                ]
            ],
            [
                [
                    'wallet_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserWallet::className(),
                'targetAttribute' => [
                    'wallet_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wallet_id' => Yii::t('app', 'Wallet'),
            'initial_amount' => Yii::t('app', 'Initial Amount'),
            'transaction_amount' => Yii::t('app', 'Transaction Amount'),
            'total_balance' => Yii::t('app', 'Total Balance'),
            'transaction_id' => Yii::t('app', 'Transaction'),
            'ride_id' => Yii::t('app', 'Ride'),
            'comment' => Yii::t('app', 'Comment'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Create Time (in UTC)'),
            'updated_on' => Yii::t('app', 'Updated On (in UTC)'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public static function getHasManyRelations()
    {
        $relations = [];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        return $relations;
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function beforeDelete()
    {
        Transaction::deleteRelatedAll([
            'wallet_history_id' => $this->id
        ]);
        return parent::beforeDelete();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['wallet_id'] = $this->wallet_id;
        $json['initial_amount'] = $this->initial_amount;
        $json['transaction_amount'] = $this->transaction_amount;
        $json['total_balance'] = $this->total_balance;
        $json['transaction_id'] = $this->transaction_id;
        $json['ride_id'] = $this->ride_id;
        $json['comment'] = $this->comment;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }
}