<?php

/**
 * Company: ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * Author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_ha_logins".
 * Esta é a classe do modelo para a tabela "tbl_ha_logins".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $loginProvider
 * @property string $loginProviderIdentifier
 *
 */
class HaLogin extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->userId;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ha_logins';
    }

    /**
     * @inheritdoc
     * 
     * @return Matriz as regras de validação.
     */
    public function rules()
    {
        return [
            [
                [
                    'userId',
                    'loginProvider',
                    'loginProviderIdentifier'
                ],
                'required'
            ],
            [
                [
                    'userId'
                ],
                'integer'
            ],
            [
                [
                    'loginProvider'
                ],
                'string',
                'max' => 50
            ],
            [
                [
                    'loginProviderIdentifier'
                ],
                'string',
                'max' => 100
            ],
            [
                [
                    'loginProvider',
                    'loginProviderIdentifier'
                ],
                'unique',
                'targetAttribute' => [
                    'loginProvider',
                    'loginProviderIdentifier'
                ],
                'message' => 'The combination of Login Provider and Login Provider Identifier has already been taken.'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getUser()
    {
        $user = User::find()->where([
            'id' => $this->user_id
        ])->one();
        return $user;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'loginProvider' => Yii::t('app', 'Login Provider'),
            'loginProviderIdentifier' => Yii::t('app', 'Login Provider Identifier')
        ];
    }

    public static function getRelations()
    {
        $relations = [];
        return $relations;
    }
}