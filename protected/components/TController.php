<?php
namespace app\components;

use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TController extends Controller
{

    public $layout = '//main';

    public $menu = [];

    public $top_menu = [];

    public $side_menu = [];

    public $user_menu = [];

    public $tabs_data = null;

    public $tabs_name = null;

    public $dryRun = false;

    public $assetsDir = '@webroot/assets';

    public $ignoreDirs = [];

    public $nav_left = '';
 // nav-left-medium';
    private $_pageCaption = 'Taxi Sitio';

    private $_pageDescription = 'ToXSL Technologies is a Software Development company offering Mobile and Web Applications Development services. We are expert in Android, iOS and Yii Framework. ';

    private $_pageKeywords = 'yii, framework, php';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => [
                    'index',
                    'view',
                    'contact',
                    'about'
                ],
                'duration' => 10,
                'enabled' => \Yii::$app->user->isGuest
            ],
            
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'download'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'download'
                        ],
                        'allow' => true
                    ],
                    [
                        'actions' => [
                            'delete'
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return User::isAdmin();
                        }
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    public function actionDownload($file)
    {
        /*
         * $model = User::findOne ( [
         * 'profile_file' => $profile_file
         * ] );
         */
        $file = UPLOAD_PATH . $file;
        
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        }
    }

    public function sendFile($thumbfile, $name = null)
    {
        if ($name == null)
            $name = basename($thumbfile);
        if (file_exists($thumbfile)) {
            header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Pragma: no-cache');
            $type = self::mime_content_type($thumbfile);
            header("Content-Type: " . $type . "");
            header("Content-Disposition: attachment; filename=\"" . $name . "\";");
            header("Content-Length: " . filesize($thumbfile));
            readfile($thumbfile);
            exit();
        } else
            throw new NotFoundHttpException('The requested page does not exist.');
    }

    public static function mime_content_type($filename)
    {
        if ($filename) {
            $mime_types = array(
                
                'txt' => 'text/plain',
                'htm' => 'text/html',
                'html' => 'text/html',
                'php' => 'text/html',
                'css' => 'text/css',
                'js' => 'application/javascript',
                'json' => 'application/json',
                'xml' => 'application/xml',
                'swf' => 'application/x-shockwave-flash',
                'flv' => 'video/x-flv',
                
                // images
                'png' => 'image/png',
                'jpe' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'gif' => 'image/gif',
                'bmp' => 'image/bmp',
                'ico' => 'image/vnd.microsoft.icon',
                'tiff' => 'image/tiff',
                'tif' => 'image/tiff',
                'svg' => 'image/svg+xml',
                'svgz' => 'image/svg+xml',
                
                // archives
                'zip' => 'application/zip',
                'rar' => 'application/x-rar-compressed',
                'exe' => 'application/x-msdownload',
                'msi' => 'application/x-msdownload',
                'cab' => 'application/vnd.ms-cab-compressed',
                
                // audio/video
                'mp3' => 'audio/mpeg',
                'mp4' => 'video/mp4',
                'qt' => 'video/quicktime',
                'mov' => 'video/quicktime',
                '3gp' => ' video/3gpp',
                
                // adobe
                'pdf' => 'application/pdf',
                'psd' => 'image/vnd.adobe.photoshop',
                'ai' => 'application/postscript',
                'eps' => 'application/postscript',
                'ps' => 'application/postscript',
                
                // ms office
                'doc' => 'application/msword',
                'rtf' => 'application/rtf',
                'xls' => 'application/vnd.ms-excel',
                'ppt' => 'application/vnd.ms-powerpoint',
                
                // open office
                'odt' => 'application/vnd.oasis.opendocument.text',
                'ods' => 'application/vnd.oasis.opendocument.spreadsheet'
            );
            
            $var_d = explode('.', $filename);
            $ext = strtolower(array_pop($var_d));
            if (array_key_exists($ext, $mime_types)) {
                return $mime_types[$ext];
            } elseif (function_exists('finfo_open')) {
                $finfo = finfo_open(FILEINFO_MIME);
                $mimetype = finfo_file($finfo, $filename);
                finfo_close($finfo);
                return $mimetype;
            }
            return 'application/octet-stream';
        }
    }

    public function afterAction($action, $result)
    {
        return parent::afterAction($action, $result);
    }

    public static function cleanRuntimeDir($dir, $delete = false)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            
            $objects = FileHelper::findFiles($dir);
            foreach ($objects as $object) {
                if (unlink($object)) {
                    Yii::$app->session->setFlash('runtime_clean', Yii::t('app', 'Runtime cleaned'));
                }
            }
            reset($objects);
            
            if ($delete) {
                FileHelper::removeDirectory($dir);
            }
        }
    }

    public function cleanAssetsDir()
    {
        $assetsDirs = glob(Yii::getAlias($this->assetsDir) . '/*', GLOB_ONLYDIR);
        foreach ($assetsDirs as $dir) {
            if (in_array(basename($dir), $this->ignoreDirs)) {
                continue;
            }
            if (! $this->dryRun) {
                FileHelper::removeDirectory($dir);
            }
        }
        Yii::$app->session->setFlash('assets_clean', Yii::t('app', 'Assets cleaned'));
    }

    public function getPageCaption()
    {
        if ($this->_pageCaption !== null)
            return $this->_pageCaption;
        else {
            $name = ucfirst(basename($this->getId()));
            if ($this->getAction() !== null && strcasecmp($this->getAction()->getId(), $this->defaultAction))
                return $this->_pageCaption = $name . ' ' . ucfirst($this->getAction()->getId());
            else
                return $this->_pageCaption = $name;
        }
    }

    /**
     *
     * @param string $value
     *            the page heading (or caption)
     */
    public function setPageCaption($value)
    {
        $this->_pageCaption = $value;
    }

    /**
     *
     * @return string the page description (or subtitle). Defaults to the page title + 'page' suffix.
     */
    public function getPageDescription()
    {
        if ($this->_pageDescription !== null)
            return $this->_pageDescription;
        else {
            return Yii::app()->name . ' ' . $this->getPageCaption();
        }
    }

    /**
     *
     * @param string $value
     *            the page description (or subtitle)
     */
    public function setPageDescription($value)
    {
        if (! empty($value))
            $this->_pageDescription = $value;
    }

    /**
     *
     * @param string $value
     *            the page description (or subtitle)
     */
    public function setPageKeywords($value)
    {
        if (! empty($value))
            $this->_pageKeywords = $value . ', ' . $this->_pageKeywords;
    }

    public function getPageKeywords()
    {
        if ($this->_pageKeywords !== null) {
            $list = explode(',', $this->_pageKeywords);
            array_map('trim', $list);
            array_unique($list);
            $this->_pageKeywords = implode(',', $list);
            return $this->_pageKeywords;
        } else {
            return Yii::app()->name . ', ' . $this->getPageCaption();
        }
    }

    public function processSEO($model = null)
    {
        if (\yii::$app->getModule('seoManager')) {
            \yii::$app->seomanager->processSEO($this->id, $this->action->id);
        }
        
        if ($model && ($model instanceof TActiveRecord && ! $model->isNewRecord)) {
            $this->_pageCaption = Html::encode($model->label()) . ' - ' . Html::encode($model) . ' | ' . $this->_pageCaption;
            
            if ($model->hasAttribute('content'))
                $this->_pageDescription = substr(strip_tags($model->content), 0, 150);
            else if ($model->hasAttribute('description'))
                $this->_pageDescription = substr(strip_tags($model->description), 0, 150);
        } elseif ($this->action->id == 'index' && $this->id == 'site') {
            $this->_pageCaption = $this->_pageCaption;
        } else {
            $this->_pageCaption = Inflector::pluralize(Inflector::camel2words(Yii::$app->controller->id)) . '-' . Inflector::camel2words($this->action->id) . ' | ' . $this->_pageCaption;
            
            // if ( isset($this->module))
            // / $this->_pageCaption = Inflector::camel2words ($this->module->id) . ' | ' . $this->_pageCaption;;
        }
        $this->getView()->registerMetaTag([
            'name' => 'description',
            'content' => $this->_pageDescription
        ]);
        $this->getView()->registerMetaTag([
            'name' => 'keywords',
            'content' => $this->_pageKeywords
        ]);
        $this->getView()->registerMetaTag([
            'name' => 'author',
            'content' => '@toxsltech'
        ]);
        
        $this->getView()->title = $this->_pageCaption;
        
        $this->getView()->registerLinkTag([
            'rel' => 'canonical',
            'href' => Url::canonical()
        ]);
        $this->getView()->registerMetaTag([
            'name' => 'google-site-verification',
            'content' => 'OZngflKigK2CwEwC2PGJKDGL4oLU2gnAVKhjG-lhAfQ'
        ]);
    }

    public function render($view, $params = [])
    {
        if (array_key_exists('model', $params)) {
            $this->processSEO($params['model']);
        } else
            $this->processSEO();
        
        return parent::render($view, $params);
        
        return parent::render($view, $params);
        
        return parent::render($view, $params);
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {
            
            default:
            case 'view':
                {
                    $this->menu['add'] = array(
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => User::isAdmin()
                    );
                    $this->menu['manage'] = array(
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => 'Manage',
                        'url' => array(
                            'index'
                        ),
                        'visible' => User::isAdmin()
                    );
                }
                break;
        }
    }

    public function beforeAction($action)
    {
        if (! yii::$app->user->isGuest) {
            $this->layout = 'main';
        } else {
            $this->layout = 'guest-main';
        }
        
        // $this->processSEO ( $model = null );
        return parent::beforeAction($action);
        if (parent::beforeAction($action)) {}
    }

    public function actionPdf()
    {
        Yii::$app->response->format = 'pdf';
        $this->layout = '//print';
        return $this->render('myview', []);
    }

    public function startPanel($name = 'tabpanel1')
    {
        $this->tabs_name = $name;
        $this->tabs_data = array();
    }

    public function addPanel($title, $objects, $relation, $model = null, $addMenu = true)
    {
        $view = Inflector::camel2id($relation);
        if ($addMenu)
            $this->user_menu[] = array(
                'label' => Yii::t('app', 'Add ') . $title,
                'icon' => 'plus ',
                'url' => [
                    $view . '/add',
                    'type' => $model ? get_class($model) : null
                ]
            );
        
        if ($objects) {
            if ($objects instanceof ActiveDataProvider)
                $dataProvider = $objects;
            elseif ($objects instanceof ActiveQuery)
                $dataProvider = new ActiveDataProvider([
                    'query' => $objects
                ]);
            else {
                
                $type = get_class($model);
                
                // $content = $this->renderPartial('/'.$view.'/_grid',['dataProvider'=>$dataProvider,'searchModel'=> null]);
                $this->tabs_data[] = array(
                    'label' => $title,
                    'url' => [
                        "$view/ajax",
                        'type' => "$type",
                        'function' => "$objects",
                        'id' => $model->id
                    ],
                    'active' => count($this->tabs_data) == 0 ? true : false
                );
            }
        }
    }

    public function endPanel()
    {
        echo \yii\jui\Tabs::widget([
            // 'type'=>'tabs', // 'tabs' or 'pills'
            'items' => $this->tabs_data,
            'options' => array(
                'class' => 'tabbable tabs-left ui-tabs-vertical ui-helper-clearfix ui-tabs-vertical'
            )
        ]);
    }

    public function actionAjax($type, $id, $function, $grid = '_ajax-grid')
    {
        $model = $type::findOne([
            'id' => $id
        ]);
        
        if (! empty($model)) {
            if (! ($model->isAllowed()))
                throw new \yii\web\HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
            $function = 'get' . ucfirst($function);
            $dataProvider = new ActiveDataProvider([
                'query' => $model->$function()
            ]);
            
            echo $this->renderAjax($grid, [
                'dataProvider' => $dataProvider,
                'searchModel' => null,
                'enablePushState' => false
            ]);
        }
    }

    public static function showState($model, $key, $index, $column)
    {
        return $model->getStateBadge();
    }

    public function missingAction($actionID)
    {
        return $this->redirect([
            'index'
        ]);
    }

    public function renderNav()
    {
        $nav = [
            [
                'label' => '<i class="fa fa-home"></i> <span>Dashboard</span>',
                'url' => [
                    '/site/index'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'site/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-users" aria-hidden="true"></i> <span>' . User::label(2) . '</span>',
                
                'url' => [
                    '#'
                ],
                'options' => [
                    'class' => 'menu-list'
                ],
                'items' => [
                    [
                        'label' => 'Customers',
                        'url' => [
                            '/user'
                        ]
                    ],
                    [
                        'label' => 'Drivers',
                        'url' => [
                            '/user/driver'
                        ]
                    ]
                ]
            
            ],
            [
                'label' => '<i class="fa fa-money"></i> <span>Car Prices</span>',
                'url' => [
                    '/car-price'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'car-price/index',
                'options' => [
                    'class' => ''
                ]
            ],
				
				/* [ 
						'label' => '<i class="fa fa-money"></i> <span>Catalogue</span>',
						'url' => [ 
								'/driver-catelog' 
						],
						'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'driver-catelog/index',
						'options' => [ 
								'class' => '' 
						] 
				], */
				[
                'label' => '<i class="fa fa-taxi" aria-hidden="true"></i> <span>Ride</span>',
                'url' => [
                    '/ride'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'ride/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-exchange" aria-hidden="true"></i> <span>Transactions</span>',
                'url' => [
                    '/transaction'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'transaction/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-credit-card-alt" aria-hidden="true"></i> <span>Wallets</span>',
                'url' => [
                    '/user-wallet'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'user-wallet/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-gift" aria-hidden="true"></i> <span>Coupons</span>',
                'url' => [
                    '/coupon'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'coupon/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-comment-o" aria-hidden="true"></i> <span>Reviews</span>',
                'url' => [
                    '/review'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'review/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-bell-o" aria-hidden="true"></i> <span>Notifications</span>',
                'url' => [
                    '/notification'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'notification/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span>Complains</span>',
                'url' => [
                    '/complain'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'complain/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-database" aria-hidden="true"></i> <span>Sepomex Data</span>',
                'url' => [
                    '/sepomex'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'sepomex/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-globe" aria-hidden="true"></i> <span>Country</span>',
                'url' => [
                    '/country'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'country/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-file-text-o"></i> <span>Pages</span>',
                'url' => [
                    '/page'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'page/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-briefcase"></i> <span>Companies</span>',
                'url' => [
                    '/company'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'company/index',
                'options' => [
                    'class' => ''
                ]
            ],
            [
                'label' => '<i class="fa fa-database"></i> <span>Backup</span>',
                'url' => [
                    '/backup'
                ],
                'active' => Yii::$app->controller->id . '/' . Yii::$app->controller->action->id == 'backup/index',
                'options' => [
                    'class' => ''
                ]
            ]
        ];
        return $nav;
    }
}
