<?php

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is just an example.
 */
class JiMenu extends \yii\widgets\Menu {
	public function run() {
		if ($this->route === null && Yii::$app->controller !== null) {
			$this->route = Yii::$app->controller->getRoute ();
		}
		if ($this->params === null) {
			$this->params = Yii::$app->request->getQueryParams ();
		}
		$items = $this->normalizeItems ( $this->items, $hasActiveChild );
		
		if (! empty ( $items )) {
			
			foreach ( $items as &$item ) {
				
				$item ['badge-color'] = 'red';
				// if ( empty($item['badge-color'])) $item['badge-color'] = 'yellow';
				
				foreach ( $item ['items'] as &$subItem ) {
					
					$subItem ['badge-color'] = 'yellow';
				}
			}
		}
		// print_r($items);
		$this->renderHtml ( $items );
	}
	protected function renderHtml($items) {
		?>

<div class="navbar-menu clearfix">
	<div class="vd_panel-menu hidden-xs">
		<span data-step="4"
			data-intro="&lt;strong&gt;Expand Button&lt;/strong&gt;&lt;br/&gt;To expand all menu on left navigation menu."
			class="menu" data-action="expand-all" data-placement="bottom"
			data-toggle="tooltip" data-original-title="Expand All"> <i
			class="fa fa-sort-amount-asc"></i>
		</span>
	</div>
	<h3 class="menu-title hide-nav-medium hide-nav-small">Main Menu</h3>
	<div class="vd_menu">
		<ul>
		
		<?php  foreach ( $items as $i => $item) {?>
			
				<?php 	if (! empty ( $item ['items'] )) {?>
				
				<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-<?php echo $item ['icon']?>"></i></span>
					<span class="menu-text"><?php echo $item ['label']?></span> <span
					class="menu-badge"><span class="badge vd_bg-black-30"><i
							class="fa fa-angle-down"></i></span></span>

			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">

					<ul><?php foreach ($item ['items'] as $subItem ) {?>
						<li><a href="<?php echo Url::to ( $subItem['url']);?>"> <span
								class="menu-text">
									<?php echo $subItem['label'];?></span>
									<?php if (! empty ( $subItem ['badge'] )) {?>
									<span class="menu-badge"><span
									class="badge vd_bg-<?php echo $subItem['badge-color']?>"><?php echo $subItem['badge']?></span></span>
									<?php }?>
						</a></li>
						<?php }?>
			
					</ul>
				</div></li>
				
				<?php } else {?>

				
			<li><a href="<?php echo Url::to ( $item['url']);?>"> <span
					class="menu-icon"><i class="fa fa-<?php echo $item ['icon']?>"></i></span>
					<span class="menu-text"><?php echo $item ['label']?></span>
					<?php if (! empty ( $item ['badge'] )) { ?>
					<span class="menu-badge"><span
						class="badge vd_bg-<?php echo $item['badge-color']?>">
					<?php echo $item['badge']?></span></span>
					<?php }?>
			</a></li>
			<?php }?>
			<?php }?>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="icon-palette"> </i></span> <span
					class="menu-text">Skin Playground</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="skin-clean-minimalist.html"> <span class="menu-text">Clean
									Minimalist Nav</span>
						</a></li>
						<li><a href="skin-nav-medium-profile-dark.html"> <span
								class="menu-text">Dark Medium Navbar</span> <span
								class="menu-badge"><span class="badge vd_bg-red">Hot</span></span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon entypo-icon"><i class="icon-bookmark"> </i></span>
					<span class="menu-text">Starting Layouts</span> <span
					class="menu-badge"><span class="badge vd_bg-black-30"><i
							class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="layouts-simple.html"> <span class="menu-text">Layout
									Simple</span>
						</a></li>
						<li><a href="layouts-simple-medium-profile.html"> <span
								class="menu-text">Layout Simple Medium Profile</span>
						</a></li>
						<li><a href="layouts-double-sidenav.html"> <span class="menu-text">Layout
									Double SideNav</span>
						</a></li>
						<li><a href="layouts-hide-rightnav.html"> <span class="menu-text">Layout
									Hide RightNav</span>
						</a></li>
						<li><a href="layouts-medium-no-header.html"> <span
								class="menu-text">Layout Medium No Header</span>
						</a></li>
						<li><a href="layouts-right-nav.html"> <span class="menu-text">Layout
									RightNav</span>
						</a></li>
						<li><a href="layouts-menu-tab.html"> <span class="menu-text">Layout
									Menu Tab</span>
						</a></li>
						<li><a href="layouts-without-subtitle.html"> <span
								class="menu-text">Layout Without Subtitle</span>
						</a></li>
						<li><a href="layouts-middle.html"> <span class="menu-text">Layout
									Middle</span>
						</a></li>
						<li><a href="layouts-boxed.html"> <span class="menu-text">Layout
									Boxed</span>
						</a></li>
						<li><a href="layouts-full-no-responsive.html"> <span
								class="menu-text">Layout Full No Responsive</span>
						</a></li>
						<li><a href="layouts-middle-no-responsive.html"> <span
								class="menu-text">Layout Middle No Responsive</span>
						</a></li>
						<li><a href="layouts-boxed-no-responsive.html"> <span
								class="menu-text">Boxed No Responsive</span>
						</a></li>
						<li><a href="layouts-boxed-custom-width-no-responsive.html"> <span
								class="menu-text">Boxed Custom Width No Responsive</span>
						</a></li>
						<li><a href="layouts-fixed-nav.html"> <span class="menu-text">Layout
									Fixed Nav</span>
						</a></li>
						<li><a href="layouts-no-header-fixed-nav.html"> <span
								class="menu-text">No Header Fixed Nav</span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-sitemap"> </i></span> <span
					class="menu-text">Nav Variations</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul class="clearfix">
						<li><a data-action="click-trigger" href="javascript:"> <span
								class="menu-text">Side Navigation</span> <span
								class="menu-badge"><span class="badge vd_bg-black-30"><i
										class="fa fa-angle-down"></i></span></span>
						</a>
							<div data-action="click-target" class="child-menu"
								style="display: none;">
								<ul class="clearfix">
									<li><a href="nav-side-large-menu-tab.html"> <span
											class="menu-text">Large Menu Tab</span>
									</a></li>
									<li><a href="nav-side-large-tabs-tab.html"> <span
											class="menu-text">Large Tabs Tab</span>
									</a></li>
									<li><a href="nav-side-large-profile-tab.html"> <span
											class="menu-text">Large Profile Tab</span>
									</a></li>
									<li><a href="nav-side-large-no-tab.html"> <span
											class="menu-text">Large No Tab</span>
									</a></li>
									<li><a href="nav-side-large-chat-content.html"> <span
											class="menu-text">Large Chat Content</span>
									</a></li>
									<li><a href="nav-side-large-email.html"> <span
											class="menu-text">Large Email</span>
									</a></li>
									<li><a href="nav-side-medium-menu-tab.html"> <span
											class="menu-text">Medium Menu Tab </span>
									</a></li>
									<li><a href="nav-side-medium-tabs-tab.html"> <span
											class="menu-text">Medium Tabs Tab</span>
									</a></li>
									<li><a href="nav-side-medium-profile-tab.html"> <span
											class="menu-text">Medium Profile Tab </span> <span
											class="menu-badge"><span class="badge vd_bg-red">HOT</span></span>
									</a></li>
									<li><a href="nav-side-medium-no-tab.html"> <span
											class="menu-text">Medium No Tab </span>
									</a></li>
									<li><a href="nav-side-small-menu-tab.html"> <span
											class="menu-text">Small Menu Tab</span>
									</a></li>
									<li><a href="nav-side-small-tabs-tab.html"> <span
											class="menu-text">Small Tabs Tab</span>
									</a></li>
									<li><a href="nav-side-small-profile-tab.html"> <span
											class="menu-text">Small Profile Tab</span>
									</a></li>
									<li><a href="nav-side-small-no-tab.html"> <span
											class="menu-text">Small No Tab</span>
									</a></li>
								</ul>
							</div></li>
						<li><a data-action="click-trigger" href="javascript:"> <span
								class="menu-text">Top Navigation</span> <span class="menu-badge"><span
									class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
						</a>
							<div data-action="click-target" class="child-menu"
								style="display: none;">
								<ul class="clearfix">
									<li><a href="nav-top-search.html">Search </a></li>
									<li><a href="nav-top-menu.html">Mega Menu </a></li>
									<li><a href="nav-top-clean-style.html"> Clean Style </a></li>
								</ul>
							</div></li>
						<li><a data-action="click-trigger" href="javascript:"> <span
								class="menu-text">Bottom Navigation</span> <span
								class="menu-badge"><span class="badge vd_bg-black-30"><i
										class="fa fa-angle-down"></i></span></span>
						</a>
							<div data-action="click-target" class="child-menu"
								style="display: none;">
								<ul class="clearfix">
									<li><a href="nav-bottom-widget.html"> Chat Widget </a></li>
									<li><a href="nav-bottom-clean-menu.html"> Clean With Menu </a>
									</li>
									<li><a href="nav-bottom-clean-center.html"> Clean Center Text </a>
									</li>
								</ul>
							</div></li>
					</ul>
				</div></li>
			<li><a href="functions-index.html"> <span class="menu-icon"><i
						class="fa fa-code"></i></span> <span class="menu-text">Custom
						Functions</span> <span class="menu-badge"><span
						class="badge vd_bg-yellow"><i class="fa fa-star"></i></span></span>
			</a></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-th-list"></i></span> <span
					class="menu-text">Forms</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="forms-elements.html"> <span class="menu-text">Form
									Elements</span>
						</a></li>
						<li><a href="forms-layouts.html"> <span class="menu-text">Form
									Layouts</span>
						</a></li>
						<li><a href="forms-wizard.html"> <span class="menu-text">Form
									Wizard</span>
						</a></li>
						<li><a href="forms-validation.html"> <span class="menu-text">Form
									Validation</span>
						</a></li>
						<li><a href="forms-sliders.html"> <span class="menu-text">Form
									Slider</span>
						</a></li>
						<li><a href="forms-multiple-file-upload.html"> <span
								class="menu-text">Form Multiple File Upload</span>
						</a></li>
						<li><a href="forms-inline-editing.html"> <span class="menu-text">Form
									Inline Editing</span> <span class="menu-badge"><span
									class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon entypo-icon"><i class="icon-tools"></i></span> <span
					class="menu-text">UI Elements</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="ui-panels.html"> <span class="menu-text">Panels</span>
						</a></li>
						<li><a href="ui-panels-draggable.html"> <span class="menu-text">Panels
									Draggable</span>
						</a></li>
						<li><a href="ui-buttons.html"> <span class="menu-text">Buttons</span>
						</a></li>
						<li><a href="ui-typography.html"> <span class="menu-text">Typography</span>
						</a></li>
						<li><a href="ui-icons.html"> <span class="menu-text">Icons</span>
						</a></li>
						<li><a href="ui-tabs-accordion.html"> <span class="menu-text">Tabs
									and Accordion</span>
						</a></li>
						<li><a href="ui-alert-notifications.html"> <span class="menu-text">Alert
									and Notifications</span>
						</a></li>
						<li><a href="ui-pricing-tables.html"> <span class="menu-text">Pricing
									Tables</span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-table"></i></span> <span
					class="menu-text">List &amp; Tables</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="listtables-tables-variation.html"> <span
								class="menu-text">Tables Variation</span>
						</a></li>
						<li><a href="listtables-data-tables.html"> <span class="menu-text">Data
									Tables</span>
						</a></li>
						<li><a href="listtables-small-list.html"> <span class="menu-text">Small
									List</span>
						</a></li>
						<li><a href="listtables-medium-list.html"> <span class="menu-text">Medium
									List</span>
						</a></li>
						<li><a href="listtables-blog-small-list.html"> <span
								class="menu-text">Blog Small List</span>
						</a></li>
						<li><a href="listtables-blog-large-list.html"> <span
								class="menu-text">Blog Large List</span>
						</a></li>
						<li><a href="listtables-grid-list.html"> <span class="menu-text">Grid
									List</span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-signal"></i></span> <span
					class="menu-text">Charts</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="charts-morris.html"> <span class="menu-text">Morris
									Chart</span>
						</a></li>
						<li><a href="charts-flot.html"> <span class="menu-text">Flot Chart</span>
						</a></li>
						<li><a href="charts-sparkline.html"> <span class="menu-text">Sparkline
									Chart</span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-map-marker"></i></span> <span
					class="menu-text">Maps</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="maps-vector.html"> <span class="menu-text">Vector Map</span>
						</a></li>
						<li><a href="maps-google.html"> <span class="menu-text">Google Map</span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-picture-o"></i></span> <span
					class="menu-text">Gallery</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="gallery-page.html"> <span class="menu-text">Gallery
									Page</span>
						</a></li>
						<li><a href="gallery-masonry.html"> <span class="menu-text">Gallery
									Masonry</span>
						</a></li>
						<li><a href="gallery-text-filter.html"> <span class="menu-text">Gallery
									Text Filter</span> <span class="menu-badge"><span
									class="badge vd_bg-red">HOT</span></span>
						</a></li>
					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon entypo-icon"><i class="icon-newspaper"></i></span>
					<span class="menu-text">Extra Pages</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="pages-custom-product.html"> <span class="menu-text">Custom
									Product Form</span> <span class="menu-badge"><span
									class="badge vd_bg-red">HOT</span></span>
						</a></li>
						<li><a href="pages-essay.html"> <span class="menu-text">Essay Form</span>
						</a></li>
						<li><a href="pages-invoice.html"> <span class="menu-text">Invoice</span>
						</a></li>
						<li><a href="pages-user-profile-form.html"> <span
								class="menu-text">User Profiles Form</span>
						</a></li>
						<li><a href="pages-user-profile.html"> <span class="menu-text">User
									Profiles Page</span>
						</a></li>
						<li><a href="pages-timeline.html"> <span class="menu-text">Timeline</span>
								<span class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
						<li><a href="pages-calendar.html"> <span class="menu-text">Calendar</span>
								<span class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
						<li><a href="pages-ecommerce-products.html"> <span
								class="menu-text">Ecommerce Products List</span> <span
								class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
						<li><a href="pages-ecommerce-product-add.html"> <span
								class="menu-text">Ecommerce Product add</span> <span
								class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
						<li><a href="pages-login.html"> <span class="menu-text">Login Page</span>
						</a></li>
						<li><a href="pages-forget-password.html"> <span class="menu-text">Forget
									Password Page</span>
						</a></li>
						<li><a href="pages-register.html"> <span class="menu-text">Register
									Page</span>
						</a></li>
						<li><a href="pages-logout.html"> <span class="menu-text">Log Out
									Page</span>
						</a></li>
						<li><a href="pages-lockscreen.html"> <span class="menu-text">Lock
									Screen</span> <span class="menu-badge"><span
									class="badge vd_bg-yellow">NEW</span></span>
						</a></li>
						<li><a href="pages-404-error.html"> <span class="menu-text">404
									Error</span>
						</a></li>
						<li><a href="pages-500-error.html"> <span class="menu-text">500
									Error</span>
						</a></li>

					</ul>
				</div></li>
			<li><a data-action="click-trigger" href="javascript:void(0);"> <span
					class="menu-icon"><i class="fa fa-desktop"></i></span> <span
					class="menu-text">Front End</span> <span class="menu-badge"><span
						class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
			</a>
				<div data-action="click-target" class="child-menu"
					style="display: none;">
					<ul>
						<li><a href="front-1.html"> <span class="menu-text">Flat Theme</span>
						</a></li>
						<li><a href="front-2.html"> <span class="menu-text">Layer Slider</span>
						</a></li>
						<li><a href="front-blog.html"> <span class="menu-text">Front Blog</span>
						</a></li>
						<li><a href="front-blog-content.html"> <span class="menu-text">Front
									Blog Content</span>
						</a></li>
					</ul>
				</div></li>
			<li><a
				href="http://themeforest.net/item/vendroid-super-flexible-multipurpose-admin-theme/7717536?ref=Venmond">
					<span class="menu-icon"><i class="fa fa-shopping-cart"></i></span>
					<span class="menu-text">Buy This Theme</span> <span
					class="menu-badge"><span class="badge vd_bg-red"><i
							class="fa fa-exclamation"></i></span></span>
			</a></li>

		</ul>
		<!-- Head menu search form ends -->
	</div>
</div>

<?php
	}
}

