<?php

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

class BreadcrumbWidget extends TBaseWidget {
	public $items;
	public $params;
	public $context;
	public function run() {
		$this->renderHtml ();
	}
	public function renderHtml() {
		?>

<div class="vd_head-section clearfix">
	<div class="vd_panel-header">
	
	<?php
		
		echo \yii\widgets\Breadcrumbs::widget ( [ 
				'links' => isset ( $this->params ['breadcrumbs'] ) ? $this->params ['breadcrumbs'] : [ ] 
		] );
		?>


		<div data-position="left" data-step="5"
			data-intro="&lt;strong&gt;Expand Control&lt;/strong&gt;&lt;br/&gt;To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code."
			class="vd_panel-menu hidden-sm hidden-xs">
			<div class="remove-navbar-button menu" data-placement="bottom"
				data-toggle="tooltip"
				data-original-title="Remove Navigation Bar Toggle"
				data-action="remove-navbar">
				<i class="fa fa-arrows-h"></i>
			</div>
			<div class="remove-header-button menu" data-placement="bottom"
				data-toggle="tooltip" data-original-title="Remove Top Menu Toggle"
				data-action="remove-header">
				<i class="fa fa-arrows-v"></i>
			</div>
			<div class="fullscreen-button menu" data-placement="bottom"
				data-toggle="tooltip"
				data-original-title="Remove Navigation Bar and Top Menu Toggle"
				data-action="fullscreen">
				<i class="glyphicon glyphicon-fullscreen"></i>
			</div>

		</div>

	</div>
</div>

<?php
	}
}


