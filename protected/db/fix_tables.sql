/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Carlos Mendoza <inhack20@gmail.com>
 * Created: 15-jul-2019
 */

ALTER TABLE `tbl_ride` CHANGE `type_id` `type_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `tbl_driver` CHANGE `type_id` `type_id` INT(11) NULL DEFAULT '0';

CREATE TABLE `tbl_company` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(120) NOT NULL ,`country_id` INT(11) NOT NULL, `enabled` BOOLEAN NOT NULL , `created_on` DATETIME NOT NULL , `updated_on` DATETIME NOT NULL , `created_by_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `tbl_company` ADD CONSTRAINT `FK_COMPANY_COUNTRY` FOREIGN KEY (`country_id`) REFERENCES `tbl_country`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `tbl_company` ADD CONSTRAINT `FK_USER_CREATED029` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `tbl_driver` ADD `company_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `tbl_driver` ADD CONSTRAINT `FK_DRIVER_COMPANY` FOREIGN KEY (`company_id`) REFERENCES `tbl_company`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


CREATE TABLE `tbl_ride_company` ( `ride_id` INT(11) NOT NULL , `company_id` INT(11) NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `tbl_ride_company` ADD CONSTRAINT `FK_ride_company_ride` FOREIGN KEY (`ride_id`) REFERENCES `tbl_ride`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `tbl_ride_company` ADD CONSTRAINT `FK_ride_company_company` FOREIGN KEY (`company_id`) REFERENCES `tbl_company`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


#7-sep-2019
ALTER TABLE `tbl_user` ADD `last_location_time` DATETIME NULL DEFAULT NULL AFTER `create_user_id`;