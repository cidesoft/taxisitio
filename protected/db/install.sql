-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------

-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `ha_logins`
-- -------------------------------------------
DROP TABLE IF EXISTS `ha_logins`;
CREATE TABLE IF NOT EXISTS `ha_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `loginProvider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `loginProviderIdentifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginProvider_2` (`loginProvider`,`loginProviderIdentifier`),
  KEY `loginProvider` (`loginProvider`),
  KEY `loginProviderIdentifier` (`loginProviderIdentifier`),
  KEY `userId` (`userId`),
  KEY `id` (`id`),
  KEY `user_id` (`id`),
  KEY `fk_ha_logins_created_by` (`user_id`),
  CONSTRAINT `fk_ha_logins_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -------------------------------------------

-- TABLE `tbl_auth_session`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_auth_session`;
CREATE TABLE IF NOT EXISTS `tbl_auth_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_code` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `device_token` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `create_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_session_create_user` (`create_user_id`),
  CONSTRAINT `fk_session_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -------------------------------------------

-- TABLE `tbl_car_price`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_car_price`;
CREATE TABLE IF NOT EXISTS `tbl_car_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_price` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `price_min` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `price_mile` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_symbol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `seat` int(11) DEFAULT NULL,
  `service_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT '1',
  `type_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_car_price_create_user` (`create_user_id`),
  CONSTRAINT `FK_car_price_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_complain`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_complain`;
CREATE TABLE IF NOT EXISTS `tbl_complain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `ride_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_complain_created_by` (`created_by_id`),
  KEY `fk_complain_driver_id` (`driver_id`),
  KEY `fk_complain_ride_id` (`ride_id`),
  CONSTRAINT `FK_complain_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_complain_driver_id` FOREIGN KEY (`driver_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_complain_ride_id` FOREIGN KEY (`ride_id`) REFERENCES `tbl_ride` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_country`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_country`;
CREATE TABLE IF NOT EXISTS `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_country_created_by` (`created_by_id`),
  CONSTRAINT `FK_country_create_user` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




-- -------------------------------------------

-- TABLE `tbl_coupon`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_coupon`;
CREATE TABLE IF NOT EXISTS `tbl_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `discount` int(11) NOT NULL,
  `max_amount` int(11) NOT NULL DEFAULT '0',
  `ride_count` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `cologne` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_coupon_created_by` (`created_by_id`),
  CONSTRAINT `FK_coupon_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_coupon_applied`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_coupon_applied`;
CREATE TABLE IF NOT EXISTS `tbl_coupon_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `coupon_sent_id` int(11) NOT NULL,
  `coupon_code` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `max_amount` int(11) NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `ride_count` int(11) NOT NULL DEFAULT '0',
  `ride_taken` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT '0',
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_coupon_applied_created_by_id` (`created_by_id`),
  KEY `fk_coupon_applied_coupon_sent_id` (`coupon_sent_id`),
  KEY `fk_coupon_applied_coupon_id` (`coupon_id`),
  CONSTRAINT `fk_coupon_applied_coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `tbl_coupon` (`id`),
  CONSTRAINT `fk_coupon_applied_coupon_sent_id` FOREIGN KEY (`coupon_sent_id`) REFERENCES `tbl_coupon_sent` (`id`),
  CONSTRAINT `fk_coupon_applied_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_coupon_sent`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_coupon_sent`;
CREATE TABLE IF NOT EXISTS `tbl_coupon_sent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `ride_count` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT '0',
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_coupon_sent_created_by_id` (`created_by_id`),
  KEY `fk_coupon_sent_user_id` (`user_id`),
  KEY `fk_coupon_sent_coupon_id` (`coupon_id`),
  CONSTRAINT `fk_coupon_sent_coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `tbl_coupon` (`id`),
  CONSTRAINT `fk_coupon_sent_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_coupon_sent_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_driver`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_driver`;
CREATE TABLE IF NOT EXISTS `tbl_driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_make` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT '1',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_driver_create_user` (`create_user_id`),
  CONSTRAINT `FK_driver_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_driver_catelog`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_driver_catelog`;
CREATE TABLE IF NOT EXISTS `tbl_driver_catelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mother_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_number` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_address_id` int(11) DEFAULT '0',
  `cp_address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_colonia_id` int(11) DEFAULT '0',
  `address_colonia` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_municipality_id` int(11) DEFAULT '0',
  `address_municipality` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_status` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rfc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `ine` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curp` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` int(11) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `license_no` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `hard_working_time` datetime DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `avg_rating` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `low_date` datetime NOT NULL,
  `state_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_driver_catelog_create_user` (`create_user_id`),
  CONSTRAINT `FK_driver_catelog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_email_queue`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_email_queue`;
CREATE TABLE IF NOT EXISTS `tbl_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `attempts` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_image_proof`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_image_proof`;
CREATE TABLE IF NOT EXISTS `tbl_image_proof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proof_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vehicle_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT '1',
  `type_id` int(11) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_image_proof_create_user` (`create_user_id`),
  CONSTRAINT `FK_image_proof_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_log`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE IF NOT EXISTS `tbl_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `error` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `api` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_log_create_user` (`create_user_id`),
  CONSTRAINT `FK_log_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_notification`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_notification`;
CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_notification_user` (`user_id`),
  KEY `FK_notification_created_by` (`created_by_id`),
  CONSTRAINT `FK_notification_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_notification_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_page`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE IF NOT EXISTS `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_page_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_page_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_peak_price`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_peak_price`;
CREATE TABLE IF NOT EXISTS `tbl_peak_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peak_price_create_user_id` (`create_user_id`),
  CONSTRAINT `fk_peak_price_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_review`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_review`;
CREATE TABLE IF NOT EXISTS `tbl_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `ride_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_review_create_user_id` (`created_by_id`),
  KEY `fk_review_driver_id` (`driver_id`),
  KEY `fk_review_ride_id` (`ride_id`),
  CONSTRAINT `FK_review_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_review_driver_id` FOREIGN KEY (`driver_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_review_ride_id` FOREIGN KEY (`ride_id`) REFERENCES `tbl_ride` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_ride`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_ride`;
CREATE TABLE IF NOT EXISTS `tbl_ride` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination_lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_long` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_passengers` int(11) NOT NULL,
  `number_of_bags` int(11) NOT NULL,
  `is_pet` int(11) NOT NULL DEFAULT '0',
  `is_hourly` tinyint(1) DEFAULT NULL,
  `is_updated` int(11) NOT NULL DEFAULT '0',
  `number_of_hours` int(11) DEFAULT NULL,
  `journey_time` datetime DEFAULT NULL,
  `journey_type` int(11) NOT NULL DEFAULT '0',
  `coupon_id` int(11) DEFAULT NULL,
  `car_price_id` int(11) NOT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrived_time` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ride_driver_id` (`driver_id`),
  KEY `FK_ride_car_price_id` (`car_price_id`),
  KEY `FK_ride_create_user` (`create_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_sepomex`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_sepomex`;
CREATE TABLE IF NOT EXISTS `tbl_sepomex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode` int(11) NOT NULL,
  `settlement` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `type_of_settlemet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `municipality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settlement_post` int(11) NOT NULL,
  `state_code` int(11) NOT NULL,
  `office_code` int(11) NOT NULL,
  `type_of_settlement_code` int(11) NOT NULL,
  `municipality_code` int(11) NOT NULL,
  `unique_identifier_settling` int(11) NOT NULL,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sepomex_created_by` (`created_by_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_service_area`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_service_area`;
CREATE TABLE IF NOT EXISTS `tbl_service_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(255) NOT NULL,
  `iso_code` varchar(255) NOT NULL,
  `state_id` int(11) DEFAULT '1',
  `type_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_service_area_create_user` (`create_user_id`),
  CONSTRAINT `FK_service_area_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -------------------------------------------

-- TABLE `tbl_transaction`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_transaction`;
CREATE TABLE IF NOT EXISTS `tbl_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wallet_history_id` int(11) DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_created_by` (`created_by_id`),
  KEY `fk_transaction_wallet_history` (`wallet_history_id`),
  KEY `fk_transaction_ride` (`ride_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_user`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` int(11) DEFAULT '0',
  `about_me` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avg_rating` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ride_count` int(11) NOT NULL DEFAULT '0',
  `address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tos` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `sepomex_id` int(11) DEFAULT NULL,
  `settlement` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) DEFAULT '0',
  `is_fb` int(11) NOT NULL DEFAULT '0',
  `is_verified` int(11) NOT NULL DEFAULT '0',
  `is_online` int(11) NOT NULL DEFAULT '0',
  `is_available` int(11) NOT NULL DEFAULT '0',
  `last_visit_time` datetime DEFAULT NULL,
  `last_action_time` datetime DEFAULT NULL,
  `last_password_change` datetime DEFAULT NULL,
  `login_error_count` int(11) DEFAULT NULL,
  `activation_key` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_user_address`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_user_address`;
CREATE TABLE IF NOT EXISTS `tbl_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_address_create_user_id` (`create_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_user_wallet`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_user_wallet`;
CREATE TABLE IF NOT EXISTS `tbl_user_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `currency_symbol` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_wallet_created_by` (`created_by_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- -------------------------------------------

-- TABLE `tbl_vehicle_catelog`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_vehicle_catelog`;
CREATE TABLE IF NOT EXISTS `tbl_vehicle_catelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_catelog_id` int(11) NOT NULL DEFAULT '0',
  `economic_no` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submarca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `placas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_series` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `secure_policy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_carrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_concession` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maturity_concession` datetime DEFAULT NULL,
  `owner_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_driver` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_base` int(11) DEFAULT NULL,
  `front_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rear_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `right_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `left_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `low_date` datetime NOT NULL,
  `state_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_vehicle_catelog_create_user` (`create_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_wallet_history`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_wallet_history`;
CREATE TABLE IF NOT EXISTS `tbl_wallet_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wallet_id` int(11) NOT NULL,
  `initial_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_balance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_wallet_history_created_by` (`created_by_id`),
  KEY `fk_wallet_history_wallet` (`wallet_id`),
  KEY `fk_wallet_history_ride` (`ride_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `tbl_user` (`id`,`full_name`,`first_name`,`last_name`,`email`,`password`,`date_of_birth`,`gender`,`about_me`,`contact_no`,`avg_rating`,`ride_count`,`address`,`lat`,`long`,`direction`,`city`,`country_id`,`zipcode`,`language`,`image_file`,`tos`,`role_id`,`sepomex_id`,`settlement`,`municipality`,`state_id`,`type_id`,`is_fb`,`is_verified`,`is_online`,`is_available`,`last_visit_time`,`last_action_time`,`last_password_change`,`login_error_count`,`activation_key`,`timezone`,`create_time`,`update_time`,`create_user_id`) VALUES
("58","Admin","Admin","Admin","admin@toxsl.in","0192023a7bbd73250516f069df18b500","1998-01-22","0","","123456789","","0","Mohali,India","76.474676","34.567568","","Mohali","0","","","user-1517462689-image_fileuser_id_58.jpg","0","0","0","","","1","0","0","1","1","1","2018-02-02 10:04:12","2017-08-16 11:16:01","2017-08-16 11:16:01","0","-kYnRGYUFhVaQI7UxbJtc4Kg0X-AdoSq_1517566074","","2017-01-16 15:33:10","2017-01-16 15:33:10","0");
 -- -------AutobackUpStarttoxsl------ 


 -- -------AutobackUpStarttoxsl------ -- -------------------------------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;
 -- -------AutobackUpStarttoxsl------ -- -------------------------------------------

-- -------------------------------------------

-- END BACKUP

-- -------------------------------------------
