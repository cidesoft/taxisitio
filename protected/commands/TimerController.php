<?php
namespace app\commands;

use app\models\Coupon;
use app\models\CouponApplied;
use app\models\EmailQueue;
use app\models\Ride;
use yii\console\Controller;

class TimerController extends Controller
{

    const MAX_ATTEMPTS = 5;

//     public function actionEmail()
//     {
//         $mails = EmailQueue::find()->where([
//             'state_id' => EmailQueue::STATUS_PENDING
//         ])
//             ->andWhere([
//             '<=',
//             'attempts',
//             self::MAX_ATTEMPTS
//         ])
//             ->limit(50)
//             ->orderBy('id asc')
//             ->all();
        
//         foreach ($mails as $mail) {
//             $mail->sendNow($mail->to_email, $mail->message, $mail->from_email, $mail->subject);
//         }
//         return true;
//     }

    public function actionRideLater()
    {
        $rides = Ride::find()->where([
            'journey_type' => Ride::RIDE_LATER,
            'state_id' => Ride::STATE_NEW
        ]);
        
        foreach ($rides->batch(50) as $ride) {
            foreach ($ride as $rid) {
                $ten = date("Y-m-d H:i", strtotime($rid->journey_time . '-10 minutes'));
                $five = date("Y-m-d H:i", strtotime($rid->journey_time . '-5 minutes'));
                
                if (strtotime($ten) == strtotime(date('Y-m-d H:i'))) {
                    $rid->saveNotification($rid->create_user_id);
                }
                if (strtotime($five) == strtotime(date('Y-m-d H:i'))) {
                    
                    $rid->saveNotification($rid->create_user_id);
                }
            }
        }
    }

    public function actionCouponExpire()
    {
        $couponsApplied = CouponApplied::find()->where([
            
            '<',
            'end_date',
            (string)new \app\services\util\MyDateTime()
        
        ]);
        
        foreach ($couponsApplied->batch(50) as $couponApplied) {
            foreach ($couponApplied as $applied) {
                $applied->delete();
            }
        }
    }

    public function actionRideCountCouponExpire()
    {
        $ride_counts = Coupon::find()->select('ride_count ,id')->all();
        if (! empty($ride_counts)) {
            foreach ($ride_counts as $ride_count) {
                $applied = CouponApplied::find()->where([
                    'coupon_id' => $ride_count->id
                ])
                    ->andWhere([
                    '>=',
                    'ride_taken',
                    $ride_count->ride_count
                ])
                    ->all();
                
                foreach ($applied as $app) {
                    $app->delete();
                }
            }
        }
    }
}

