<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Feedback $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
echo TGridView::widget ( [ 
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'summary' => '',
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'rate_driver',
								[ 
										'attribute' => 'state_id',
										'filter' => $searchModel->getStateOptions (),
										'value' => function ($data) {
											return $data->getStateOptions ( $data->state_id );
										} 
								],
								'created_on:datetime',
								
								[ 
										'class' => 'yii\grid\ActionColumn',
										'header' => '<a>Actions</a>' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

