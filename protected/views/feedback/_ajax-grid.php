<?php

use yii\helpers\Html;
use app\components\TGridView;
use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Feedback $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php echo TGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'',
        	'tableOptions'=>['class'=>'table table-bordered'],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],

            'id',
            'rate_driver',
            'review',
            'model_id',
            'model_type',
            [
			'attribute' => 'state_id','filter'=>$searchModel->getStateOptions(),
			'value' => function ($data) { return $data->getStateOptions($data->state_id);  },],
            /* ['attribute' => 'type_id','filter'=>$searchModel->getTypeOptions(),
			'value' => function ($data) { return $data->getTypeOptions($data->type_id);  },],*/
            /* 'created_on:datetime',*/
            /* 'created_by_id',*/

            ['class' => 'yii\grid\ActionColumn','header'=>'<a>Actions</a>'],
        ],
    ]); ?>
<?php Pjax::end(); ?>

