<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = Yii::t('app', '{modelClass} Module', [
    'modelClass' => 'Feedback',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div
	class="feedback-create">

<?=  \app\components\PageHeader::widget(['title'=>$this->title]); ?>
</div>
<div class="wrapper">
	<div class="vd_content-section clearfix panel">
		<?= $this->render ( '_form', [ 'model' => $model ] )?></div>
</div>

