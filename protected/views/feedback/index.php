<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Feedback */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div
	class="feedback-index">

<?=  \app\components\PageHeader::widget(['title'=>$this->title]); ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
<div class="wrapper">
		<div class="panel">
			<header class="panel-heading head-border">   <?php echo strtoupper(Yii::$app->controller->action->id); ?> </header>
		<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
</div>
	</div>
</div>

