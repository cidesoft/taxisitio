<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title =  $model->label() .' : ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php echo  \app\components\PageHeader::widget(['title'=>$this->title,'model'=>$model]); ?>

<div class="wrapper">
	<div class="vd_content-section clearfix panel">

		<div class=" panel widget light-widget ">
			<div
				class="feedback-view panel-body">

    <?= \app\components\TDetailView::widget([
        'model' => $model,
        	'options'=>['class'=>'table table-bordered'],
        'attributes' => [
            'id',
            'rate_driver',
            'review',
            'model_id',
            'model_type',
            [
			'attribute' => 'state_id', 
			'value' => $model->getStateOptions($model->$column->name),
			'contentOptions' => ['class' => 'bg-red']],
            [
			'attribute' => 'type_id', 
			'value' => $model->getTypeOptions($model->type_id), 	
			],
            'created_on:datetime',
            'created_by_id',
        ],
    ]) ?>

</div>

 			<div
				class="feedback-panel">
<?php
$this->context->startPanel();
	$this->context->addPanel('User', 'CreatedBy', 'User',$model);

$this->context->endPanel();
?>
</div>


</div>
	</div>
</div>

