<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CouponApplied $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
echo TGridView::widget ( [ 
						'id' => 'coupon-applied-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								
								'id',
								[ 
										'attribute' => 'coupon_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->getRelatedDataLink ( 'coupon_id' );
										} 
								],
								[ 
										'attribute' => 'coupon_sent_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->getRelatedDataLink ( 'coupon_sent_id' );
										} 
								],
								'coupon_code',
								'discount',
								'start_date:datetime',
								'end_date:datetime',
								'created_on:datetime',
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

