<?php
use app\components\useraction\UserAction;

/* @var $this yii\web\View */
/* @var $model app\models\Sepomex */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Sepomexes' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="sepomex-view">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'sepomex-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'settlement_post',
								'settlement',
								'type_of_settlemet',
								
								'municipality',
								'state',
								
								'city',
								'zipcode',
								'state_code',
								
								'office_code',
								'type_of_settlement_code',
								
								'municipality_code',
								'unique_identifier_settling',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								/* [ 
										'attribute' => 'type_id',
										'value' => $model->getType () 
								], */
								'created_on:datetime',
								'updated_on:datetime' 
						
						] 
				] )?>


<?php  ?>


	

		</div>
	</div>


</div>
