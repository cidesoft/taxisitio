<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Sepomex $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'sepomex-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								// ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
								
								'id',
								'settlement_post',
								'settlement',
								'type_of_settlemet',
								'municipality',
								/* 'state',
								
								'city', */
								'zipcode',
								/* 'state_code', */
								
								'municipality_code',
            /* 'unique_identifier_settling',*/
           

            [ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

