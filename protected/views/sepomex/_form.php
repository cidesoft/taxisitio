<?php
use app\components\TActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sepomex */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php echo strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="panel-body">

    <?php
				$form = TActiveForm::begin ( [ 
						'layout' => 'horizontal',
						'id' => 'sepomex-form' 
				] );
				?>





<div class="col-md-6">

	
		 <?php echo $form->field($model, 'settlement_post')->textInput() ?>
	 		


		 <?php echo $form->field($model, 'settlement')->textInput(['maxlength' => 512]) ?>
	 		


		 <?php echo $form->field($model, 'type_of_settlemet')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'municipality')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'state')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>
	 		


		
	 		

	</div>
	<div class="col-md-6">

		 <?php echo $form->field($model, 'zipcode')->textInput() ?>
		
		
		 <?php echo $form->field($model, 'state_code')->textInput()?>
	 		


		 <?php echo $form->field($model, 'office_code')->textInput() ?>
	 		


		 <?php echo $form->field($model, 'type_of_settlement_code')->textInput()?>
	 		


		 <?php echo $form->field($model, 'municipality_code')->textInput() ?>
	 		


		 <?php echo $form->field($model, 'unique_identifier_settling')->textInput() ?>
	 		


		
	 			</div>




	<div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'sepomex-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
