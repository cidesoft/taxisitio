<?php

use app\components\PageHeader;
use app\components\TDetailView;
use app\components\useraction\UserAction;
use app\models\ImageProof;
use app\models\User;
use yii\helpers\Html;

if ($model->role_id == User::ROLE_DRIVER) {
    $this->title = Yii::t('app', 'Driver') . ' - ' . $model->first_name . ' ' . $model->last_name . ' | ' . Yii::$app->name;
    $this->params ['breadcrumbs'] [] = [
        'label' => Yii::t('app', 'Drivers'),
        'url' => [
            'driver'
        ]
    ];
} else {
    $this->title = Yii::t('app', 'Customer') . ' - ' . $model->first_name . ' ' . $model->last_name . ' | ' . Yii::$app->name;
    $this->params ['breadcrumbs'] [] = [
        'label' => Yii::t('app', 'Customers'),
        'url' => [
            'index'
        ]
    ];
}
$this->params ['breadcrumbs'] [] = ([
    'label' => $model->first_name . ' ' . $model->last_name
        ]);
?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger">
        <?php echo Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success">
        <?php echo Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>
<div class="wrapper">
    <div class=" panel ">
        <?= PageHeader::widget(['title' => $model->first_name . ' ' . $model->last_name]); ?>
    </div>

    <div class=" panel ">
        <div class="panel-body">

            <div class="col-md-2">
                <p>						
                    <?php
                    if (!empty($model->image_file)) {
                        echo Html::img([
                            'user/download',
                            'image_file' => $model->image_file
                                ], [
                            'height' => '150',
                            'width' => '150'
                        ]);
                    } else {
                        echo Html::img([
                            'themes/green/img/default.png'
                                ], [
                            'height' => '150',
                            'width' => '150'
                        ]);
                    }
                    ?>
                </p>
            </div>
            <?php if ($model->role_id == User::ROLE_DRIVER) { ?>
                <div class="col-md-10">
                    <?php
                    echo TDetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'first_name',
                            'last_name',
                            'avg_rating',
                            'email',
                            'contact_no',
                            'country',
                            [
                                'attribute' => 'Vehicle Name',
                                'value' => isset($model->driver->vehicle) ? $model->driver->getTypeOptions($model->driver->vehicle) : 'Not Set'
                            ],
                            [
                                'attribute' => 'Vehicle Model',
                                'value' => isset($model->driver->model) ? $model->driver->model : 'Not Set'
                            ],
                            [
                                'attribute' => 'License Number',
                                'value' => isset($model->driver->license_no) ? $model->driver->license_no : 'Not Set'
                            ],
                            [
                                'attribute' => 'Company',
                                'value' => isset($model->driver->company_id) ? (string)$model->driver->company : 'Not Set'
                            ],
                            /* [ 
                              'attribute' => 'Car Type',
                              'value' => Driver::getTypeOptions ( isset ( $model->driver->type_id ) ? $model->driver->type_id : '' )
                              ], */
                            [
                                'attribute' => 'Available',
                                'format' => 'raw',
                                'value' => $model->getAvailabilityBadge()
                            ],
                            [
                                'attribute' => 'Online/Offline',
                                'format' => 'raw',
                                'value' => $model->getOnlineBadge()
                            ],
                            [
                                'attribute' => 'Driver State',
                                'format' => 'raw',
                                'value' => $model->getStateBadge()
                            ],
                            'create_time:datetime'
                        ]
                    ]);
                    ?>
                    <div>
                    <?php
                    if ($model->type_id == 2) {
                        $actions = $model->getStateOptions();
                        array_shift($actions);
                        echo UserAction::widget([
                            'model' => $model,
                            'attribute' => 'state_id',
                            'states' => $model->getStateOptions()
                                // 'allowed' => $actions
                        ]);
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>	
    <?php
    if ($model->type_id < 2) {
        print_r("Profile Not Complete");
    } else {
        $files = ImageProof::find()->where([
                    'create_user_id' => $model->id
                ])->all();
        if (!empty($files)) {
            ?>
                <div class="panel">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <h3>Driver Documents</h3>
            <?php foreach ($files as $file) { ?>
                                <div class="col-md-3">
                                    <h2>Id Proof File</h2>
                                <?php echo Html::img(['user/download', 'image_file' => $file->id_proof_file], ['height' => '180', 'width' => '180']) ?><br />
                                </div>

                                <div class="col-md-3">
                                    <h2>Licence File</h2>
                <?php echo Html::img(['user/download', 'image_file' => $file->license_file], ['height' => '180', 'width' => '180']) ?><br />
                                </div>

                                <div class="col-md-3">
                                    <h2>Document File</h2>
                <?php echo Html::img(['user/download', 'image_file' => $file->document_file], ['height' => '180', 'width' => '180']) ?><br />
                                </div>

                                <div class="col-md-3">
                                    <h2>Vehicle File</h2>
                <?php echo Html::img(['user/download', 'image_file' => $file->vehicle_image], ['height' => '180', 'width' => '180']) ?><br />
                                </div>

                                <?php }
                            }
                            ?>	
                    </div>
                </div>
            </div>


            <div class="panel">
                <div class="panel-body">
        <?php
        $this->context->startPanel();

        $this->context->addPanel('Rides', 'driverRides', 'Ride', $model);
        $this->context->addPanel('Complains', 'driverComplains', 'Complain', $model);

        $this->context->endPanel();
        ?>
                </div>
            </div>

        <?php
    }
}

if ($model->role_id == User::ROLE_PASSENGER) {
    ?>
        <div class="col-md-10">
            <?php
            echo TDetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'contact_no',
                    'ride_count',
                    'zipcode',
                    'settlement',
                    'municipality',
                    [
                        'attribute' => 'zip_code',
                        'format' => 'raw',
                        'value' => ($model->getSepomex() !== null) ? $model->getSepomex(true)->zipcode : ''
                    ],
                    [
                        'attribute' => 'is_fb',
                        'format' => 'raw',
                        'value' => $model->getSignupBadge()
                    ],
                    'country',
                    [
                        'attribute' => 'Customer State',
                        'format' => 'raw',
                        'value' => $model->getStateBadge()
                    ],
                    'create_time:datetime'
                ]
            ])
            ?>

            <div>
                <?php
                $actions = $model->getStateOptions();
                array_shift($actions);
                echo UserAction::widget([
                    'model' => $model,
                    'attribute' => 'state_id',
                    'states' => $model->getStateOptions()
                        // 'allowed' => $actions
                ]);
                ?>
            </div>
        </div>
    </div>
    </div>

    <div class="panel">
        <div class="panel-body">
            <?php
            $this->context->startPanel();

            $this->context->addPanel('Rides', 'customerRides', 'Ride', $model);
            $this->context->addPanel('Wallet', 'UserWallet', 'UserWallet', $model);
            $this->context->addPanel('Registered Complains', 'customerComplains', 'Complain', $model);
            $this->context->addPanel('Coupons', 'couponList', 'coupon-sent', $model);

            $this->context->endPanel();
            ?>
        </div>
    </div>
<?php } ?>
</div>








