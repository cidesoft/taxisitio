<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

/* $this->title = Yii::t ( 'app', 'Add {modelClass}', [ 
		'modelClass' => 'User' 
] ); */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Users' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] =(string)$model;
?>
<div class="user-create">
	<?=  \app\components\PageHeader::widget(['model' => $model]); ?>
	<div class="vd_content-section clearfix">
		<?= $this->render ( '_form', [ 'model' => $model ] )?>
	</div>
</div>