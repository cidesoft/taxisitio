<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
// $this->title = Yii::t ( 'app', 'Customers' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Customers' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Index' );
?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-danger">
    <?php echo Yii::$app->session->getFlash('error')?>
</div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
<div class="wrapper">
	<div class="user-index">
		<div class=" panel ">

	<?= \app\components\PageHeader::widget( ['title'=>Yii::t ( 'app', 'Customers' )] ); ?>
	</div>
	</div>
	
	<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">
		<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
	</div>
	</div>
</div>
</div>

