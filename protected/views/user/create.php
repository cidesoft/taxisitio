<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

// $this->title = 'Add User';
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Users',
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>
<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>
<div class="vd_content-section clearfix">
    <?=$this->render ( '_form', [ 'model' => $model ] )?>
</div>