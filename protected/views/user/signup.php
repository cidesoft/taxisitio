<div class="signup-outer-front">
	<?php
	use app\models\User;
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Html;
	use yii\helpers\Url;
	
	/* @var $this yii\web\View */
	/* @var $form yii\bootstrap\ActiveForm */
	/* @var $model \frontend\models\SignupForm */
	
	//$this->title = 'Signup';
	?>

<h2 class="text-center" style="color:#fff;">Registration form</h2>
	<div class="container log-row">
               
		<?php
		
		$form = ActiveForm::begin ( [ 
				'id' => 'form-signup',
				'options' => [ 
						'class' => 'form-signin' 
				] 
		] );
		?>
             <? ?>
              <p>Enter your account details below</p>
              <?= $form->field($model, 'full_name')->textInput(['maxlength' => true])?>
                <?= $form->field($model, 'city')->textInput(['maxlength' => 256])?>
                         <?= $form->field($model, 'country')->textInput(['maxlength' => 256])?>
           
                <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
                     
                <?= $form->field($model, 'password')->passwordInput()?>
            <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-lg btn-success btn-block', 'name' => 'signup-button'])?>
                </div>


		<div class="registration m-t-20 m-b-20">
			<a class="" href="<?php echo Url::toRoute(['user/login']);?>"> Login
			</a>
		</div>
            <?php ActiveForm::end(); ?>
    
    </div>



</div>