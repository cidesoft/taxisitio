<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

//$this->title = 'Sign In';

$fieldOptions1 = [ 
		'options' => [ 
				'class' => 'form-group has-feedback' 
		],
		
		'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>" 
];

$fieldOptions2 = [ 
		'options' => [ 
				'class' => 'form-group has-feedback' 
		],
		'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>" 
];
?>

<style>
.checkbox, .radio {
  margin-top:0px;
}
</style>
<div class="login_form clearfix col-md-4 col-sm-6 col-xs-12 login-design">
	<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
		<h1 class="text-center white">Login</h1>
<div class="form-signin" >
              <?php
														
$form = ActiveForm::begin ( [ 
																'id' => 'login-form',
																// 'enableClientValidation' => false,
																'options' => [ 
																		'class' => 'form-signin' 
																],
																/* 'action' => [ 
																		'api/user/login' 
																] */ 
														] );
														?>
     
         <?=$form->field ( $model, 'username', $fieldOptions1 )->label ( false )->textInput ( [ 'placeholder' => $model->getAttributeLabel ( 'email' ) ] )?>
         <?=$form->field ( $model, 'password', $fieldOptions2 )->label ( false )->passwordInput ( [ 'placeholder' => $model->getAttributeLabel ( 'password' ) ] )?>
       
                 
                 
        <?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-success btn-block', 'name' => 'login-button'])?>
              
                  
			
			<div class="row">
				<div class="col-md-6 text-left">
					<div class="checkbox remember">
					<?php echo $form->field($model, 'rememberMe')->checkbox();?>

					</div>
				</div>
			
			
			<div class="col-md-6">
			<a class="pull-right forgot"
			data-toggle="modal"
			href="<?php echo Url::toRoute(['user/recover'])?>"> Forgot Password?</a></div>
		
		 </div>
		</div>

</div>		
	</div>
<?php \yii\bootstrap\ActiveForm::end()?>
	


</div>
</div>
</div>

