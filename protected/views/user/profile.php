<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
use app\components\PageHeader;
use app\components\TDetailView;
use app\components\useraction\UserAction;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

// $this->title = $model->id;

$this->params ['breadcrumbs'] [] = ([ 
		'label' => $model->full_name 
]);

?>

<div class="wrapper">
<?php if (Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-danger">
    <?php echo Yii::$app->session->getFlash('error')?>
</div>
<?php endif; ?>

<?php

if (Yii::$app->session->hasFlash ( 'success' )) :
	?>

<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>


	<div class=" panel ">
		<?php echo  PageHeader::widget(['title'=>$model->first_name,'model'=>$model]); ?><div
			class="custom-view-button">
			<a href="<?=Url::toRoute(['user/update-admin','id'=>$model->id])?>"
				class="btn btn-success">Update</a>
		</div>

	</div>
	<div class=" panel ">
		<div class=" panel-body ">
			<div class="col-md-2">
				<p>						
        			 <?php
												if (! empty ( $model->image_file )) {
													echo Html::img ( [ 
															'user/download',
															'image_file' => $model->image_file 
													], [ 
															'height' => '150',
															'width' => '150' 
													] );
												} else {
													echo Html::img ( [ 
															'themes/green/img/default.png' 
													], [ 
															'height' => '150',
															'width' => '150' 
													] );
												}
												?>
   			 		</p>

			</div>
			<div class="col-md-10">
   				 <?php
								
								echo TDetailView::widget ( [ 
										'model' => $model,
										'attributes' => [ 
												'id',
												'full_name',
												'email:email',
												'contact_no',
												[ 
														'attribute' => 'state',
														'format' => 'raw',
														'value' => $model->getStateBadge () 
												] 
											
											/*
										 * [
										 * 'attribute' => 'role_id',
										 * 'visible' => ($model->role_id== User::ROLE_PASSENGER ||$model->role_id== User::ROLE_DRIVER ) ? true : false,
										 * 'format' => 'raw',
										 * 'value' => $model->getRoleOptions ( $model->role_id )
										 * ],
										 */
											// 'created_o:datetime'
										] 
								] )?>
		</div>

		</div>
		<div>
				<?php
				echo UserAction::widget ( [ 
						'model' => $model,
						'attribute' => 'state_id',
						'states' => $model->getStateOptions () 
				] );
				?>	
			</div>



	</div>



</div>

