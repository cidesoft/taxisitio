<?php
use app\components\TActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel">
	<div class="panel-body">

    <?php
				$form = TActiveForm::begin ( [ 
						'layout' => 'horizontal',
						'id' => 'user-form',
						'options' => [ 
								'enctype' => 'multipart/form-data' 
						] 
				
				] );
				?>
    
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => 55])?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 55])?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 128])?>

    <?= $form->field($model, 'contact_no')->textInput(['maxlength' => 11])?>  

   <?= $form->field ( $model, 'image_file', [ 'enableAjaxValidation' => false,'enableClientValidation' => true ])->fileInput()?>

   
 
    <div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom">
			<div class="form-group text-center">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['id'=>'user-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success'])?>
	    </div>
		</div>
    <?php TActiveForm::end(); ?>
    <div class="clearfix"></div>
	</div>
</div>