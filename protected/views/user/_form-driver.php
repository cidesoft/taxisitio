<?php

use app\components\TActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="wrapper">
    <div class="panel"> 
        <div class="panel-body">
            <?php
            $form = TActiveForm::begin([
                        'layout' => 'horizontal',
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                    ]);
            ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 55]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 55]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 128]) ?>

            <?= $form->field($model, 'contact_no')->textInput(['maxlength' => 11]) ?>  

            <?= $form->field($driver, 'vehicle')->dropDownList($driver->getTypeOptions()) ?> 

            <?= $form->field($driver, 'model')->textInput(['maxlength' => 11]) ?> 

            <?= $form->field($driver, 'license_no')->textInput(['maxlength' => 11]) ?> 

            <?= $form->field($driver, 'registration_no')->textInput(['maxlength' => 11]) ?> 

            <?= $form->field($driver, 'company_id')->dropDownList(\yii\helpers\ArrayHelper::map($companies, 'id', 'name'),['prompt' => 'Seleccione Uno' ]) ?> 


            <div class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom">
                <div class="form-group text-center">
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                </div>
            </div>
<?php TActiveForm::end(); ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
