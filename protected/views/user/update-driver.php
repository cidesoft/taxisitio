<?php

use app\components\PageHeader;

/* @var $this yii\web\View */
/* @var $model app\models\User */

/*
 * $this->title = Yii::t ( 'app', 'Update {modelClass}: ', [
 * 'modelClass' => 'User'
 * ] ) . ' ' . $model->id;
 */
$this->title = $model->first_name.' - Update | '. Yii::$app->name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Drivers' ),
		'url' => [ 
				'driver' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->first_name.' '.$model->last_name,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>
<?= PageHeader::widget(['title'=>$model->first_name .' '. $model->last_name ,'model'=>$model]); ?>

<div class="vd_content-section clearfix">
	<?= $this->render ( '_form-driver', [ 'model' => $model, 'driver' => $driver,"companies" => $companies ] )?>
</div>