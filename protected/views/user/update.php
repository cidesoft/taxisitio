<?php
use app\components\PageHeader;

/* @var $this yii\web\View */
/* @var $model app\models\User */

/*
 * $this->title = Yii::t ( 'app', 'Update {modelClass}: ', [
 * 'modelClass' => 'User'
 * ] ) . ' ' . $model->id;
 */

$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Admin',
		'url' => [ 
				'profile',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" panel ">
<?= PageHeader::widget(['title'=>'Admin' ,'model'=>$model]); ?>
</div>
	</div>
<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">
	<?= $this->render ( '_form', [ 'model' => $model ] )?>
</div>
</div>
</div>
</div>