<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
?>

<section class="content-header">
	<h1><?php echo Html::encode(Html::valueEx($model)); ?></h1>
</section>

<div class="content">
	<div class="content-header">
		<h3 class="sub-history box-title">
			<span>Change Password</span>
		</h3>
		<div class="clearfix"></div>
		<br>

		<div class="clearfix"></div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"></h3>
		</div>

		<div class="box-body">
			<div class="col-md-8">
		<?php if(Yii::$app->user->hasFlash('danger')): ?>
			<div class="alert alert-danger">
					<div class="flash-success">
				<?php echo Yii::$app->user->getFlash('danger');  ?>
				</div>
				</div>

			<?php endif;?>

			<?php if(Yii::$app->user->hasFlash('success')): ?>
			<div class="alert alert-success">
					<div class="flash-success">
				<?php echo Yii::$app->user->getFlash('success'); ?>
				</div>
				</div>

			<?php endif;?>
			<?php
			
$form = $this->beginWidget ( 'booster.widgets.TbActiveForm', array (
					'id' => 'user-form',
					'enableAjaxValidation' => true,
					'enableClientValidation' => true,
					'clientOptions' => array (
							'validateOnSubmit' => true 
					),
					
					// 'action'=>Yii::app()->createUrl('api/user/changepassword'),
					'htmlOptions' => array (
							'enctype' => 'multipart/form-data' 
					) 
			) );
			?>
			<p class="help-block form-group text-right">
			<?php echo Yii::t('app', 'Fields with'); ?>
				<span class="required">*</span>
				<?php echo Yii::t('app', 'are required'); ?>
				.
			</p>
				<div class="clearfix"></div>
				<div class="form-group">
			<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'form-control','maxlength'=>128,'placeHolder'=>'Email','readOnly'=>true)))); ?>

			</div>

				<div class="form-group">
			<?php echo $form->passwordFieldGroup($model, 'password', array('placeHolder'=>$model->getAttributeLabel('password'),'class'=>'form-control')); ?>
			
			</div>


				<div class="form-group">
			<?php echo $form->passwordFieldGroup($model, 'password_2', array('placeHolder'=>$model->getAttributeLabel('password_2'),'class'=>'form-control')); ?>
			
			</div>
				<div class="form-group">
					<div class=" ">
				<?php
				$this->widget ( 'booster.widgets.TbButton', array (
						'buttonType' => 'submit',
						'context' => 'info',
						'label' => ' Save ' 
				)
				 );
				?>
				</div>
				</div>
			<?php $this->endWidget(); ?>
</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>






