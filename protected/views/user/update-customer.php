<?php
use app\components\PageHeader;

/* @var $this yii\web\View */
/* @var $model app\models\User */

/*
 * $this->title = Yii::t ( 'app', 'Update {modelClass}: ', [
 * 'modelClass' => 'User'
 * ] ) . ' ' . $model->id;
 */
$this->title = $model->first_name.' - Update | '. Yii::$app->name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Customers' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->first_name . ' ' . $model->last_name,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" panel ">

<?= PageHeader::widget(['title'=>$model->first_name .' '. $model->last_name ,'model'=>$model]); ?>
</div>
</div>
<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">
	<?= $this->render ( '_form-customer', [ 'model' => $model ] )?>
</div>
</div>
</div>
</div>