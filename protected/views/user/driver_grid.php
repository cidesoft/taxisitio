<?php
use app\components\TGridView;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

Pjax::begin();

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\User $searchModel
 */
// $this->title = Yii::t ( 'app', 'Drivers' );
$this->title = Yii::t('app', 'Driver') . ' - Index | ' . Yii::$app->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Drivers'),
    'url' => [
        'driver'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Index');
?>
<style>
.FullName {
	text-transform: capitalize;
}
</style>
<div class="wrapper">
	<div class=" panel ">

		<div class="car-price-index">
        	<?php if(Yii::$app->session->hasFlash('success')): ?>
        		<div class="alert alert-success">
        			<?php echo Yii::$app->session->getFlash('success'); ?>
        			<?php endif; ?>
        			<?= \app\components\PageHeader::widget( ['title'=>Yii::t ( 'app', 'Drivers' ),'showAdd' => false ] ); ?>
        	
        			<?php Pjax::begin(); ?>
				</div>
		</div>
	</div>
	<div class="panel panel-margin">
		<div class="panel-body">
			<div class="content-section clearfix">
        		<?php
                echo TGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover'
                    ],
                    'emptyText' => '<i>No Driver Found</i>',
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'first_name',
                            'contentOptions' => [
                                'class' => 'FullName'
                            ],
                            'value' => 'first_name'
                        
                        ],
                        [
                            'attribute' => 'last_name',
                            'contentOptions' => [
                                'class' => 'FullName'
                            ],
                            'value' => 'last_name'
                        
                        ],
                        [
                            'attribute' => 'state_id',
                            'format' => 'raw',
                            'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                            'value' => function ($data) {
                                return $data->getStateBadge();
                            }
                        ],
                        'email:email',
                        'contact_no',
                        
                        [
                            'attribute' => 'state_id',
                            'filter' => User::getStateOptions(),
                            'value' => function ($data) {
                                return User::getStateOptions($data->state_id);
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => '<a>Actions</a>',
                            // 'template' => '{view} {delete}',
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                                        '/user/update-driver',
                                        'id' => $model->id
                                    
                                    ], [
                                        'title' => Yii::t('app', 'update')
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a('<i class="glyphicon glyphicon-trash"></i>', Url::toRoute([
                                        '/user/delete-driver',
                                        'id' => $model->id
                                    ]), [
                                        'title' => Yii::t('app', 'delete'),
                                        'data-method' => 'post',
                                        'data-confirm' => 'Do you really want to delete this user? '
                                    ]);
                                }
                            ]
                        ]
                    ]
                ]);
                ?>
        		<?php Pjax::end(); ?>
			</div>
		</div>
	</div>
</div>