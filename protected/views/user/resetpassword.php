<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

// $this->title = 'Change Password';
$this->params ['breadcrumbs'] [] = ( string ) $model;
?><br>
<br>
<br>
<br>

<div class="container">
<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-danger">
    <?php echo Yii::$app->session->getFlash('error')?>
</div>
<?php endif; ?>

	<div
		class="login_form clearfix col-md-4 col-sm-6 col-xs-12 login-design">
		<h1 class="text-center white">Change-Password | Taxi-sitio</h1>



		<br>

		<p class="text-center white">Please fill out the following fields to
			change password :</p>
		<br>
   
    <?php
				
				$form = ActiveForm::begin ( [ 
						'id' => 'changepassword-form',
						'options' => [ 
								'class' => 'form-horizontal' 
						],
						'fieldConfig' => [ 
								'template' => "{label}\n<div class=\"col-lg-12\">
                        {input}</div>\n<div class=\"col-lg-5\">
                        {error}</div>",
								'labelOptions' => [ 
										'class' => 'col-lg-2 control-label' 
								] 
						] 
				] );
				?>
				
				
         <?=$form->field ( $model, 'password', [ 'inputOptions' => [ 'placeholder' => 'Password' ] ] )->label(false)->passwordInput ()?>
          
      
        
       
        <div class="clearfix">
			<div class="">
                <?=Html::submitButton ( 'Change password', [ 'class' => 'btn btn-lg btn-success btn-block' ] )?>
            </div>
		</div>
    <?php ActiveForm::end(); ?>
</div>
</div>
</div>