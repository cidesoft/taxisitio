<?php
use app\components\TActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Users' ),
		'url' => [ 
				'index' 
		] 
];

$this->params ['breadcrumbs'] [] = ([ 
		'label' => $model->first_name.' '.$model->last_name 
]);
?>

<?php 
/*
       * $this->title = Yii::t ( 'app', 'Update : ', [
       * 'modelClass' => 'User'
       * ] ) . ' ' . \yii\helpers\Html::encode ( $model->full_name );
       */
?>
<?=  \app\components\PageHeader::widget(['title'=> $model->first_name.' '.$model->last_name ,'model'=>$model]); ?>
<div class="wrapper">
	<div class="panel">
		<div class="panel-body">

    <?php
				
				$form = TActiveForm::begin ( [ 
						'layout' => 'horizontal',
						'options' => [ 
								'enctype' => 'multipart/form-data' 
						],
						'enableAjaxValidation' => true,
				 		'enableClientValidation' => false 
				
				] );
				?>
	

    
    <?= $form->field($model, 'full_name')->textInput(['maxlength' => 55])?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => 128])?>


    <?= $form->field($model, 'contact_no')->textInput(['maxlength' => 128])?>

 


 

 
    <div
				class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom">
				<div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success'])?>
    </div>
			</div>

    <?php TActiveForm::end(); ?>

</div>
	</div>
</div>


