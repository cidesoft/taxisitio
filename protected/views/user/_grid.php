<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

Pjax::begin();

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\User $searchModel
 */
$this->title = Yii::t('app', 'Customer') . ' - Index | ' . Yii::$app->name;
?>
<style>
.FullName {
	text-transform: capitalize;
}
</style>
<?php Pjax::begin(); ?>
    <?php
    
    echo TGridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-bordered table-hover'
        ],
        'filterModel' => $searchModel,
        'emptyText' => '<i>No Customer Found</i>',
        'columns' => [
            'id',
            [
                'attribute' => 'first_name',
                'contentOptions' => [
                    'class' => 'FullName'
                ],
                'value' => 'first_name'
            
            ],
            [
                'attribute' => 'last_name',
                'contentOptions' => [
                    'class' => 'FullName'
                ],
                'value' => 'last_name'
            
            ],
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            'email:email',
            'contact_no',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<a>Actions</a>',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [
                            
                            '/user/update-customer',
                            
                            'id' => $model->id
                        ], [
                            'title' => 'Update'
                        ]);
                    }
                ]
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>