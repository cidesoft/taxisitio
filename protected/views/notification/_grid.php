<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Notification $searchModel
 */

?>
<style>
.ui-widget-content {
	background: #227521 !important;
}
</style>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'notification-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								// ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
								
								'id',
								'message',
								[ 
										'attribute' => 'user_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->full_name : '';
										} 
								],
								
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createdBy ? $data->createdBy->first_name . ' ' . $data->createdBy->last_name : '';
										} 
								],
								[ 
										'attribute' => 'create_time',
										'filter' => \yii\jui\DatePicker::widget ( [ 
												'model' => $searchModel,
												'attribute' => 'create_time',
												'dateFormat' => 'yyyy-MM-dd',
												'options' => [ 
														'class' => 'form-control ' 
												],
												'clientOptions' => [ 
														'changeMonth' => true,
														'changeYear' => true 
												] 
										] ),
										'value' => function ($data) {
											return date ( 'Y-m-d', strtotime ( $data->create_time ) );
										} 
								],
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

