<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Notification $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php
				
echo TGridView::widget ( [ 
						'id' => 'notification-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'message',
								[ 
										'attribute' => 'user_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->full_name : '';
										} 
								],
								
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createdBy ? $data->createdBy->first_name . ' ' . $data->createdBy->last_name : '';
										} 
								],
								'create_time:datetime',
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

