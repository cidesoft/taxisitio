<?php

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Notifications' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="notification-view">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'notification-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'message',
								[
										'attribute' => 'user_id',
										'format' => 'raw',
										'value' => isset($model->driver)?$model->driver->first_name.' '.$model->driver->last_name : ''
								],
								'model_id',
								[ 
										'attribute' => 'type_id',
										'value' => $model->getType () 
								],
								'create_time:datetime',
								[
										'attribute' => 'created_by_id',
										'format' => 'raw',
										
										'value' => isset($model->createdBy)?$model->createdBy->first_name.' '.$model->createdBy->last_name: ''
								] 
						] 
				] )?>


<?php  ?>



		</div>
	</div>


</div>
