<?php
/* @var $this yii\web\View */
/* @var $model app\models\Page */

/* $this->title = $model->label() .' : ' . $model->title; */
$this->params ['breadcrumbs'] [] = [
    'label' => Yii::t('app', 'Companies'),
    'url' => [
        'index'
    ]
];
$this->params ['breadcrumbs'] [] = (string) $model;
?>

<div class="wrapper">
    <div class=" panel ">

        <div class="page-view">
            <?php echo \app\components\PageHeader::widget(['model' => $model]); ?>



        </div>
    </div>

    <div class=" panel ">
        <div class=" panel-body ">
            <?php
            echo \app\components\TDetailView::widget([
                'id' => 'page-detail-view',
                'model' => $model,
                'options' => [
                    'class' => 'table table-bordered'
                ],
                'attributes' => [
                    'id',
                    'name',
                    'country',
//                    [
//                        'attribute' => 'enabled',
//                        'format' => 'raw',
//                        'value' => $model->getStateBadge()
//                    ],
                    'enabled:boolean',
                    'created_on:datetime',
                    'updated_on:datetime'
                ]
            ])
            ?>





        </div>
    </div>


</div>
