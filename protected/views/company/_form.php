<?php

use app\components\TActiveForm;
use yii\helpers\Html;
use app\models\Country;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
    <?php echo strtoupper(Yii::$app->controller->action->id); ?>
</header>
<div class="panel-body">

    <?php
    $form = TActiveForm::begin([
                'layout' => 'horizontal',
                'id' => 'page-form'
    ]);
    ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => 120]) ?>
    <?php echo $form->field ($model, 'country_id' )->dropDownList(Country::find()->select(['title', 'id'])->indexBy('id')->column(),['prompt'=>'Select Country']);?>
    <?php echo $form->field($model, 'enabled')->checkbox($model->getStatusOptions(), ['prompt' => '']) ?>


    <div class="form-group">
        <div
            class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id' => 'page-form-submit', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php TActiveForm::end(); ?>

</div>
