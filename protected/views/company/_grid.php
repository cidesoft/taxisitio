<?php

use app\components\TGridView;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Page $searchModel
 */
?>
<style>
    .ui-widget-content {
        background: #227521 !important;
    }
</style>
<?php Pjax::begin(); ?>
<?php

echo TGridView::widget([
    'id' => 'page-grid-view',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-bordered'
    ],
    'columns' => [
        'id',
        'name',
        'country',
        'enabled',
//        [
//            'attribute' => 'name',
//            'filter' => isset($searchModel) ? $searchModel->name : null,
//            'value' => function ($data) {
//                return $data->name;
//            }
//        ],
        [
            'attribute' => 'created_on',
            'filter' => \yii\jui\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'created_on',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control '
                ],
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear' => true
                ]
            ]),
            'value' => function ($data) {
                return date('Y-m-d', strtotime($data->created_on));
            }
        ],
        [
            'class' => 'app\components\TActionColumn',
            'header' => '<a>Actions</a>'
        ]
    ]
]);
?>
<?php Pjax::end(); ?>

