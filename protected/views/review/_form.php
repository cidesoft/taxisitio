<?php
use app\components\TActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                          
                        </header>
<div class="panel-body">

    <?php
				$form = TActiveForm::begin ( [ 
						'layout' => 'horizontal',
						'id' => 'review-form' 
				] );
				?>





		 <?php echo  $form->field($model, 'comment')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'comment')->textarea(['rows' => 6]); ?>
	 		


		 <?php /*echo $form->field($model, 'rate')->textInput(['maxlength' => 125]) */ ?>
	 		


		 <?php echo $form->field($model, 'driver_id')->textInput() ?>
	 		


		 <?php echo $form->field($model, 'ride_id')->textInput() ?>
	 		


	 		


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'review-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
