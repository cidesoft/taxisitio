<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Review $searchModel
 */

?>
<style>
.ui-widget-content {
	background: #227521 !important;
}
</style>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'review-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								
								'id',
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->full_name : '';
										} 
								],
								[
										'attribute' => 'created_on',
										'filter' => \yii\jui\DatePicker::widget ( [
												'model' => $searchModel,
												'attribute' => 'created_on',
												'dateFormat' => 'yyyy-MM-dd',
												'options' => [
														'class' => 'form-control '
												],
												'clientOptions' => [
														'changeMonth' => true,
														'changeYear' => true
												]
										] ),
										'value' => function ($data) {
										return date ( 'Y-m-d', strtotime ( $data->created_on ) );
												}
												],
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createdBy ? $data->createdBy->first_name . ' ' . $data->createdBy->last_name : '';
										} 
								],
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

