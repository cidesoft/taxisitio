<?php

/* @var $this yii\web\View */
/* @var $model app\models\Review */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Reviews' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id 
];
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="review-view">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model, 'title'=>$model->id]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'review-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'comment:html',
								'rate',
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => isset($model->driver)?$model->driver->first_name.' '.$model->driver->last_name: '' 
								],
								'ride_id',
								[ 
										'attribute' => 'ride_id',
										'format' => 'raw',
										'label' => 'Ride Amount',
										'value' => $model->getRelatedDataLink ( 'ride_id' ) 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										
										'value' => isset($model->createdBy)?$model->createdBy->first_name.' '.$model->createdBy->last_name: '' 
								] 
						] 
				] )?>


<?php  ?>


		

		</div>
	</div>


</div>
