<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UserWallet $searchModel
 */

?>


<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'user-wallet-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								// ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
								
								'id',
								[ 
										'attribute' => 'amount',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->symbol->currency_symbol . ' ' . $data->symbol->amount;
										} 
								],
								[ 
										'attribute' => 'created_on',
										'filter' => \yii\jui\DatePicker::widget ( [ 
												'model' => $searchModel,
												'attribute' => 'created_on',
												'dateFormat' => 'yyyy-MM-dd',
												'options' => [ 
														'class' => 'form-control ' 
												],
												'clientOptions' => [ 
														'changeMonth' => true,
														'changeYear' => true 
												] 
										] ),
										'value' => function ($data) {
											return date ( 'Y-m-d', strtotime ( $data->created_on ) );
										} 
								],
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser->first_name . ' ' . $data->createUser->last_name;
										} 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

