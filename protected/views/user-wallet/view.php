<?php

/* @var $this yii\web\View */
/* @var $model app\models\UserWallet */

/* $this->title = $model->label() .' : ' . $model->id; */
$userName = $model->createUser->first_name . ' ' . $model->createUser->last_name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'User Wallets' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $userName;
$userName = $model->createUser->first_name . ' ' . $model->createUser->last_name;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="user-wallet-view">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model,'title' => $userName]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'user-wallet-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								[ 
										'attribute' => 'amount',
										'format' => 'raw',
										'value' => $model->currency_symbol . ' ' . $model->amount 
								],
								'currency_code',
								
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'value' => $model->createUser ? $model->createUser->first_name . ' ' . $model->createUser->last_name : '' 
								] 
						] 
				] )?>
		</div>
	</div>
	<div class="panel">
		<div class="panel-body">
			<?php 	$this->context->startPanel ();
					
					$this->context->addPanel ( 'Transactions', 'transactions', 'WalletHistory', $model );
					
					$this->context->endPanel ();
			?>
		</div>
	</div>

</div>
