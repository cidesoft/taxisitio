<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\UserWallet $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'user-wallet-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'currency_code',
								[ 
										'attribute' => 'amount',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->symbol->currency_symbol . ' ' . $data->symbol->amount;
										} 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser->first_name . ' ' . $data->createUser->last_name;
										} 
								],
								[
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}'
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

