<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CarPrice */
/* @var $dataProvider yii\data\ActiveDataProvider */
   
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Car Prices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Index');
 
?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-danger">
    <?php echo Yii::$app->session->getFlash('error')?>
</div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
<div class="wrapper">
	<div class=" panel ">
		<div class="car-price-index">
	<?= \app\components\PageHeader::widget( [] ); ?>
	</div>
	</div>
	
	<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">
		<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
	</div>
	</div>
</div>
</div>