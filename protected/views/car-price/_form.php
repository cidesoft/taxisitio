<?php
use app\components\TActiveForm;
use app\models\Country;
use app\models\Driver;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CarPrice */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if (Yii::$app->session->hasFlash ('error')) { ?>
    <div id="alert" class="alert alert-danger" align="center">
    	<?php echo Yii::$app->session->getFlash('error'); ?>
	</div>
<?php }?>

<div class="wrapper">
	<div class="panel">
		<div class="panel-body"> 
            <?php
            $form = TActiveForm::begin([
                'layout' => 'horizontal',
                'id' => 'car-price-form'
            ]);
            ?>
        	 
        	<?php echo $form->field ( $model, 'country_id' )->dropDownList(Country::find()->select(['title', 'id'])->indexBy('id')->column(),['prompt'=>'Select Country']);?>
        	 
        			
        	<?php echo $form->field($model, 'type_id')->dropDownList(Driver::getTypeOptions(), ['prompt' => 'Select Car Type'])?>
        	 
        	<?=$form->field($model, 'seat')->textInput(); ?>
        			 
        	<?=$form->field($model, 'base_price')->textInput()?>
        
        	<?=$form->field($model, 'price_mile')->textInput()?>
        	 	 
        	<?=$form->field($model, 'price_min')->textInput()?>
        	 
        	<?=$form->field($model, 'description')->textArea(); ?>
        
        	<div class="form-group">
        		<div class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
                	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['id'=>'car-price-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success'])?>
            	</div>
        	</div>

     		<?php TActiveForm::end(); ?>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#alert").fadeIn();
	$("#alert").fadeOut(3000);

});
</script>