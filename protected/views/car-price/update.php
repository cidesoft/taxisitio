<?php

/* @var $this yii\web\View */
/* @var $model app\models\CarPrice */

/*
 * $this->title = Yii::t('app', 'Update {modelClass}: ', [
 * 'modelClass' => 'Car Price',
 * ]) . ' ' . $model->id;
 */
$this->title = $model->id.' - Update | '. Yii::$app->name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Car Prices' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>
<div class="wrapper">
	
		<div class=" panel ">
<div class="car-price-update">
	<?=  \app\components\PageHeader::widget(['model' => $model, 'title' => $model->id]); ?>
	</div>
</div>
<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">	
	<?= $this->render ( '_form', [ 'model' => $model ] )?>
</div>
</div>
</div>
</div>