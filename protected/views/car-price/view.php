<?php
use app\models\Driver;

/* @var $this yii\web\View */
/* @var $model app\models\CarPrice */

/* $this->title = $model->label () . ' : ' . $model->id; */
$this->title = Yii::t('app', 'Car Price').' - '.$model->id.' | '. Yii::$app->name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Car Prices' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id 
];
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" panel ">
<?=  \app\components\PageHeader::widget(['model'=>$model, 'title' => $model->id]); ?>
</div>
</div>
<div class="panel panel-margin">
			<div class="panel-body">
				<div class="content-section clearfix">	
    		<?php
						echo \app\components\TDetailView::widget ( [ 
								'model' => $model,
								'options' => [ 
										'class' => 'table table-bordered' 
								],
								'attributes' => [ 
										'id',
										[ 
												'attribute' => 'type_id',
												'value' => Driver::getTypeOptions ( $model->type_id ) 
										],
										[ 
												'attribute' => 'base_price',
												'value' => $model->currency_symbol . $model->base_price 
										],
										[ 
												'attribute' => 'price_mile',
												'value' => $model->currency_symbol . $model->price_mile 
										],
										[ 
												'attribute' => 'price_min',
												'value' => $model->currency_symbol . $model->price_min 
										],
										'seat',
										[ 
												'attribute' => 'country_id',
												'value' => $model->country ? $model->country->title : '' 
										],
										'create_time',
										[ 
												'attribute' => 'create_user_id',
												'value' => $model->createUser->first_name
										],
										[
												'attribute' => 'description',
												'format' => 'raw',
												'value' => '<p>'.$model->description.'</p>'
										], 
								] 
						] )?>
		</div>
		</div>
	</div>
</div>