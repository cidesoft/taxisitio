<?php
use app\components\TGridView;
use yii\widgets\Pjax;
use app\models\CarPrice;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CarPrice $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				echo TGridView::widget ( [ 
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'emptyText' => '<i>No Car Price Found</i>',
						'tableOptions' => [ 
								'class' => 'table table-bordered table-hover' 
						],
						'columns' => [ 
								'id',
								[ 
										'attribute' => 'country_id',
										'value' => function ($data) {
											return $data->country ? $data->country->title : '';
										} 
								],
								'base_price',
								'price_mile',
								'price_min',
								[ 
										'attribute' => 'type_id',
										'filter' => CarPrice::getTypeOptions (),
										'value' => function ($data) {
											return CarPrice::getTypeOptions ( $data->type_id );
										} 
								],
								[ 
										'class' => 'yii\grid\ActionColumn',
										'header' => '<a>Actions</a>' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>