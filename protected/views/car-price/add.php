<?php


/* @var $this yii\web\View */
/* @var $model app\models\CarPrice */

/* $this->title = Yii::t ( 'app', '{modelClass}', [ 
		'modelClass' => 'Car Price' 
] ); */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Car Prices' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] =(string)$model;
?>

<div class="car-price-create">
	<?=  \app\components\PageHeader::widget(); ?>
	<div class="vd_content-section clearfix">
		<?= $this->render ( '_form', [ 'model' => $model ] )?>
	</div>
</div>