<?php
use app\assets\AppAsset;
use app\models\User;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

$user = Yii::$app->user->identity;

/* @var $this \yii\web\View */
/* @var $content string */
/* $this->title = yii::$app->name;
 */
AppAsset::register ( $this );
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>" />
    <?= Html::csrfMetaTags()?>
  	<title><?php echo $this->title?></title>	
    <?php $this->head()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Mosaddek">
	<meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina">
	<link rel="shortcut icon" href="<?php echo $this->theme->getUrl('img/favicon.ico')?>" type="image/png">
	<link href="<?php echo $this->theme->getUrl('css/slidebars.css')?>" rel="stylesheet">
	<link href="<?php echo $this->theme->getUrl('js/switchery/switchery.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo $this->theme->getUrl('css/style-admin.css')?>"rel="stylesheet">
	<link href="<?php echo $this->theme->getUrl('css/dropzone.css')?>" rel="stylesheet">
	<link href="<?php echo $this->theme->getUrl('css/style-responsive.css')?>" rel="stylesheet">
	<link href="<?php echo $this->theme->getUrl('css/layout-theme-two.css')?>" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body class="sticky-header">
<?php $this->beginBody()?>
    <section>
		<div class="sidebar-left">
			<div class="logo theme-logo-bg visible-xs-* visible-sm-*">
				<a href="<?php echo Url::home();?>"> <span class="brand-name"><?= Html::encode(Yii::$app->name) ?></span>
				</a>
			</div>
			<div class="sidebar-left-info">
				<div class=" search-field"></div>
				<?= Menu::widget ( [ 'encodeLabels' => false,'activateParents' => true,'items' => $this->context->renderNav (),'options' => [ 'class' => 'nav nav-pills nav-stacked side-navigation' ],'submenuTemplate' => "\n<ul class='child-list'>\n{items}\n</ul>\n" ] );?>
			</div>
		</div>
		<div class="body-content">
			<div class="header-section bg-danger light-color">
				<div class="logo theme-logo-bg hidden-xs hidden-sm">
					<a href="<?php echo Url::home();?>">
						<span class="brand-name"><?=Html::encode(Yii::$app->name) ?></span>
					</a>
				</div>
				<a class="toggle-btn"><i class="fa fa-outdent"></i></a>
				<div class="notification-wrap">
					<div class="right-notification">
						<ul class="notification-menu">
							<li><a href="javascript:;"
								class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                               <?php if(!empty(Yii::$app->user->identity->profile_file)){
								echo Html::img ( [ 
										'/user/download',
										'profile_file' => Yii::$app->user->identity->profile_file 
								], [ 
										'height' => '50',
										'width' => '50' 
								] ); } 
								if(empty(Yii::$app->user->identity->profile_file)){
			echo Html::img ( [ 
					'/themes/green/img/default.png' 
			], [ 
					'height' => '50',
					'width' => '50' 
			] );}?>
                                <?php echo Yii::$app->user->identity->first_name;?>
                                <span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu purple pull-right">
									<li><a href="<?php echo Url::toRoute(['/user/profile','id'=>Yii::$app->user->id]);?>"><i class="fa fa-user pull-right"></i> Profile</a></li>
									<li><a href="<?php echo Url::toRoute(['/user/changepassword','id'=>Yii::$app->user->id]);?>"><span class="fa fa-key pull-right"></span> <span> Change Password</span></a></li>
									<li><a href="<?php echo Url::toRoute(['/user/profile-update','id'=>Yii::$app->user->id]);?>"><span class="fa fa-pencil pull-right"></span> Update</a></li>
									<li><a href="<?php echo Url::toRoute(['/user/logout']);?>"><i class="fa fa-sign-out pull-right"></i> Log Out	</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<section class=" main_wrapper">
                <?=  $content; ?>
			</section>
			<footer>
				<div class="">
					<div class="row text-center">
						<p target="_blank"> <?php echo ' &copy; ' . date('Y').' '. Yii::$app->name .' | All Rights Reserved | Powered by <a href="'.Yii::$app->params['companyUrl'].'">ToXSL TECHNOLOGIES</a>' ; ?>.</p>
					</div>
				</div>
			</footer>
		</div>
	</section>


	<script src="<?php echo $this->theme->getUrl('js/jquery-migrate.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/bootstrap.min.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/modernizr.min.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/jquery.nicescroll.js')?>" type="text/javascript"></script>
	<script src="<?php echo $this->theme->getUrl('js/slidebars.min.js')?>"></script>
	<script	src="<?php echo $this->theme->getUrl('js/switchery/switchery.min.js')?>"></script>
	<script	src="<?php echo $this->theme->getUrl('js/switchery/switchery-init.js')?>"></script>
	<script	src="<?php echo $this->theme->getUrl('js/sparkline/jquery.sparkline.js')?>"></script>
	<script	src="<?php echo $this->theme->getUrl('js/sparkline/sparkline-init.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/scripts.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/dropzone.js')?>"></script>        
	<?php $this->endBody()?>
	</body>
	<script>
	$(document).ready(function() {
		function setHeight() {
			windowHeight = $(window).innerHeight() -110;
			$('.main_wrapper').css('min-height', windowHeight);
		}
		;
		setHeight();
		$(window).resize(function() {
			setHeight();
		});
	});
	</script>
<?php $this->endPage()?>
</html>