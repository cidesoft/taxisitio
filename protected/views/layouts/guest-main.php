<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Alert;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\JiMenu;
use app\models\search\MenuSearch;
use app\models\User;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register ( $this );

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>" />
	<?= Html::csrfMetaTags()?>
	  
	<?php $this->head()?>    
	<title><?php echo $this->title?></title>	
	<link rel="shortcut icon"
	href="<?php echo $this->theme->getUrl('img/favicon.ico')?>">
<link href="<?php echo $this->theme->getUrl('css/slidebars.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('js/switchery/switchery.min.css')?>"
	rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo $this->theme->getUrl('css/style.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('css/style-responsive.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('css/layout-theme-two.css')?>"
	rel="stylesheet">
<script type="text/javascript"
	src="<?php echo $this->theme->getUrl('js/custom.js')?>"></script>
</head>
<body class="sticky-header">
	<?php $this->beginBody()?>
		<section>
		<header role="banner" id="top"
			class="navbar navbar-static-top bs-docs-nav bg-danger light-color ">
			<div class="">
				<div class="container">

					<div class="navbar-header">
						<button aria-expanded="false" aria-controls="bs-navbar"
							data-target="#bs-navbar" data-toggle="collapse" type="button"
							class="navbar-toggle collapsed">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="javascript::"> 
						</a>
					</div>
					<nav class="collapse navbar-collapse" id="bs-navbar">
						<ul class=" nav navbar-nav navbar-right mega-menu">
							<?php if(\Yii::$app->user->isGuest){?>
								<li><a href="<?php echo Url::to(['user/login']);?>">Login</a></li>
							<?php }	else { ?>
								<li><a href="<?php echo Url::to(['user/dashboard']);?>">Dashboard</a></li>
							<?php }?>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<div class="well site-index back">
			<div class="main_wrapper">
			  
				<div class="">
				
				
	                 	<?= $content?>
	            	</div>
			</div>
			<footer>
			<div class="text-center footer-bottom">	<?php echo ' &copy; ' . date('Y').' '. Yii::$app->name .' | All Rights Reserved | Powered by <a href="'.Yii::$app->params['companyUrl'].'">ToXSL TECHNOLOGIES</a>' ; ?></div>
		</footer>
		</div>
		
	</section>
	<script src="<?= $this->theme->getUrl('js/bootstrap.min.js')?>"></script>
	<script src="<?= $this->theme->getUrl('js/jquery.nicescroll.js')?>"
		type="text/javascript"></script>
	<script src="<?php echo $this->theme->getUrl('js/slidebars.min.js')?>"></script>
	<script src="<?php echo $this->theme->getUrl('js/scripts.js')?>"></script>
	<?php $this->endBody()?>
	
	<script>
		$(document).ready(function() {
			  function setHeight() {
			    windowHeight = $(window).innerHeight()-85;
			    $('.main_wrapper').css('min-height', windowHeight);
			  };
			  setHeight();	  
			  $(window).resize(function() {
			    setHeight();
			  });
		});
	</script>
</body>
<?php $this->endPage()?>
</html>