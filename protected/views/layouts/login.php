<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Alert;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\JiMenu;
use app\models\MenuSearch;
use app\models\User;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register ( $this );
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>" />
    <?= Html::csrfMetaTags()?>
   
    <?php $this->head()?>
    <link rel="shortcut icon" href="img/ico/favicon.png">
<title>Login</title>

<!-- Base Styles -->
<link href="<?php echo $this->theme->getUrl('css/style.css')?>"
	rel="stylesheet">
<link
	href="<?php echo $this->theme->getUrl('css/style-responsive.css')?>"
	rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->


</head>

<body class="login-body">
<?php $this->beginBody()?>
                     <?= $content?>

      <!--jquery-1.10.2.min-->
	<script
		src="<?php echo $this->theme->getUrl('js/jquery-1.11.1.min.js')?>"></script>
	<!--Bootstrap Js-->
	<script src="<?php echo $this->theme->getUrl('js/bootstrap.min.js')?>"></script>

<?php $this->endBody()?>
</body>
  <?php $this->endPage()?>

</html>
