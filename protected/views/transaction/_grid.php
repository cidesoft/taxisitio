<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Transaction $searchModel
 */

?>
<style>
.ui-widget-content {
    background: #227521 !important; 
</style>

<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'transaction-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								
								'id',
								[ 
										'attribute' => 'type_id',
										'filter' => isset ( $searchModel ) ? $searchModel->getTypeOptions () : null,
										'value' => function ($data) {
											return $data->getType ();
										} 
								],
								 [
										'attribute' => 'created_on',
										'filter' => \yii\jui\DatePicker::widget ( [
												'model' => $searchModel,
												'attribute' => 'created_on',
												'dateFormat' => 'yyyy-MM-dd',
												'options' => [
														'class' => 'form-control '
												],
												'clientOptions' => [
														'changeMonth' => true,
														'changeYear' => true
												]
										] ),
										'value' => function ($data) {
										return date ( 'Y-m-d', strtotime ( $data->created_on) );
												},
								], 				
								[
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
										return $data->createUser->first_name.' '.$data->createUser->last_name;
								}
								],
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>', 'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

