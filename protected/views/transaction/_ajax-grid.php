 <?php
	use app\components\TGridView;
	use yii\widgets\Pjax;
	/**
	 *
	 * @var yii\web\View $this
	 * @var yii\data\ActiveDataProvider $dataProvider
	 * @var app\models\search\Transaction $searchModel
	 */
	
	?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'transaction-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'amount',
								[ 
										'attribute' => 'type_id',
										'filter' => isset ( $searchModel ) ? $searchModel->getTypeOptions () : null,
										'value' => function ($data) {
											return $data->getType ();
										} 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser->first_name;
										} 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}'
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

