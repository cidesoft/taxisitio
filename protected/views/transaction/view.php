<?php
use app\components\PageHeader;
use app\components\TDetailView;
use app\models\Transaction;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Transactions' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $model->id;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="transaction-view">
			<?php echo  PageHeader::widget(['model'=>$model, 'title' => $model->id]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo TDetailView::widget ( [ 
						'id' => 'transaction-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'amount',
								[ 
										'attribute' => 'transaction_id',
										'value' => isset ( $model->transaction_id ) ? $model->transaction_id : '(not set)' 
								],
								'ride_id',
								[ 
										'attribute' => 'type_id',
										'value' => $model->getType () 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser->first_name . ' ' . $data->createUser->last_name;
										} 
								] 
						] 
				] )?>

		</div>
	</div>
	<?php if ($model->wallet_history_id) {?>
	
	<div class="panel">
		<div class="panel-body">
			<?php 	$this->context->startPanel ();
					
					$this->context->addPanel ( 'Wallet History', 'walletHistory', 'WalletHistory', $model );
					
					$this->context->endPanel ();
			?>
		</div>
	</div>
	<?php }?>
</div>	