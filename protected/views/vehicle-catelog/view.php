<?php

use app\components\useraction\UserAction;

/* @var $this yii\web\View */
/* @var $model app\models\VehicleCatelog */

/*$this->title =  $model->label() .' : ' . $model->id; */
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehicle Catelogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = (string)$model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div
			class="vehicle-catelog-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php echo \app\components\TDetailView::widget([
    	'id'	=> 'vehicle-catelog-detail-view',
        'model' => $model,
        'options'=>['class'=>'table table-bordered'],
        'attributes' => [
            'id',
            'economic_no',
            'brand',
            'submarca',
            'model',
            'placas',
            'number_series',
            'date_added:datetime',
            'secure_policy',
            'insurance_carrier',
            'number_concession',
            'maturity_concession:datetime',
            'owner_name',
            'owner_address',
            'assigned_driver',
            'assigned_base',
            'front_photo',
            'rear_photo',
            'right_photo',
            'left_photo',
            [
			'attribute' => 'status',
			'format'=>'raw',
			'value' => $model->getStateBadge(),],
            'low_date:datetime',
            [
			'attribute' => 'state_id',
			'format'=>'raw',
			'value' => $model->getStateBadge(),],
            [
			'attribute' => 'type_id',
			'value' => $model->getType(),
			],
            'create_time:datetime',
            'update_time:datetime',
            [
			'attribute' => 'create_user_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('create_user_id'),
			],
        ],
    ]) ?>


<?php  ?>


		<?php				echo UserAction::widget ( [
						'model' => $model,
						'attribute' => 'state_id',
						'states' => $model->getStateOptions ()
				] );
				?>

		</div>
</div>
 
	<div class=" panel ">
		<div class=" panel-body ">

<?php echo app\components\comment\CommentsWidget::widget(['model'=>$model]); ?>
			</div>
	</div>
</div>
