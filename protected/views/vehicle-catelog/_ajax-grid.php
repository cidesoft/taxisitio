<?php

use app\components\TGridView;
use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\VehicleCatelog $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php echo TGridView::widget([
    	'id' => 'vehicle-catelog-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions'=>['class'=>'table table-bordered'],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],

            'id',
            'economic_no',
            'brand',
            /* 'submarca',*/
            /* 'model',*/
            /* 'placas',*/
            /* 'number_series',*/
            'date_added:datetime',
            /* 'secure_policy',*/
            /* 'insurance_carrier',*/
            /* 'number_concession',*/
            /* 'maturity_concession:datetime',*/
            /* 'owner_name',*/
            /* 'owner_address',*/
            /* 'assigned_driver',*/
            /* 'assigned_base',*/
            /* 'front_photo',*/
            /* 'rear_photo',*/
            /* 'right_photo',*/
            /* 'left_photo',*/
            [
			'attribute' => 'status','format'=>'raw','filter'=>isset($searchModel)?$searchModel->getStateOptions():null,
			'value' => function ($data) { return $data->getStateBadge();  },],
            'low_date:datetime',
            [
			'attribute' => 'state_id','format'=>'raw','filter'=>isset($searchModel)?$searchModel->getStateOptions():null,
			'value' => function ($data) { return $data->getStateBadge();  },],
            /* ['attribute' => 'type_id','filter'=>isset($searchModel)?$searchModel->getTypeOptions():null,
			'value' => function ($data) { return $data->getType();  },],*/
            'create_time:datetime',
            /* 'update_time:datetime',*/
            /* [
				'attribute' => 'create_user_id',
				'format'=>'raw',
				'value' => function ($data) { return $data->getRelatedDataLink('create_user_id');  },
				],*/

            ['class' => 'app\components\TActionColumn','header'=>'<a>Actions</a>'],
        ],
    ]); ?>
<?php Pjax::end(); ?>

