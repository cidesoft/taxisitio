<?php

use yii\helpers\Html;
use app\components\TActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VehicleCatelog */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php echo strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="panel-body">

    <?php 
$form = TActiveForm::begin([
						'layout' => 'horizontal',
						'id'	=> 'vehicle-catelog-form',
						]);
?>





<div class="col-md-6">

	
		 <?php echo $form->field($model, 'economic_no')->textInput(['maxlength' => 256]) ?>
	 		


		 <?php echo $form->field($model, 'brand')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php /*echo $form->field($model, 'submarca')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'model')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'placas')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'number_series')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php echo $form->field($model, 'date_added')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) ?>
	 		


		 <?php /*echo $form->field($model, 'secure_policy')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'insurance_carrier')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'number_concession')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'maturity_concession')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'owner_name')->textInput(['maxlength' => 255]) */ ?>
	 		

	</div>
	<div class="col-md-6">

		
		 <?php /*echo $form->field($model, 'owner_address')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'assigned_driver')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'assigned_base')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'front_photo')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'rear_photo')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'right_photo')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'left_photo')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php echo $form->field($model, 'status')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		


		 <?php echo $form->field($model, 'low_date')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) ?>
	 		


		 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		


		 <?php /*echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) */ ?>
	 			</div>

	


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'vehicle-catelog-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
