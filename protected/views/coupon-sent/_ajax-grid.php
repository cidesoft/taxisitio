<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CouponSent $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'coupon-sent-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'enableRowClick' => false,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								[ 
										'attribute' => 'coupon_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->getRelatedDataLink ( 'coupon_id' );
										} 
								],
								
								[ 
										'attribute' => 'user_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->user->full_name ;
										} 
								],
								'discount',
								[
										'attribute' => 'state_id',
										'format' => 'raw',
										'filter' => isset ( $searchModel ) ? $searchModel->getStateOptions () : null,
										'value' => function ($data) {
										return $data->getStateBadge ();
								}
								],
								'start_date:date',
								'end_date:date',
								
								/* [ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}' 
								]  */
						] 
				] );
				?>
<?php Pjax::end(); ?>

