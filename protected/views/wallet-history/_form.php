<?php

use yii\helpers\Html;
use app\components\TActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WalletHistory */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php echo strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="panel-body">

    <?php 
$form = TActiveForm::begin([
						'layout' => 'horizontal',
						'id'	=> 'wallet-history-form',
						]);
?>





<div class="col-md-6">

	
		 <?php echo $form->field($model, 'wallet_id')->textInput() ?>
	 		


		 <?php echo $form->field($model, 'initial_amount')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'transaction_amount')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'total_balance')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php /*echo $form->field($model, 'transaction_id')->textInput(['maxlength' => 255]) */ ?>
	 		

	</div>
	<div class="col-md-6">

		
		 <?php /*echo $form->field($model, 'ride_id')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'comment')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		


		 <?php echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) ?>
	 			</div>

	


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'wallet-history-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
