<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\WalletHistory $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'wallet-history-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'enableRowClick' => false,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'transaction_amount',
								'transaction_id',
								
								[ 
										'attribute' => 'type_id',
										'filter' => isset ( $searchModel ) ? $searchModel->getTypeOptions () : null,
										'value' => function ($data) {
											return $data->getType ();
										} 
								],
								'created_on:datetime',
								
						] 
				] );
				?>
<?php Pjax::end(); ?>

