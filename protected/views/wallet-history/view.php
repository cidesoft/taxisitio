<?php

use app\components\useraction\UserAction;

/* @var $this yii\web\View */
/* @var $model app\models\WalletHistory */

/*$this->title =  $model->label() .' : ' . $model->id; */
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wallet Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = (string)$model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div
			class="wallet-history-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php echo \app\components\TDetailView::widget([
    	'id'	=> 'wallet-history-detail-view',
        'model' => $model,
        'options'=>['class'=>'table table-bordered'],
        'attributes' => [
            'id',
            'wallet_id',
            'initial_amount',
            'transaction_amount',
            'total_balance',
            'transaction_id',
            'ride_id',
            'comment',
            [
			'attribute' => 'type_id',
			'value' => $model->getType(),
			],
            'created_on:datetime',
        		[
        				'attribute' => 'create_user_id',
        				'value' => $model->createUser ? $model->createUser->fullName : ''
        		]
        ],
    ]) ?>


<?php  ?>


	

		</div>
</div>
 

</div>
