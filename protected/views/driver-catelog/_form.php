<?php

use yii\helpers\Html;
use app\components\TActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DriverCatelog */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php echo strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="panel-body">

    <?php 
$form = TActiveForm::begin([
						'layout' => 'horizontal',
						'id'	=> 'driver-catelog-form',
						]);
?>





<div class="col-md-6">

	
		 <?php echo $form->field($model, 'first_name')->textInput(['maxlength' => 256]) ?>
	 		


		 <?php echo $form->field($model, 'last_name')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php /*echo $form->field($model, 'mother_last_name')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'contact_no')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'street_address')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_number')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'cp_address_id')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'cp_address')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_colonia_id')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_colonia')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_municipality_id')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_municipality')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'address_status')->textInput(['maxlength' => 512]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'rfc')->textInput(['maxlength' => 256]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'date_of_birth')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		

	</div>
	<div class="col-md-6">

		
		 <?php /*echo $form->field($model, 'age')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'ine')->textInput(['maxlength' => 256]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'curp')->textInput(['maxlength' => 256]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'marital_status')->textInput() */ ?>
	 		


		 <?php echo $form->field($model, 'date_added')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) ?>
	 		


		 <?php echo $form->field($model, 'license_no')->textInput(['maxlength' => 256]) ?>
	 		


		 <?php /*echo $form->field($model, 'expiration_date')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'hard_working_time')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		


		 <?php echo  $form->field($model, 'comment')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'comment')->textarea(['rows' => 6]); ?>
	 		


		 <?php /*echo $form->field($model, 'avg_rating')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'image_file')->fileInput() */ ?>
	 		


		 <?php echo $form->field($model, 'status')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		


		 <?php echo $form->field($model, 'low_date')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) ?>
	 		


		 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		


		 <?php /*echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) */ ?>
	 			</div>

	


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'driver-catelog-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
