<?php
use app\components\useraction\UserAction;

/* @var $this yii\web\View */
/* @var $model app\models\DriverCatelog */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Driver Catelogs' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="driver-catelog-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'driver-catelog-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								[ 
										'attribute' => 'first_name',
										
										'value' => isset ( $model->first_name ) ? $model->first_name : 'Not Set' 
								],
								
								[ 
										'attribute' => 'last_name',
										
										'value' => isset ( $model->last_name ) ? $model->last_name : 'Not Set' 
								],
								[ 
										'attribute' => 'mother_last_name',
										
										'value' => isset ( $model->last_name ) ? $model->last_name : 'Not Set' 
								],
								[ 
										'attribute' => 'contact_no',
										
										'value' => isset ( $model->contact_no ) ? $model->contact_no : 'Not Set' 
								],
								
								[ 
										'attribute' => 'street_address',
										
										'value' => isset ( $model->street_address ) ? $model->street_address : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_number',
										
										'value' => isset ( $model->address_number ) ? $model->address_number : 'Not Set' 
								],
								
								[ 
										'attribute' => 'cp_address_id',
										
										'value' => isset ( $model->cp_address_id ) ? $model->cp_address_id : 'Not Set' 
								],
								
								[ 
										'attribute' => 'cp_address_id',
										
										'value' => isset ( $model->cp_address_id ) ? $model->cp_address_id : 'Not Set' 
								],
								
								[ 
										'attribute' => 'cp_address',
										
										'value' => isset ( $model->cp_address ) ? $model->cp_address : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_colonia_id',
										
										'value' => isset ( $model->address_colonia_id ) ? $model->address_colonia_id : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_colonia',
										
										'value' => isset ( $model->address_colonia ) ? $model->address_colonia : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_municipality_id',
										
										'value' => isset ( $model->address_municipality_id ) ? $model->address_municipality_id : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_municipality',
										
										'value' => isset ( $model->address_municipality ) ? $model->address_municipality : 'Not Set' 
								],
								
								[ 
										'attribute' => 'address_status',
										
										'value' => isset ( $model->address_status ) ? $model->address_status : 'Not Set' 
								],
								
								[ 
										'attribute' => 'rfc',
										
										'value' => isset ( $model->rfc ) ? $model->rfc : 'Not Set' 
								],
								
								[ 
										'attribute' => 'date_of_birth',
										
										'value' => isset ( $model->date_of_birth ) ? $model->date_of_birth : 'Not Set' 
								],
								
								[ 
										'attribute' => 'age',
										
										'value' => isset ( $model->age ) ? $model->age : 'Not Set' 
								],
								
								[ 
										'attribute' => 'ine',
										
										'value' => isset ( $model->ine ) ? $model->ine : 'Not Set' 
								],
								[ 
										'attribute' => 'curp',
										
										'value' => isset ( $model->curp ) ? $model->curp : 'Not Set' 
								],
								
								[ 
										'attribute' => 'marital_status',
										
										'value' => isset ( $model->marital_status ) ? $model->marital_status : 'Not Set' 
								],
								
								[ 
										'attribute' => 'date_added',
										
										'value' => isset ( $model->date_added ) ? $model->date_added : 'Not Set' 
								],
								
								[ 
										'attribute' => 'license_no',
										
										'value' => isset ( $model->license_no ) ? $model->license_no : 'Not Set' 
								],
								
								[ 
										'attribute' => 'expiration_date',
										
										'value' => isset ( $model->expiration_date ) ? $model->expiration_date : 'Not Set' 
								],
								
								[ 
										'attribute' => 'hard_working_time',
										
										'value' => isset ( $model->hard_working_time ) ? $model->hard_working_time : 'Not Set' 
								],
								
								[ 
										'attribute' => 'avg_rating',
										
										'value' => isset ( $model->avg_rating ) ? $model->avg_rating : 'Not Set' 
								],
								
								[ 
										'attribute' => 'economic_no',
										
										'value' => isset ( $model->vehicleCatelog->economic_no ) ? $model->vehicleCatelog->economic_no : 'Not Set' 
								],
								
								[ 
										'attribute' => 'brand',
										
										'value' => isset ( $model->vehicleCatelog->brand ) ? $model->vehicleCatelog->brand : 'Not Set' 
								],
								
								[ 
										'attribute' => 'submarca',
										
										'value' => isset ( $model->vehicleCatelog->submaca ) ? $model->vehicleCatelog->submarca : 'Not Set' 
								],
								[ 
										'attribute' => 'number_series',
										
										'value' => isset ( $model->vehicleCatelog->number_series ) ? $model->vehicleCatelog->number_series : 'Not Set' 
								],
								
								[ 
										'attribute' => 'placas',
										
										'value' => isset ( $model->vehicleCatelog->placas ) ? $model->vehicleCatelog->placas : 'Not Set' 
								],
								[ 
										'attribute' => 'date_added',
										
										'value' => isset ( $model->vehicleCatelog->date_added ) ? $model->vehicleCatelog->date_added : 'Not Set' 
								],
								
								[ 
										'attribute' => 'secure_policy',
										
										'value' => isset ( $model->vehicleCatelog->secure_policy ) ? $model->vehicleCatelog->secure_policy : 'Not Set' 
								],
								
								[ 
										'attribute' => 'number_concession',
										
										'value' => isset ( $model->vehicleCatelog->number_concession ) ? $model->vehicleCatelog->number_concession : 'Not Set' 
								],
								
								[ 
										'attribute' => 'insurance_carrier',
										
										'value' => isset ( $model->vehicleCatelog->insurance_carrier ) ? $model->vehicleCatelog->insurance_carrier : 'Not Set' 
								],
								
								[ 
										'attribute' => 'maturity_concession',
										
										'value' => isset ( $model->vehicleCatelog->maturity_concession ) ? $model->vehicleCatelog->maturity_concession : 'Not Set' 
								],
								
								[ 
										'attribute' => 'owner_name',
										
										'value' => isset ( $model->vehicleCatelog->owner_name ) ? $model->vehicleCatelog->owner_name : 'Not Set' 
								],
								[ 
										'attribute' => 'owner_address',
										
										'value' => isset ( $model->vehicleCatelog->owner_address ) ? $model->vehicleCatelog->owner_address : 'Not Set' 
								],
								
								[ 
										'attribute' => 'assigned_base',
										
										'value' => isset ( $model->vehicleCatelog->assigned_base ) ? $model->vehicleCatelog->assigned_base : 'Not Set' 
								],
								'comment:html',
								
								'image_file',
								[ 
										'attribute' => 'status',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								'low_date:datetime',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								[ 
										'attribute' => 'type_id',
										'value' => $model->getType () 
								],
								'create_time:datetime',
								'update_time:datetime',
								[ 
										'attribute' => 'create_user_id',
										'format' => 'raw',
										'value' => $model->getRelatedDataLink ( 'create_user_id' ) 
								] 
						] 
				] )?>


<?php  ?>


		<?php
		
		echo UserAction::widget ( [ 
				'model' => $model,
				'attribute' => 'state_id',
				'states' => $model->getStateOptions () 
		] );
		?>

		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">

<?php echo app\components\comment\CommentsWidget::widget(['model'=>$model]); ?>
			</div>
	</div>
</div>
