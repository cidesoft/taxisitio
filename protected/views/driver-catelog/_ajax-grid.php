<?php

use app\components\TGridView;
use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\DriverCatelog $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php echo TGridView::widget([
    	'id' => 'driver-catelog-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions'=>['class'=>'table table-bordered'],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],

            'id',
            'first_name',
            'last_name',
            /* 'mother_last_name',*/
            /* 'contact_no',*/
            /* 'street_address',*/
            /* 'address_number',*/
            /* 'cp_address_id',*/
            /* 'cp_address',*/
            /* 'address_colonia_id',*/
            /* 'address_colonia',*/
            /* 'address_municipality_id',*/
            /* 'address_municipality',*/
            /* 'address_status',*/
            /* 'rfc',*/
            /* 'date_of_birth:date',*/
            /* 'age',*/
            /* 'ine',*/
            /* 'curp',*/
            /* 'marital_status',*/
            'date_added:datetime',
            'license_no',
            /* 'expiration_date:datetime',*/
            /* 'hard_working_time:datetime',*/
            /* 'comment:html',*/
            /* 'avg_rating',*/
            /* ['attribute' => 'image_file','filter'=>$searchModel->getFileOptions(),
			'value' => function ($data) { return $data->getFileOptions($data->image_file);  },],*/
            [
			'attribute' => 'status','format'=>'raw','filter'=>isset($searchModel)?$searchModel->getStateOptions():null,
			'value' => function ($data) { return $data->getStateBadge();  },],
            'low_date:datetime',
            [
			'attribute' => 'state_id','format'=>'raw','filter'=>isset($searchModel)?$searchModel->getStateOptions():null,
			'value' => function ($data) { return $data->getStateBadge();  },],
            /* ['attribute' => 'type_id','filter'=>isset($searchModel)?$searchModel->getTypeOptions():null,
			'value' => function ($data) { return $data->getType();  },],*/
            /* 'create_time:datetime',*/
            /* 'update_time:datetime',*/
            /* [
				'attribute' => 'create_user_id',
				'format'=>'raw',
				'value' => function ($data) { return $data->getRelatedDataLink('create_user_id');  },
				],*/

            ['class' => 'app\components\TActionColumn','header'=>'<a>Actions</a>'],
        ],
    ]); ?>
<?php Pjax::end(); ?>

