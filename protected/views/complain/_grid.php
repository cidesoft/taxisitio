<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Complain $searchModel
 */

?>
<style>
.ui-widget-content {
	background: #227521 !important;
}
</style>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'complain-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'ride_id',
								/* [ 
										'attribute' => 'ride_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->getRelatedDataLink ( 'ride_id' );
										} 
								], */
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->first_name . ' ' . $data->driver->last_name : '(not set)';
										} 
								],
								
								[ 
										'attribute' => 'created_on',
										'filter' => \yii\jui\DatePicker::widget ( [ 
												'model' => $searchModel,
												'attribute' => 'created_on',
												'dateFormat' => 'yyyy-MM-dd',
												'options' => [ 
														'class' => 'form-control ' 
												],
												'clientOptions' => [ 
														'changeMonth' => true,
														'changeYear' => true 
												] 
										] ),
										'value' => function ($data) {
											return date ( 'Y-m-d', strtotime ( $data->created_on ) );
										} 
								],
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
										return isset($data->createdBy) ? $data->createdBy->first_name . ' ' . $data->createdBy->last_name : '';
										} 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
								    'template'=>'{view}{delete}'
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

