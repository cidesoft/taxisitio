<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Complain $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
echo TGridView::widget ( [ 
						'id' => 'complain-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'comment:html',
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->first_name . ' ' . $data->driver->last_name : '(not set)';
										} 
								],
								'ride_id',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'filter' => isset ( $searchModel ) ? $searchModel->getStateOptions () : null,
										'value' => function ($data) {
											return $data->getStateBadge ();
										} 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createdBy->first_name . ' ' . $data->createdBy->last_name;
										} 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
								    'template'=>'{view}{delete}'
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

