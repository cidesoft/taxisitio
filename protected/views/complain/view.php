<?php

/* @var $this yii\web\View */
/* @var $model app\models\Complain */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Complains' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="complain-view">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'complain-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'comment:html',
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->first_name . ' ' . $data->driver->last_name : '(not set)';
										} 
								],
								'ride_id',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								'created_on:datetime',
								[ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => function ($data) {
										return isset($data->createdBy) ? $data->createdBy->first_name . ' ' . $data->createdBy->last_name : '';
										} 
								] 
						] 
				] )?>


		</div>
	</div>
</div>
