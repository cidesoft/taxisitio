<script type="text/javascript"
src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=<?php echo app\modules\api\controllers\RideController::GOOGLE_KEY ?>"></script>

<?php
use app\components\PageHeader;
use app\components\TDetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Ride */

/* $this->title = $model->label() .' : ' . $model->id; */
$this->title = Yii::t ( 'app', 'Ride' ) . ' - ' . $model->id . ' | ' . Yii::$app->name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Rides' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id,
		'url' => [ 
				'view' 
		] 
];
?>

<div class="wrapper">
	<div class=" panel ">
			<?php echo  PageHeader::widget(['model'=>$model, 'title' => $model->id]); ?>
	</div>
	

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				echo TDetailView::widget ( [ 
						'id' => 'ride-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'amount',
								'location',
								'destination',
								'number_of_passengers',
								'number_of_bags',
								'is_pet:boolean',
								'is_hourly:boolean',
								'is_updated:boolean',
								'number_of_hours',
								'journey_time',
								[ 
										'attribute' => 'journey_type',
										'format' => 'raw',
										'value' => $model->getJourneyOptions ( $model->journey_type ) 
								],
								[ 
										'attribute' => 'car_price_id',
										'value' => $model->carPrice ? $model->carPrice->getTypeOptions ( $model->carPrice->type_id ) : '' 
								],
								'start_time',
								'end_time',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								[ 
										'attribute' => 'driver_id',
										'value' => $model->driver ? $model->driver->first_name . ' ' . $model->driver->last_name : '(not assigned)' 
								],
								[ 
										'attribute' => 'create_user_id',
										'value' => $model->createUser ? $model->createUser->first_name . ' ' . $model->createUser->last_name : '' 
								] 
						] 
				] )?>
		</div>
	</div>
</div>
<div class="wrapper">
	<div class="panel">
		<div class="panel-body">
				<?php
				$this->context->startPanel ();
				
				$this->context->addPanel ( 'Notifications', 'notification', 'Notification', $model );
				$this->context->addPanel ( 'Coupon', 'coupon', 'Coupon', $model);
				$this->context->addPanel ( 'Transactions', 'transactions', 'Transaction', $model );
				$this->context->addPanel ( 'Reviews', 'review', 'Review', $model );
				$this->context->addPanel ( 'Complains', 'complain', 'Complain', $model );
				
				$this->context->endPanel ();
				?>
			
		</div>
	</div>
</div>
<div id="driver_googleMap" style="width: 100%; height: 400px"></div>
<script type="text/javascript">


var map = null;
var gmarkers = [];
var intervalNumber = 0;

$(document).ready(function() {
	initialize ();
});
function initialize() {
	var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var mapOptions = {
        center: new google.maps.LatLng(-1.0, 1.0),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    map = new google.maps.Map(document.getElementById("driver_googleMap"), mapOptions);
    directionsDisplay.setMap(map);
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }
    gmarkers = [];
    calculateAndDisplayRoute(directionsService, directionsDisplay);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
    	origin: new google.maps.LatLng('<?= isset($model->location_lat) ? $model->location_lat: ''?>','<?= isset($model->location_long) ? $model->location_long: ''?>'),
        destination: new google.maps.LatLng('<?= isset($model->destination_lat) ? $model->destination_lat: ''?>','<?= isset($model->destination_long) ? $model->destination_long: ''?>'),
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

setInterval(function () {
	$.ajax({
		url:'<?= Url::toRoute(['ride/get-location', 'id' => $model->id]) ?>',
		success:function(response) {
			if(response.status === "OK") {
				update_map(response.data);
			    intervalNumber++;
			}
		}
	});    
}, 2000);

update_map = function (data) {
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }
    gmarkers = [];
    latLng = new google.maps.LatLng(data.lat, data.lng);
    bounds.extend(latLng);

    var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
    var image = {
           // url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
            path: car,        
            anchor: new google.maps.Point(0, 32)
    };
          var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
          };
        
    var marker = new google.maps.Marker({
         position: latLng,
         map: map,
         icon:image,
         title: data.title,
    });
    var infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, "click", function (e) {
         infoWindow.setContent(data.description+"<br>"+marker.getPosition().toUrlValue(6));
         infoWindow.open(map, marker);
    });
    (function (marker, data) {
         google.maps.event.addListener(marker, "click", function (e) {
             infoWindow.open(map, marker);
         });
    })(marker, data);
    gmarkers.push(marker);
};


</script>
