<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Ride $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'ride-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'location',
								'destination',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'filter' => isset ( $searchModel ) ? $searchModel->getStateOptions () : null,
										'value' => function ($data) {
											return $data->getStateBadge ();
										} 
								],
								[ 
										'attribute' => 'driver_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->driver ? $data->driver->first_name . ' ' . $data->driver->last_name : '(not set)';
										} 
								],
								[ 
										'attribute' => 'create_user_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser ? $data->createUser->first_name . ' ' . $data->createUser->last_name : '(Not Set)';
										} 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

