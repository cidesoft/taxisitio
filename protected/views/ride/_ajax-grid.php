<?php
use app\components\TGridView;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Ride $searchModel
 */
$user= User::find ()->where ( [ 
		'id' => $_GET ['id'] 
] )->one ();
?>
<?php  Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false]);?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'ride-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'location',
								'destination',
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'filter' => isset ( $searchModel ) ? $searchModel->getStateOptions () : null,
										'value' => function ($data) {
											return $data->getStateBadge ();
										} 
								],
								[ 
										'attribute' => 'driver_id',
										'value' => function ($data) {
											return $data->driver ? $data->driver->first_name : '';
										},
										
										'visible' => ($user->role_id == User::ROLE_PASSENGER) ? true : false 
								
								],
								[ 
										'attribute' => 'create_user_id',
										'format' => 'raw',
										'value' => function ($data) {
											return $data->createUser ? $data->createUser->first_name : '';
										},
										'visible' => ($user->role_id == User::ROLE_DRIVER) ? true : false 
								],
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

