<?php

use yii\helpers\Html;
use app\components\TActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ride */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php echo strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="panel-body">

    <?php 
$form = TActiveForm::begin([
						'layout' => 'horizontal',
						'id'	=> 'ride-form',
						]);
?>
	




<div class="col-md-6">
	
			
		 <?php /*echo $form->field($model, 'amount')->textInput(['maxlength' => 255]) */ ?>
	 		

		
		 <?php echo $form->field($model, 'location')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'location_lat')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'location_long')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'destination')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'destination_lat')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'destination_long')->textInput(['maxlength' => 255]) ?>
	 		

		
		 <?php echo $form->field($model, 'number_of_passengers')->textInput() ?>
	 		

		
		 <?php echo $form->field($model, 'number_of_bags')->textInput() ?>
	 		

		
		 <?php echo $form->field($model, 'is_hourly')->textInput() ?>
	 		

	</div>
	<div class="col-md-6">

				
		 <?php echo $form->field($model, 'number_of_hours')->textInput() ?>
	 		

		
		 <?php /*echo $form->field($model, 'journey_time')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		

		
		 <?php echo $form->field($model, 'journey_type')->textInput() ?>
	 		

		
		 <?php echo $form->field($model, 'car_price_id')->textInput() ?>
	 		

		
		 <?php /*echo $form->field($model, 'start_time')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		

		
		 <?php /*echo $form->field($model, 'end_time')->widget(yii\jui\DatePicker::className(),
			[
					//'dateFormat' => 'php:Y-m-d',
	 				'options' => [ 'class' => 'form-control' ],
	 				'clientOptions' =>
	 				[
			//'minDate' => 0,
			'changeMonth' => true,'changeYear' => true ] ]) */ ?>
	 		

		
		 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 		

		
		 <?php echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) ?>
	 		

		
		 <?php /*echo $form->field($model, 'driver_id')->textInput() */ ?>
	 			</div>
	
	


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
