<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Coupon $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'coupon-ajax-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								'id',
								'title',
								'description:html',
								'discount',
								'max_amount',
								'start_date:date',
								'end_date:date',
								[ 
										'attribute' => 'type_id',
										'filter' => isset ( $searchModel ) ? $searchModel->getCouponTypeOptions () : null,
										'value' => function ($data) {
											return $data->getCouponType ();
										} 
								],
								'created_on:date',
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}'
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

