<?php
use app\components\TActiveForm;
use app\models\Coupon;
use app\models\Sepomex;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Coupon */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading"></header>
<div class="panel-body">

    <?php
    $form = TActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'coupon-form'
    ]);
    ?>

	<div class="col-md-6">

		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 256])?>
	 		
		
	 	 <?php echo  /* $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 2 ],'preset' => 'basic' ] ); */ $form->field($model, 'description')->textarea(['rows' => 2]); ?>
	 	
	 	
	 	 <?php echo $form->field($model, 'ride_count')->textInput(['maxlength' => 256])?>
	 	 	 <?php echo $form->field($model, 'type_id')->dropDownList($model->getCouponTypeOptions(), ['prompt' => ' '])?>
	 		
	</div>

	<div class="col-md-6">

		 <?php

echo $form->field($model, 'start_date')->widget(yii\jui\DatePicker::className(), [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        // 'minDate' => 0,
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
        			
		 <?php

echo $form->field($model, 'end_date')->widget(yii\jui\DatePicker::className(), [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        // 'minDate' => 0,
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		
	 	 <?php echo $form->field($model, 'discount')->textInput(['maxlength' => 256]) ?>	
	 	 
	 	 <?php echo $form->field($model, 'max_amount')->textInput(['maxlength' => 256]) ?>	
		 

		<?php
$formatJs = <<< 'JS'
		var formatRepo = function (repo) {
    		if (repo.loading) {
        		return repo.text;
    		}
    		var markup =
			'<div class="row">' +
    			'<div class="col-sm-5">' +        
        			'<b style="margin-left:5px">' + repo.text + '</b>' +
    			'</div>' +   
			'</div>';    
    		return '<div style="overflow:hidden;">' + markup + '</div>';
		};
		var formatRepoSelection = function (repo) {
    		return repo.text || repo.text;
		}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
			function (data, params) {		 		
    			params.page = params.page || 1;
    			return {
        			results: data.items,
        			pagination: {
            			more: (params.page * 10) < data.total_count
        			}
    			};
			}
JS;

?>

		
		 
		  <div
			class="dropdown display_<?php echo Coupon::TYPE_SPECIFIC_USER?>"
			style="display: none">
			 <?php
    
echo $form->field($model, 'user_id')->widget(Select2::classname(), [
        /*
         * 'name' => 'kv-repo-template',
         * // 'value' => '14719648',
         * 'model' => $model,
         * 'attribute' => 'cologne',
         * 'initValueText' => 'kartik-v/yii2-widgets',
         */
        'options' => [
            'placeholder' => 'Search for a Customer ...'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            // 'minimumInputLength' => 3,
            'ajax' => [
                'url' => "search-customer",
                'dataType' => 'JSON',
                'delay' => 100,
                'data' => new JsExpression('function(params) { return {user_id:params.term, page: params.page}; }'),
                'processResults' => new JsExpression($resultsJs),
                'cache' => true
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('formatRepo'),
            'templateSelection' => new JsExpression('formatRepoSelection')
        ]
    ]);
    ?>
		 </div>


		<div class="dropdown display_<?php echo Coupon::TYPE_COLOGNE?>"
			style="display: none">
		
		<?php
echo $form->field($model, 'cologne')->widget(Select2::classname(), [
    /*
     * 'name' => 'kv-repo-template',
     * // 'value' => '14719648',
     * 'model' => $model,
     * 'attribute' => 'cologne',
     * 'initValueText' => 'kartik-v/yii2-widgets',
     */
    'options' => [
        'placeholder' => 'Search for a cologne ...'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'ajax' => [
            'url' => "search-cologen",
            'dataType' => 'JSON',
            'delay' => 100,
            'data' => new JsExpression('function(params) { return {settlement:params.term, page: params.page}; }'),
            'processResults' => new JsExpression($resultsJs),
            'cache' => true
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('formatRepo'),
        'templateSelection' => new JsExpression('formatRepoSelection')
    ]
]);
?>
		 </div>

		<div class="dropdown  display_<?php echo Coupon::TYPE_MUNICIPALITY?>"
			style="display: none">
		 <?php
echo $form->field($model, 'municipality')->widget(Select2::classname(), [
    /*
     * 'name' => 'kv-repo-template',
     * // 'value' => '14719648',
     * 'model' => $model,
     * 'attribute' => 'municipality',
     * 'initValueText' => 'kartik-v/yii2-widgets',
     */
    'options' => [
        'placeholder' => 'Search for a municipality ...'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'ajax' => [
            'url' => "search-cologen",
            'dataType' => 'JSON',
            'delay' => 100,
            'data' => new JsExpression('function(params) { return {municipality:params.term, page: params.page}; }'),
            'processResults' => new JsExpression($resultsJs),
            'cache' => true
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('formatRepo'),
        'templateSelection' => new JsExpression('formatRepoSelection')
    ]
]);
?>
	     </div>

		<div class="dropdown  display_<?php echo Coupon::TYPE_STATE?>"
			style="display: none">
		 	 <?php echo $form->field($model, 'state')->widget ( Select2::classname (), [ 'data' => \yii\helpers\ArrayHelper::Map ( Sepomex::find ()->limit(100)->all(), 'id', 'state' ),'language' => 'en','options' => [ 'placeholder' => 'Select' ],'pluginOptions' => [ 'allowClear' => true,'multiple' => false ] ] )->label(true);?>
	     </div>

	</div>


	<div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'coupon-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    	</div>
	</div>

    <?php TActiveForm::end(); ?>

</div>

<script>
$(document).ready(function(){
	 var id = $("#coupon-type_id").val();
	 abc(id);		
});
$( "#coupon-type_id" ).change(function() {
	 var id = $(this).val();
	 abc(id);
});
function abc(id)
{
	 $('.dropdown').hide();
	 $('.display_'+id).show();
}



</script>
