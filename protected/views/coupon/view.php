<?php
use app\components\useraction\UserAction;

/* @var $this yii\web\View */
/* @var $model app\models\Coupon */

/* $this->title = $model->label() .' : ' . $model->title; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Coupons' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">

		<div class="coupon-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'coupon-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'title',
								'start_date:date',
								'end_date:date',
								'discount',
								'max_amount',
								'ride_count',
								[ 
										'attribute' => 'user_id',
										'value' => $model->user ? $model->user->full_name : '',
										'visible' => $model->user ? true : false 
								],
								[ 
										'attribute' => 'cologne',
										'value' => $model->cologne,
										'visible' => $model->cologne ? true : false 
								],
								[ 
										'attribute' => 'municipality',
										'value' => $model->municipality,
										'visible' => $model->municipality ? true : false 
								],
								[ 
										'attribute' => 'state_id',
										'format' => 'raw',
										'value' => $model->getStateBadge () 
								],
								[ 
										'attribute' => 'type_id',
										'format' => 'raw',
										'value' => $model->getCouponTypeBadge() 
								],
								'created_on:date',
								/* [ 
										'attribute' => 'created_by_id',
										'format' => 'raw',
										'value' => $model->getRelatedDataLink ( 'created_by_id' ) 
								]  */
						] 
				] )?>


<?php  echo $model->description;?>


		

		</div>
	</div>
</div>
<div class="wrapper">
	<div class="panel">
		<div class="panel-body">
				<?php
				$this->context->startPanel ();
				
				$this->context->addPanel ( 'Sent To', 'sentCoupons', 'CouponSent', $model );
				
				$this->context->endPanel ();
				?>
			
		</div>
	</div>
</div>