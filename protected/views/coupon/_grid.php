<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Coupon $searchModel
 */

?>
<?php Pjax::begin(); ?>
    <?php
				
				echo TGridView::widget ( [ 
						'id' => 'coupon-grid-view',
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'tableOptions' => [ 
								'class' => 'table table-bordered' 
						],
						'columns' => [ 
								
								'id',
								'title',
								'description:html',
								'start_date:date',
								'end_date:date',
								'discount',
								
								[ 
										'attribute' => 'type_id',
										'format' => 'raw',
										'filter' => isset ( $searchModel ) ? $searchModel->getCouponTypeOptions () : null,
										'value' => function ($data) {
											return $data->getCouponTypeBadge ();
										} 
								],
								'created_on:date',
								
								[ 
										'class' => 'app\components\TActionColumn',
										'header' => '<a>Actions</a>',
										'template' => '{view}{delete}' 
								] 
						] 
				] );
				?>
<?php Pjax::end(); ?>

