<?php
use app\components\TActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="panel-heading">
                            <?php /* echo strtoupper(Yii::$app->controller->action->id); */ ?>
                        </header>
<div class="panel-body">

    <?php
				$form = TActiveForm::begin ( [ 
						'layout' => 'horizontal',
						'id' => 'country-form' 
				] );
				?>
	


<?php if (Yii::$app->controller->action->id != 'update'){?>
		
		 <?php echo $form->field ( $model, 'country' )->widget ( Select2::classname (), [ 'data' => $model->getCountry(),'language' => 'en','options' => [ 'placeholder' => 'Select Region' ],'pluginOptions' => [ 'allowClear' => true ] ] )?>
	 		
	<?php } ?>
		 <?php echo $form->field($model, 'telephone_code',['template'=>'{label}<div class="col-sm-6"><div class="input-group"><span class="input-group-addon">+</span>{input}</div></div>{error}'])->textInput(['maxlength' => 255])?>
	 		


	   <div class="form-group">
		<div
			class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
