<?php

/* @var $this yii\web\View */
/* @var $model app\models\Country */

/* $this->title = $model->label() .' : ' . $model->title; */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Countries' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = ( string ) $model;
?>

<div class="wrapper">
	<div class=" panel ">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>

	</div>

	<div class=" panel ">
		<div class=" panel-body ">
    <?php
				
				echo \app\components\TDetailView::widget ( [ 
						'id' => 'country-detail-view',
						'model' => $model,
						'options' => [ 
								'class' => 'table table-bordered' 
						],
						'attributes' => [ 
								'id',
								'title',
								'country_code',
								'telephone_code',
								'currency_code',
								'currency_symbol',
								'created_on',
								[ 
										'attribute' => 'created_by_id',
										'value' => $model->createdBy->first_name 
								] 
						] 
				] )?>

		</div>
	</div>
</div>
<div class="wrapper">
	<div class="panel">
		<div class="panel-body">
			<?php
			$this->context->startPanel ();
			
			$this->context->addPanel ( 'Cars Types', 'countryCarPrices', 'CarPrice', $model );
			
			$this->context->endPanel ();
			?>
			
		</div>
	</div>
</div>
