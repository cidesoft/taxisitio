<?php

/* @var $this yii\web\View */
/* @var $model app\models\Country */

/*
 * $this->title = Yii::t('app', 'Update {modelClass}: ', [
 * 'modelClass' => 'Country',
 * ]) . ' ' . $model->title;
 */
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Countries' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->title,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>
<div class="wrapper">
	<div class=" panel ">
		<div class="country-update">
	<?=  \app\components\PageHeader::widget(['model' => $model]); ?>
	</div>
	</div>


	<div class="content-section clearfix panel">
		<?= $this->render ( '_form', [ 'model' => $model ] )?></div>
</div>

