<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<!-- Main content -->
<div class="wrapper">
<section class="content">

	<div class="error-page">
		<h2 class="headline text-info">
			<i class="fa fa-warning text-yellow"></i> <span><?= $name ?></span>
		</h2>

		<div class="error-content">
			<p>
                <?= nl2br(Html::encode($message))?>
            </p>

			<p>
				The above error occurred while the Web server was processing your
				request. Please contact us if you think this is a server error.
				Thank you. Meanwhile, you may <a href='<?= Yii::$app->homeUrl ?>'>return
					to dashboard</a>
			</p>
		<?php /* ?>
			<form class='search-form'>
				<div class='input-group'>
					<input type="text" name="search" class='form-control'
						placeholder="Search" />

					<div class="input-group-btn">
						<button type="submit" name="submit" class="btn btn-primary">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</form>
		<?php */ ?>
		</div>
	</div>

</section>
</div>
