<?php
use app\models\Complain;
use app\models\Ride;
use app\models\User;
use miloschuman\highcharts\Highcharts;
/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Dashboard' . '|' . 'Taxi-sitio');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Dashboard')
]?>

<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
<div class="alert alert-danger">
    <?php echo Yii::$app->session->getFlash('error')?>
</div>
<?php endif; ?>

<div class="wrapper">
	<div class="row state-overview">
		<a href="<?= \yii::$app->urlManager->createUrl("user/index") ?>">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<section class="panel purple">
					<div class="symbol">
						<i class="fa fa-users"></i>
					</div>
					<div class="value white">
					<?php
    
    $customers = User::find()->where([
        'role_id' => User::ROLE_PASSENGER
    ])->count();
    ?>
					<h1 class="timer" data-from="0" data-to="<?= $customers ?>"
							data-speed="1000">
					<?= $customers ?>
				</h1>
						<p>Total Customers</p>
					</div>
				</section>
			</div>
		</a> <a href="<?= \yii::$app->urlManager->createUrl("user/driver") ?>">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<section class="panel purple">
					<div class="symbol">
						<i class="fa fa-drivers-license-o"></i>
					</div>
					<div class="value white">
					<?php
    
    $drivers = User::find()->where([
        'role_id' => User::ROLE_DRIVER
    ])->count();
    ?>
					<h1 class="timer" data-from="0" data-to="<?= $drivers ?>"
							data-speed="1000">
					<?= $drivers ?>
				</h1>
						<p>Total Drivers</p>
					</div>
				</section>
			</div>
		</a> <a href="<?= \yii::$app->urlManager->createUrl("ride/index") ?>">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<section class="panel purple">
					<div class="symbol">
						<i class="fa fa-taxi"></i>
					</div>
					<div class="value white">
					<?php $rides = Ride::find()->count();?>
					<h1 class="timer" data-from="0" data-to="<?= $rides ?>"
							data-speed="1000">
					<?= $rides ?>
				</h1>
						<p>Total Rides</p>
					</div>
				</section>
			</div>
		</a><a
			href="<?= \yii::$app->urlManager->createUrl("complain/index") ?>">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<section class="panel purple">
					<div class="symbol">
						<i class="fa fa-exclamation-triangle"></i>
					</div>
					<div class="value white">
					<?php $complain = Complain::find()->count();?>
					<h1 class="timer" data-from="0" data-to="<?= $complain?>"
							data-speed="1000">
					<?= $complain?>
				</h1>
						<p>Total Complains</p>
					</div>
				</section>
			</div>
		</a>
	</div>

	<div class="clearfix"></div>
	<div class="panel">
		<div class="panel-body">
			Welcome <strong>
         <?php
        
        echo Yii::$app->user->identity->full_name;
        ?></strong>


		</div>
	</div>
	<div class="panel">

		<div class="panel-body">
			<div class="panel-heading">
				<span class="tools pull-right"> </span>
			</div>
			<div class="col-md-6">
		
		<?php $arrR = User::RideGraph();?>
	 
		<?php
echo Highcharts::widget([
    'scripts' => [
        'highcharts-3d',
        'modules/exporting'
    ],
    
    'options' => [
        'credits' => array(
            'enabled' => false
        ),
        'chart' => [
            'type' => 'area'
        ],
        'title' => [
            'text' => 'Monthly Rides',
            'x' => - 20
        ],
        
        'xAxis' => [
            'categories' => [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ]
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Number of Rides'
            ]
        ],
        'legend' => [
            'enabled' => true
        ],
        'tooltip' => [
            'shared' => true
        ],
        'series' => [
            
            [
                'name' => 'Rides ',
                'color' => '#227521',
                'data' => [
                    intval($arrR['jan']),
                    intval($arrR['feb']),
                    intval($arrR['march']),
                    intval($arrR['april']),
                    intval($arrR['may']),
                    intval($arrR['june']),
                    intval($arrR['july']),
                    intval($arrR['august']),
                    intval($arrR['sept']),
                    intval($arrR['oct']),
                    intval($arrR['nov']),
                    intval($arrR['dec'])
                ]
            ]
        ]
    ]
]);
?>
	</div>
			<div class="col-md-6">
	<?php
$data = User::MonthlySignups();

?>
					<?php
    echo Highcharts::widget([
        'scripts' => [
            'highcharts-3d',
            'modules/exporting'
        ],
        
        'options' => [
            'credits' => array(
                'enabled' => false
            ),
            'chart' => [
                'plotBackgroundColor' => null,
                'plotBorderWidth' => null,
                'plotShadow' => false,
                'type' => 'pie'
            ],
            'title' => [
                'text' => 'Statistics'
            ],
            'tooltip' => [
                'valueSuffix' => ''
            ],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => [
                        'enabled' => true
                    ],
                    // 'format' => '<b>{point.name}</b>: {point.percentage:.1f} %'
                    'showInLegend' => true
                ]
            ],
            
            'htmlOptions' => [
                'style' => 'min-width: 100%; height: 400px; margin: 0 auto'
            ],
            'series' => [
                [
                    'name' => 'Total Count',
                    'colorByPoint' => true,
                    
                    'data' => [
                        
                        [
                            'name' => 'Total User',
                            'color' => '#227521',
                            'y' => (int) User::find()->count(),
                            'sliced' => true,
                            'selected' => true
                        ],
                        [
                            'name' => 'Active User',
                            'color' => '#20BF55',
                            'y' => (int) User::findActive()->count(),
                            'sliced' => true,
                            'selected' => true
                        ]
                    ]
                ]
            ]
        ]
    ]);
    ?>
							</div>
		</div>
	</div>


</div>