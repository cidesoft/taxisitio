<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [ 
		'options' => [ 
				'class' => 'form-group has-feedback' 
		],
		'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>" 
];

$fieldOptions2 = [ 
		'options' => [ 
				'class' => 'form-group has-feedback' 
		],
		'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>" 
];
?>
<?php if (Yii::$app->session->hasFlash('success')): ?>
<div class="alert alert-success">
    <?php echo Yii::$app->session->getFlash('success')?>
</div>
<?php endif; ?>
<h2 class="form-heading ">login</h2>
<div class="container log-row">
	<div class="login-wrap">
              <?php
														
$form = ActiveForm::begin ( [ 
																'id' => 'login-form',
																'enableClientValidation' => false,
																'options' => [ 
																		'class' => 'form-signin' 
																] 
														] );
														?>
            
             <?=$form->field ( $model, 'username', $fieldOptions1 )->label ( false )->textInput ( [ 'placeholder' => $model->getAttributeLabel ( 'username' ) ] )?>
                
                  <?=$form->field ( $model, 'password', $fieldOptions2 )->label ( false )->passwordInput ( [ 'placeholder' => $model->getAttributeLabel ( 'password' ) ] )?>
                  <?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-success btn-block', 'name' => 'login-button'])?>
                  <div class="login-social-link">
			<a href="facebook.com" class="facebook"> Facebook </a> <a
				href="twitter.com" class="twitter"> Twitter </a>
		</div>
		<label class="checkbox-custom check-success"> <input type="checkbox"
			value="remember-me" id="checkbox1"> <label for="checkbox1">Remember
				me</label> <a class="pull-right" data-toggle="modal"
			href="<?php echo Url::toRoute(['site/recover'])?>"> Forgot Password?</a>
		</label>

		<div class="registration">
			Don't have an account yet? <a class=""
				href="<?php echo Url::toRoute(['user/signup'])?>"> Add an account
			</a>
		</div>

	</div>

	<!-- Modal -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
		tabindex="-1" id="forgotPass" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Forgot Password ?</h4>
				</div>
				<div class="modal-body">
					<p>Enter your e-mail address below to reset your password.</p>
					<input type="text" name="email" placeholder="Email"
						autocomplete="off" class="form-control placeholder-no-fix">

				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
					<button class="btn btn-success" type="button">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<!-- modal -->

</div>

