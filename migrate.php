<?php

defined ( 'YII_ENV' ) or define ( 'YII_ENV', 'dev' );

if (YII_ENV == 'dev') {
	error_reporting ( E_ALL );
	ini_set ( "display_errors", 1 );
	
	// remove the following lines when in production mode
	defined ( 'YII_DEBUG' ) or define ( 'YII_DEBUG', true );
}

defined ( 'DB_CONFIG_PATH' ) or define ( 'DB_CONFIG_PATH', dirname ( __FILE__ ) . '/protected/config/' );
defined ( 'DB_CONFIG_FILE_PATH' ) or define ( 'DB_CONFIG_FILE_PATH', DB_CONFIG_PATH . 'db-'. YII_ENV .  '.php' );
defined ( 'DB_BACKUP_FILE_PATH' ) or define ( 'DB_BACKUP_FILE_PATH', dirname ( __FILE__ ) . '/protected' );

defined ( 'VENDOR_PATH' ) or define ( 'VENDOR_PATH', __DIR__ . '/../vendor/' );

require (VENDOR_PATH . 'autoload.php');
require (VENDOR_PATH . 'yiisoft/yii2/Yii.php');

$config = require (__DIR__ . '/protected/config/console.php');

$application = new yii\console\Application ( $config );

ob_start ();
try {
	$application->runAction ( 'migrate/up', [ 
			'interactive' => false 
	] );
} catch ( \Exception $ex ) {
	echo $ex->getMessage ();
}
echo htmlentities ( ob_get_clean (), null, Yii::$app->charset );
